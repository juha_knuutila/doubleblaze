# DoubleBlaze common

## Overview

DoubleBlaze code custom code documentation. 

## Installation

* Edit properties in util/install.properties. Define you pricer installation directory, and the working directory for smart and final data.
``` python
    directory = C:\jda\wec\2019.2
    smartandfinaldir=C:\doubleblaze
```
* Run command
``` python
    python install.py
```

## Required software


* SQLAlchemy
* cx_Oracle
* Openpyxl

These can be installed using pip-tool.
```
pip install SQLAchemy
etc.
```

## Delivered scripts

* dblz_listpriceviews.cmd, this tool helps generating List Price views from XLS file
* dblz_queue.cmd, this is internal script which is used for putting imports and exports in que
* dblz_register_assets.cmd, this tool reads generated assets file and registeres unregistered assets
* dblz_run_statistics.cmd, this internal script will run statistics to database
