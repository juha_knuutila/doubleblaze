
## Overview

DoubleBlaze code custom code documentation. 

## Installation

* Edit properties in util/install.properties. Define you pricer installation directory, and the working directory for smart and final data.
``` python
    directory = C:\jda\wec\2017.1.0rc1
    smartandfinaldir=C:\smart_and_final
```
* Run command
``` python
    python install.py
```

## Required software


* SQLAlchemy
* cx_Oracle
* Openpyxl

These can be installed using pip-tool.
```
pip install SQLAchemy
etc.
```

## Produced scripts

* saf_generate_dyanmicgroups.cmd, this will generate dynamic groups backbus file
* saf_securitymodel.cmd, this will generate backbus import files from the securitymodel
* saf_register_assets.cmd, this tool reads generated assets file and registeres unregistered assets
* saf_listpriceviews.cmd, this tool helps generating List Price views from XLS file
* saf_find_emergencies.cmd, this tool lists adjustments which have EMERGENCY_EXPORT flag marked
* saf_run_statistics.cmd, this tool will run statistics to database
* internal scripts saf_queue.cmd
* various export and import scripts

## Scripts in detail

### SmartAndFinal.properties

You need to setup correct values here. 

### saf_generate_dynamicgroups.cmd

Manually invoked when there is new store group data.
This script reads the store groups file and stores in storegroups files and produces a backbus file which 
needs to be imported into pricer. The generated file creates new dynamig groups according to storegroup information.
The output file place is defined by property DYNAMICGROUPS.backbus_file

### saf_securitymodel.cmd

Manually invoked to generate needed security model backbus files or data from db.
Usage:
saf_securitymodel.cmd &lt;operation&gt;
where operation is
```
    backbus_hierarchies_location - generates backbus file which contains privileges for location hierarchies
    backbus_variables - generates backbus file which contains privileges pricing variables
    uisecurity - generates ui security file
    read_hierarchies_product - prints product hierarchies from db
    read_hierarchies_location - prints location hierarchies from db
    read_variables - print pricing variables from db
```

### saf_listpriceviews.cmd

Manually invoked to generate List Price view transferfile from DB or from XLS-file or to import that file to DB
Usage:
saf_listpriceviews.cmd &lt;operation&gt;
where operation is
```
    read_from_db - Reads SHARED_SEARCHES table and writes List Price views to transfer file
    write_to_db - Reads transfer file and writes it to DB
    read_from_xls - Reads Excel file and generates transfer file based on excel definitions
```
### saf_run_statistics.cmd

This script will update oracle database statistics. It needs to be run before item imports.


### saf_combine_approval.py

This is called by export commands. Reads approval information from LP rows
and appends that to I rows.


### saf_queue.cmd

This is called by export and import commands. This command creates a queue lock, and lets script to continue if lock is not on.
If lock is found it keeps waiting lock to be freed. Polling of lock is done every 10 seconds.

### saf_find_emergencies.cmd

This is utility command to query possible emergency_export flags from database. It lists the adjustments which have emergency_export set.

### saf_report_family_exceptions.cmd

This utility will list items which have different Pricebook prices in datatabase than corresponding family items. 

### saf_report_pb_prices.cmd

This utility will list item and family prices from pricebooks PricerPrice, UserSzRegPrice and UserRegPrice. 


## Pricer Import

### Import Scripts

* pricer_import_dma.cmd, imports DMA data
* pricer_import_mdm_adgroups.cmd, imports AdGroups and stores in each group into pricer
* pricer_import_mdm_items.cmd, imports items from mdm
* pricer_import_mdm.prices.cmd, imports prices from mdm into price book
* pricer_import_mdm_product_hierarchy.cmd, imports product hierarchy from mdm
* pricer_import_mdm_stores.cmd, imports location hierarchy and businesses from mdm
* pricer_import_promo.cmd, imports promocop feed
* pricer_import_sp_price.cmd, imports approved prices from SP into pricer
* pricer_import.cmd, base command which is wrapped those other commands. This command is not run directly.


### DMA import

Reads DMA file format and imports regular retails and promotions into pricer based on property settings.

### AdGroups import

Pricer keeps track of loaded AdGroups. Once new feed is received from MDM the corresponding new groups are created and if old groups are modified then new versions for those groups are created.


### Promocop import

Reads AdGroup priorities from file: 
```
    sppromo.AdGroupPriorityFile=Ad Groups.csv
    File format: comma separated fields, "BANNERID","GROUPID","LNAME","PRIORITY"
    First line is ignored as header row.
```

Fields:
   event_no,event_desc,mod,startDate,endDate,smartaDv,priceType,adGroup,storeOverride,itemId,upc,dealAmount,promoCost,pmId,promoMultiple,promoPrice,limitQty,pkgMultiple,pkgPrice,discAmount,sfiFamilyCode,subPromo,dispLoc,merchLoc,print,media,sourceId,priceLevel,signPrint,signDesc,updatedOn,fgDesc,isFamilyPrice
  

### DMA import

Properties starting with sppromo.dma. are for DMA import.

Default rule parameters can be altered in property
sppromo.dma.RuleParamNames=PRC_REG_PRICE_TOTAL_1|PRC_REG_PRICE
This is comma separated list of rule parameter name tuples.

Import of individual adjustment types can be turned on / off in properties
sppromo.dma.enabled.RegularRetailMB=true


## Pricer Export

### Export Scripts

* pricer_exportComplexDaily.cmd, exports next day's complex promos
* pricer_exportComplexEmergency.cmd, exports todays's complex promos which are marked as Emergency
* pricer_exportComplexWeekly.cmd, exports +5 day's complex promos
* pricer_exportSimpleDaily.cmd, exports next day's regular price changes and simple promos
* pricer_exportSimpleDailyConstraint.cmd, exports next day's regular price changes and simple promos with constraint information
* pricer_exportSimpleEmergency.cmd, exports today's regular price changes and simple promos which are marked as Emergency
* pricer_exportSimpleGoogle.cmd, exports next day's regular price changes and simple promos
* pricer_exportSimpleWeekly.cmd, exports +5 day's regular price changes and simple promos
* pricer_exportSimpleDaily_manual.cmd, same as pricer_exportSimpleDaily.cmd
* pricer_exportSimpleEmergency_manual.cmd, same as pricer_exportSimpleEmergency.cmd

All exports will generate Pricer export file name like PRICEREXPORT_COMPLEXPROMO_EMERGENCY_{ExtractDate}_{ExtractTime}.csv PRICEREXPORT_SIMPLEPROMO_EMERGENCY_{ExtractDate}_{ExtractTime}.csv etc name is defined in corresponding property file. Also .done file is generated once export and post processing is done. pricer_exportSimpleDailyConstraint.cmd will also generate separate report file PRICEREXPORT_CONSTRAINT_REPORT_{ExtractDate}_{ExtractTime}.csvreport.csv


#### pricer_exportSimpleDailyConstraint.cmd

This export will p

## Post install

### property file settings

All properties are set in properties/SmartAndFinal.properties file.
There are different sections for each tool. Sections are marked with [], e.g. [BUSINESSCONSTRAINTS]



### Business constraints

#### StoreItemMargin

 * Checks item margin. Uses variables
 * property: BusinessConstraints.variable.StoreMargin, default StoreMargin
 * property: BusinessConstraints.variable.StoreMarginLimit, default StoreMarginLimit
 * 
 * 1. if StoreMargin is below 0, hard constraint is raised
 * 2. if StoreMargin is below StoreMarginLimit, soft constraint is raised.
 * 3. otherwise margin is accepted. 

#### MinMaxPrice

 * Checks item price is between min and max prices. Uses variables
 * property: BusinessConstraints.variable.RetailPrice, default RetailPrice
 * property: BusinessConstraints.variable.MinPrice, default MinPrice
 * property: BusinessConstraints.variable.MaxPrice, default MaxPrice
 * 
 * 1. if price is below min or above max, then hard constraint is raised
 * 2. otherwise price is accepted. 

#### MixAndMatch
    * Checks that user entered MixAndMatch code is three digits.
    * This is checked in list price views and in export. These places use different variable
    View-to-Variable mapping is defined in properties file.
    * property: usinessConstraints.mixAndMatch.variableMapping=HDL|MIX_N_MATCH_CODE_21,BMSM|MIX_N_MATCH_CODE_10,|MIX_N_MATCH_CODE
    Notice empty key value, which is used in export.

#### NegativeUSave
    * Checks that U_Save is not negative or zero in that case returns hard constraint, otherwise returns OK
    * property: BusinessConstraints.usave.variable=U_SAVE
    * property: BusinessConstraints.usave.limit=0.0

#### Mod Check

  * Checks that mod value in the fired adjustment matches the adjustments start and end dates.
  * Mod properties are queried from MOD matrix, this can be defined in properties.
  * Matrix column can also be defined in properties, but here's default values:

  ![alt text](./doc/images/modmatrix.png "Sample MOD matrix")


#### PriceType filtering business constraints

These follow the pricing variable defined by property BusinessConstraints.priceType.variable, 
default value = PRC_EFF_PRICE_TYPE
They return OK if type is corresponding the constraint.


#### PriceType Multivalue

* This can be used to filter items which are one of wanted type.
* Types are defined in properties
```
  BusinessConstraints.multiValueTypes=Types1|Types2
  BusinessConstraints.multiValueTypes.Types1=150,151
  BusinessConstraints.multiValueTypes.Types2=0,1,10,15,21,80,90

  This block defines two views, named Types1 and Types2. Types1 displays price changes which have price type 150 or 151
  And Types2 displays types 0,1,10,15,21,80,90

  View name needs to be given in ListPriceScreen Pricing Criteria as CustomVariable=Pricer.MultiValueTypes=Types1
```
![alt text](./doc/images/pricetypesconstraint.png "Sample MOD matrix")


#### VariableFilters

* This can be used to filter items by variable value. New variables and values can be defined in property file.
* Types defined in properties
```
  BusinessConstraints.variableFilters=BMSM|Multibuy
  BusinessConstraints.variableFilters.BMSM.variable=PRC_REG_PRICE_CHANGE_BMSM
  BusinessConstraints.variableFilters.BMSM.value=1
  BusinessConstraints.variableFilters.Multibuy.variable=PRC_REG_PRICE_CHANGE_MB
  BusinessConstraints.variableFilters.Multibuy.value=1
  

  This block defines two views, named BMSM and Multibuy. BMSM displays price changes which have PRC_REG_PRICE_CHANGE_BMSM=1
  and other view "Multibuy" which displays price changes which have PRC_REG_PRICE_CHANGE_MB=1

  View name needs to be given in ListPriceScreen Pricing Criteria as CustomVariable=Pricer.variableFilters=BMSM
```


#### Constraints:
```
    BOGO
    BuyMoreSaveMore
    Clearance
    ComplexOffer
    HardLimit
    Regular
    WeeklySpecial
```

#### RequiredFields constraint.

This constraint works in ListPrice view. It checks if all required fields are filled. The views and corresponding fields
are defined in properties file.

e.g.
```
BusinessConstraints.requiredFields.Views=HardLimitDiscount
BusinessConstraints.requiredFields.view.HardLimitDiscount=PRC_MAX_QTY_21|PRC_EFF_PRICE_21
```

defines that there's one view named "HardLimitDiscount". And required fields for this view are 
PRC_MAX_QTY_21 and PRC_EFF_PRICE_21. In the list price view user needs to specify custom variable
Pricer.ListPriceView=HardLimitDiscount to indicate which is the view name.

#### RequiredFieldsByLineType constraint

This constraint reads price type from the current row and applied required fields filtering based on that type.

Row type variable is defined in property:
```
BusinessConstraints.requiredFieldsByLineType.variable=PRC_EFF_PRICE_TYPE
```

And value to view name is mapped in this property:
```
BusinessConstraints.requiredFieldsByLineType.valueToViewMapping=1|RegularRetail,10|BMSM,15|WeeklySpecial,21|HardLimitDiscount,90|Clearance
```

#### Emergency Filter Constraint

This constraint can be used for filtering in items which have Emergency flag = 1 and start date today.

Properties:
    BusinessConstraints.emergencyFilter.flagVariable defines the pricing variable by which items are filtered
    BusinessConstraints.emergencyFilter.startDateOffset how many days is added to current date to get filtering date.

#### Search By Adjustment name of oid constraint

This constraint can be used for filtering items which prices are calculated by certain adjustments. You can give search condition
to limit adjusments. Adjustment name and adjustment oid conditions are supported.

"Adjustment Name Filter" is the constraint name.

Parameters are defined by CustomVariable: Pricer.Filter.AdjustmentName and Pricer.Filter.AdjustmentOID. Symbol \* can be used as wildcard. otherwise Parameter format is java regular expression.

![alt text](./doc/images/searchbyname.png "Adjustment name search condition")


#### PrePricedCheck constraint

Checks that item price is below PrePrice. If current price is higher then raises hard constraint.

Properties:
  BusinessConstraints.PrePiced.variable.PrePricedAttribute, default PrePricedItem
  BusinessConstraints.PrePiced.variable.PrePrice, default PRC_REG_MDM_PRICE
  BusinessConstraints.PrePiced.variable.RetailPrice, default PRC_REG_PRICE


## Queuing Imports and exports

All imports and exports are put into queue. They compete on lock file and the script that has the lock can do it's job.
lock file is by default 
```
C\temp\saf_transfer.lock
```
After script ends it deletes the file.

You can turn off queuing by creating a unlock file. If this file exists then scripts will be executed without que.
```
C\temp\saf_transfer.unlock
```


## Dev activities
see documentation [here](README_DEV.md)


## Versions

### v1.55.1
  - Bug fixes: 
    - SFO-199, items with zero assortment were included in the constraint report. They should not be there.

### v1.55
  - SFO-201, Do not create .done file if there's no csv generated.
  - SFO-199, Added item description to constrain report
  - SFO-204, USave < 0 added to constrain report

### v1.54
  - Added MixAndMatch and PrePriceCheck constraints to constraints report.
  - SFO-194, Reporting automatization, constraint report

### v1.53
  - SFO-190, new manual scripts

### v1.52
  - SFO-173, PRICE import adapter changes. according to new 2019/07 design
  - SFO-177, Remove MixAndMatch constraint from simple exports

### v1.51
  - SFO-172, added PriceBookLocationFilter to listprice view. Changed list-prices-use-location-pricing-on-leaf-nodes to true
  - SFO-162, Added PRC_ASS_MDM to compare variables, this will trigger item export on assortment change. 
  - SFO-168, code change to update only 1 values to Assortment PB.

### v1.50
  - SFO-162, added saf_report_family_exceptions.cmd utility script to list items which have different regular prices in database than corresponding family item price. 
  - SFO-162, added saf_report_pb_prices.cmd utility which lists pricebook prices for normal items and family items from UserRegPrice and PricerPrice pricebooks.
  - SFO-164, changed list-prices-price-book-max-items-per-search and list-prices-price-book-max-search-results to 1000000. Earlier these was 10k and it caused confusing search results in list price screen. With new value users should be careful not to do too big searches.

### v1.49
  - SFO-150, fixed typo in SMS TYPE6 rule.
  - SFO-153, Add Family attribute to exports
  - SFO-157, Some cases items will not pass EmergencyItemFilter. Removed removeVariantDuplicates call.
  - SFO-160, 0-family will be ignored. Items which are sent as IS_FAMILY_ITEM, but has 0 as family value will be used as normal items
  
### v1.48
  - SFO-141, StartDate is picked also from from SpecialZonePrice 
  - Use DummyBook as published pricebook
  - Returned start date to compare variables
  - Fix to EmergencyItemFilter to support case items

### v1.47
  - SFO-120, exports will no longer generate .done file if there's other return value than 0
  - SF-81, emergency export peformance enhancement. Only limited item group is calculated for emergency. Requires GA-16
  - Changed compare variable list for simple exports. Start&End date removed. 
  - Published pricebooks were removed

### v1.46
  - MDM initial load changes:
      Load regular price to zone level (and not store level)
      Load regular price to assortmen pricebook to store level
      Load list cost to zone level (and not store level)
      New flag to control when initial load is done.
  - DMA changes:
      Do not import regular retails, BMSM
      Regular Retail MultiBuys will be imported to new pricebook to zone level.
      Other promos are imported as earlier

  - SFO-43,
      Added PRC_PKG_MULT as compare variable to simple exports
      Added STOP_SMS as separate field to complex exports

  - SFO-40, create .done file on after export even if no data was exported

  - SFO-69, property changes so that .done files are required on imports
  - Empty price will be in PRICE feed will be interpeted as assortment info.
    Price will not be affect zonelevel regular retail, but it will be added to assortment pricebook as value 1
  
### v1.45
  - Empty Price or empty cost value in Price file will not be converted to 0 price any longer.
  - Removed NegativeUSAVE from simple weekly, simple daily, simple emergency and simple google exports
  - Removed Special event handling from promocops adapter. These will be handled like other rows.

### v1.44
  - Emergency Export approach changed. EMERGENCY_EXPORT flags are first queried from database. If not found then export is not done

### v1.43
  - Valid return values to export scripts. If export fails the script returns corresponding return value.
  - Added PRC_REG_MULT,PRC_PROMO_START,PRC_PROMO_END variables to compare variables

### v1.42
  -  PricingDateOffset added. 
  - Change export scripts to have sligthly different starting time

### v1.41
    - PrePriceCheck added, items which are flagged as pre priced must have lower retail price than given pre price.

### v1.40
    - Added PRC_REG_MULT compare variable to simple exports
    - Added Queuing of imports and exports
    
### v1.39
    - PrePriced items support added to ITEMS feed
    - Family Item support added to PROMO feed

### v1.38,
    - Added throttling setting for imports: ThreadsPerCPUCore=0.4
    - AdGroup data needed to be moved different place, inbound_data

### v1.37,
    - Weekly export to include also regular retails
    - Support to convert promocops price type to other type. Changed to Base type.
    - Added PROMO_SYMBOL to compare variables

### v1.36,
    - Export compare variables changed as JDA fixed bug on this area
    - Emergency export value reset moved to be done later.

### v1.35,
    - Mod check in weekly export changed to filter only hard constraint lines. earlier also soft were filtered out.

### v1.34,
    - Promocops load change, if promoprice & discount amount both are 0 then don't populate promoprice as 0 but disc amount=0

### v1.33,
    - Added case items also style item link to family item.
    - Family added as search condition to ListPrice View
    
### v1.32, 
    - Simple exports changed to export only approved prices

### v1.31, 
    - Start date is read from LP rows
    - Special Zone pricing changes, filter these lines out of export
    - Added utility script to run statistics on oracle

### v1.30, 2019.2.21
    - clearance required fields changed

### v1.29, 2019.2.20
    - Removed QUAL_COUNT and AWARD_LIMIT from required fields for SMS

### v1.28, 2019.2.19
    - U_SAVE in complex exports changed U_SAVE_100
    - MDM_BASE_PRICE added as required field in Regular Retail view
    - pricing variable changed from AdGroup to ADGROUP
    - Fixed possible race condition with export file post processing Approval code cleaning
    - ExpandVariants was turned on all exports

### v1.27, 2019.2.11
    - Special Zone pricing support. Import adapter changes to create new zone-businesses, and to create special zone hierarchies. Export changes to set correct store number on special zone lines. SpecialZonePrice pricebook added to published price books.
    - Emergency export change. Emergency flag is turned from 1 to 0 once line is published.
    - Sign Description handling. Support to read descriptions from new feed. This might require other changes once specification is finalized.
    - Required Fields tuning
    - Some UI text changes and other minor technical changes.

### v1.26, 2019.1.23

    - Support to import DMA prices into own price book
    - DMA mixAndMatch code change, if code is -1 turn it into 0
    - New business constraint, NegativeUSave
    - Added doubleblaze version number
    - MultiValueTypes added 80

### v1.25, 2019.1.17
    - LP rows enabled in exports
    - Approval information combination is done for exports

### v1.24,
    - Family items are assigned to product hierarchy
    - MixAndMatch changes. DMA adapter changed so that it's possible to control in properties if MixAndMatch code is populated or not.
    MixAndMatch constraint created, there's property where you can define view-to-variable mapping. This is because MixAndMatch is read from differerent variable
    in different views. And then in export phase it's even different variable.

### v1.23,
    - PriceExport filtering by different constraints.
    - RequiredFieldsByLineType constraint added

### v1.22, 
    - Added PrintCaseFlag support in ITEMS adapter.
    
### v1.21,
    - NbrItemsInCase attribute populated in ITEMS import
    - 2017.2.1.1 version libraries into use.
    - Promocop adapter change, if event id's 5th digit is other than 1 then do not populate mod-parameter.
    
### v1.20,
    - EmergencyFilter constraint added

### v1.19,
    - Promocop adapter change, DISPLOC is now imported and populated to corresponding rule parameter
    - Case Pack price is picked from RegularCaseRetail column in item prices feed
    - Added price link support
    - Added support to disable zone prices creation
    - Event Id separator in adjustment description was changed to __
    - Ad Group id was added as rule parameter
    - New JDA libraries in use 2017.2.1.RC1
    - Fixed: item prices went to wrong item when same item name was in two different product group
    - Fixed: zone id was picked from wrong column.
    
### v1.18, 5/11/2018
    - DMA file support added. Regular price and multiple adjustment types are supported.

### v1.17, 10/8/2018
    - Added StoreManagerSpecial constraint
    - In Item Price import the item is picked from product group which store represents.
      Earlier this was random.
    - Default price type to 0, Base price
    - Libraries updated to 2017.2.1.RC1 level
    - DMA adapter stub added.
    
### v1.16, 25/9/2018
    - Mod value handling added, text value converted into numeric value.
    - Logging improvements.
    - Mod check changed so that only specific adjustments are checked.
    - Added trimming of property values.
    - Exclude_SP_SF support

### v1.15, 18/9/2018
    - Added support to import prices to proposed state or approved state.
    - More debug info added to imports

### v1.14, 4/9/2018
    - SFP1-299, Set dates to imported prices. Earlier prices were imported to no-date version.
    Now prices are imported to date that is specified in price file name.
    - New JDA jars from 2017.2.03 version.

### v1.13. 31/8/2018
    - SFP1-301, Support to configure pricegroup name in import adapter. 
    Name was different when it was creted from import from clipboard vs from Adapter
    
### v1.12.
    - Added separate log file on problems during import
    - Fixed issue with long OIDs in promocop adapter

### v1.11.
    - Changed location hierarchy name to start with store number prefix
    - Changed Promocop import to exit if priority file is not available
    - SFP1-293, add support to have different parent business for SF & CC
    - SFP1-292, add store number as prefix in description

### v1.10.
    - Added support for ad group priority.

### v1.09.
  - VariableFilters business constraint added
  - utility scripts can now create log dir automatically if it does not exist
  - JDA integration adapters added
  - Removed precompiled classes from repository
  - ItemImport added support for style/variants and linked case items

### v1.08.
 - ModCheck fixes. Empty Mod value causes soft constraint.

### v1.06.
 - Added Mod Check constraint

### v1.06.
 - Added PriceTypeMultiValues constraint
 - Added \pricer\applications\SEUILibrary\Resources\ListPrice_SFAdminPortal.properties
