package com.i2.se.custom.smartandfinal.integration;

import java.io.*;
import java.math.BigDecimal;
import java.text.*;
import java.util.*;
import java.util.Map.Entry;

import com.i2.se.app.integrations.imports.TranslatorException;
import com.i2.se.app.integrations.imports.data.*;
import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.util.bo.session.TSession;

public class RecommendedPriceFileTranslator extends SAFConverterBase
{
  protected String getAdapterName()
  {
    return "SCPO";
  }
  
  private final static class PriceElement
  {
    private BigDecimal approvedPrice;

    // collect SKU item prices
    private Map<String, PriceElement> skuPrices = new HashMap<String, PriceElement>( );


    public PriceElement( )
    {
    }

    private PriceElement( BigDecimal approvedPrice )
    {
      this.approvedPrice = approvedPrice;
    }

    public void addPrices( BigDecimal otherApprovedPrice, String oid )
    {
      skuPrices.put( oid,
          new PriceElement( otherApprovedPrice ) );
    }

    public BigDecimal getApprovedPrice( )
    {
      return approvedPrice;
    }

    public Map<String, PriceElement> getSkuPrices( )
    {
      return skuPrices;
    }
  }

  private Map<String, Map<Date, Map<String, PriceElement>>> zoneDatePrices = new HashMap<String, Map<Date, Map<String, PriceElement>>>( );
  private Map<String, String> zoneCurrencies = new HashMap<String, String>( );

  private SimpleDateFormat dateFormat = null;
  

  private void addItemPrice( ProductDataCache productDataCache, ImportSettings settings,
      String zoneId, BigDecimal approvedPrice, Date effectiveDate, String itemOid )
  {
    // ZoneID
    Map<Date, Map<String, PriceElement>> zDatePrices = zoneDatePrices
        .get( zoneId );
    if( zDatePrices == null )
    {
      zDatePrices = new HashMap<Date, Map<String, PriceElement>>( );
      zoneDatePrices.put( zoneId, zDatePrices );
    }

    // Effective date
    Map<String, PriceElement> itemPrices = zDatePrices.get( effectiveDate );
    if( itemPrices == null )
    {
      itemPrices = new HashMap<String, PriceElement>( );
      zDatePrices.put( effectiveDate, itemPrices );
    }

    // item price
    PriceElement prices = itemPrices.get( itemOid );
    if( prices == null )
    {
      prices = new PriceElement( );
      itemPrices.put( itemOid, prices );
    }

    prices.addPrices( approvedPrice, itemOid );
  }


  public int doConvert(TSession session, File importFile, BackBusContainer target, SAFSettings settings,
      ProductDataCache productDataCache ) throws TranslatorException,
      IOException
  {
    BufferedReader bf = null;
    int trueLineNo = 0;
    
    dateFormat = new SimpleDateFormat( settings.getScpoDateFieldFormat(), Locale.US );
    String filename = importFile.getName();
    try
    {
      Date versionDate = resolveFileDate(settings.getRecommendedPriceFileNamePattern(), filename);
      
      bf = new BufferedReader(
          new InputStreamReader( new FileInputStream( importFile ) ) );
      while( true )
      {
        String s = bf.readLine( );
        if( s == null )
        {
          break;
        }
        ++trueLineNo;
        String[] elements = s.split( "\\|", -1 );
        int i = 0;
        String category = elements[ i++ ];
        String subCategory = elements[ i++ ];
        String component = elements[ i++ ];

        // item attributes
        String itemCode = elements[ i++ ];
        String dmdGroup = elements[ i++ ];
        String locationNode = elements[ i++ ];
        String startDate = elements[ i++ ];
        BigDecimal approvedPrice = getBigDecimal( elements[ i++ ], trueLineNo );
        String updatedBy = elements[ i++ ];

        // Prices
        String updatedOn = elements[ i++ ];

        String pricingZone = elements[ i++ ];
        String udc_size_sf_attribute = elements[ i++ ];

        String approvedLVL = elements[ i++ ];
        String prodCode = elements[ i++ ];

        if( "0".equals( startDate ) )
        {
          startDate = "";
        }
        else if( startDate.length( ) < 6 )
        {
          int zeroPadding = 6 - startDate.length( );
          while( zeroPadding-- > 0 )
          {
            startDate = "0" + startDate;
          }
        }
        Date effectiveDate = null;
        if( !startDate.isEmpty( ) )
        {
          try
          {
            effectiveDate = dateFormat.parse( startDate );
          }
          catch( ParseException e )
          {
            throw new TranslatorException(
                importFile.getName( ) + ": Invalid date format \"" + startDate + "\", line"
                    + trueLineNo + ": " + e.getMessage( ), e);
          }
        }

        String storeNumber = prodCode;
        String itemOid = productDataCache.getSkuToItem(itemCode, storeNumber);
        String prdGroup = productDataCache.getOidToPG(itemOid);

        if( prdGroup == null )
        {
          logWarn( "Error reading product group for item " + itemCode + ": item not found!" );
          continue;
        }

        addItemPrice( productDataCache, settings, locationNode, 
            approvedPrice, effectiveDate, itemOid );

        // Currently only one currency - USD

        String zCurrency = zoneCurrencies.get( locationNode );
        if( zCurrency == null )
        {
          zoneCurrencies.put( locationNode, "USD" );
        }
       
        if( prdGroup != null && !prdGroup.isEmpty( ) )
        {
          int pGroup = Integer.parseInt( prdGroup );

          // attributes
          target.addItemAttribute(
              ItemAttribute.create( pGroup, itemCode,
                  0,
                  settings.getUdcSizeAttribute( ),
                  udc_size_sf_attribute ) );

          String currentDate = new SimpleDateFormat( "dd.MM.YYYY HH:mm:ss" ).format( new Date( ) );

          target.addItemAttribute(
              ItemAttribute.create( pGroup, itemCode,
                  0,
                  settings.getUpdatedOnAttribute( ),
                  currentDate ) );
        }
      }

      for( String zone : zoneCurrencies.keySet( ) )
      {
        String locationHierarchyID = productDataCache.getExternalIDToLocation( zone );
        if( locationHierarchyID == null )
        {
          logWarn( "Ignoring prices of zone " + zone + ", zone location not found" );
        }
        else
        {
          addPriceLists( target, settings, locationHierarchyID,  zone, versionDate);
          // Why this is done to zones only???
        }
      }

      // zone item prices
      for( String zoneID : zoneDatePrices.keySet( ) )
      {
        Map<Date, Map<String, PriceElement>> datePrices = zoneDatePrices
            .get( zoneID );
        String locationHierarchyID = productDataCache.getExternalIDToLocation( zoneID );
        if( locationHierarchyID == null )
        {
          logWarn( "Ignoring prices of zone " +
                  zoneID + ", zone location not found" );
          continue;
        }
        String currencyID = zoneCurrencies.get( zoneID );
        if( currencyID == null || currencyID.isEmpty( ) )
        {
          logWarn( "Ignoring prices of zone " +
                  zoneID + ", zone currency not defined" );

          continue;
        }
        String approvedPriceGroupId = settings.getApprovedPricePriceGroup( );

        for( Date effectiveDate : datePrices.keySet( ) )
        {
          if( effectiveDate != null )
          {
            // add price group version
            PriceListVersion version = new PriceListVersion( );
            version.setPriceListID( approvedPriceGroupId );
            version.setLocationHierarchyID( locationHierarchyID );
            version.setPriceListVersionDate( effectiveDate );
            target.addPriceListVersion( version );
          }

          Map<String, PriceElement> zItemPrices = datePrices.get( effectiveDate );

          for( Entry<String, PriceElement> entry : zItemPrices.entrySet( ) )
          {
            String itemOid = entry.getKey( );
            PriceElement prices = entry.getValue( );
            String prdGroup = productDataCache.getOidToPG(itemOid);

            if( prdGroup != null && !prdGroup.isEmpty( ) )
            {
              int pgGroup = Integer.parseInt( prdGroup );
              for( Entry<String, PriceElement> skuPrice : prices.getSkuPrices( ).entrySet( ) )
              {
                String skuCode = productDataCache.getItemCode(itemOid);
                PriceElement skuPrices = skuPrice.getValue( );
                ItemPrice price = new ItemPrice( );
                price.setProductGroupID( pgGroup );
                price.setItemName( skuCode );
                price.setItemLevel( 0 );
                price.setPriceListID( approvedPriceGroupId ); //
                price.setLocationHierarchyID( locationHierarchyID );
                price.setPriceListVersionDate( effectiveDate );
                price.setPrice( skuPrices.getApprovedPrice( ) );
                target.addItemPrice( price );
              }
            }
            else
            {
              logWarn("ProductGroup for item not found: "+itemOid+", line "+trueLineNo);
            }
          }
        }
      }

      logInfo("Read " + trueLineNo + " lines" );
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
      throw new TranslatorException(
          "Error reading file, line " + trueLineNo + ": " + e.getMessage( ), e );
    }
    catch( IOException e )
    {
      throw new TranslatorException(
          "IOException reading file, line " + trueLineNo + ": " + e.getMessage( ), e );
    }
    catch( NumberFormatException e ) {
      throw new TranslatorException(
          "Error parsing date from filename " + filename + ": " + e.getMessage( ), e );
    }
    finally
    {
      if( bf != null )
      {
        bf.close( );
      }
    }
    return trueLineNo;
  }


  private BigDecimal getBigDecimal( String cost, int trueLineNo )
  {
    try
    {
      return new BigDecimal( cost );
    }
    catch( NumberFormatException ne )
    {
      ObjectFactory.getLogManager( ).getStaticLogger( ).addLogEntry(
          this.getClass( ), StaticLogger.WARN, 0,
          "line " + trueLineNo + ", error reading price: " + cost );
      return null;
    }
  }

  private void addPriceLists( BackBusContainer target, SAFSettings settings,
      String locationHierarchyID, String storeName, Date versionDate )
  {
    String approvedPriceGroupId = settings.getApprovedPricePriceGroup( );
    createPriceList(target, locationHierarchyID, storeName, approvedPriceGroupId, settings, versionDate );
  }
  
}
