package com.i2.se.custom.smartandfinal.integration;

import java.io.*;
import java.util.*;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.app.integrations.imports.TranslatorException;
import com.i2.se.app.integrations.imports.data.*;
import com.i2.se.bd.catalog.THierarchy;
import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.util.bo.session.TSession;

public class ComponentHierarchyFileTranslator extends SAFConverterBase
{
  private Set<String> productHierarchyIDs = new HashSet<String>( );
  public ComponentHierarchyFileTranslator( )
  {
  }

  public int doConvert(TSession session, File importFile, BackBusContainer target, SAFSettings settings,
      ProductDataCache productDataCache) throws TranslatorException, IOException
  {
    BufferedReader bf = null;
    int trueLineNo = 0;
    try
    {
      bf = new BufferedReader(
          new InputStreamReader( new FileInputStream( importFile ) ) );
      while( true )
      {
        String s = bf.readLine( );
        if( s == null )
        {
          break;
        }
        if( StringUtils.isEmpty( s ) || StringUtils.startsWith( s, "#" ) )
        {
          logInfo( "Comment line "+trueLineNo+" : "+s);
          continue;
        }
        ++trueLineNo;
        String[] elements = s.split( "\\|", -1 );
        int i = 0;
        String companyNumber = elements[ i++ ];
        String company = elements[ i++ ];
        String categoryCode = elements[ i++ ];
        String category = elements[ i++ ];
        String subCategoryCode = elements[ i++ ];
        String subCategory = elements[ i++ ];
        String componentCode = elements[ i++ ];
        String component = elements[ i++ ];
        String productGroupName = "";

        String productGroup = settings.getCompanyNumberToProductGroup( ).get( companyNumber );

        if( productGroup != null )
        {
          productGroupName = settings.getProductGroupNames( ).get( productGroup );

          if( productGroupName != null && productGroupName.isEmpty( ) )
          {
            productGroupName = companyNumber;
          }
        }

        if( company == null || company.isEmpty( ) )
        {
          logWarn( "Missing Company for  line  " + trueLineNo );
          continue;
        }

        if( category == null || category.isEmpty( ) )
        {
          logWarn( "Missing category for  line  " + trueLineNo );
          continue;
        }

        if( subCategory == null || subCategory.isEmpty( ) )
        {
          logWarn( "Missing subCategory for  line  " + trueLineNo );
          continue;
        }
        if( component == null || component.isEmpty( ) )
        {
          logWarn( "Missing component for  line  " + trueLineNo );
          continue;
        }

        String parentHierarchyID = Hierarchy.PRODUCT_HIERARCHY_ROOT;
        StringBuilder hierarchyID = new StringBuilder( );
        hierarchyID.append( "P" ).append( companyNumber );
        addHierarchyIfMissing( hierarchyID.toString( ), company, parentHierarchyID,
            target );

        parentHierarchyID = hierarchyID.toString( );
        hierarchyID.append( "-" ).append( categoryCode );
        addHierarchyIfMissing( hierarchyID.toString( ), category, parentHierarchyID, target );

        parentHierarchyID = hierarchyID.toString( );
        hierarchyID.append( "-" ).append( subCategoryCode );
        addHierarchyIfMissing( hierarchyID.toString( ), subCategory, parentHierarchyID,
                target );

        parentHierarchyID = hierarchyID.toString( );
        hierarchyID.append( "-" ).append( componentCode );
        addHierarchyIfMissing( hierarchyID.toString( ), component, parentHierarchyID,
            target );
      }

      ObjectFactory.getLogManager( ).getStaticLogger( ).addLogEntry(
          this.getClass( ), StaticLogger.INFO, 0,
          "Read " + trueLineNo + " lines" );
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
      throw new TranslatorException(
          "Error reading file, line " + trueLineNo + ": " + e.getMessage( ), e );
    }
    catch( IOException e )
    {
      throw new TranslatorException(
          "IOException reading file, line " + trueLineNo + ": " + e.getMessage( ), e );
    }
    finally
    {
      if( bf != null )
      {
        bf.close( );
      }
    }
    return trueLineNo;
  }

  private void addHierarchyIfMissing( String hierarchyID, String hierarchyName,
      String parentHierarchyID, BackBusContainer target )
  {
    if( !productHierarchyIDs.contains( hierarchyID ) )
    {
      productHierarchyIDs.add( hierarchyID );

      Hierarchy h = new Hierarchy( );
      h.setHierarchyID( hierarchyID );
      h.setHierarchyName( hierarchyName );
      h.setHierarchyType( THierarchy.PRODUCT );
      h.setParentHierarchyID( parentHierarchyID );
      target.addHierarchy( h );
    }
  }

  @Override
  String getAdapterName()
  {
    return "Components";
  }
}
