package com.i2.se.custom.smartandfinal.integration;

public class DMAFileFields
{
  static int posidx=0;
  
  /* Starting values from line 10,
   * Then it's easier to see posidx value from line number
   */
  final static int CLASS = posidx++;
  final static int CLASS_ID = posidx++;
  final static int STORE_ID = posidx++;
  final static int PRINT_DATE = posidx++;
  final static int ITM_WHSE_ITEM = posidx++;
  final static int ITM_UPC_EAN = posidx++;
  final static int ITM_DESCRIPTION = posidx++;
  final static int ITM_SIZE_DESC = posidx++;
  final static int ITM_SUOM = posidx++;
  final static int ITM_PER_UNIT_MSG = posidx++;
  final static int ITM_BARCODE = posidx++;
  final static int ITM_SCALE = posidx++;
  final static int ITM_WIC_ELIGIBLE = posidx++;
  final static int VND_VENDOR = posidx++;
  final static int VND_SHORT_NAME = posidx++;
  final static int VI_CASE_PACK = posidx++;
  final static int VI_VENDOR_ITEM = posidx++;
  final static int CAT_CAT = posidx++;
  final static int CAT_SUBCAT = posidx++;
  final static int CAT_FAMILY = posidx++;
  final static int CAT_DESCRIPTION = posidx++;
  final static int DPT_DEPARTMENT = posidx++;
  final static int DPT_DESCRIPTION = posidx++;
  final static int PRC_REG_SU_PRICE = posidx++;
  final static int PRC_REG_PRICE = posidx++;
  final static int PRC_REG_MULT = posidx++;
  final static int PRC_REG_PPU = posidx++;
  final static int PRC_REG_SDATE = posidx++;
  final static int PRC_REG_EDATE = posidx++;
  final static int PRC_REG_SDATE_10 = posidx++;
  final static int PRC_REG_EDATE_10 = posidx++;
  final static int PRC_REG_SDATE_DDMM = posidx++;
  final static int PRC_REG_EDATE_DDMM = posidx++;
  final static int PRC_REG_PRICE_TYPE = posidx++;
  final static int PRC_REG_PRICE_LEVEL = posidx++;
  final static int PRC_EFF_SU_PRICE = posidx++;
  final static int PRC_EFF_PRICE = posidx++;
  final static int PRC_EFF_MULT = posidx++;
  final static int PRC_EFF_PPU = posidx++;
  final static int PRC_EFF_SDATE = posidx++;
  final static int PRC_EFF_EDATE = posidx++;
  final static int PRC_EFF_SDATE_10 = posidx++;
  final static int PRC_EFF_EDATE_10 = posidx++;
  final static int PRC_EFF_SDATE_DDMM = posidx++;
  final static int PRC_EFF_EDATE_DDMM = posidx++;
  final static int PRC_EFF_PRICE_TYPE = posidx++;
  final static int PRC_EFF_PRICE_LEVEL = posidx++;
  final static int PRC_EFF_PRICE_METHOD = posidx++;
  final static int PRC_EFF_DISC_FLAG = posidx++;
  final static int PRC_EFF_DISCOUNT = posidx++;
  final static int PRC_EFF_DISC_MULTIPLE = posidx++;
  final static int PRC_EFF_QTY_LIMIT = posidx++;
  final static int PRC_EFF_MIX_MATCH = posidx++;
  final static int PRC_EFF_MIN_ORDER = posidx++;
  final static int PRC_PKG_SU_PRICE = posidx++;
  final static int PRC_PKG_PRICE = posidx++;
  final static int PRC_PKG_MULT = posidx++;
  final static int PRC_PKG_PPU = posidx++;
  final static int PRC_USAVE_PRICE = posidx++;
  final static int TAS_MSG_CRV = posidx++;
  final static int TAS_MSG_PROMO1 = posidx++;
  final static int TAS_MSG_PROMO2 = posidx++;
  final static int TAS_MSG_MKT1 = posidx++;
  final static int TAS_MSG_MKT2 = posidx++;
  final static int TAS_MSG_MKT3 = posidx++;
  final static int SIGN_DESC = posidx++;
  final static int COOL = posidx++;
  final static int PROMO_SYMBOL =posidx++ ;
  final static int SFS_SMARTBUY = posidx++;
  final static int SFS_CASE_LABEL = posidx++;
  final static int SIGN_PRINT_FLAG = posidx++;
  final static int PRC_MKT_PRICE = posidx++;
  final static int PRC_MKT_PPU = posidx++;
  final static int SFI_CM_ID = posidx++;
  final static int SFI_CM_TYPE = posidx++;
  final static int SFI_CM_DATA_TYPE = posidx++;
  final static int SFI_REWARD_PRICE = posidx++;
  final static int SFI_CM_USE_UNIT_PRICE = posidx++;
  final static int SFI_CM_PROMOTION_LIMIT = posidx++;
  final static int SFI_PM_MULTIPLIER = posidx++;
  final static int RECLAIM = posidx++;
  final static int IMD_SHELF_LIFE = posidx++;
  final static int SFI_EDLP = posidx++;
  final static int SCHORDER = posidx++;
  final static int SFI_TAG_TYPE = posidx++;
  final static int SFI_TAG_SIZE = posidx++;
  final static int LABEL_ID = posidx++;
  final static int TAG_COUNT = posidx++;
  final static int TAG_STOCK = posidx++;

}
