package com.i2.se.custom.smartandfinal.integration;

import java.io.File;
import java.text.DateFormat;
import java.util.*;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.app.integrations.imports.TranslatorException;
import com.i2.se.app.integrations.imports.data.*;

public class DMAAdjustmentWeeklySpecial extends DMAAdjustmentBase
{
  private static DMAAdjustmentWeeklySpecial instance = null;

  private DMAAdjustmentWeeklySpecial(DMAFileTranslator dmaFileTranslator, BackBusContainer target,
      ProductDataCache productDataCache, SAFSettings settings, File importFile, DateFormat df,
      Set<String> adjustmentNodeIds, Set<String> allPostFixes, Set<String> adjustmentIds, Date pricingStartDate)
  {
    super( dmaFileTranslator,  target, productDataCache,  settings,  importFile,  df, adjustmentNodeIds, allPostFixes,  adjustmentIds, pricingStartDate );
    pricingRuleName = settings.getPricingRuleWeeklySpecial();
    adjustmentNamePattern = settings.getAdjustmentNamePatternWeeklySpecial();
    adjustmentIdPattern = "WS_%s_%s_%s";
  }

  public static DMAAdjustmentWeeklySpecial getInstance(DMAFileTranslator dmaFileTranslator, BackBusContainer target,
      ProductDataCache productDataCache, SAFSettings settings, File importFile, DateFormat df,
      Set<String> adjustmentNodeIds, Set<String> allPostFixes, Set<String> adjustmentIds, Date pricingStartDate)
  {
    if (instance == null)
    {
      instance = new DMAAdjustmentWeeklySpecial(dmaFileTranslator, target, productDataCache, settings, importFile, df,
          adjustmentNodeIds, allPostFixes, adjustmentIds, pricingStartDate);
    }
    return instance;
  }

  public boolean createWeeklySpecial(String[] elements, int trueLineNo, String itemCode, String itemOID)
      throws TranslatorException
  {
    return createAdjustment(target, productDataCache, settings, elements, trueLineNo, itemCode, df, importFile,
        itemOID);
  }

  boolean setCustomRuleParams(String paramName, PriceAdjustmentParameter priceParam, String[] elements, int trueLineNo)
  {
    boolean paramSet = false;
    
    String fixedPromoPriceEachParam = settings.getRuleParamName( "FixedPromoPriceEach" );
    String fixedPromoPriceCaseParam = settings.getRuleParamName( "FixedPromoCaseEach" );
    String centsOffPriceEachParam = settings.getRuleParamName( "CentsOffPriceEach" );
    String centsOffPriceCaseParam = settings.getRuleParamName( "CentsOffPriceCase" );
    String promoMultiQtyPriceParam = settings.getRuleParamName( "PromoMultiQtyPrice" );
    String promoQtyeParam = settings.getRuleParamName( "PromoQty" );
    String sfsCaseLabelParam = settings.getRuleParamName( "SFS_CASE_LABEL" );
    String promoSymbolParam = settings.getRuleParamName( "PROMO_SYMBOL" );
   // String signDescParam = settings.getRuleParamName( "SIGN_DESC" );
  
    if( fixedPromoPriceEachParam.equalsIgnoreCase( paramName ) )
    {
      String effDiscFlag = elements[DMAFileFields.PRC_EFF_DISC_FLAG];
      String pkgMulti = elements[DMAFileFields.PRC_PKG_MULT];
      String priceLevel = elements[DMAFileFields.PRC_EFF_PRICE_LEVEL];
      
      String value = "";
      if( "2".equals(effDiscFlag) && "1".equals(pkgMulti) && "5".equals(priceLevel)) {
        value = elements[DMAFileFields.PRC_EFF_DISCOUNT];
      }
      priceParam.setValue( value );
      paramSet = true;
    }
    else if ( fixedPromoPriceCaseParam.equalsIgnoreCase( paramName ) )
    {
      String effDiscFlag = elements[DMAFileFields.PRC_EFF_DISC_FLAG];
      String pkgMulti = elements[DMAFileFields.PRC_PKG_MULT];
      String priceLevel = elements[DMAFileFields.PRC_EFF_PRICE_LEVEL];
      
      String value = "";
      if( "2".equals(effDiscFlag) && "1".equals(pkgMulti) && "6".equals(priceLevel)) {
        value = elements[DMAFileFields.PRC_EFF_DISCOUNT];
      }
      priceParam.setValue( value );
      paramSet = true;
    }
    else if ( centsOffPriceEachParam.equalsIgnoreCase( paramName ) )
    {
      String effDiscFlag = elements[DMAFileFields.PRC_EFF_DISC_FLAG];
      String pkgMulti = elements[DMAFileFields.PRC_PKG_MULT];
      String priceLevel = elements[DMAFileFields.PRC_EFF_PRICE_LEVEL];
      
      String value = "";
      if( "0".equals(effDiscFlag) && "1".equals(pkgMulti) && "5".equals(priceLevel)) {
        value = elements[DMAFileFields.PRC_EFF_DISCOUNT];
      }
      priceParam.setValue( value );
      paramSet = true;
    }
    else if ( centsOffPriceCaseParam.equalsIgnoreCase( paramName ) )
    {
      String effDiscFlag = elements[DMAFileFields.PRC_EFF_DISC_FLAG];
      String pkgMulti = elements[DMAFileFields.PRC_PKG_MULT];
      String priceLevel = elements[DMAFileFields.PRC_EFF_PRICE_LEVEL];
      
      String value = "";
      if( "0".equals(effDiscFlag) && "1".equals(pkgMulti) && "6".equals(priceLevel)) {
        value = elements[DMAFileFields.PRC_EFF_DISCOUNT];
      }
      priceParam.setValue( value );
      paramSet = true;
    }
    else if ( promoMultiQtyPriceParam.equalsIgnoreCase( paramName ) )
    {
      String pkgPrice = elements[DMAFileFields.PRC_PKG_PRICE];
      String pkgMult = elements[DMAFileFields.PRC_PKG_MULT];

      String value = "";
      try
      {
        int ipkgMult = Integer.parseInt(pkgMult);

        if (ipkgMult > 1)
        {
          value = pkgPrice;
        }
      } catch (NumberFormatException e)
      {

      }
      priceParam.setValue(value);

      paramSet = true;
    }
    else if ( promoQtyeParam.equalsIgnoreCase( paramName ) )
    {
      String pkgMult = elements[DMAFileFields.PRC_PKG_MULT];

      String value = "";
      try
      {
        int ipkgMult = Integer.parseInt(pkgMult);

        if (ipkgMult > 1)
        {
          value = pkgMult;
        }
      } catch (NumberFormatException e)
      {

      }
      priceParam.setValue(value);

      paramSet = true;
    }
    else if( sfsCaseLabelParam.equalsIgnoreCase( paramName ) )
    {
      String caseLabel = elements[DMAFileFields.SFS_CASE_LABEL];
      priceParam.setValue( caseLabel );
      paramSet = true;
    }
    else if (promoSymbolParam.equalsIgnoreCase(paramName))
    {
      String promoSymbol = elements[DMAFileFields.PROMO_SYMBOL];
      
      String symbolValue = settings.getPromoSymbolValue(promoSymbol);
      if( StringUtils.isEmpty(symbolValue) ) {
        converter.logWarn("Invalid promo symbol value "+promoSymbol+ " at line="+trueLineNo);
      }
      priceParam.setValue(symbolValue);
      paramSet = true;
    }
    /*
    else if ( signDescParam.equalsIgnoreCase( paramName ) )
    {
      String signDesc = elements[DMAFileFields.SIGN_DESC];
      priceParam.setValue(signDesc);
      paramSet = true;
    }
    */
    return paramSet;
  }

}
