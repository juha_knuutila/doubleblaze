package com.i2.se.custom.smartandfinal.integration;

import java.io.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.app.integrations.imports.TranslatorException;
import com.i2.se.app.integrations.imports.data.*;
import com.i2.se.bd.util.bo.session.TSession;
import com.i2.se.uic.util.DateUtils;

public class PriceFileTranslator extends SAFConverterBase
{
  public final static class PriceElement
  {
    private BigDecimal listCost;
    private int currentPriceType;
    private BigDecimal regEachRetail;
    private boolean normalZone;
    private boolean specialZone;
    
    // collect SKU item prices
    private Map<String, PriceElement> skuPrices = new HashMap<String, PriceElement>();

    public PriceElement()
    {
    }

    private PriceElement(BigDecimal listCost, int currentPriceType, BigDecimal netCost, BigDecimal lastRecvCost,
        BigDecimal regEachRetail, boolean isSpecialZoneRow)
    {
      this.listCost = listCost;
      this.currentPriceType = currentPriceType;
      
      if( isSpecialZoneRow ) {
        this.specialZone = true; 
      } else {
        this.normalZone = true;
        this.regEachRetail = regEachRetail;
      }
    }
    public void addPrices(String skuCode, BigDecimal otherListCost, int otherCurrentPriceType, BigDecimal otherNetCost,
        BigDecimal otherLastRecvCost, BigDecimal otherRegEachRetail, boolean isSpecialZoneRow)
    {
      PriceElement current = skuPrices.get(skuCode);
      if( current == null ) {
        skuPrices.put(skuCode, new PriceElement(otherListCost, otherCurrentPriceType, otherNetCost, otherLastRecvCost,
            otherRegEachRetail, isSpecialZoneRow));
      } else {
        current.listCost = otherListCost;
        current.currentPriceType = otherCurrentPriceType;
        
        if( isSpecialZoneRow ) {
          current.specialZone = true; 
        } else {
          current.normalZone = true;
          if( otherRegEachRetail != null )
          {
            current.regEachRetail = otherRegEachRetail;
          }
        }
      }
    }

    public BigDecimal getListCost()
    {
      return listCost;
    }

    public int getCurrentPriceType()
    {
      return currentPriceType;
    }

    public BigDecimal getRegEachRetail()
    {
      return regEachRetail;
    }

    public Map<String, PriceElement> getSkuPrices()
    {
      return skuPrices;
    }
    public boolean isSpecialZone()
    {
      return specialZone;
    }
    public boolean isNormalZone()
    {
      return normalZone;
    }
  }

  private static final BigDecimal ZERO = new BigDecimal(0.0);

  private Map<String, Map<String, Map<String, PriceElement>>> zoneItemPrices = new HashMap<String, Map<String, Map<String, PriceElement>>>();
  private Map<String, Map<String, Map<String, PriceElement>>> storeItemPrices = new HashMap<String, Map<String, Map<String, PriceElement>>>();
  private Map<String, String> zoneCurrencies = new HashMap<String, String>();
  private Map<String, String> storeCurrencies = new HashMap<String, String>();
  private Set<String> specialZonePricesForStore = new HashSet<String>();

  /**
   * added for testing purposes
   * 
   * @return
   */
  public Map<String, Map<String, Map<String, PriceElement>>> getZoneItemPrices()
  {
    return zoneItemPrices;
  }

  public Map<String, Map<String, Map<String, PriceElement>>> getStoreItemPrices()
  {
    return storeItemPrices;
  }

  void addItemPrice(ProductDataCache productDataCache, ImportSettings settings, String zoneId, String storeNumber,
      String itemCode, BigDecimal listCost, BigDecimal netCost, BigDecimal lastRecvCost, BigDecimal regularEachRetail,
      int priceType, boolean isSpecialZoneRow)
  {
    String itemOid = productDataCache.getSkuToItem(itemCode, storeNumber);

    if (itemOid == null)
    {
      logWarn("Error reading SKU: " + itemCode + ", store=" + storeNumber + ": item not found!");
      return;
    }

    if (zoneItemPrices == null)
    {
      zoneItemPrices = new HashMap<String, Map<String, Map<String, PriceElement>>>();
    }

    // ZoneID
    Map<String, Map<String, PriceElement>> zonePrices = zoneItemPrices.get(zoneId);
    if (zonePrices == null)
    {
      zonePrices = new HashMap<String, Map<String, PriceElement>>();
      zoneItemPrices.put(zoneId, zonePrices);
    }

    // zone prices
    Map<String, PriceElement> itemZonePrices = zonePrices.get(zoneId);
    if (itemZonePrices == null)
    {
      itemZonePrices = new HashMap<String, PriceElement>();
      zonePrices.put(zoneId, itemZonePrices);
    }
    // item price
    // if( regularEachRetail != null && regularEachRetail.compareTo(ZERO) > 0 ) {
    PriceElement prices = itemZonePrices.get(itemOid);
    if (prices == null)
    {
      prices = new PriceElement();
      itemZonePrices.put(itemOid, prices);

      logDebug(
          "-- Adding zone price: " + itemCode + "/" + itemOid + ", zone=" + zoneId + ", price=" + regularEachRetail);
    }
    prices.addPrices(itemCode, listCost, priceType, netCost, lastRecvCost, regularEachRetail, isSpecialZoneRow);
    // }

    if (storeItemPrices == null)
    {
      storeItemPrices = new HashMap<String, Map<String, Map<String, PriceElement>>>();
    }
    // Store price
    Map<String, Map<String, PriceElement>> storePrices = storeItemPrices.get(storeNumber);
    if (storePrices == null)
    {
      storePrices = new HashMap<String, Map<String, PriceElement>>();
      storeItemPrices.put(storeNumber, storePrices);
    }

    // store prices
    Map<String, PriceElement> itemStorePrices = storePrices.get(storeNumber);
    if (itemStorePrices == null)
    {
      itemStorePrices = new HashMap<String, PriceElement>();
      storePrices.put(storeNumber, itemStorePrices);
    }
    
    // store item price
    PriceElement sPrices = itemStorePrices.get(itemCode);
    if (sPrices == null)
    {
      sPrices = new PriceElement();
      itemStorePrices.put(itemCode, sPrices);
    }

    sPrices.addPrices(itemCode, listCost, priceType, netCost, lastRecvCost, regularEachRetail, false);
  }

  public int doConvert(TSession session, File importFile, BackBusContainer target, SAFSettings settings,
      ProductDataCache productDataCache) throws TranslatorException, IOException
  {
    BufferedReader bf = null;
    int trueLineNo = 0;
    int ignoredLinesCount = 0;
    int commentLineCount = 0;
    String filename = importFile.getName();
    try
    {
      Date pricingDate = resolveFileDate(settings.getPriceFileNamePattern(), filename);
      int pricingDateOffset = settings.getPricingDateOffset();
      // + offset
      pricingDate = DateUtils.addDays(pricingDate, pricingDateOffset);
      logDebug("Pricing Date: "+ pricingDate);
      
      bf = getReader(importFile);

      Set<String> specialZones = settings.getSpecialZones();
      int proposedPriceApprovalLevel = settings.getPriceApprovalProposedPriceLevel();
      Map<String, String> specialZoneItems = new HashMap<String,String>();
      while (true)
      {
        String s = bf.readLine();
        if (s == null)
        {
          break;
        }
        ++trueLineNo;
        if (StringUtils.isEmpty(s) || StringUtils.startsWith(s, "#"))
        {
          logInfo("Comment line " + trueLineNo + " : " + s);
          commentLineCount++;
          continue;
        }
        
        // logInfo("Line: "+trueLineNo);
        String[] elements = s.split("\\|", -1);
        int i = 0;
        String storeNumber = elements[i++];
        String itemCode = elements[i++];

        // item attributes
        String sVendorNumber = elements[i++];
        String pVendorNumber = elements[i++];
        String nbrItemIncase = elements[i++];

        // Prices
        BigDecimal listCost = getBigDecimal(elements, trueLineNo, i++, null);
        BigDecimal netCost = getBigDecimal(elements, trueLineNo, i++, null);
        BigDecimal lastRecvCost = getBigDecimal(elements, trueLineNo, i++, null);

        // item attribute
        String costSource = elements[i++];

        // Pricing Variables
        BigDecimal regularEachRetail = getBigDecimal(elements, trueLineNo, i++, null);
        BigDecimal regularCaseRetail = getBigDecimal(elements, trueLineNo, i++, null);
        if( itemCode.endsWith("1") ) {
          if( regularEachRetail == null || regularEachRetail.compareTo(ZERO) <= 0 ) 
          {
            regularEachRetail = regularCaseRetail;
          }
        }
        if( regularEachRetail != null && regularEachRetail.compareTo(ZERO) == 0 ) 
        {
          regularEachRetail = null;
        }
        
        int priceType = settings.getDefaultPriceType();
        /*
         * Integer priceTypeRequest = getPriceTypeId( priceTypeId ); if(
         * priceTypeRequest != null ) { priceType = priceTypeRequest; }
         */
        // item attribute
        @SuppressWarnings("unused")
        String dummy = elements[i++];
        @SuppressWarnings("unused")
        String dummy2 = elements[i++];

        String pricingZone = elements[i++];
        
        String itemOid = productDataCache.getSkuToItem(itemCode, storeNumber);
        String prdGroup = productDataCache.getOidToPG(itemOid);
        
        if (prdGroup == null)
        {
          logWarn("Error reading product group for item " + itemCode + ": item not found!");
          ignoredLinesCount++;
          continue;
        }

        String normalZone = productDataCache.getExternalIDToZoneLocation(storeNumber);
        logDebug("Normal Zone:"+normalZone);
        boolean isSpecialZoneRow = false;
        if(specialZones.contains(pricingZone)) {
          specialZoneItems.put(getStoreItemKey(storeNumber, itemCode), pricingZone);
          logDebug("SPITEM:"+itemCode+"_"+storeNumber+","+itemOid);
          isSpecialZoneRow=true;
          
          String locationHierarchyID = productDataCache.getExternalIDToLocation(storeNumber);
          specialZonePricesForStore.add(locationHierarchyID);
        }
        
        addItemPrice(productDataCache, settings, normalZone, storeNumber, itemCode, listCost, netCost, lastRecvCost,
            regularEachRetail, priceType, isSpecialZoneRow);

        // Currently only one currency - USD

        String zCurrency = zoneCurrencies.get(normalZone);
        if (zCurrency == null)
        {
          zoneCurrencies.put(normalZone, "USD");
        }
        
        String sCurrency = storeCurrencies.get(storeNumber);
        if (sCurrency == null)
        {
          storeCurrencies.put(storeNumber, "USD");
        }
        
        if (prdGroup != null && !prdGroup.isEmpty())
        {
          int pGroup = Integer.parseInt(prdGroup);
          // attributes
          target.addItemAttribute(ItemAttribute.create(pGroup, itemCode, 0, settings.getSupplyVendorNumber(),
              ItemFileTranslator.getValidValue(sVendorNumber)));

          target.addItemAttribute(ItemAttribute.create(pGroup, itemCode, 0, settings.getPrimaryVendorNumber(),
              ItemFileTranslator.getValidValue(pVendorNumber)));

          target.addItemAttribute(ItemAttribute.create(pGroup, itemCode, 0, settings.getNbrItemsInCase(),
              ItemFileTranslator.getValidValue(nbrItemIncase)));

          target.addItemAttribute(ItemAttribute.create(pGroup, itemCode, 0, settings.getCostSource(),
              ItemFileTranslator.getValidValue(costSource)));

          if (specialZones.contains(pricingZone)) {
            target.addItemAttribute(ItemAttribute.create(pGroup, itemCode, 0, settings.getPricingZone(),
                ItemFileTranslator.getValidValue(pricingZone)));
          }
        }
      }

      for (String zone : zoneCurrencies.keySet())
      {
        String locationHierarchyID = productDataCache.getExternalIDToLocation(zone);
        
        if (locationHierarchyID == null)
        {
          logError("Error Zone not found. "+zone+ ". Ignoring prices. Line = " + trueLineNo);
        } else
        {
          
          boolean isSpecialZone = false;
          if( specialZones.contains(zone) ) {
            isSpecialZone = true;
            // addPriceLists(target, settings, locationHierarchyID, zone, pricingDate, isSpecialZone, false);
          } else {
            addPriceLists(target, settings, locationHierarchyID, zone, pricingDate, isSpecialZone, false);
          }
        }
      }

      for (String store : storeCurrencies.keySet())
      {
        String locationHierarchyID = productDataCache.getExternalIDToLocation(store);
        if (locationHierarchyID == null)
        {
          logWarn("Ignoring prices of store " + store + ", store location not found. Line " + trueLineNo);
        } else
        {
          addPriceLists(target, settings, locationHierarchyID, store, pricingDate, false, true);
        }
      }

      logDebug("is zone prices enabled:"+settings.isZonePricesEnabled());
     
      // zone item prices
      if (settings.isZonePricesEnabled())
      {
        for (String zoneID : zoneItemPrices.keySet())
        {
          Map<String, Map<String, PriceElement>> zonePrices = zoneItemPrices.get(zoneID);
          
          String currencyID = zoneCurrencies.get(zoneID);

          if (currencyID == null || currencyID.isEmpty())
          {
            logWarn("Ignoring prices of zone " + zoneID + ", zone currency not defined. Line " + trueLineNo);
            continue;
          }
          
          String locationHierarchyID = productDataCache.getExternalIDToLocation(zoneID);
          if (locationHierarchyID == null)
          {
            logWarn("Ignoring prices of zone " + zoneID + ", zone location not found. Line " + trueLineNo);
            continue;
          }

    //      String locationHierarchyID = productDataCache.getExternalIDToLocation( storeNumber );
          String listCostPriceGroupId = settings.getListCostPriceGroup();
          String regularEachRetailPriceGroupId = settings.getRegularEachRetailPriceGroup();
          String specialZonePriceGroupId = settings.getSpecialZonePriceGroup();
          logDebug("--zoneloop: " +zoneID  + ", " + locationHierarchyID );
          for (String zoneId : zonePrices.keySet())
          {
            Map<String, PriceElement> itemPrices = zonePrices.get(zoneId);

            for (Entry<String, PriceElement> entry : itemPrices.entrySet())
            {
              String itemOid = entry.getKey();
              PriceElement prices = entry.getValue();

              logDebug("--zoneprice: item " +itemOid  + ", " + locationHierarchyID );
            
              String prdGroup = productDataCache.getOidToPG(itemOid);
              if (prdGroup != null && !prdGroup.isEmpty())
              {
                int pgGroup = Integer.parseInt(prdGroup);
                for (Entry<String, PriceElement> skuPrice : prices.getSkuPrices().entrySet())
                {
                  String skuCode = skuPrice.getKey();
                  PriceElement skuPrices = skuPrice.getValue();
                  String publishLevel = settings.getPriceApprovalState();
                  
                  BigDecimal regularPrice = skuPrices.getRegEachRetail();
                  BigDecimal listCost = skuPrices.getListCost();
                  
                  logDebug("--price: " + specialZonePriceGroupId + ", " + locationHierarchyID + "," + skuCode + ", "
                      + regularPrice+ ", listcost="+listCost);
                  if (SAFSettings.PUBLISH_LEVEL_PROPOSED.equalsIgnoreCase(publishLevel))
                  {
                    if (specialZones.contains(zoneId))
                    {
                   // to normal zone
                      if (listCost != null) 
                      {
                        ItemProposedPrice proposedPrice = getProposedPriceItem(listCostPriceGroupId, pricingDate,
                            locationHierarchyID, pgGroup, skuCode, skuPrices.getCurrentPriceType(), listCost,
                            proposedPriceApprovalLevel);
                        target.addItemProposedPrice(proposedPrice);
                      }
                    } else
                    {
                      if (listCost != null)
                      {
                        ItemProposedPrice proposedPrice = getProposedPriceItem(listCostPriceGroupId, pricingDate,
                            locationHierarchyID, pgGroup, skuCode, skuPrices.getCurrentPriceType(), listCost,
                            proposedPriceApprovalLevel);
                        target.addItemProposedPrice(proposedPrice);
                      }
                      if ( regularPrice != null )
                      {
                        ItemProposedPrice proposedPrice = getProposedPriceItem(regularEachRetailPriceGroupId, pricingDate,
                            locationHierarchyID, pgGroup, skuCode, skuPrices.getCurrentPriceType(),
                            regularPrice, proposedPriceApprovalLevel);
                        target.addItemProposedPrice(proposedPrice);
                      }
                    }

                  } else
                  {
                    if (specialZones.contains(zoneId))
                    {
                      // no price to special zone
                    } else
                    {
                      if (listCost != null)
                      {
                        ItemPrice price = getPublishedPriceItem(listCostPriceGroupId, pricingDate, locationHierarchyID,
                            pgGroup, skuCode, skuPrices.getCurrentPriceType(), listCost);
                        target.addItemPrice(price);
                      }
                      if ( regularPrice != null )
                      {
                        ItemPrice price = getPublishedPriceItem(regularEachRetailPriceGroupId, pricingDate, locationHierarchyID,
                            pgGroup, skuCode, skuPrices.getCurrentPriceType(), regularPrice);
                        target.addItemPrice(price);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }

      // Store item prices
      for (String storeNumber : storeItemPrices.keySet())
      {
        Map<String, Map<String, PriceElement>> storePrices = storeItemPrices.get(storeNumber);
        String locationHierarchyID = productDataCache.getExternalIDToLocation(storeNumber);
        if (locationHierarchyID == null)
        {
          logWarn("Ignoring prices of store " + storeNumber + ", store location not found. Line " + trueLineNo);
          continue;
        }

        String currencyID = storeCurrencies.get(storeNumber);
        if (currencyID == null || currencyID.isEmpty())
        {
          logWarn("Ignoring prices of store " + storeNumber + ", store currency not defined. Line " + trueLineNo);
          continue;
        }

        String regularEachRetailPriceGroupId = settings.getAssortmentPriceBook();
        String specialZonePriceGroupId = settings.getRegularEachRetailPriceGroup();
        boolean assortmentOnly = false;
        
        logDebug("Doing store prices: " );

        for (String store : storePrices.keySet())
        {
          Map<String, PriceElement> itemPrices = storePrices.get(store);

          for (Entry<String, PriceElement> entry : itemPrices.entrySet())
          {
            String itemCode = entry.getKey();
            logDebug("storelevel item="+itemCode);
            PriceElement prices = entry.getValue();
            
            String itemOid = productDataCache.getSkuToItem(itemCode, storeNumber);
            String prdGroup = productDataCache.getOidToPG(itemOid);
            
            if (prdGroup != null && !prdGroup.isEmpty())
            {
              int pgGroup = Integer.parseInt(prdGroup);
              for (Entry<String, PriceElement> skuPrice : prices.getSkuPrices().entrySet())
              {
                String skuCode = skuPrice.getKey();
                PriceElement skuPrices = skuPrice.getValue();
                BigDecimal regularPrice = skuPrices.getRegEachRetail();
                BigDecimal listCost = skuPrices.getListCost();
                String publishLevel = settings.getPriceApprovalState();
                
                logDebug(itemCode+" is regular:"+regularPrice);
                
                if (regularPrice == null)
                {
                  logDebug("no regular, key:" + getStoreItemKey(store, skuCode));
                  assortmentOnly = true;
                  regularPrice = new BigDecimal(1);
                  String spZone = specialZoneItems.get(getStoreItemKey(store, skuCode));
                  logDebug("is sp:" + spZone);
                  try
                  {
                    Map<String, Map<String, PriceElement>> a = zoneItemPrices.get(spZone);
                    if (a != null)
                    {
                      Map<String, PriceElement> b = a.get(spZone);
                      if (b != null)
                      {
                        PriceElement spPrice = b.get(itemOid);
                        if (spPrice != null && spPrice.skuPrices != null)
                        {
                          regularPrice = spPrice.skuPrices.get(skuCode).getRegEachRetail();
                          assortmentOnly = false;
                          logDebug("Not only assortment");
                        }
                      }
                    }
                  } catch (NullPointerException e)
                  {
                    logWarn("Problem in mapping. " + e);
                  }
                }
                else 
                {
                  assortmentOnly = false;
                  logDebug("Received price.");
                }
                if( listCost==null ) {
                  logDebug("Empty list cost. Item = "+itemCode+", store="+store+". Line="+trueLineNo);
                }
                logDebug("item "+itemCode+","+storeNumber+", "+regularPrice+"; "+assortmentOnly);
                if (SAFSettings.PUBLISH_LEVEL_PROPOSED.equalsIgnoreCase(publishLevel))
                {
                  if( regularPrice != null ) {
                    // force 1 for assortment
                    ItemProposedPrice proposedPrice = getProposedPriceItem(regularEachRetailPriceGroupId, pricingDate, locationHierarchyID,
                        pgGroup, skuCode, skuPrices.getCurrentPriceType(), new BigDecimal(1),
                        proposedPriceApprovalLevel);
                    target.addItemProposedPrice(proposedPrice);
                    
                    if (!assortmentOnly)
                    {
                      if (specialZoneItems.containsKey(getStoreItemKey(store, skuCode)))
                      {
                        proposedPrice = getProposedPriceItem(specialZonePriceGroupId, pricingDate, locationHierarchyID,
                            pgGroup, skuCode, skuPrices.getCurrentPriceType(), regularPrice,
                            proposedPriceApprovalLevel);
                        target.addItemProposedPrice(proposedPrice);
                      }
                    }
                  }
                } else
                {
                  if( regularPrice != null ) {
                    // force 1 for assortment
                    ItemPrice price = getPublishedPriceItem(regularEachRetailPriceGroupId, pricingDate, locationHierarchyID, pgGroup,
                        skuCode, skuPrices.getCurrentPriceType(), new BigDecimal(1));
                    target.addItemPrice(price);
                  }
                }
              }
            }
          }
        }
      }

      trueLineNo = trueLineNo-ignoredLinesCount-commentLineCount;
      logInfo("Read " + trueLineNo + " valid lines");
      if (ignoredLinesCount > 0)
      {
        logWarn("Skipped/ignored lines count : " + ignoredLinesCount);
      }
      if (commentLineCount > 0)
      {
        logWarn("Comment line count : " + commentLineCount);
      }

    } catch (ArrayIndexOutOfBoundsException e)
    {
      throw new TranslatorException("Error reading file, line " + trueLineNo + ": " + e.getMessage(), e);
    } catch (IOException e)
    {
      throw new TranslatorException("IOException reading file, line " + trueLineNo + ": " + e.getMessage(), e);
    } catch (NumberFormatException e)
    {
      throw new TranslatorException("Error parsing date from filename " + filename + ": " + e.getMessage(), e);
    } finally
    {
      if (bf != null)
      {
        bf.close();
      }
    }
    return trueLineNo;
  }

  private String getStoreItemKey(String store, String skuCode)
  {
    return skuCode + "_" + store;
  }

  
  protected BufferedReader getReader(File importFile) throws FileNotFoundException
  {
    BufferedReader bf;
    bf = new BufferedReader(new InputStreamReader(new FileInputStream(importFile)));
    return bf;
  }

  protected void addPriceLists(BackBusContainer target, SAFSettings settings, String locationHierarchyID,
      String storeName, Date versionDate, boolean createSpecialZone, boolean assortment)
  {
    String listCostPriceGroupId = settings.getListCostPriceGroup();
    String regularEachRetailPriceGroupId = settings.getRegularEachRetailPriceGroup();
    
    if (createSpecialZone)
    {
      createPriceList(target, locationHierarchyID, storeName, listCostPriceGroupId, settings, versionDate);
    } else
    {
      if( assortment) 
      {
        if( specialZonePricesForStore.contains(locationHierarchyID) ) 
        {
          createPriceList(target, locationHierarchyID, storeName, regularEachRetailPriceGroupId, settings, versionDate);
        }
        createPriceList(target, locationHierarchyID, storeName, settings.getAssortmentPriceBook(), settings, versionDate);
      }
      else
      {
        createPriceList(target, locationHierarchyID, storeName, regularEachRetailPriceGroupId, settings, versionDate);
        createPriceList(target, locationHierarchyID, storeName, listCostPriceGroupId, settings, versionDate);
      }
    }
  }

  @Override
  String getAdapterName()
  {
    return "ItemPrices";
  }

}
