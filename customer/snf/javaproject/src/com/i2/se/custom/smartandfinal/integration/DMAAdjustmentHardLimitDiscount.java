package com.i2.se.custom.smartandfinal.integration;

import java.io.File;
import java.text.DateFormat;
import java.util.*;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.app.integrations.imports.TranslatorException;
import com.i2.se.app.integrations.imports.data.*;

public class DMAAdjustmentHardLimitDiscount extends DMAAdjustmentBase
{
  private static DMAAdjustmentHardLimitDiscount instance = null;

  private DMAAdjustmentHardLimitDiscount(DMAFileTranslator dmaFileTranslator, BackBusContainer target,
      ProductDataCache productDataCache, SAFSettings settings, File importFile, DateFormat df,
      Set<String> adjustmentNodeIds, Set<String> allPostFixes, Set<String> adjustmentIds, Date pricingStartDate)
  {
    super( dmaFileTranslator,  target, productDataCache,  settings,  importFile,  df, adjustmentNodeIds, allPostFixes,  adjustmentIds, pricingStartDate );
    pricingRuleName = settings.getPricingRuleHardLimitDiscount();
    adjustmentNamePattern = settings.getAdjustmentNamePatternHardLimitDiscount();
    adjustmentIdPattern = "HDL_%s_%s_%s";
  }

  public static DMAAdjustmentHardLimitDiscount getInstance(DMAFileTranslator dmaFileTranslator, BackBusContainer target,
      ProductDataCache productDataCache, SAFSettings settings, File importFile, DateFormat df,
      Set<String> adjustmentNodeIds, Set<String> allPostFixes, Set<String> adjustmentIds, Date pricingStartDate)
  {
    if (instance == null)
    {
      instance = new DMAAdjustmentHardLimitDiscount(dmaFileTranslator, target, productDataCache, settings, importFile, df,
          adjustmentNodeIds, allPostFixes, adjustmentIds, pricingStartDate);
    }

    return instance;
  }

  public boolean createHardLimitDiscount(String[] elements, int trueLineNo, String itemCode, String itemOID)
      throws TranslatorException
  {
    return createAdjustment(target, productDataCache, settings, elements, trueLineNo, itemCode, df, importFile,
        itemOID);
  }

  boolean setCustomRuleParams(String paramName, PriceAdjustmentParameter priceParam, String[] elements, int trueLineNo)
  {
    boolean paramSet = false;
    
    String maxParam = settings.getRuleParamName( "MAX" );
    String promoPriceParam = settings.getRuleParamName( "PROMOTIONAL_PRICE" );
    String sfsCaseLabelParam = settings.getRuleParamName( "SFS_CASE_LABEL" );
    String promoSymbolParam = settings.getRuleParamName( "PROMO_SYMBOL" );
    String mixNMatchParam = settings.getRuleParamName( "MIX_N_MATCH" );
    String multQtyParam = settings.getRuleParamName( "PRC_MULT_QTY" );
    
    
    if ( multQtyParam.equalsIgnoreCase(paramName))
    {
      String regMult = elements[DMAFileFields.PRC_REG_MULT];
      priceParam.setValue(regMult);
      paramSet = true;
    }
    else if ( maxParam.equalsIgnoreCase( paramName ) )
    {
      String regMult = elements[DMAFileFields.PRC_PKG_MULT];
      priceParam.setValue(regMult);
      paramSet = true;
    }
    else if( promoPriceParam.equalsIgnoreCase(paramName)) 
    {
      String pgkPrice = elements[DMAFileFields.PRC_PKG_PRICE];
      priceParam.setValue(pgkPrice);
      paramSet = true;
    }
    else if( sfsCaseLabelParam.equalsIgnoreCase( paramName ) )
    {
      String caseLabel = elements[DMAFileFields.SFS_CASE_LABEL];
      priceParam.setValue( caseLabel );
      paramSet = true;
    }
    else if ( promoSymbolParam.equalsIgnoreCase( paramName ) )
    {
      String promoSymbol = elements[DMAFileFields.PROMO_SYMBOL];
      
      String symbolValue = settings.getPromoSymbolValue(promoSymbol);
      if( StringUtils.isEmpty(symbolValue) ) {
        converter.logWarn("Invalid promo symbol value "+promoSymbol+ " at line="+trueLineNo);
      }
      priceParam.setValue(symbolValue);
      paramSet = true;
    }
    else if ( mixNMatchParam.equalsIgnoreCase( paramName ) )
    {
      String mixNMatch = elements[DMAFileFields.PRC_EFF_MIX_MATCH];
      if( "-1".equals(mixNMatch) ) {
        mixNMatch = "";
      }
      priceParam.setValue(mixNMatch);
      paramSet = true;
    }
    
    return paramSet;
  }

}
