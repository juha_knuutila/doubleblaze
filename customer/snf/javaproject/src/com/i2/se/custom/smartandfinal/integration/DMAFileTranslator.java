package com.i2.se.custom.smartandfinal.integration;

import java.io.*;
import java.math.BigDecimal;
import java.text.*;
import java.util.*;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.app.integrations.imports.TranslatorException;
import com.i2.se.app.integrations.imports.data.*;
import com.i2.se.bd.catalog.*;
import com.i2.se.bd.util.bo.session.*;

public class DMAFileTranslator extends SAFConverterBase
{
  private Set<String> adjustmentIds = new HashSet<String>( );
  Set<String> allPostFixes = new HashSet<String>();
  Set<String> adjustmentNodeIds = new HashSet<String>( );
  
  private DateFormat df;
  /** Pricing date from filename as Date and String */
  private Date pricingStartDate;
  private String pricingStartDateStr;
  
  
  
  private static class PriceListVersionKey
  {
    private String store;
    private String dateStr;
    private String locationHierarchyID;
    
    public PriceListVersionKey(String store, String dateStr, String locationHierarchyID)
    {
      super();
      this.store = store;
      this.dateStr = dateStr;
      this.locationHierarchyID = locationHierarchyID;
    }
    
    public String getStore()
    {
      return store;
    }
    public String getDateStr()
    {
      return dateStr;
    }
    public String getLocationHierarchyID()
    {
      return locationHierarchyID;
    }

    @Override
    public int hashCode()
    {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((dateStr == null) ? 0 : dateStr.hashCode());
      result = prime * result + ((locationHierarchyID == null) ? 0 : locationHierarchyID.hashCode());
      result = prime * result + ((store == null) ? 0 : store.hashCode());
      return result;
    }

    @Override
    public boolean equals(Object obj)
    {
      if (this == obj)
        return true;
      if (obj == null)
        return false;
      if (getClass() != obj.getClass())
        return false;
      PriceListVersionKey other = (PriceListVersionKey) obj;
      if (dateStr == null)
      {
        if (other.dateStr != null)
          return false;
      } else if (!dateStr.equals(other.dateStr))
        return false;
      if (locationHierarchyID == null)
      {
        if (other.locationHierarchyID != null)
          return false;
      } else if (!locationHierarchyID.equals(other.locationHierarchyID))
        return false;
      if (store == null)
      {
        if (other.store != null)
          return false;
      } else if (!store.equals(other.store))
        return false;
      return true;
    }

  }
  
  protected String getAdapterName()
  {
    return "DMA";
  }
  
  public int doConvert(TSession session, File importFile, BackBusContainer target, SAFSettings settings,
      ProductDataCache productDataCache) throws TranslatorException, IOException
  {
    BufferedReader bf = null;
    int trueLineNo = 0;
    int ignoredLinesCount = 0;
    int commentLineCount = 0;
    
    df = new SimpleDateFormat(settings.getDMADateFieldFormat());
    
    // CLASS] [int] NOT NULL,|[CLASS_ID] [int] NOT NULL,|[STORE_ID] [int] NOT NULL,|[PRINT_DATE] [datetime] NOT NULL,|[ITM_WHSE_ITEM] [nvarchar](20) NOT NULL,
    // |[ITM_UPC_EAN] [nvarchar](20) NULL,|[ITM_DESCRIPTION] [nvarchar](50) NOT NULL,|[ITM_SIZE_DESC] [nvarchar](20) NULL,|[ITM_SUOM] [nvarchar](10) NULL,
    // |[ITM_PER_UNIT_MSG] [nvarchar](14) NULL,|[ITM_BARCODE] [nvarchar](26) NULL,|[ITM_SCALE] [smallint] NOT NULL,|[ITM_WIC_ELIGIBLE] [smallint] NOT NULL,
    // |[VND_VENDOR] [nvarchar](20) NOT NULL,|[VND_SHORT_NAME] [nvarchar](10) NOT NULL,|[VI_CASE_PACK] [decimal](17, 5) NOT NULL,
    // |[VI_VENDOR_ITEM] [nvarchar](15) NULL,|[CAT_CAT] [int] NULL,|[CAT_SUBCAT] [int] NULL,|[CAT_FAMILY] [int] NULL,
    // |[CAT_DESCRIPTION] [nvarchar](50) NOT NULL,|[DPT_DEPARTMENT] [nvarchar](15) NULL,|[DPT_DESCRIPTION] [nvarchar](50) NULL,
    // |[PRC_REG_SU_PRICE] [decimal](14, 2) NULL,|[PRC_REG_PRICE] [decimal](14, 2) NOT NULL,|[PRC_REG_MULT] [smallint] NOT NULL,
    // |[PRC_REG_PPU] [numeric](14, 3) NULL,|[PRC_REG_SDATE] [varchar](30) NULL,|[PRC_REG_EDATE] [varchar](30) NULL,
    // |[PRC_REG_SDATE_10] [varchar](30) NULL,|[PRC_REG_EDATE_10] [varchar](30) NULL,|[PRC_REG_SDATE_DDMM] [varchar](4) NULL,
    // |[PRC_REG_EDATE_DDMM] [varchar](4) NULL,|[PRC_REG_PRICE_TYPE] [smallint] NOT NULL,|[PRC_REG_PRICE_LEVEL] [smallint] NOT NULL,
    // |[PRC_EFF_SU_PRICE] [decimal](14, 2) NOT NULL,|[PRC_EFF_PRICE] [decimal](14, 2) NOT NULL,|[PRC_EFF_MULT] [smallint] NOT NULL,
    // |[PRC_EFF_PPU] [numeric](14, 3) NULL,|[PRC_EFF_SDATE] [varchar](30) NULL,|[PRC_EFF_EDATE] [varchar](30) NULL,
    // |[PRC_EFF_SDATE_10] [varchar](30) NULL,|[PRC_EFF_EDATE_10] [varchar](30) NULL,|[PRC_EFF_SDATE_DDMM] [varchar](4) NULL,
    // |[PRC_EFF_EDATE_DDMM] [varchar](4) NULL,|[PRC_EFF_PRICE_TYPE] [int] NOT NULL,|[PRC_EFF_PRICE_LEVEL] [smallint] NOT NULL,
    // |[PRC_EFF_PRICE_METHOD] [smallint] NOT NULL,|[PRC_EFF_DISC_FLAG] [int] NOT NULL,|[PRC_EFF_DISCOUNT] [decimal](14, 2) NOT NULL,
    // |[PRC_EFF_DISC_MULTIPLE] [smallint] NOT NULL,|[PRC_EFF_QTY_LIMIT] [smallint] NOT NULL,|[PRC_EFF_MIX_MATCH] [int] NULL,
    // |[PRC_EFF_MIN_ORDER] [decimal](14, 2) NOT NULL,|[PRC_PKG_SU_PRICE] [decimal](14, 2) NULL,|[PRC_PKG_PRICE] [decimal](14, 2) NOT NULL,
    // |[PRC_PKG_MULT] [smallint] NOT NULL,|[PRC_PKG_PPU] [decimal](14, 3) NULL,|[PRC_USAVE_PRICE] [decimal](14, 2) NOT NULL,
    // |[TAS_MSG_CRV] [varchar](4) NOT NULL,|[TAS_MSG_PROMO1] [varchar](50) NOT NULL,|[TAS_MSG_PROMO2] [varchar](50) NOT NULL,
    // |[TAS_MSG_MKT1] [varchar](13) NOT NULL,|[TAS_MSG_MKT2] [varchar](13) NOT NULL,|[TAS_MSG_MKT3] [varchar](50) NULL,|[SIGN_DESC] [varchar](100) NULL,
    // |[COOL] [varchar](9) NOT NULL,|[PROMO_SYMBOL] [char](1) NULL,|[SFS_SMARTBUY] [smallint] NOT NULL,|[SFS_CASE_LABEL] [tinyint] NOT NULL,
    // |[SIGN_PRINT_FLAG] [int] NULL,|[PRC_MKT_PRICE] [decimal](14, 2) NOT NULL,|[PRC_MKT_PPU] [decimal](14, 3) NULL,|[SFI_CM_ID] [bigint] NULL,
    // |[SFI_CM_TYPE] [smallint] NULL,|[SFI_CM_DATA_TYPE] [smallint] NULL,|[SFI_REWARD_PRICE] [decimal](14, 2) NULL,
    // |[SFI_CM_USE_UNIT_PRICE] [int] NULL,|[SFI_CM_PROMOTION_LIMIT] [int] NULL,|[SFI_PM_MULTIPLIER] [int] NULL,|[RECLAIM] [int] NULL,
    // |[IMD_SHELF_LIFE] [int] NOT NULL,|[SFI_EDLP] [int] NULL,|[SCHORDER] [nvarchar](20) NULL,|[SFI_TAG_TYPE] [int] NULL,
    // |[SFI_TAG_SIZE] [varchar](2) NOT NULL,|[LABEL_ID] [int] NOT NULL,|[TAG_COUNT] [int] NOT NULL|Tag Stock
   
   // String filename = importFile.getName();
   // Date pricingDate = resolveFileDate(settings.getPriceFileNamePattern(), filename);
    try
    {
      String filename = importFile.getName();
      pricingStartDate = resolveFileDate(settings.getDMAFileNamePattern(), filename);
      pricingStartDateStr = df.format(pricingStartDate);
      
      bf = new BufferedReader( new InputStreamReader( new FileInputStream( importFile ) ) );
      
      Map <PriceListVersionKey, Map<String,BigDecimal>> priceListVersions = new HashMap<PriceListVersionKey, Map<String,BigDecimal>>();
      Map <PriceListVersionKey, Map<String,BigDecimal>> priceListVersionsRRMB = new HashMap<PriceListVersionKey, Map<String,BigDecimal>>();
      
      while( true )
      {
        String s = bf.readLine( );
        if( s == null )
        {
          break;
        }
        ++trueLineNo;
        if( StringUtils.isEmpty(s) || s.startsWith("#") ) {
          commentLineCount++;
          continue;
        }
        
        String[] elements = s.split( "\\|", -1 );
        if( elements != null ) {
          for(int i = 0; i<elements.length; i++) {
            elements[i] = elements[i].trim();
          }
        }
        
        String effPriceType = elements[DMAFileFields.PRC_EFF_PRICE_TYPE];
        if( "150".equals(effPriceType) || "151".equals(effPriceType)) {
          logWarn("Skipping by rule PRICE_TYPE=150/151, value="+effPriceType+", row="+trueLineNo);
          ignoredLinesCount++;
          continue;
        }
        
        String classId = elements[DMAFileFields.CLASS_ID];
        if( classId.startsWith("-") ) 
        {
          if( "15".equals(effPriceType) ) 
          {
            logWarn("Skipping negative CLASS_ID, "+classId+", pricetype="+effPriceType+", line="+trueLineNo);
            ignoredLinesCount++;
            continue;
          }
          logInfo("Accepted negative CLASS_ID, "+classId+", pricetype="+effPriceType+", line="+trueLineNo);
        }
        
        String whseItem = elements[DMAFileFields.ITM_WHSE_ITEM];
        String priceLevel = elements[DMAFileFields.PRC_EFF_PRICE_LEVEL];
        String itemCode = createItemCode(whseItem, priceLevel, trueLineNo);
        logInfo("ITEM CODE:"+whseItem+"->"+itemCode);
        String store = elements[DMAFileFields.STORE_ID];
        
        boolean lineConsumed = false;
        
        String itemOID = productDataCache.getSkuToItem(itemCode, store);
        if (itemOID == null )
        {
          logWarn( "Error reading item  " + itemCode + ": item not found! Line: " + trueLineNo );
          ignoredLinesCount++;
          continue;
        }
       
        if (settings.isEnabledRegularPrice()) 
        {
          lineConsumed = readRegularRetail(elements, trueLineNo, settings, productDataCache, target, priceListVersions, itemCode, itemOID, null);
        }
       
        if (itemOID != null)
        {
          if (settings.isEnabledMixAndMatch())
          {
            String mixAndMatch = elements[DMAFileFields.PRC_EFF_MIX_MATCH];

            if (mixAndMatch != null)
            {
              try
              {
                logDebug("Setting mm: " + itemCode + "/" + mixAndMatch);
                TBoxItem item = session.getProductData().getBoxItem(itemOID);
                TAttributeValue mm = item.getAttribute(settings.getMixAndMatchAttributeName());
                if (mm != null)
                {
                  if( mixAndMatch != null && "-1".equals( mixAndMatch.trim() ) )
                  {
                    mixAndMatch = "0";
                  }
                  mm.setValue(mixAndMatch);
                  mm.commit();
                }
                
              } catch (TSDKInternalException e)
              {
                logError("Error setting MixAndMatch attribute: item="+itemCode+", code="+mixAndMatch+", line="+trueLineNo+", error: "+e);
              }
            }
          }
          
            // try to read adjustments
          
          // Regular_retail_mb
          if (settings.isEnabledRegularRetailBM())
          {
            String mult = elements[DMAFileFields.PRC_REG_MULT];
            if (!StringUtils.isEmpty(mult))
            {
              try
              {
                int iMult = Integer.parseInt(mult);
                if ("1".equals(elements[DMAFileFields.PRC_EFF_PRICE_TYPE]) && iMult > 1)
                {
                  BigDecimal priceValue = getBigDecimal( elements, trueLineNo, DMAFileFields.PRC_REG_MULT);
                  logDebug("Mult value="+priceValue);
                  lineConsumed = lineConsumed | readRegularRetail(elements, trueLineNo, settings, productDataCache, target, priceListVersionsRRMB, itemCode, itemOID,
                      priceValue);

                  // DMAAdjustmentRegularRetailMB adjCreator = DMAAdjustmentRegularRetailMB.getInstance(this, target,
                  //    productDataCache, settings, importFile, df, adjustmentNodeIds, allPostFixes, adjustmentIds, pricingStartDate);
                  // lineConsumed = lineConsumed | adjCreator.createRegularRetailMB(elements, trueLineNo, itemCode, itemOID);
                }
              } catch( NumberFormatException e) {
                logWarn( "Error reading REG_MULT : "+mult+ ", line="+trueLineNo);
              }
            } 
            else
            {
              logDebug("REG_MULT is empty, line="+trueLineNo);
            }
          }

          // BMSM
          if (settings.isEnabledBMSM())
          {
            if ("10".equals(elements[DMAFileFields.PRC_EFF_PRICE_TYPE]))
            {
             DMAAdjustmentBMSM adjCreator = DMAAdjustmentBMSM.getInstance(this, target, productDataCache, settings, importFile, df,adjustmentNodeIds,allPostFixes, adjustmentIds, pricingStartDate );
             lineConsumed = lineConsumed | adjCreator.createBMSM( elements, trueLineNo, itemCode, itemOID);
            }
          }

          // Weekly special
          if (settings.isEnabledWeeklySpecial())
          {
            if ("15".equals(elements[DMAFileFields.PRC_EFF_PRICE_TYPE]))
            {
             DMAAdjustmentWeeklySpecial adjCreator = DMAAdjustmentWeeklySpecial.getInstance(this, target, productDataCache, settings, importFile, df,adjustmentNodeIds,allPostFixes, adjustmentIds, pricingStartDate );
             lineConsumed = lineConsumed | adjCreator.createWeeklySpecial( elements, trueLineNo, itemCode, itemOID);
            }
          }
          
          // Clearance
          if (settings.isEnabledClearance())
          {
            if ("90".equals(elements[DMAFileFields.PRC_EFF_PRICE_TYPE]))
            {
             DMAAdjustmentClearance adjCreator = DMAAdjustmentClearance.getInstance(this, target, productDataCache, settings, importFile, df,adjustmentNodeIds,allPostFixes, adjustmentIds, pricingStartDate);
             lineConsumed = lineConsumed | adjCreator.createClearance( elements, trueLineNo, itemCode, itemOID);
            }
          }

          // HardLimitDiscount
          if (settings.isEnabledHardLimitDiscount())
          {
            if ("21".equals(elements[DMAFileFields.PRC_EFF_PRICE_TYPE]))
            {
             DMAAdjustmentHardLimitDiscount adjCreator = DMAAdjustmentHardLimitDiscount.getInstance(this, target, productDataCache, settings, importFile, df,adjustmentNodeIds,allPostFixes, adjustmentIds, pricingStartDate );
             lineConsumed = lineConsumed | adjCreator.createHardLimitDiscount( elements, trueLineNo, itemCode, itemOID);
            }
          }
        }
        
        if( !lineConsumed ) {
          ignoredLinesCount++;
        }
      }
      
      createPriceListVersions( target, settings, priceListVersions,  settings.getDMARegularRetailPriceGroup() );
      createPriceListVersions( target, settings, priceListVersionsRRMB, settings.getRegularRetailMBPriceBook() );
      
      // add regular prices
      ignoredLinesCount = setRegularPricesToPriceBook(target, settings, productDataCache, trueLineNo, ignoredLinesCount,
          priceListVersions, settings.getDMARegularRetailPriceGroup());
      // add regular MB values
      ignoredLinesCount = setRegularPricesToPriceBook(target, settings, productDataCache, trueLineNo, ignoredLinesCount,
          priceListVersionsRRMB, settings.getRegularRetailMBPriceBook());
      
      logInfo( "Read " + trueLineNo + " lines." );
      if( ignoredLinesCount>0 ) {
        logWarn( "Skipped/ignored lines count : "+ ignoredLinesCount );
      }
      if( commentLineCount>0 ) {
        logWarn( "Comment line count : "+ commentLineCount );
      }
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
      throw new TranslatorException(
          "Error reading file, line " + trueLineNo + ": " + e.getMessage( ), e );
    }
    catch( IOException e )
    {
      throw new TranslatorException(
          "IOException reading file, line " + trueLineNo + ": " + e.getMessage( ), e );
    }
    finally
    {
      if( bf != null )
      {
        bf.close( );
      }
    }
    return trueLineNo;
  }

  private int setRegularPricesToPriceBook(BackBusContainer target, SAFSettings settings,
      ProductDataCache productDataCache, int trueLineNo, int ignoredLinesCount,
      Map<PriceListVersionKey, Map<String, BigDecimal>> priceListVersions,
      String priceBook)
  {
    for (PriceListVersionKey key : priceListVersions.keySet())
    {
      Map<String, BigDecimal> listVersion = priceListVersions.get(key);
      
      Date pricingDate=null;
      try
      {
        pricingDate = df.parse(key.getDateStr());
      } catch (ParseException e)
      {
         logError("Error parsing date from: "+key.getDateStr()+", at line="+trueLineNo+", error: "+e);
         ignoredLinesCount++;
         continue;
      }

      logDebug( "Date:  "+ key.getDateStr() + " -> "+pricingDate);
      int priceType = settings.getDefaultPriceType( );
      
      
      for (String itemOID : listVersion.keySet())
      {
        String skuCode = productDataCache.getItemCode(itemOID);
        if( skuCode == null ) {
          logWarn("Item not found by oid "+itemOID+ ", at line "+trueLineNo);
          ignoredLinesCount++;
          continue;
        }
        String prdGroup = productDataCache.getOidToPG(itemOID);
        logInfo("ITEM " + skuCode + ", " + prdGroup);
        if( prdGroup == null ) {
          logWarn("Product Group not found for item "+itemOID+ ", probably missing item.");
          ignoredLinesCount++;
          continue;
        }
        
        int pgGroup = Integer.parseInt(prdGroup);
        BigDecimal priceValue = listVersion.get(itemOID);
       
        String regularEachRetailPriceGroupId = priceBook;
        String publishLevel = settings.getPriceApprovalState();
        int proposedPriceApprovalLevel = settings.getPriceApprovalProposedPriceLevel();
        String locationHierarchyID = key.getLocationHierarchyID();
            
        if (SAFSettings.PUBLISH_LEVEL_PROPOSED.equalsIgnoreCase(publishLevel))
        {
          logInfo("Setting proposed price");
          ItemProposedPrice proposedPrice = getProposedPriceItem(regularEachRetailPriceGroupId, pricingDate,
              locationHierarchyID, pgGroup, skuCode, priceType, priceValue,
              proposedPriceApprovalLevel);
          target.addItemProposedPrice(proposedPrice);

        } else
        {
          logInfo("Setting published price");
          ItemPrice price = getPublishedPriceItem(regularEachRetailPriceGroupId, pricingDate, locationHierarchyID,
              pgGroup, skuCode, priceType, priceValue);
          target.addItemPrice(price);
        }
      }
    }
    return ignoredLinesCount;
  }
  
  private void createPriceListVersions(BackBusContainer target, SAFSettings settings, Map<PriceListVersionKey, Map<String, BigDecimal>> priceListVersions,
      String regularEachRetailPriceGroupId )
  {
    for( PriceListVersionKey listVersion : priceListVersions.keySet() ) {
      Date pricingDate=null;
      try
      {
        pricingDate = df.parse(listVersion.getDateStr());
      } catch (ParseException e)
      {
        e.printStackTrace();
      }
      createPriceList(target, listVersion.getLocationHierarchyID(), listVersion.getStore(), regularEachRetailPriceGroupId, settings, pricingDate);
    }
  }

  /**
   * 
   * @param elements
   * @param trueLineNo
   * @param settings
   * @param productDataCache
   * @param target
   * @param priceListVersions
   * @param itemCode
   * @param itemOID
   * @param priceValue if this is given then use this value. otherwise use normal price value fields.
   * @return
   */
  private boolean readRegularRetail(String[] elements, int trueLineNo, SAFSettings settings, ProductDataCache productDataCache, 
      BackBusContainer target, Map<PriceListVersionKey, Map<String, BigDecimal>> priceListVersions, String itemCode, String itemOID,
      BigDecimal priceValue)
  {
    boolean isCaseItem = itemCode.endsWith("1");
    
    String storeNumber = elements[DMAFileFields.STORE_ID];
    BigDecimal regPrice = priceValue;
    
    if (regPrice == null)
    {
      if (isCaseItem)
      {
        regPrice = getBigDecimal(elements, trueLineNo, DMAFileFields.PRC_EFF_PRICE);
      } else
      {
        regPrice = getBigDecimal(elements, trueLineNo, DMAFileFields.PRC_REG_SU_PRICE);
      }
    }
   
    String pricingDateStr;
    if( settings.getDMAUseFileDateAsPricingDate() ) {
      pricingDateStr = pricingStartDateStr;
    } else {
      pricingDateStr = elements[DMAFileFields.PRC_REG_SDATE];
    }
    
    logInfo( "POS_PRC_EFF_PRICE_METHOD "+ elements[DMAFileFields.PRC_EFF_PRICE_METHOD] + ", pos="+DMAFileFields.PRC_EFF_PRICE_METHOD);
    logInfo( "POS_PRC_REG_PRICE "+ regPrice+", pos="+(isCaseItem ? DMAFileFields.PRC_EFF_PRICE : DMAFileFields.PRC_REG_SU_PRICE));
    
    //String locationHierarchyID = productDataCache.getExternalIDToLocation( storeNumber );
    String locationHierarchyID = productDataCache.getExternalIDToZone( storeNumber );
    
    if( locationHierarchyID == null )
    {
      logWarn( "Ignoring prices of store " +
              storeNumber + ", store zone not found. Line "+trueLineNo );
      return false;
    }
    
    PriceListVersionKey key = new PriceListVersionKey(storeNumber,pricingDateStr, locationHierarchyID);
    Map<String, BigDecimal> itemPrices = priceListVersions.get(key);
    if( itemPrices == null ) {
      itemPrices = new HashMap<String, BigDecimal>();
      priceListVersions.put(key, itemPrices);
    }
    
    BigDecimal earlierPrice = itemPrices.get(itemOID); 
    if( earlierPrice != null ) {
      boolean same = regPrice.compareTo(earlierPrice) == 0;
      logWarn("Price for item in this store already received: "+ itemCode + " / "+itemOID + " at " + storeNumber + ". Price is "+regPrice + ( same ? " same as earlier.":", different earlier price: "+earlierPrice) );
    }
    
    itemPrices.put(itemOID, regPrice);
    
    return true;
  }


  protected String createItemCode(String whseItem, String priceLevel, int lineNumber)
  {
    if( StringUtils.isAnyEmpty(whseItem, priceLevel) )
    {
      return null;
    }
    if( whseItem.length() > 6 ) {
      return whseItem;
    }
    
    StringBuilder sb = new StringBuilder(whseItem);
    
    if( "0".equals(priceLevel) || "5".equals(priceLevel)) {
      sb.append("2");
    }
    else if( "1".equals(priceLevel) || "6".equals(priceLevel)) {
      sb.append("1");
    }
    else {
      logWarn("Not item pricelevel: "+ priceLevel+", at line="+lineNumber); 
    }
    
    while( sb.length() < 6 )
    {
      sb.insert(0, "0");
    }
    
    return sb.toString();
  }

}
