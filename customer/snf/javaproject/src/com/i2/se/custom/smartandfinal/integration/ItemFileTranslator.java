package com.i2.se.custom.smartandfinal.integration;

import java.io.*;
import java.util.*;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.app.integrations.imports.*;
import com.i2.se.app.integrations.imports.data.*;
import com.i2.se.bd.catalog.*;
import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.util.bo.session.*;

public class ItemFileTranslator extends SAFConverterBase
{
  // set of item codes we have processed
  private Set<String> itemCodes = new HashSet<String>();
  private Map<String, String> styleItemOids = new HashMap<String, String>();
  private Map<String, Integer> companyNumberClassId = new HashMap<String, Integer>();
  private FileWriter caseFileWriter = null;
  private boolean failedToOpenCaseFile = false;
  private String caseFileName = null;
  
  protected String getAdapterName()
  {
    return "Items";
  }

  public int doConvert(TSession session, File importFile, BackBusContainer target, SAFSettings settings,
      ProductDataCache productDataCache) throws TranslatorException, IOException
  {
    BufferedReader bf = null;
    int trueLineNo = 0;
    int ignoredLinesCount = 0;
    int commentLineCount = 0;

    logInfo("Settings: " + settings);
    boolean generateCaseItemFile = "true".equalsIgnoreCase(settings.getGenerateCaseItemFile());
    if( importFile.getName().indexOf("_cases") > 0 ) {
      generateCaseItemFile = false;
      logInfo("Not doing cases file, already reading such : "+importFile.getName());
    }

    try
    {
      TProductData pd = session.getProductData();
      bf = new BufferedReader(new InputStreamReader(new FileInputStream(importFile)));
      while (true)
      {
        String s = bf.readLine();
        if (s == null)
        {
          break;
        }
        ++trueLineNo;
        if (StringUtils.isEmpty(s) || StringUtils.startsWith(s, "#"))
        {
          logInfo("Comment line " + trueLineNo + " : " + s);
          commentLineCount++;
          continue;
        }

        // Some item description has "|" chars in double quotes. Do not split
        // those if present.
        // | Split on seperator
        // (?= Followed by
        // (?: Start a non-capture group
        // [^"]* 0 or more non-quote characters
        // " 1 quote
        // [^"]* 0 or more non-quote characters
        // " 1 quote
        // )* 0 or more repetition of non-capture group
        // [^"]* 0 or more non-quotes
        // $ Till the end

        String regEx = "\\|(?=(?:[^\"]*\"[^\"]*)*[^\"]*$)";
        String[] elements = s.split(regEx, -1);

        int i = 0;
        String companyNumber = elements[i++];
        String itemCode = elements[i++];
        String upcCode = elements[i++];
        String itemDescription = elements[i++];
        String categoryCode = elements[i++];
        String subCatCode = elements[i++];
        String componentCode = elements[i++];
        String sf_pos_dep = elements[i++];
        String size = elements[i++];
        String uom = elements[i++];
        String familyCode = elements[i++];
        String upcUnit = elements[i++];
        String status = elements[i++];
        String packageSize = elements[i++];
        String scaleIndicator = elements[i++];
        String smartBuyType = elements[i++];
        String mdmItemCode = elements[i++];
        String excludeSPSF = elements[i++];
        String printCaseFlagStr = elements[i++];
        String prePricedItemStr = elements[i++];
        
        String printCaseFlag = "1".equals( printCaseFlagStr ) || "y".equalsIgnoreCase( printCaseFlagStr ) ? "1" : "0";
        String prePricedItem = "1".equals( prePricedItemStr ) || "y".equalsIgnoreCase( prePricedItemStr ) ? "1" : "0";
        boolean caseItemWithoutUnit = false;
        
        productDataCache.addSkuToItem(itemCode, itemCode);

        // replace all double quotes
        itemDescription = itemDescription.replaceAll("^\"|\"$", "");

        // Create item only if missing
        if (itemCode.isEmpty())
        {
          logWarn("item number empty for company " + companyNumber + ", line " + trueLineNo);

        } else if (!itemCodes.contains(itemCode + "_" + companyNumber))
        {
          itemCodes.add(itemCode + "_" + companyNumber);

          String prodGroup = settings.getCompanyNumberToProductGroup().get(companyNumber);

          if (prodGroup == null)
          {
            logWarn("product group not set for company " + companyNumber + ", line " + trueLineNo);
            ignoredLinesCount++;
            continue;
          }

          int pGroup = Integer.parseInt(prodGroup);
          String styleItemOid = null;
          String styleCode = null;
          
          if (!StringUtils.isEmpty(familyCode))
          {
            styleCode = familyCode + "_family"; 
            createStyleItem(target, settings, trueLineNo, pd, companyNumber, categoryCode, subCatCode, componentCode,
                familyCode, pGroup, styleItemOid, styleCode, itemCode);
          }
          
          if ("C".equals(upcUnit) || "UC".equals(upcUnit))
          {
            if (!itemCode.endsWith("1"))
            {
              logWarn("Case item not ending with 1: " + upcUnit);
            }
            // Create link to Unit item
            String unitCode = itemCode.substring(0, itemCode.length() - 1) + "2";

            logInfo("Doing item link from " + itemCode + " to " + unitCode + "/" + pGroup + " at line " + trueLineNo);

            try
            {
              TBoxItem unitItem = pd.findBoxItem(pGroup, unitCode);
              if (unitItem == null)
              {

                TBoxItem tmp = null;
                for (int ii = -1; ii < 4; ii++)
                {
                  unitItem = pd.findBoxItem(pGroup, ii, unitCode);
                  logInfo("Item: " + unitItem + " " + ii);
                  if (unitItem != null)
                  {
                    tmp = unitItem;
                  }
                }
                unitItem = tmp;
              }
              if (unitItem != null)
              {
                logInfo("Unit found, creating link to  " + unitItem.getObjectID());

                Item item = createItem(target, settings, pGroup, prodGroup, productDataCache, trueLineNo, companyNumber,
                    itemCode, upcCode, itemDescription, sf_pos_dep, size, uom, familyCode, upcUnit, status, packageSize,
                    scaleIndicator, smartBuyType, mdmItemCode, excludeSPSF, unitItem.getObjectID(), styleCode, true, unitCode, 
                    unitItem.getItemLevel(), printCaseFlag, prePricedItem );
              } else
              {
                caseItemWithoutUnit = true;
                logWarn("Unit item does not exist for case: " + itemCode + "/" + unitItem + "/" + pGroup + " at line "
                    + trueLineNo);
                if ( !generateCaseItemFile ) {
                  logError("Unit item does not exist for case - importing cases, item should exist!: " + itemCode + "/" + unitItem + "/" + pGroup + " at line "
                      + trueLineNo);
                }
                if (generateCaseItemFile)
                {
                  writeToCaseFile(importFile, s, false, settings);
                }
              }
            } catch (TSDKInternalException e)
            {
              logWarn("Error creating link item " + itemCode + " at line " + trueLineNo + "\n" + e);
            }
          } else
          {
            if (!StringUtils.isEmpty(familyCode))
            {
              createStyleItem(target, settings, trueLineNo, pd, companyNumber, categoryCode, subCatCode, componentCode,
                  familyCode, pGroup, styleItemOid, styleCode, itemCode);

              if ("U".equals(upcUnit))
              {
                ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(), StaticLogger.INFO, 0,
                    " Creating variant  " + upcUnit);
                // create variant
                Item item = createItem(target, settings, pGroup, prodGroup, productDataCache, trueLineNo, companyNumber,
                    itemCode, upcCode, itemDescription, sf_pos_dep, size, uom, familyCode, upcUnit, status, packageSize,
                    scaleIndicator, smartBuyType, mdmItemCode, excludeSPSF, null, styleCode, false, null, 0, printCaseFlag, prePricedItem);
              } else
              {
                ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(), StaticLogger.INFO, 0,
                    " Creating normal linked " + upcUnit);

                // No action
                // for CASE item create normal item, with link
                // Item item = createItem(target, settings, pGroup, prodGroup, productDataCache,
                // trueLineNo, companyNumber,
                // itemCode, upcCode, itemDescription, sf_pos_dep, size, uom, familyCode,
                // upcUnit, status, packageSize,
                // scaleIndicator, smartBuyType, styleItemOid, null, true);
              }

            } else
            {
              // create normal item
              Item item = createItem(target, settings, pGroup, prodGroup, productDataCache, trueLineNo, companyNumber,
                  itemCode, upcCode, itemDescription, sf_pos_dep, size, uom, familyCode, upcUnit, status, packageSize,
                  scaleIndicator, smartBuyType, mdmItemCode, excludeSPSF, null, null, true, null, 0, printCaseFlag, prePricedItem);
             }
          }

          if( !caseItemWithoutUnit )
          {
            ItemHierarchyAssignment assign = createHierarchyAssignment(target, companyNumber, itemCode, categoryCode,
                subCatCode, componentCode, pGroup, 0);
          }
        }
      }
      trueLineNo = trueLineNo-ignoredLinesCount-commentLineCount;
      logInfo("Read " + trueLineNo + " valid lines");
      if (ignoredLinesCount > 0)
      {
        logWarn("Skipped/ignored lines count : " + ignoredLinesCount);
      }
      if (commentLineCount > 0)
      {
        logWarn("Comment line count : " + commentLineCount);
      }
    } catch (ArrayIndexOutOfBoundsException e)
    {
      logError("Exception " + e);
      throw new TranslatorException("Error reading file, line " + trueLineNo + ": " + e.getMessage(), e);
    } catch (IOException e)
    {
      logError("Exception " + e);
      throw new TranslatorException("IOException reading file, line " + trueLineNo + ": " + e.getMessage(), e);
    } finally
    {
      writeToCaseFile(null, null, true, settings);
      if (bf != null)
      {
        bf.close();
      }
    }
    return trueLineNo;
  }

  private void createStyleItem(BackBusContainer target, SAFSettings settings, int trueLineNo, TProductData pd,
      String companyNumber, String categoryCode, String subCatCode, String componentCode, String familyCode, int pGroup,
      String styleItemOid, String styleCode, String itemCode) throws TranslatorException
  {
    styleItemOid = styleItemOids.get(familyCode);
    logInfo("Item " + itemCode + " family " + familyCode + " oid=" + styleItemOid + " at line " + trueLineNo);

    // create style item if is not yet existing
    // style item
    if (styleItemOid == null)
    {
      TBoxItem style = null;
      
      try
      {
        // TODO: cache this
        style = pd.findBoxItem(pGroup, styleCode);
        logInfo("Style item " + style + " at line " + trueLineNo);

        if (style == null)
        {
          int classId = settings.getFamilyItemClassId();
          style = pd.createBoxItem(pGroup, TBoxItem.ITEM_TYPE_STYLE, styleCode, classId);
          
          // create style item in backbus as well
        //  Item leadItem = createItem(target, settings, pGroup, prodGroup, productDataCache, trueLineNo,
        //      companyNumber, styleCode, upcCode, itemDescription, sf_pos_dep, size, uom, familyCode, upcUnit,
        //      status, packageSize, scaleIndicator, smartBuyType, mdmItemCode, excludeSPSF, null, null, false);
          ItemHierarchyAssignment assign = createHierarchyAssignment(target, companyNumber, styleCode, categoryCode,
              subCatCode, componentCode, pGroup, 1);
        }
       
      } catch (TSDKInternalException | TRuntimeException e)
      {
        throw new TranslatorException("Error creating style item, line " + trueLineNo + ": " + e.getMessage(),
            e);
      }

      styleItemOid = style.getObjectID();

      /*
       * using backbus styleItemOid = productDataCache.getSkuToItem(familyCode);
       * ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(),
       * StaticLogger.WARN, 0, "Style item created "+ styleItem);
       * 
       */
      styleItemOids.put(familyCode, styleItemOid);
    }
  }

  private void writeToCaseFile(File importFile, String s, boolean closeFile, SAFSettings settings)
  {
    // close only
    if (closeFile)
    {
      if (caseFileWriter != null)
      {
        try
        {
          caseFileWriter.close();
          parent.createDoneFile(new File(caseFileName));
        } catch (IOException e)
        {
          logWarn("Failed to close cases file: " + e);
        }
      }
      return;
    }
    if (failedToOpenCaseFile)
    {
      return;
    }

    if (caseFileWriter == null)
    {
      String dir = settings.getInboundDirectory();
            
      String name = importFile.getName();
      int pos = name.lastIndexOf(".");
      String fileName = name.substring(0, pos) + "_cases." + name.substring(pos + 1);
      caseFileName  = dir+File.separator+fileName;
      logDebug("Writing case items to file:"+ caseFileName);
      try
      {
        caseFileWriter = new FileWriter(caseFileName);
      } catch (IOException e)
      {
        failedToOpenCaseFile = true;
        logWarn("Failed to open cases file: " + e+ ". "+caseFileName);
      }
    }

    try
    {
      caseFileWriter.write(s + "\n");
    } catch (IOException e)
    {
      logWarn("Failed to write to cases file: " + e);
    }
  }

  private int getClassIdByCompanyNumber(TProductData pd, SAFSettings settings, String companyNumber)
      throws TSDKInternalException
  {
    Integer classId = companyNumberClassId.get(companyNumber);
    if (classId == null)
    {
      String className = settings.getClassNamesPerCompany().get(companyNumber);
      TClass cls = pd.getClass(className);
      classId = cls.getID();
      companyNumberClassId.put(companyNumber, classId);
    }
    return classId;
  }

  private Item createItem(BackBusContainer target, SAFSettings settings, int pGroup, String prodGroup,
      ProductDataCache productDataCache, int trueLineNo, String companyNumber, String itemCode, String upcCode,
      String itemDescription, String sf_pos_dep, String size, String uom, String familyCode, String upcUnit,
      String status, String packageSize, String scaleIndicator, String smartBuyType, String mdmItemCode,
      String excludeSPSF, String leadOid, String styleCode, boolean normalItem, String leadItemCode, int leadItemLevel, 
      String printCaseFlag, String prePricedItem)
  {
    Item item = new Item();

    productDataCache.addSkuToPG(itemCode, prodGroup);
    String className = settings.getClassNamesPerCompany().get(companyNumber);

    item.setProductGroupID(pGroup);
    item.setItemName(itemCode);
    item.setClassName(className);

    if (normalItem) // normal / case
    {
      item.setItemLevel(0);
      if(styleCode != null) { // cases can have style items
        item.setStyleItemLevel(1);
        item.setStyleItemName(styleCode);
      }
    } else if (styleCode != null)
    {
      // variant
      item.setItemLevel(0);
      item.setStyleItemLevel(1);
      item.setStyleItemName(styleCode);
    } else
    {
      item.setItemLevel(1);
    }

    target.addItem(item);

    ItemDescription description = new ItemDescription();
    description.setProductGroupID(pGroup);
    description.setItemName(itemCode);
    description.setProductLanguageID(settings.getProductLanguageID());
    description.setItemLevel(0);
    description.setDescription(itemDescription);
    target.addItemDescription(description);

    // attributes
    target.addItemAttribute(ItemAttribute.create(pGroup, itemCode, 0, settings.getUpcAttribute(), upcCode));

    target.addItemAttribute(ItemAttribute.create(pGroup, itemCode, 0, settings.getPosAttribute(), sf_pos_dep));
    target.addItemAttribute(ItemAttribute.create(pGroup, itemCode, 0, settings.getSizeAttribute(), size));

    target.addItemAttribute(ItemAttribute.create(pGroup, itemCode, 0, settings.getUomAttribute(), uom));

    target.addItemAttribute(ItemAttribute.create(pGroup, itemCode, 0, settings.getFamilyCodeAttribute(), familyCode));

    target.addItemAttribute(ItemAttribute.create(pGroup, itemCode, 0, settings.getUpcUnitAttribute(), upcUnit));

    status = StringUtils.upperCase(status);

    target.addItemAttribute(ItemAttribute.create(pGroup, itemCode, 0, settings.getItemStatusAttribute(), status));

    target.addItemAttribute(ItemAttribute.create(pGroup, itemCode, 0, settings.getPackageSizeAttribute(), packageSize));
    target.addItemAttribute(ItemAttribute.create(pGroup, itemCode, 0, settings.getNbrItemsInCase(), packageSize));
    
    target.addItemAttribute(
        ItemAttribute.create(pGroup, itemCode, 0, settings.getScaleIndicatorAttribute(), scaleIndicator));

    target
        .addItemAttribute(ItemAttribute.create(pGroup, itemCode, 0, settings.getSmartBuyTypeAttribute(), smartBuyType));

    target.addItemAttribute(ItemAttribute.create(pGroup, itemCode, 0, settings.getMDMItemCodeAttribute(), mdmItemCode));

    if (leadOid != null)
    {
      target.addItemAttribute(ItemAttribute.create(pGroup, itemCode, 0, settings.getStyleItemAttribute(), leadOid));
      
      item.setPriceLinkItemLevel(leadItemLevel);
      item.setPriceLinkItemName(leadItemCode);
    }

    target.addItemAttribute(ItemAttribute.create(pGroup, itemCode, 0, settings.getExcludeSPSFAttribute(), excludeSPSF));
    
    target.addItemAttribute(ItemAttribute.create(pGroup, itemCode, 0, settings.getPrintCaseFlagAttribute(), printCaseFlag));
    target.addItemAttribute(ItemAttribute.create(pGroup, itemCode, 0, settings.getPrePricedItemAttribute(), prePricedItem));
    
    return item;
  }

  private ItemHierarchyAssignment createHierarchyAssignment(BackBusContainer target, String companyNumber,
      String itemCode, String categoryCode, String subCatCode, String componentCode, int pGroup, int level)
  {
    if (categoryCode == null || categoryCode.isEmpty())
    {
      logWarn("Hierarchy Assignment not created :CategoryCode is empty for item " + itemCode);
      return null;
    }

    if (subCatCode == null || subCatCode.isEmpty())
    {
      logWarn("Hierarchy Assignment not created : SubCategory Code is empty for item " + itemCode);
      return null;
    }

    if (componentCode == null || componentCode.isEmpty())
    {
      logWarn("Hierarchy Assignment not created : Component Code is empty for item " + itemCode);
      return null;
    }

    ItemHierarchyAssignment assign = new ItemHierarchyAssignment();
    assign.setProductGroupID(pGroup);
    assign.setItemName(itemCode);
    assign.setItemLevel(level);
    assign.setHierarchyID("P" + companyNumber + "-" + categoryCode + "-" + subCatCode + "-" + componentCode);
    target.addItemHierarchyAssignment(assign);
    return assign;
  }

  public static String getBooleanValue(String attValue)
  {
    return ("Y".equals(attValue)) ? "1" : "0";
  }

  public static String getValidValue(String attValue)
  {
    return ("?".equals(attValue)) ? "" : attValue;
  }
}
