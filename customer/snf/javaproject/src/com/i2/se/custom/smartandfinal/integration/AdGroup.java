package com.i2.se.custom.smartandfinal.integration;

import java.util.*;

class AdGroup
{
  private static final String TEST_GROUP = "TEST_GROUP";

  private String id;
  private String name;
  private int priority;
  private Date endDate;
  private Date startDate;
  
  private Set<String> stores;
  
  public AdGroup(String id, String name, int priority, Date start, Date end)
  {
    super();
    this.id = id;
    this.name = name;
    this.priority = priority;
    this.stores = new HashSet<String>();
    this.startDate = start;
    this.endDate = end;
  }
  
  public AdGroup(String id, String name, int priority)
  {
    this(id, name, priority, null, null);
  }

  public String getId()
  {
    return id;
  }

  public String getName()
  {
    return name;
  }

  public int getPriority()
  {
    return priority;
  }

  public boolean isTestGroup()
  {
    return TEST_GROUP.equals(id);
  }

  public static AdGroup getTestGroup()
  {
    return new AdGroup(TEST_GROUP, null, 0, null, null);
  }

  public void addStore(String store)
  {
    stores.add(store);
  }
  public Set<String> getStores()
  {
    return stores;
  }
  
  public Date getEndDate()
  {
    return endDate;
  }

  public Date getStartDate()
  {
    return startDate;
  }

  @Override
  public int hashCode()
  {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    result = prime * result + priority;
    result = prime * result + ((stores == null) ? 0 : stores.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj)
  {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    AdGroup other = (AdGroup) obj;
    if (id == null)
    {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    if (name == null)
    {
      if (other.name != null)
        return false;
    } else if (!name.equals(other.name))
      return false;
    if (priority != other.priority)
      return false;
    if (stores == null)
    {
      if (other.stores != null)
        return false;
    } else {
 
      if( stores.size() != other.stores.size() ) {
        return false;
      }
      
      for( String store: stores ) {
        if( !other.stores.contains(store) ) {
          return false;
        }
      }
    }
      
    return true;
  }

  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    sb.append("AdGroup [id=" + id + ", name=" + name + ", priority=" + priority + ", end=" + endDate + ", start=" + startDate
        + ", stores=" + stores + "] \n");
    
    for( String store: stores ) 
    {
      sb.append("  Store: "+store);
    }
    return sb.toString();
  }
}