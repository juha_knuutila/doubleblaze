package com.i2.se.custom.smartandfinal.integration;

import java.util.*;
import java.util.Map.Entry;

import com.i2.se.app.integrations.imports.mmsv1.PriceEndCondition;
import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.uic.util.StringUtils;

/**
 * Base class Import setting. Common settings are included with this class.
 * 
 * @author J1013162
 *
 */
public abstract class ImportSettingsBase
{
  private boolean archiveFiles;
  private boolean cleanWorkDirBeforeStart;
  private boolean createBaseData;
  private int productGroupID;
  private String productGroupName;
  private String productLanguageID;
  private String businessIDPrefix;
  private String parentBusinessOID;
  private Set<String> itemSets;
  private Map<Integer, String> priceTypeToName;
  private Map<String, Integer> nameToPriceType;
  private int defaultPriceType;
  private Map<Integer, Integer> priceTypeConversion;
  
  private boolean customPriceTypeCondition;
  private int maxNoOfRecords;
  private List<PriceEndCondition> priceEndConditions;
  private Map<Integer, Integer> defaultPriceTypes;
  private Map<String, String> productGroupNames;
  private Map<String, String> productGroupByPathMap;
  private List<String> productGroupIds;
  
  private void logMessage(int level, String msg)
  {
    ObjectFactory.getLogManager( ).getStaticLogger( ).addLogEntry(
        ImportSettingsBase.class, level, 0, msg);
  }
  private void logWarn(String msg)
  {
    logMessage(StaticLogger.WARN, msg);
  }
  private void logError(String msg)
  {
    logMessage(StaticLogger.ERROR, msg);
  }
  void logInfo(String msg)
  {
    logMessage(StaticLogger.INFO, msg);
  }
  
  public ImportSettingsBase( Map<String, String> properties )
  {
    initInner(properties);
  }
  
  void initInner(Map<String, String> properties)
  {
    archiveFiles = getProperty( properties, "ArchiveFiles", false );
    cleanWorkDirBeforeStart = getProperty( properties, "CleanWorkDirBeforeStart", false );
    createBaseData = getProperty( properties, "CreateBaseData", true );
    productLanguageID = getProperty( properties, "ProductLanguageID", "Pen" );

    businessIDPrefix = getProperty( properties, "BusinessIDPrefix", "" );
    parentBusinessOID = getProperty( properties, "ParentBusinessOID", "" );

    String[] productGroupIdsArray = getProperty( properties, "ProductGroupIDs", "" ).split( "," );

    productGroupIds = Arrays.asList( productGroupIdsArray );

    String[] sItemSets = getProperty( properties, "ItemSets", "" ).split( "," );
    itemSets = new HashSet<String>( );
    for( String itemSet : sItemSets )
    {
      String trimmedItemSet = itemSet.trim( );
      if( !trimmedItemSet.isEmpty( ) )
      {
        itemSets.add( trimmedItemSet );
      }
    }

    priceTypeToName = new HashMap<Integer, String>( );
    nameToPriceType = new HashMap<String, Integer>( );
    Map<String, String> priceTypeMap = StringUtils
        .extractValuePairs( getProperty( properties, "PriceTypes", "" ), ",", "|" );
    if( !priceTypeMap.isEmpty( ) )
    {
      for( Entry<String, String> e : priceTypeMap.entrySet( ) )
      {
        Integer priceType = Integer.valueOf( e.getKey( ) );
        priceTypeToName.put( priceType, e.getValue( ) );
        nameToPriceType.put( e.getValue( ), priceType );
      }
    }

    priceTypeConversion = new HashMap<Integer,Integer>();
    Map<String, String> convertTypes = StringUtils
        .extractValuePairs( getProperty( properties, "PriceTypeConversion", "" ), ",", "|" );
    if(!convertTypes.isEmpty())
    {
      for( Entry<String, String> e : convertTypes.entrySet( ) )
      {
        Integer fromPriceType = Integer.valueOf( e.getKey( ) );
        Integer toPriceType = Integer.valueOf( e.getValue( ) );
        priceTypeConversion.put( fromPriceType, toPriceType );
      }
    }

    productGroupNames = new HashMap<String, String>( );

    Map<String, String> pgNames = StringUtils
        .extractValuePairs( getProperty( properties, "ProductGroupName", "" ), ",", "|" );

    if( !pgNames.isEmpty( ) )
    {
      for( Entry<String, String> e : pgNames.entrySet( ) )
      {
        productGroupNames.put( e.getKey( ), e.getValue( ) );

      }
    }
    
    productGroupByPathMap = new HashMap<String, String>();
    Map<String, String> pgByPath = StringUtils
        .extractValuePairs( getProperty( properties, "HierarchyPathForProductGroup", "" ), ",", "|" );
    if( !pgByPath.isEmpty( ) )
    {
      for( Entry<String, String> e : pgByPath.entrySet( ) )
      {
        productGroupByPathMap.put( e.getValue( ), e.getKey( ) );
      }
    }


    defaultPriceType = getProperty( properties, "DefaultPriceType", 0 );

    customPriceTypeCondition = getProperty( properties, "CustomPriceTypeCondition", false );

    maxNoOfRecords = getProperty( properties, "File.MaxNoOfRecords", 0 );

    defaultPriceTypes = parseDefaultPriceTypes( properties );
    
  }

  public Map<Integer, Integer> parseDefaultPriceTypes( Map<String, String> properties )
  {
    Map<Integer, Integer> dPriceTypes = new HashMap<Integer, Integer>( );
    if( isCustomPriceTypeCondition( ) )
    {
      int currentCondition = 0;

      Map<String, Integer> priceTypes = getNameToPriceType( );
      while( true )
      {
        String defaultPrefix = "DefaultPriceType." + currentCondition;
        if( properties.containsKey( defaultPrefix ) )
        {
          String defPriceType = getProperty( properties, defaultPrefix,
              "" ).trim( );
          dPriceTypes.put( currentCondition, priceTypes.get( defPriceType ) );
          ++currentCondition;
        }
        else
        {
          break;
        }
      }

    }
    return dPriceTypes;
  }

  String getProperty( Map<String, String> properties, String key,
      String defaultValue )
  {
    String value = properties.get( key );
    if( value == null )
    {
      value = defaultValue;
    }
    logInfo( key + "=" + value );
    
    String trimmed = value.trim();
    if( !value.equals( trimmed ) ) {
      logWarn( "Property value has whitespace characters: key=" + key + ", value="+value );
    }
    
    return trimmed;
  }

 
  public String getProperty( Map<String, String> properties, String key,
      String defaultValue, String appendCode )
  {
    String multiKey = key + appendCode;
    String value = properties.get( multiKey );
    if( value == null )
    {
      value = defaultValue;
    }
    logInfo( key + "=" + value );
    return value;
  }

  public boolean getProperty( Map<String, String> properties, String key,
      boolean defaultValue )
  {
    String sValue = properties.get( key );
    boolean bValue = defaultValue;
    if( sValue != null )
    {
      bValue = "1".equals( sValue ) || "true".equalsIgnoreCase( sValue );
    }
    logInfo( key + "=" + ( bValue ? "1" : "0" ) );
    return bValue;
  }

  public int getProperty( Map<String, String> properties, String key, int defaultValue )
  {
    String sValue = properties.get( key );
    int value = defaultValue;
    if( sValue != null )
    {
      try {
      value = Integer.parseInt( sValue );
      } catch (NumberFormatException e) {
        logWarn("Invalid number property value for: "+key+", value="+sValue);
        throw(e);
      }
    }
    logInfo( key + "=" + value );
    return value;
  }

  public boolean isCustomPriceTypeCondition( )
  {
    return customPriceTypeCondition;
  }

  public Map<String, Integer> getNameToPriceType( )
  {
    return nameToPriceType;
  }

  public boolean isArchiveFiles( )
  {
    return archiveFiles;
  }

  public boolean isCleanWorkDirBeforeStart( )
  {
    return cleanWorkDirBeforeStart;
  }

  public boolean isCreateBaseData( )
  {
    return createBaseData;
  }

  public int getProductGroupID( )
  {
    return productGroupID;
  }

  public String getProductGroupName( )
  {
    return productGroupName;
  }

  public String getProductLanguageID( )
  {
    return productLanguageID;
  }

  public Set<String> getItemSets( )
  {
    return itemSets;
  }

  public String getBusinessIDPrefix( )
  {
    return businessIDPrefix;
  }

  public String getParentBusinessOID( )
  {
    return parentBusinessOID;
  }

  public int getDefaultPriceType( )
  {
    return defaultPriceType;
  }

  public int getMaxNoOfRecords( )
  {
    return maxNoOfRecords;
  }

  public Map<Integer, String> getPriceTypeToName( )
  {
    return priceTypeToName;
  }

  public List<PriceEndCondition> getPriceEndConditions( )
  {
    return priceEndConditions;
  }

  public Map<Integer, Integer> getDefaultPriceTypes( )
  {
    return defaultPriceTypes;
  }

  public List<String> getProductGroupIds( )
  {
    return productGroupIds;
  }

  public void setProductGroupIds( List<String> productGroupIds )
  {
    this.productGroupIds = productGroupIds;
  }
  
  public Map<String, String> getProductGroupNames( )
  {
    return productGroupNames;
  }

  public void setProductGroupNames( Map<String, String> productGroupNames )
  {
    this.productGroupNames = productGroupNames;
  }

  public Map<String, String> getProductGroupByPathMap()
  {
    return productGroupByPathMap;
  }
  
  public int getConvertedPriceType(int fromType)
  {
    if( priceTypeConversion.containsKey(fromType) )
    {
      return priceTypeConversion.get(fromType);
    }
    return fromType;
  }
  
}
