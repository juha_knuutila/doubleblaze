package com.i2.se.custom.smartandfinal.integration;

import java.io.File;
import java.text.DateFormat;
import java.util.*;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.app.integrations.imports.TranslatorException;
import com.i2.se.app.integrations.imports.data.*;

public class DMAAdjustmentClearance extends DMAAdjustmentBase
{
  private static DMAAdjustmentClearance instance = null;

  private DMAAdjustmentClearance(DMAFileTranslator dmaFileTranslator, BackBusContainer target,
      ProductDataCache productDataCache, SAFSettings settings, File importFile, DateFormat df,
      Set<String> adjustmentNodeIds, Set<String> allPostFixes, Set<String> adjustmentIds, Date pricingStartDate)
  {
    super( dmaFileTranslator,  target, productDataCache,  settings,  importFile,  df, adjustmentNodeIds, allPostFixes,  adjustmentIds, pricingStartDate );
    pricingRuleName = settings.getPricingRuleClearance();
    adjustmentNamePattern = settings.getAdjustmentNamePatternClearance();
    adjustmentIdPattern = "CLR_%s_%s_%s";
  }

  public static DMAAdjustmentClearance getInstance(DMAFileTranslator dmaFileTranslator, BackBusContainer target,
      ProductDataCache productDataCache, SAFSettings settings, File importFile, DateFormat df,
      Set<String> adjustmentNodeIds, Set<String> allPostFixes, Set<String> adjustmentIds, Date pricingStartDate)
  {
    if (instance == null)
    {
      instance = new DMAAdjustmentClearance(dmaFileTranslator, target, productDataCache, settings, importFile, df,
          adjustmentNodeIds, allPostFixes, adjustmentIds, pricingStartDate);
    }

    return instance;
  }

  public boolean createClearance(String[] elements, int trueLineNo, String itemCode, String itemOID)
      throws TranslatorException
  {
    return createAdjustment(target, productDataCache, settings, elements, trueLineNo, itemCode, df, importFile,
        itemOID);
  }

  boolean setCustomRuleParams(String paramName, PriceAdjustmentParameter priceParam, String[] elements, int trueLineNo)
  {
    boolean paramSet = false;
    
    String clearanceParam = settings.getRuleParamName( "CLEARANCE" );
    String sfsCaseLabelParam = settings.getRuleParamName( "SFS_CASE_LABEL" );
    String promoSymbolParam = settings.getRuleParamName( "PROMO_SYMBOL" );
    
    if( clearanceParam.equalsIgnoreCase( paramName ) )
    {
      String promoSymbol = elements[DMAFileFields.PRC_PKG_PRICE];
      priceParam.setValue(promoSymbol);
      paramSet = true;
    }
    else if ( sfsCaseLabelParam.equalsIgnoreCase( paramName ) )
    {
      String caseLabel = elements[DMAFileFields.SFS_CASE_LABEL];
      priceParam.setValue( caseLabel );
      paramSet = true; 
    }
    else if ( promoSymbolParam.equalsIgnoreCase( paramName ) )
    {
      String promoSymbol = elements[DMAFileFields.PROMO_SYMBOL];
      
      String symbolValue = settings.getPromoSymbolValue(promoSymbol);
      if( StringUtils.isEmpty(symbolValue) ) {
        converter.logWarn("Invalid promo symbol value "+promoSymbol+ " at line="+trueLineNo);
      }
      priceParam.setValue(symbolValue);
      paramSet = true;
    }
    
    return paramSet;
  }

}
