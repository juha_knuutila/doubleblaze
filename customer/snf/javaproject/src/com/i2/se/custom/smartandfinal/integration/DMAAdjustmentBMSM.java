package com.i2.se.custom.smartandfinal.integration;

import java.io.File;
import java.text.DateFormat;
import java.util.*;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.app.integrations.imports.TranslatorException;
import com.i2.se.app.integrations.imports.data.*;

public class DMAAdjustmentBMSM extends DMAAdjustmentBase
{
  private static DMAAdjustmentBMSM instance = null;

  DMAAdjustmentBMSM(DMAFileTranslator dmaFileTranslator, BackBusContainer target,
      ProductDataCache productDataCache, SAFSettings settings, File importFile, DateFormat df,
      Set<String> adjustmentNodeIds, Set<String> allPostFixes, Set<String> adjustmentIds, Date pricingStartDate)
  {
    super( dmaFileTranslator,  target, productDataCache,  settings,  importFile,  df, adjustmentNodeIds, allPostFixes,  adjustmentIds, pricingStartDate );
    pricingRuleName = settings.getPricingRuleBMSM();
    adjustmentNamePattern = settings.getAdjustmentNamePatternBMSM();
    adjustmentIdPattern = "BMSM_%s_%s";
  }

  public static DMAAdjustmentBMSM getInstance(DMAFileTranslator dmaFileTranslator, BackBusContainer target,
      ProductDataCache productDataCache, SAFSettings settings, File importFile, DateFormat df,
      Set<String> adjustmentNodeIds, Set<String> allPostFixes, Set<String> adjustmentIds, Date pricingStartDate)
  {
    if (instance == null)
    {
      instance = new DMAAdjustmentBMSM(dmaFileTranslator, target, productDataCache, settings, importFile, df,
          adjustmentNodeIds, allPostFixes, adjustmentIds, pricingStartDate);
    }
    return instance;
  }

  public boolean createBMSM(String[] elements, int trueLineNo, String itemCode, String itemOID)
      throws TranslatorException
  {
    return createAdjustment(target, productDataCache, settings, elements, trueLineNo, itemCode, df, importFile,
        itemOID);
  }

  boolean setCustomRuleParams(String paramName, PriceAdjustmentParameter priceParam, String[] elements, int trueLineNo)
  {
    boolean paramSet = false;

    String sfsCaseLabel = settings.getRuleParamName("SFS_CASE_LABEL");
    String promoSymbolParam = settings.getRuleParamName("PROMO_SYMBOL");
    String mixnmatchParam = settings.getRuleParamName("MIX_N_MATCH");
    String promoMultiQtyPriceParam = settings.getRuleParamName("PromoMultiQtyPrice");
    String promoQtyParam = settings.getRuleParamName("PromoQty");

    if (promoMultiQtyPriceParam.equalsIgnoreCase(paramName))
    {
      String pkgPrice = elements[DMAFileFields.PRC_PKG_PRICE];
      priceParam.setValue(pkgPrice);
      paramSet = true;
    } else if (promoQtyParam.equalsIgnoreCase(paramName))
    {
      String pkgPrice = elements[DMAFileFields.PRC_PKG_MULT];
      priceParam.setValue(pkgPrice);
      paramSet = true;
    } else if (sfsCaseLabel.equalsIgnoreCase(paramName))
    {
      String caseLabel = elements[DMAFileFields.SFS_CASE_LABEL];
      priceParam.setValue(caseLabel);
      paramSet = true;
    } else if (promoSymbolParam.equalsIgnoreCase(paramName))
    {
      String promoSymbol = elements[DMAFileFields.PROMO_SYMBOL];

      String symbolValue = settings.getPromoSymbolValue(promoSymbol);
      if (StringUtils.isEmpty(symbolValue))
      {
        converter.logWarn("Invalid promo symbol value " + promoSymbol + " at line=" + trueLineNo);
      }
      priceParam.setValue(symbolValue);
      paramSet = true;
    } else if (mixnmatchParam.equalsIgnoreCase(paramName))
    {
      String mixNMatch = elements[DMAFileFields.PRC_EFF_MIX_MATCH];
      if ("-1".equals(mixNMatch))
      {
        mixNMatch = "";
      }
      priceParam.setValue(mixNMatch);
      paramSet = true;
    }
   
    return paramSet;
  }

}
