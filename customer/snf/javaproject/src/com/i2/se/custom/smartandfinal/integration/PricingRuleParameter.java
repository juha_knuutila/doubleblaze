package com.i2.se.custom.smartandfinal.integration;

public class PricingRuleParameter
{
  private String name;
  private String defaultValue;
  private String defaultCurrency;

  public PricingRuleParameter( String paramName, String defaultValue, String defaultCurrency )
  {
    this.name = paramName;
    this.defaultValue = defaultValue;
    this.defaultCurrency = defaultCurrency;
  }

  public String getName( )
  {
    return name;
  }

  public String getDefaultValue( )
  {
    return defaultValue;
  }

  public String getDefaultCurrency( )
  {
    return defaultCurrency;
  }

}
