package com.i2.se.custom.smartandfinal.integration;

import java.io.File;
import java.text.DateFormat;
import java.util.*;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.app.integrations.imports.TranslatorException;
import com.i2.se.app.integrations.imports.data.*;

public class DMAAdjustmentRegularRetailMB extends DMAAdjustmentBase
{
  private static DMAAdjustmentRegularRetailMB instance = null;

  private DMAAdjustmentRegularRetailMB(DMAFileTranslator dmaFileTranslator, BackBusContainer target,
      ProductDataCache productDataCache, SAFSettings settings, File importFile, DateFormat df,
      Set<String> adjustmentNodeIds, Set<String> allPostFixes, Set<String> adjustmentIds, Date pricingStartDate)
  {
    super( dmaFileTranslator,  target, productDataCache,  settings,  importFile,  df, adjustmentNodeIds, allPostFixes,  adjustmentIds, pricingStartDate );
    pricingRuleName = settings.getPricingRuleRegularRetailMB();
    adjustmentNamePattern = settings.getAdjustmentNamePatternRegularRetailMB();
    adjustmentIdPattern = "RRMB_%s_%s";
  }

  public static DMAAdjustmentRegularRetailMB getInstance(DMAFileTranslator dmaFileTranslator, BackBusContainer target,
      ProductDataCache productDataCache, SAFSettings settings, File importFile, DateFormat df,
      Set<String> adjustmentNodeIds, Set<String> allPostFixes, Set<String> adjustmentIds, Date pricingStartDate)
  {
    if (instance == null)
    {
      instance = new DMAAdjustmentRegularRetailMB(dmaFileTranslator, target, productDataCache, settings, importFile, df,
          adjustmentNodeIds, allPostFixes, adjustmentIds, pricingStartDate);
    }

    return instance;
  }

  public boolean createRegularRetailMB(String[] elements, int trueLineNo, String itemCode, String itemOID)
      throws TranslatorException
  {
    return createAdjustment(target, productDataCache, settings, elements, trueLineNo, itemCode, df, importFile,
        itemOID);
  }

  boolean setCustomRuleParams(String paramName, PriceAdjustmentParameter priceParam, String[] elements, int trueLineNo)
  {
    boolean paramSet = false;
    
    String sfsCaseLabelParam = settings.getRuleParamName( "SFS_CASE_LABEL" );
    String promoSymbolParam = settings.getRuleParamName( "PROMO_SYMBOL" );
    String regMultParam = settings.getRuleParamName( "PRC_REG_MULT_1" );
    String priceTotalParam = settings.getRuleParamName( "PRC_REG_PRICE_TOTAL_1" );


    if( sfsCaseLabelParam.equalsIgnoreCase( paramName ) )
    {
      String caseLabel = elements[DMAFileFields.SFS_CASE_LABEL];
      priceParam.setValue( caseLabel );
      paramSet = true;
    }
    else if ( promoSymbolParam.equalsIgnoreCase( paramName ) )
    {
      String promoSymbol = elements[DMAFileFields.PROMO_SYMBOL];
      
      String symbolValue = settings.getPromoSymbolValue(promoSymbol);
      if( StringUtils.isEmpty(symbolValue) ) {
        converter.logWarn("Invalid promo symbol value "+promoSymbol+ " at line="+trueLineNo);
      }
      priceParam.setValue(symbolValue);
      paramSet = true;
    }
    else if ( regMultParam.equalsIgnoreCase( paramName ) )
    {
      String promoSymbol = elements[DMAFileFields.PRC_REG_MULT];
      priceParam.setValue(promoSymbol);
      paramSet = true;
    }
    else if ( priceTotalParam.equalsIgnoreCase( paramName ) )
    {
      String promoSymbol = elements[DMAFileFields.PRC_REG_PRICE];
      priceParam.setValue(promoSymbol);
      paramSet = true;
    }
    
    return paramSet;
  }

}
