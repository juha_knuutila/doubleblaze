package com.i2.se.custom.smartandfinal.integration;

import java.io.*;

import com.i2.se.app.integrations.imports.*;
import com.i2.se.app.integrations.imports.data.BackBusContainer;
import com.i2.se.bd.util.bo.session.TSession;

interface SAFTranslator extends GroceryStoreFileTranslator {
	
	 public void convert( TSession session, File importFile, BackBusContainer target, SAFSettings settings,
		      ProductDataCache productDataCache, FileOutputStream logFile, PricerImport parent ) throws TranslatorException,
		      IOException;

}