package com.i2.se.custom.smartandfinal.integration;

import java.util.*;

public class AdGroupComparison
{
  Set<AdGroup> newGroups;
  Set<AdGroup> deletedGroups;
  Set<AdGroup> modifiedGroups;
  
  
  public AdGroupComparison()
  {
    super();
    this.newGroups = new HashSet<AdGroup>();
    this.deletedGroups = new HashSet<AdGroup>();
    this.modifiedGroups = new HashSet<AdGroup>();
  }
  public Set<AdGroup> getNewGroups()
  {
    return newGroups;
  }
  public Set<AdGroup> getDeletedGroups()
  {
    return deletedGroups;
  }
  public Set<AdGroup> getModifiedGroups()
  {
    return modifiedGroups;
  }
  @Override
  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    
    for( AdGroup g : newGroups) {
      sb.append("new group "+g+ "\n");
      
    }
    for( AdGroup g : deletedGroups) {
      sb.append("deleted group "+g+ "\n");
      
    }
    for( AdGroup g : modifiedGroups) {
      sb.append("modified group "+g+ "\n");
      
    }
    
    return "GroupComparison [newGroups=" + newGroups + ", deletedGroups=" + deletedGroups + ", modifiedGroups="
        + modifiedGroups + "]\n"+sb.toString();
  }
   
  
}
