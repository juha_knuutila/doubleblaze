package com.i2.se.custom.smartandfinal.integration;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.i2.se.app.integrations.imports.data.*;
import com.i2.se.bd.catalog.TBaseAttribute;
import com.i2.se.bd.catalog.TClass;

/**
 * Base data creator for Grocery Adapter.
 * 
 * Creates common item attributes , product group and price types
 * 
 * @author J1013162
 *
 */
public class BaseDataCreator
{
  public void createBaseData( BackBusContainer target, ImportSettings settings )
  {

    List<String> productGroupIds = settings.getProductGroupIds( );
    
    Map<String,String> productGroupNames = settings.getProductGroupNames( );

    for( String productGroupId : productGroupIds )
    {
      ProductGroup productGroup = new ProductGroup( );

      int pgId = Integer.parseInt( productGroupId );
      productGroup.setProductGroupID( pgId );

      String pgName = productGroupNames.get( productGroupId );
      productGroup.setProductGroupName( pgName );
      target.addProductGroup( productGroup );

    }

    String[] characteristics = settings.getCharacteristics( );

    if( characteristics != null )
    {
      for( String characteristic : characteristics )
      {

        Characteristic newCharacteristic = new Characteristic( );
        newCharacteristic.setCharacteristicName( characteristic );
        newCharacteristic.setDataLength( "100" );
        newCharacteristic.setDataType( "CH" );
        newCharacteristic.setReqFlag( "false" );
        target.addCharacteristic( newCharacteristic );
      }

    }


    // UPC
    Attribute UPCattribute = new Attribute( );
    UPCattribute.setAttributeID( settings.getUpcAttribute( ) );
    UPCattribute.setAttributeType( TBaseAttribute.TEXT );
    target.addAttribute( UPCattribute );

    // SF_POS_Dep
    Attribute POSAttribute = new Attribute( );
    POSAttribute.setAttributeID( settings.getPosAttribute( ) );
    POSAttribute.setAttributeType( TBaseAttribute.INTEGER );
    target.addAttribute( POSAttribute );

    // Size
    Attribute SizeAttribute = new Attribute( );
    SizeAttribute.setAttributeID( settings.getSizeAttribute( ) );
    SizeAttribute.setAttributeType( TBaseAttribute.TEXT );
    target.addAttribute( SizeAttribute );

    // UOM
    Attribute uomAttribute = new Attribute( );
    uomAttribute.setAttributeID( settings.getUomAttribute( ) );
    uomAttribute.setAttributeType( TBaseAttribute.TEXT );
    target.addAttribute( uomAttribute );

    // UDC Size
    Attribute UDCSizeAttribute = new Attribute( );
    UDCSizeAttribute.setAttributeID( settings.getUdcSizeAttribute( ) );
    UDCSizeAttribute.setAttributeType( TBaseAttribute.TEXT );
    target.addAttribute( UDCSizeAttribute );

    // Updated on date
    Attribute UpdatedOnAttribute = new Attribute( );
    UpdatedOnAttribute.setAttributeID( settings.getUpdatedOnAttribute( ) );
    UpdatedOnAttribute.setAttributeType( TBaseAttribute.DATETIME );
    UpdatedOnAttribute.setMultiValueAttributeFlag( 0 );
    target.addAttribute( UpdatedOnAttribute );

    // FAMILY
    Attribute familyCodeAttribute = new Attribute( );
    familyCodeAttribute.setAttributeID( settings.getFamilyCodeAttribute( ) );
    familyCodeAttribute.setAttributeType( TBaseAttribute.INTEGER );
    target.addAttribute( familyCodeAttribute );

    // UPC_Unit
    Attribute upcUnitAttribute = new Attribute( );
    upcUnitAttribute.setAttributeID( settings.getUpcUnitAttribute( ) );
    upcUnitAttribute.setAttributeType( TBaseAttribute.TEXT );
    target.addAttribute( upcUnitAttribute );

    // Domain Values U and C
    AttributeDomainValue upcUnitDomainValues = new AttributeDomainValue( );
    upcUnitDomainValues.setAttributeID( upcUnitAttribute.getAttributeID( ) );
    upcUnitDomainValues.setProductLanguageID( settings.getProductLanguageID( ) );
    upcUnitDomainValues.setIndex( 0 );
    upcUnitDomainValues.setValue( "U" );
    upcUnitDomainValues.setVisibleValue( "U" );
    target.addAttributeDomainValue( upcUnitDomainValues );
    AttributeDomainValue upcUnitDomainValues1 = new AttributeDomainValue( );
    upcUnitDomainValues1.setAttributeID( upcUnitAttribute.getAttributeID( ) );
    upcUnitDomainValues1.setProductLanguageID( settings.getProductLanguageID( ) );
    upcUnitDomainValues1.setIndex( 1 );
    upcUnitDomainValues1.setValue( "C" );
    upcUnitDomainValues1.setVisibleValue( "C" );
    target.addAttributeDomainValue( upcUnitDomainValues1 );

    // Price Item Attributes -Start

    Attribute vendorNumber = new Attribute( );
    vendorNumber.setAttributeID( settings.getSupplyVendorNumber( ) );
    vendorNumber.setAttributeType( TBaseAttribute.INTEGER );
    target.addAttribute( vendorNumber );

    // Primary vendor number
    Attribute primaryVendorNumber = new Attribute( );
    primaryVendorNumber.setAttributeID( settings.getPrimaryVendorNumber( ) );
    primaryVendorNumber.setAttributeType( TBaseAttribute.INTEGER );
    target.addAttribute( primaryVendorNumber );

    // NBR Items in case
    Attribute nbrItemsInCase = new Attribute( );
    nbrItemsInCase.setAttributeID( settings.getNbrItemsInCase( ) );
    nbrItemsInCase.setAttributeType( TBaseAttribute.INTEGER );
    target.addAttribute( nbrItemsInCase );

    // Weight
    Attribute itemWeight = new Attribute( );
    itemWeight.setAttributeID( settings.getWeight( ) );
    itemWeight.setAttributeType( TBaseAttribute.INTEGER );
    target.addAttribute( itemWeight );

    // Scale Indicator
    Attribute scaleIdicator = new Attribute( );
    scaleIdicator.setAttributeID( settings.getScaleIndicator( ) );
    scaleIdicator.setAttributeType( TBaseAttribute.BOOLEAN );
    target.addAttribute( scaleIdicator );

    // Cost Source
    Attribute costSource = new Attribute( );
    costSource.setAttributeID( settings.getCostSource( ) );
    costSource.setAttributeType( TBaseAttribute.TEXT );
    target.addAttribute( costSource );

    // Pricing Zone
    Attribute pricingZone = new Attribute( );
    pricingZone.setAttributeID( settings.getPricingZone( ) );
    pricingZone.setAttributeType( TBaseAttribute.INTEGER );
    target.addAttribute( pricingZone );

    // Package Size
    Attribute packageSizeAttribute = new Attribute( );
    packageSizeAttribute.setAttributeID( settings.getPackageSizeAttribute( ) );
    packageSizeAttribute.setAttributeType( TBaseAttribute.TEXT );
    target.addAttribute( packageSizeAttribute );

    Map<String, String> classNames = settings.getClassNamesPerCompany( );
    
    for( String className : classNames.values( ) )
    {
      //  class and attributes
      ProductClass pClass = new ProductClass( );
      pClass.setClassName( className );
      pClass.setHierarchyLevel( TClass.MODULE );
      pClass.setParentClassName( "" );
      target.addClass( pClass );

      // UPC
      ClassAttribute classAttribute = new ClassAttribute( );
      classAttribute.setClassName( className );
      classAttribute.setAttributeID( UPCattribute.getAttributeID( ) );
      target.addClassAttribute( classAttribute );

      // SF_POS_Dep
      ClassAttribute posClassAttribute = new ClassAttribute( );
      posClassAttribute.setClassName( className );
      posClassAttribute.setAttributeID( POSAttribute.getAttributeID( ) );
      target.addClassAttribute( posClassAttribute );

      // Size
      ClassAttribute sizeClassAttribute = new ClassAttribute( );
      sizeClassAttribute.setClassName( className );
      sizeClassAttribute.setAttributeID( SizeAttribute.getAttributeID( ) );
      target.addClassAttribute( sizeClassAttribute );

      // UOM
      ClassAttribute uomClassAttribute = new ClassAttribute( );
      uomClassAttribute.setClassName( className );
      uomClassAttribute.setAttributeID( uomAttribute.getAttributeID( ) );
      target.addClassAttribute( uomClassAttribute );

      // UDC Size
      ClassAttribute udcSizeClassAttribute = new ClassAttribute( );
      udcSizeClassAttribute.setClassName( className );
      udcSizeClassAttribute.setAttributeID( UDCSizeAttribute.getAttributeID( ) );
      target.addClassAttribute( udcSizeClassAttribute );

      // Updated on
      ClassAttribute updatedOnClassAttribute = new ClassAttribute( );
      updatedOnClassAttribute.setClassName( className );
      updatedOnClassAttribute.setAttributeID( UpdatedOnAttribute.getAttributeID( ) );
      target.addClassAttribute( updatedOnClassAttribute );

      // FAMILIY
      ClassAttribute familyClassAttribute = new ClassAttribute( );
      familyClassAttribute.setClassName( className );
      familyClassAttribute.setAttributeID( familyCodeAttribute.getAttributeID( ) );
      target.addClassAttribute( familyClassAttribute );

      // UPC_Unit
      ClassAttribute upcUnitClassAttribute = new ClassAttribute( );
      upcUnitClassAttribute.setClassName( className );
      upcUnitClassAttribute.setAttributeID( upcUnitAttribute.getAttributeID( ) );
      target.addClassAttribute( upcUnitClassAttribute );

      // Package Size
      ClassAttribute packageSizeClassAttribute = new ClassAttribute( );
      packageSizeClassAttribute.setClassName( className );
      packageSizeClassAttribute.setAttributeID( packageSizeAttribute.getAttributeID( ) );
      target.addClassAttribute( packageSizeClassAttribute );

      // Item Supply Vendor
      ClassAttribute itemVendorNumberAttribute = new ClassAttribute( );
      itemVendorNumberAttribute.setClassName( className );
      itemVendorNumberAttribute.setAttributeID( vendorNumber.getAttributeID( ) );
      target.addClassAttribute( itemVendorNumberAttribute );

      // Item Primary Vendor
      ClassAttribute itemPVendorNumberAttribute = new ClassAttribute( );
      itemPVendorNumberAttribute.setClassName( className );
      itemPVendorNumberAttribute.setAttributeID( primaryVendorNumber.getAttributeID( ) );
      target.addClassAttribute( itemPVendorNumberAttribute );

      // NBR items in case
      ClassAttribute nbrItemsInCaseAttribute = new ClassAttribute( );
      nbrItemsInCaseAttribute.setClassName( className );
      nbrItemsInCaseAttribute.setAttributeID( nbrItemsInCase.getAttributeID( ) );
      target.addClassAttribute( nbrItemsInCaseAttribute );

      // Weight
      ClassAttribute itemWeightAttribute = new ClassAttribute( );
      itemWeightAttribute.setClassName( className );
      itemWeightAttribute.setAttributeID( itemWeight.getAttributeID( ) );
      target.addClassAttribute( itemWeightAttribute );

      // Scale Indicator
      ClassAttribute itemScaleIndicatorAttribute = new ClassAttribute( );
      itemScaleIndicatorAttribute.setClassName( className );
      itemScaleIndicatorAttribute.setAttributeID( scaleIdicator.getAttributeID( ) );
      target.addClassAttribute( itemScaleIndicatorAttribute );

      // Cost Source
      ClassAttribute costSourceAttribute = new ClassAttribute( );
      costSourceAttribute.setClassName( className );
      costSourceAttribute.setAttributeID( costSource.getAttributeID( ) );
      target.addClassAttribute( costSourceAttribute );

      // Pricing Zone
      ClassAttribute pricingZoneAttribute = new ClassAttribute( );
      pricingZoneAttribute.setClassName( className );
      pricingZoneAttribute.setAttributeID( pricingZone.getAttributeID( ) );
      target.addClassAttribute( pricingZoneAttribute );
    }


    for( Entry<Integer, String> priceTypeEntry : settings.getPriceTypeToName( ).entrySet( ) )
    {
      PriceType priceType = new PriceType( );
      priceType.setPriceTypeID( priceTypeEntry.getKey( ) );
      priceType.setPriceTypeName( priceTypeEntry.getValue( ) );
      priceType.setPriceTypeAdjustment( true );
      priceType.setPriceTypePriceGroup( true );
      target.addPriceType( priceType );
    }
  }

}
