package com.i2.se.custom.smartandfinal.integration;

import java.io.*;
import java.util.*;

import com.i2.se.app.integrations.imports.TranslatorException;
import com.i2.se.app.integrations.imports.data.*;
import com.i2.se.bd.catalog.THierarchy;
import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;

public class SubCategoriesHierarchyFileTranslator implements GroceryStoreFileTranslator
{
  private Set<String> productHierarchyIDs = new HashSet<String>( );
  public SubCategoriesHierarchyFileTranslator( )
  {
  }

  public void convert( File importFile, BackBusContainer target, ImportSettings settings,
      ProductDataCache productDataCache ) throws TranslatorException,
      IOException
  {
    BufferedReader bf = null;
    int trueLineNo = 0;
    try
    {
      bf = new BufferedReader(
          new InputStreamReader( new FileInputStream( importFile ) ) );
      while( true )
      {
        String s = bf.readLine( );
        if( s == null )
        {
          break;
        }
        ++trueLineNo;
        String[] elements = s.split( "\\|", -1 );
        int i = 0;
        String companyNumber = elements[ i++ ];
        String categoryCode = elements[ i++ ];
        String subCategoryCode = elements[ i++ ];
        String subcategory = elements[ i++ ];


        String parentHierarchyID = Hierarchy.PRODUCT_HIERARCHY_ROOT;
        StringBuilder hierarchyID = new StringBuilder( );
        hierarchyID.append( "P" ).append( companyNumber );

        parentHierarchyID = hierarchyID.toString( );
        hierarchyID.append( "-" ).append( categoryCode );

        parentHierarchyID = hierarchyID.toString( );
        hierarchyID.append( "-" ).append( subCategoryCode );
        addHierarchyIfMissing( hierarchyID.toString( ), subcategory, parentHierarchyID,
                target );

      }

      ObjectFactory.getLogManager( ).getStaticLogger( ).addLogEntry(
          this.getClass( ), StaticLogger.INFO, 0,
          "Read " + trueLineNo + " lines" );
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
      throw new TranslatorException(
          "Error reading file, line " + trueLineNo + ": " + e.getMessage( ), e );
    }
    catch( IOException e )
    {
      throw new TranslatorException(
          "IOException reading file, line " + trueLineNo + ": " + e.getMessage( ), e );
    }
    finally
    {
      if( bf != null )
      {
        bf.close( );
      }
    }
  }

  private void addHierarchyIfMissing( String hierarchyID, String hierarchyName,
      String parentHierarchyID, BackBusContainer target )
  {
    if( !productHierarchyIDs.contains( hierarchyID ) )
    {
      productHierarchyIDs.add( hierarchyID );

      Hierarchy h = new Hierarchy( );
      h.setHierarchyID( hierarchyID );
      h.setHierarchyName( hierarchyName );
      h.setHierarchyType( THierarchy.PRODUCT );
      h.setParentHierarchyID( parentHierarchyID );
      target.addHierarchy( h );
    }
  }

}
