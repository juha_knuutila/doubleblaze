package com.i2.se.custom.smartandfinal.integration;

import java.io.*;
import java.util.*;
import java.util.function.Function;
import java.util.regex.*;
import java.util.stream.Collectors;

import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;

public class AdGroups
{
  private Map<String, AdGroup> groupMap = null;

  /**
   * Initialize group map
   * 
   * @param settings
   */
  public AdGroups(SAFSettings settings)
  {
    String file = settings.getCurrentAdGroupsFileName();

    List<AdGroup> groups = processInputFile(file);
    groupMap = new HashMap<String, AdGroup>();
    for (AdGroup g : groups)
    {
      if (!g.isTestGroup())
      {
        groupMap.put(g.getId(), g);
      }
    }
  }

  /** Returns the priority for AdGroup from the AdGroups file
   * 
   * @param id
   * @return groups priority, calculated 10000-group id. If not found then returns -1
   */
  public int getAdGroupPriority(String id)
  {
    AdGroup group = groupMap.get(id);
    logDebug("Prioroty for group: "+id+" = "+ group);
    if ( group == null ) {
      return -1;
    }
    return 10000 - group.getPriority();
  }
  
  private static final int IDX_GROUP_ID = 0;
  private static final int IDX_GROUP_NAME = 1;
  private static final int IDX_GROUP_PRIORITY = 2;
  private static int counter=0;
  
  private Function<String, AdGroup> mapToItem = (line) ->
  {
    counter++;
    // String[] p = line.split(COMMA);// a CSV has comma separated lines
    if (line.startsWith("#"))
    {
      return AdGroup.getTestGroup();
    }

    try {
      
      String regEx = "\\s*(\\d*)\\s*,\\s*((?:\\\".*\\\")|[^,]*)\\s*,\\s*(\\d*)\\s*(?:,\\s*([^,]+)\\s*)?(?:,\\s*([^,]+)\\s*)?";
      Pattern p = Pattern.compile(regEx);
      Matcher matcher = p.matcher(line);
      int groupCount = matcher.groupCount();
      if( !matcher.matches() || groupCount < 3 || groupCount > 5 ) {
        logWarn("Not accepted line: " + counter + ", line: " + line);
        return AdGroup.getTestGroup();
      }
      
      String id = matcher.group(IDX_GROUP_ID+1);
      String name = matcher.group(IDX_GROUP_NAME+1);
      int priority = Integer.parseInt(matcher.group(IDX_GROUP_PRIORITY+1));

    AdGroup item = new AdGroup(id, name, priority);
    logDebug("Group:"+item);
    return item;
    } catch (Exception e ) {
      logWarn("Exception at LINE:"+line);
      logWarn("Exception at "+counter+", "+e);
      throw e;
    }
  };

  private List<AdGroup> processInputFile(String inputFilePath)
  {

    List<AdGroup> inputList = new ArrayList<AdGroup>();
    try
    {
      File f = new File(inputFilePath);
      if ( f.exists() )
      {
        InputStream is = new FileInputStream(inputFilePath);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        inputList = br.lines().map(mapToItem).collect(Collectors.toList());
        br.close();
      } else
      {
        logError("AdGroup priority file not found: " + inputFilePath);
        System.exit(0);
      }

    } catch (IOException e)
    {
      logError("Error reading AdGroup priority file: " + e);
      System.exit(0);
    }
    return inputList;

  }

  private void logError(String message)
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(), StaticLogger.ERROR, 0, message);
  }

  private void logWarn(String message)
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(), StaticLogger.WARN, 0, message);
  }

  private void logDebug(String message)
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(), StaticLogger.DEBUG, 0, message);
  }

}
