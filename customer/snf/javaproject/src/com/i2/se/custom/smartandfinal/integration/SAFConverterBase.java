package com.i2.se.custom.smartandfinal.integration;

import java.io.*;
import java.math.BigDecimal;
import java.text.*;
import java.util.*;
import java.util.regex.*;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.app.integrations.imports.*;
import com.i2.se.app.integrations.imports.data.*;
import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.pricer.*;
import com.i2.se.bd.util.bo.session.*;
import com.i2.se.uic.tags.util.ListSelection;

public abstract class SAFConverterBase implements SAFTranslator
{
  FileOutputStream logFile=null;
  Map<String, String> adGroupNameMap = new HashMap<String, String>();
  PricerImport parent=null;
  
  protected void logError( String message ) {
    log( message, StaticLogger.ERROR );
  }
  
  protected void logWarn( String message )
  {
    log( message, StaticLogger.WARN );
  }
  
  protected void logDebug(String message)
  {
    log( message, StaticLogger.DEBUG );
  }

  private void log(String message, int level)
  {
    ObjectFactory.getLogManager( ).getStaticLogger( ).addLogEntry(
        this.getClass( ), level, 0, message );
    if( level >= 3 ) {
      writeToLogFile(message);
    }
  }
  
  private void writeToLogFile(String message)
  {
    if(logFile!=null) {
      try
      {
        logFile.write(message.getBytes());
        logFile.write('\r');
        logFile.write('\n');
      } catch (IOException e)
      {
        e.printStackTrace();
      }
    }
  }
  
  protected void logInfo(String message)
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(), StaticLogger.INFO, 0, message);
  }
  
  @Override
  public void convert(TSession session, File importFile, BackBusContainer target, SAFSettings settings,
      ProductDataCache productDataCache, FileOutputStream logFile, PricerImport parent) throws TranslatorException, IOException
  {
    this.logFile = logFile;
    this.parent = parent;
    
    try {
    
      writeToLogFile("Convert started at:   " + Calendar.getInstance().getTime());
      int lines = doConvert(session, importFile, target, settings, productDataCache);
      writeToLogFile("Processed line count: " + lines);
      writeToLogFile("Convert ended at:     " + Calendar.getInstance().getTime());

    } catch (TranslatorException| IOException e) {
      logError("Exception " + e);
      writeToLogFile("ABORT at :     " + Calendar.getInstance().getTime());
      writeToLogFile("Due      :     " + e+"/"+e.getCause());
      throw e;
    }
  }
  
  abstract String getAdapterName();

  /** 
   * 
   * @param session
   * @param importFile
   * @param target
   * @param settings
   * @param productDataCache
   * @return how many datalines were processed
   * @throws TranslatorException
   * @throws IOException
   */
  abstract int doConvert(TSession session, File importFile, BackBusContainer target, SAFSettings settings,
      ProductDataCache productDataCache) throws TranslatorException, IOException;
 
  @Override
  public void convert(File arg0, BackBusContainer arg1, ImportSettings arg2, ProductDataCache arg3)
      throws TranslatorException, IOException
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(), StaticLogger.ERROR, 0,
        "SHOULD NOT HAPPEN!");
    System.exit(1);
  }
  
  protected void createPriceList(BackBusContainer target, String locationHierarchyID, String storeName,
      String priceGroupId, SAFSettings settings, Date versionDate)
  {
    String priceGroupNameFormat = settings.getPriceGroupNameFormat();
    PriceList priceList = new PriceList( );
    priceList.setPriceListID( priceGroupId );
    String name = getPriceListName(storeName, priceGroupId, priceGroupNameFormat);
    priceList.setPriceListName( name );
    priceList.setLocationHierarchyID( locationHierarchyID );
    priceList.setCurrencyID( "USD" );
    target.addPriceList( priceList );

    PriceListVersion version = new PriceListVersion( );
    version.setPriceListID( priceGroupId );
    version.setLocationHierarchyID( locationHierarchyID );
    version.setPriceListVersionDate( versionDate );
    target.addPriceListVersion( version );
  }

  private String getPriceListName(String storeName, String groupId, String priceGroupNameFormat)
  {
    String name = groupId;
    if( StringUtils.isNotEmpty(priceGroupNameFormat)) {
      name = MessageFormat.format(priceGroupNameFormat, groupId, storeName);
    }
    return name;
  }

  protected ItemProposedPrice getProposedPriceItem(String priceGroupId, Date pricingDate, String locationHierarchyID,
      int pgGroup, String skuCode, int priceType, BigDecimal priceValue, int approvalState)
  {
    ItemProposedPrice price2 = new ItemProposedPrice();
    price2.setProductGroupID(pgGroup);
    price2.setItemName(skuCode);
    price2.setItemLevel(0);
    price2.setPriceListID(priceGroupId);
    price2.setLocationHierarchyID(locationHierarchyID);
    price2.setPriceListVersionDate(pricingDate);
    price2.setPrice(priceValue);
    price2.setPriceType(priceType);
    price2.setState(approvalState);
    return price2;
  }

  protected ItemPrice getPublishedPriceItem(String priceGroupId, Date pricingDate, String locationHierarchyID, int pgGroup,
      String skuCode, int priceType, BigDecimal priceValue)
  {
    ItemPrice price = new ItemPrice();
    price.setProductGroupID(pgGroup);
    price.setItemName(skuCode);
    price.setItemLevel(0);
    price.setPriceListID(priceGroupId);
    price.setLocationHierarchyID(locationHierarchyID);
    price.setPriceListVersionDate(pricingDate);
    price.setPrice(priceValue);
    price.setPriceType(priceType);
    return price;
  }

  /** Resolves pricing date from given filename
   * 
   * @param fileNamePattern, expects to find three groups in pattern, year, month, day.
   * @param filename
   * @return Date which represents pricing day
   */
  protected Date resolveFileDate(String fileNamePattern, String filename)
  {
    Pattern p = Pattern.compile(fileNamePattern);
    logInfo("FileName Pattern: "+fileNamePattern+", filename: "+filename);
    Matcher m = p.matcher(filename);
    if( !m.matches() ) {
      logError("Invalid filename for prices, cannot determinen pricing date : "+filename);
    }
    
    int year, month, day;
    year = Integer.parseInt(m.group(1));
    month = Integer.parseInt(m.group(2))-1;
    day = Integer.parseInt(m.group(3));
    Calendar c = Calendar.getInstance();
    c.set(year, month, day);
    logInfo("Date:"+c.getTime());
    return c.getTime();
  }

  /**
   * Converts given string into BigDecimal
   * 
   * @param elements[field]
   * @param trueLineNo
   * @param field
   * @return
   */
  protected BigDecimal getBigDecimal( String[] elements, int trueLineNo, int field)
  {
    return getBigDecimal(elements, trueLineNo, field, BigDecimal.ZERO);
  }
  protected BigDecimal getBigDecimal( String[] elements, int trueLineNo, int field, BigDecimal emptyValue )
  {
    try
    {
      logInfo("BIGD:"+elements[field]+" at pos="+field);
      if( StringUtils.isEmpty( elements[field] ) ) {
        return emptyValue;
      }
      if(  elements[field].indexOf(",")>=0) {
        logWarn("Invalid decimal separator, at line "+trueLineNo);
        elements[field] = elements[field].replace(",",  ".");
      }
      return new BigDecimal( elements[field] );
    }
    catch( NumberFormatException ne )
    {
      ObjectFactory.getLogManager( ).getStaticLogger( ).addLogEntry(
          this.getClass( ), StaticLogger.WARN, 0,
          "line " + trueLineNo + ", field="+field+", error reading price: " + elements[field] );
      return null;
    }
  }
  
  protected Date parseDate(DateFormat df, int trueLineNo, File importFile, String startDate)
      throws TranslatorException
  {
    Date parsedDate = null;
    if( !startDate.isEmpty( ) )
    {
      try
      {
        parsedDate = df.parse( startDate );
      }
      catch( ParseException e )
      {
        throw new TranslatorException(
            importFile.getName( ) + ": Invalid date format \"" + startDate + "\", line"
                + trueLineNo + ": " + e.getMessage( ), e );
      }
    }
    return parsedDate;
  }

  protected void addHierarchyNode(BackBusContainer target, Set<String> nodeIds, String adjId, String nodeId, String nodeExtId, int hierarchyType, String rootId)
  {
    if (!nodeIds.contains(nodeId))
    {
      PriceAdjustmentNode node = new PriceAdjustmentNode();
      node.setPriceAdjustmentID(adjId);
      node.setHierarchyType(hierarchyType);
      node.setHierarchyID(rootId);
      node.setPriceAdjustmentNodeID(nodeId);
      node.setPriceAdjustmentNodeXReferenceNodeID(nodeExtId);
      target.addPriceAdjustmentNode(node);

      nodeIds.add(nodeId);
    }
  }
  
  protected String[] readExistingGroups(TPricer pricer) throws TSDKInternalException
  {
    TDynamicGroup[] currentGroups = pricer.getDynamicGroupsByType(TDynamicGroup.TYPE_LOCATION);
    String[] groupNames = new String[currentGroups.length];
    for (int i = 0; i < currentGroups.length; i++)
    {
      groupNames[i] = currentGroups[i].getName();
    }
    Arrays.sort(groupNames);
    return groupNames;
  }
  
  protected String getCurrentAdGroupNameByBaseName(String[] existingGroupNames, String baseName )
  {
    String cachedName = adGroupNameMap.get(baseName);
    if( cachedName != null ) {
      return cachedName;
    }
    
    int biggestVersion = -1;
    String lastMatching = null;
    String baseNameOnVersion = baseName + "_";
    for (int i = 0; i < existingGroupNames.length; i++)
    {
      if( baseName.equals(existingGroupNames[i])) {
        lastMatching = existingGroupNames[i];
        continue;
      }
      if (existingGroupNames[i].startsWith(baseNameOnVersion))
      {
        lastMatching = existingGroupNames[i];
        
        String lastIdxStr = lastMatching.substring(baseName.length() + 1);
        int vnum = Integer.parseInt(lastIdxStr);
        if( vnum > biggestVersion ) 
        {
          biggestVersion = vnum;
        }
      } else if (existingGroupNames[i].compareTo(baseNameOnVersion) > 0)
      {
        break;
      }
    }
    
    if( biggestVersion >= 0 )
    {
      lastMatching = baseNameOnVersion + biggestVersion;
      logDebug("Last version: "+lastMatching);
    }
    
    adGroupNameMap.put(baseName, lastMatching);
    
    return lastMatching;
  }
}
