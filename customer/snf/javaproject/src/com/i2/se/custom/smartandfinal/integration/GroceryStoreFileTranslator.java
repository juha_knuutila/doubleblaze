package com.i2.se.custom.smartandfinal.integration;

import java.io.File;
import java.io.IOException;

import com.i2.se.app.integrations.imports.TranslatorException;
import com.i2.se.app.integrations.imports.data.BackBusContainer;

public interface GroceryStoreFileTranslator
{

  public void convert( File importFile, BackBusContainer target, ImportSettings settings,
      ProductDataCache productDataCache ) throws TranslatorException,
      IOException;
}
