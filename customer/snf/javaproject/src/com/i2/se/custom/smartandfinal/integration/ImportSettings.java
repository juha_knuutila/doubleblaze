package com.i2.se.custom.smartandfinal.integration;

import java.util.*;
import java.util.Map.Entry;

import com.i2.se.uic.util.StringUtils;

/**
 * custom settings for Grocery adapter.
 * 
 * @author J1013162
 *
 */
public class ImportSettings extends ImportSettingsBase
{
  private Map<String, String> companyNumberToProductGroup;
  private Map<String, String> classNamesPerCompany;
  private Map<String, String> businessIdsPerCompany;
  private Map<Integer, String> priceRulesPerQuantity;
  private List<String> priceRules;
  private Map<String, String[]> priceRuleParameters;
  private String upcAttribute;
  private String excludeSPSFAttribute;
  private String posAttribute;
  private String familyCodeAttribute;
  private String upcUnitAttribute;
  private String sizeAttribute;
  private String uomAttribute;
  private String sVendorNumber;
  private String pVendorNumber;
  private String nbrItemsInCase;
  private String weight;
  private String scaleIndicator;
  private String costSource;
  private String pricingZone;
  private String printCaseFlagAttribute;
  private String inboundDirectory;
  private String prePricedItemAttribute;
  
  private String udcSizeAttribute;
  private String packageSizeAttribute;
  private String dynamicGroupPrefix;
  private String updatedOnAttribute;

  private String listCostPriceGroup;
  private String approvedPricePriceGroup;
  private String netCostPriceGroup;
  private String lastRecvPriceGroup;
  private String regularEachRetailPriceGroup;
  private String defaultLocationId;
  private String[] characteristics;

  private String fixedPromoPriceCase;
  private String fixedPromoPriceEach;
  private String centsOffPriceCase;
  private String centsOffPriceEach;
  private String promoMultiplePrice;
  private String promoQty;
  private String mod;
  private String pricerMod;
  private String[] dmaPricingRules;
  private String adGroupParam;
  private String dispLocParam;

  public ImportSettings( Map<String, String> properties )
  {
    super( properties );
 
    inboundDirectory = getProperty( properties, "InboundDirectory", "" );
    
    upcAttribute = getProperty( properties, "UpcAttribute", "UPC" );
    posAttribute = getProperty( properties, "PosAttribute", "SF_POS_Dep" );
    sizeAttribute = getProperty( properties, "SizeAttribute", "Size" );
    uomAttribute = getProperty( properties, "UOMAttribute", "UOM" );
    udcSizeAttribute = getProperty( properties, "UDCSizeAttribute", "UDC_SIZE_SF" );
    packageSizeAttribute = getProperty( properties, "PackageSizeAttribute", "PackageSize" );

    updatedOnAttribute = getProperty( properties, "UpdatedOnAttribute", "updatedOn" );

    familyCodeAttribute = getProperty( properties, "FamilyCodeAttribute", "Family" );

    upcUnitAttribute = getProperty( properties, "UpcUnitAttribute", "UPC_Unit" );
    excludeSPSFAttribute = getProperty( properties, "excludeSPSFAttribute", "Exclude_SP_SF" );
    printCaseFlagAttribute = getProperty( properties, "PrintCaseFlagAttribute", "PrintCaseFlag" );
    prePricedItemAttribute = getProperty( properties, "PrePricedItemAttribute", "PrePricedItem");
    
    sVendorNumber = getProperty( properties, "SupplyVendorNumberAttribute", "Vendor_Number" );
    pVendorNumber = getProperty( properties, "PrimaryVendorNumberAttribute",
        "Primary_Vendor_Number" );
    nbrItemsInCase = getProperty( properties, "NbrItemsInCaseAttribute", "NbrItemsInCase" );
    weight = getProperty( properties, "ItemWeightAttribute", "Weight" );
    scaleIndicator = getProperty( properties, "ItemScaleIndicatorAttribute", "ScaleIndicator" );
    costSource = getProperty( properties, "ItemCostSourceAttribute", "Cost_Source" );
    pricingZone = getProperty( properties, "ItemPricingZoneAttribute", "Pricing_Zone" );

    listCostPriceGroup = getProperty( properties, "ListCostPriceGroup", "ListCost" );
    approvedPricePriceGroup = getProperty( properties, "ApprovedPricePriceGroup", "ApprovedPrice" );

    netCostPriceGroup = getProperty( properties, "NetCostPriceGroup", "NetCost" );
    lastRecvPriceGroup = getProperty( properties, "LastRecvCostPriceGroup", "LastRecvCost" );
    regularEachRetailPriceGroup = getProperty( properties, "RegularEachRetailPriceGroup",
        "RegularEachRetail" );

    dynamicGroupPrefix = getProperty( properties, "DynamicGroupPrefix", "DG_" );
    defaultLocationId = getProperty( properties, "DefaultLocationId", "" );
    String chracterstic = getProperty( properties, "Characteristics", "" );

    if( !chracterstic.isEmpty( ) )
    {
      characteristics = chracterstic.split( "," );
    }

    companyNumberToProductGroup = new HashMap<String, String>( );

    Map<String, String> companyTypeMap = StringUtils
        .extractValuePairs( getProperty( properties, "CompanyNumberProductGroups", "" ), ",", "|" );

    if( !companyTypeMap.isEmpty( ) )
    {
      for( Entry<String, String> e : companyTypeMap.entrySet( ) )
      {
        String companyNumber = e.getKey( );
        companyNumberToProductGroup.put( companyNumber, e.getValue( ) );

      }
    }

    classNamesPerCompany = new HashMap<String, String>( );

    Map<String, String> classNames = StringUtils
        .extractValuePairs( getProperty( properties, "ClassName", "" ), ",", "|" );

    if( !classNames.isEmpty( ) )
    {
      for( Entry<String, String> e : classNames.entrySet( ) )
      {
        classNamesPerCompany.put( e.getKey( ), e.getValue( ) );

      }
    }

    businessIdsPerCompany = new HashMap<String, String>( );

    Map<String, String> businessiDS = StringUtils
        .extractValuePairs( getProperty( properties, "BusinessIDPrefix", "" ), ",", "|" );

    if( !businessiDS.isEmpty( ) )
    {
      for( Entry<String, String> e : businessiDS.entrySet( ) )
      {
        businessIdsPerCompany.put( e.getKey( ), e.getValue( ) );

      }
    }

    priceRulesPerQuantity = new HashMap<Integer, String>( );
    priceRules = new ArrayList<String>( );
    
    fixedPromoPriceCase = getProperty( properties, "FixedPromoPriceCase", "FixedPromoPriceCase" );
    fixedPromoPriceEach = getProperty( properties, "FixedPromoPriceEach", "FixedPromoPriceEach" );

    centsOffPriceCase = getProperty( properties, "CentsOffPriceCase", "CentsOffPriceCase" );

    centsOffPriceEach = getProperty( properties, "CentsOffPriceEach", "CentsOffPriceEach" );

    promoMultiplePrice = getProperty( properties, "PromoMultiplePrice", "PromoMultiplePrice" );

    promoQty = getProperty( properties, "PromoQty", "PromoQty" );

    mod = getProperty( properties, "MOD", "mod" );
    pricerMod = getProperty( properties, "PRICER_MOD", "PRICER_MOD" );
    adGroupParam = getProperty( properties, "AdGroupParam", "");
    dispLocParam = getProperty( properties, "DispLocParam", "");
    
    priceRuleParameters = new HashMap<String, String[]>( );


    Map<String, String> priceRuleMap = StringUtils
        .extractValuePairs( getProperty( properties, "PricegroupPricingRuleNames", "" ), ",", "|" );
    if( !priceRuleMap.isEmpty( ) )
    {
      for( Entry<String, String> e : priceRuleMap.entrySet( ) )
      {
        Integer qty = Integer.valueOf( e.getKey( ) );
        priceRulesPerQuantity.put( qty, e.getValue( ) );
        priceRules.add( e.getValue( ) );
      }
    }
    
    dmaPricingRules = getProperty( properties, "dma.PricingRuleNames", "" ).split("\\,");

    Map<String, String[]> priceRuleParameterMap = StringUtils
        .extractValuePairstoArray( getProperty( properties, "PricegroupPricingRuleParameters", "" ),
            ",",
            "|", "-" );
    if( !priceRuleParameterMap.isEmpty( ) )
    {
      for( String priceRuleName : priceRuleParameterMap.keySet( ) )
      {
        priceRuleParameters.put( priceRuleName, priceRuleParameterMap.get( priceRuleName ) );
      }
    }

  }

  public String getPackageSizeAttribute( )
  {
    return packageSizeAttribute;
  }

  public void setPackageSizeAttribute( String packageSizeAttribute )
  {
    this.packageSizeAttribute = packageSizeAttribute;
  }

  public String getUomAttribute( )
  {
    return uomAttribute;
  }

  public void setUomAttribute( String uomAttribute )
  {
    this.uomAttribute = uomAttribute;
  }

  public String getRegularEachRetailPriceGroup( )
  {
    return regularEachRetailPriceGroup;
  }

  public Map<String, String> getCompanyNumberToProductGroup( )
  {
    return companyNumberToProductGroup;
  }

  public void setCompanyNumberToProductGroup( Map<String, String> companyNumberToProductGroup )
  {
    this.companyNumberToProductGroup = companyNumberToProductGroup;
  }

  public Map<String, String> getClassNamesPerCompany( )
  {
    return classNamesPerCompany;
  }

  public void setClassNamesPerCompany( Map<String, String> classNamesPerCompany )
  {
    this.classNamesPerCompany = classNamesPerCompany;
  }

  public Map<String, String> getBusinessIdsPerCompany( )
  {
    return businessIdsPerCompany;
  }

  public void setBusinessIdsPerCompany( Map<String, String> businessIdsPerCompany )
  {
    this.businessIdsPerCompany = businessIdsPerCompany;
  }

  public String getUpcAttribute( )
  {
    return upcAttribute;
  }

  public void setUpcAttribute( String upcAttribute )
  {
    this.upcAttribute = upcAttribute;
  }

  public String getPosAttribute( )
  {
    return posAttribute;
  }

  public void setPosAttribute( String posAttribute )
  {
    this.posAttribute = posAttribute;
  }

  public String getFamilyCodeAttribute( )
  {
    return familyCodeAttribute;
  }

  public void setFamilyCodeAttribute( String familyCodeAttribute )
  {
    this.familyCodeAttribute = familyCodeAttribute;
  }

  public String getUpcUnitAttribute( )
  {
    return upcUnitAttribute;
  }

  public void setUpcUnitAttribute( String upcUnitAttribute )
  {
    this.upcUnitAttribute = upcUnitAttribute;
  }


  public String getSizeAttribute( )
  {
    return sizeAttribute;
  }

  public void setSizeAttribute( String sizeAttribute )
  {
    this.sizeAttribute = sizeAttribute;
  }

  public String getUdcSizeAttribute( )
  {
    return udcSizeAttribute;
  }

  public void setUdcSizeAttribute( String udcSizeAttribute )
  {
    this.udcSizeAttribute = udcSizeAttribute;
  }

  public String getSupplyVendorNumber( )
  {
    return sVendorNumber;
  }


  public void setSupplyVendorNumber( String sVendorNumber )
  {
    this.sVendorNumber = sVendorNumber;
  }

  public String getPrimaryVendorNumber( )
  {
    return pVendorNumber;
  }

  public void setPrimaryVendorNumber( String pVendorNumber )
  {
    this.pVendorNumber = pVendorNumber;
  }

  public String getNbrItemsInCase( )
  {
    return nbrItemsInCase;
  }

  public void setNbrItemsInCase( String nbrItemsInCase )
  {
    this.nbrItemsInCase = nbrItemsInCase;
  }

  public String getWeight( )
  {
    return weight;
  }

  public void setWeight( String weight )
  {
    this.weight = weight;
  }

  public String getScaleIndicator( )
  {
    return scaleIndicator;
  }

  public void setScaleIndicator( String scaleIndicator )
  {
    this.scaleIndicator = scaleIndicator;
  }

  public String getCostSource( )
  {
    return costSource;
  }

  public void setCostSource( String costSource )
  {
    this.costSource = costSource;
  }

  public String getPricingZone( )
  {
    return pricingZone;
  }

  public void setPricingZone( String pricingZone )
  {
    this.pricingZone = pricingZone;
  }

  public String getListCostPriceGroup( )
  {
    return listCostPriceGroup;
  }

  public String getApprovedPricePriceGroup( )
  {
    return approvedPricePriceGroup;
  }

  public String getNetCostPriceGroup( )
  {
    return netCostPriceGroup;
  }


  public String getLastRecvPriceGroup( )
  {
    return lastRecvPriceGroup;
  }

  public Map<Integer, String> getPriceRulesPerQuantity( )
  {
    return priceRulesPerQuantity;
  }

  public void setPriceRulesPerQuantity( Map<Integer, String> priceRulesPerQuantity )
  {
    this.priceRulesPerQuantity = priceRulesPerQuantity;
  }

  public List<String> getPriceRules( )
  {
    return priceRules;
  }

  public void setPriceRules( List<String> priceRules )
  {
    this.priceRules = priceRules;
  }

  public Map<String, String[]> getPriceRuleParameters( )
  {
    return priceRuleParameters;
  }

  public void setPriceRuleParameters( Map<String, String[]> priceRuleParameters )
  {
    this.priceRuleParameters = priceRuleParameters;
  }

  public String getDynamicGroupPrefix( )
  {
    return dynamicGroupPrefix;
  }

  public void setDynamicGroupPrefix( String dynamicGroupPrefix )
  {
    this.dynamicGroupPrefix = dynamicGroupPrefix;
  }

  public String getUpdatedOnAttribute( )
  {
    return updatedOnAttribute;
  }

  public void setUpdatedOnAttribute( String updatedOnAttribute )
  {
    this.updatedOnAttribute = updatedOnAttribute;
  }

  public String getDefaultLocationId( )
  {
    return defaultLocationId;
  }

  public void setDefaultLocationId( String defaultLocationId )
  {
    this.defaultLocationId = defaultLocationId;
  }

  public String[] getCharacteristics( )
  {
    return characteristics;
  }

  public void setCharacteristics( String[] chracterstics )
  {
    this.characteristics = chracterstics;
  }

  public String getFixedPromoPriceCase( )
  {
    return fixedPromoPriceCase;
  }

  public void setFixedPromoPriceCase( String fixedPromoPriceCase )
  {
    this.fixedPromoPriceCase = fixedPromoPriceCase;
  }

  public String getFixedPromoPriceEach( )
  {
    return fixedPromoPriceEach;
  }

  public void setFixedPromoPriceEach( String fixedPromoPriceEach )
  {
    this.fixedPromoPriceEach = fixedPromoPriceEach;
  }

  public String getCentsOffPriceCase( )
  {
    return centsOffPriceCase;
  }

  public void setCentsOffPriceCase( String centsOffPriceCase )
  {
    this.centsOffPriceCase = centsOffPriceCase;
  }

  public String getCentsOffPriceEach( )
  {
    return centsOffPriceEach;
  }

  public void setCentsOffPriceEach( String centsOffPriceEach )
  {
    this.centsOffPriceEach = centsOffPriceEach;
  }

  public String getPromoMultiplePrice( )
  {
    return promoMultiplePrice;
  }

  public void setPromoMultiplePrice( String promoMultiplePrice )
  {
    this.promoMultiplePrice = promoMultiplePrice;
  }

  public String getPromoQty( )
  {
    return promoQty;
  }

  public void setPromoQty( String promoQty )
  {
    this.promoQty = promoQty;
  }

  public String getMod( )
  {
    return mod;
  }

  public void setMod( String mod )
  {
    this.mod = mod;
  }

  public String getPricerMod()
  {
    return pricerMod;
  }

  public String getExcludeSPSFAttribute()
  {
    return excludeSPSFAttribute;
  }
  
  public String getPrintCaseFlagAttribute()
  {
    return printCaseFlagAttribute;
  }
  
  public String getPrePricedItemAttribute()
  {
    return prePricedItemAttribute;
  }

  public String[] getDmaPricingRules()
  {
    return dmaPricingRules;
  }

  public String getAdGroupParam()
  {
    return adGroupParam;
  }

  public String getDispLocParam()
  {
    return dispLocParam;
  }

  public String getInboundDirectory()
  {
    return inboundDirectory;
  }
  
}
