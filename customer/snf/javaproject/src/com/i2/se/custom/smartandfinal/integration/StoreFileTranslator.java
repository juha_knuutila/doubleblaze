package com.i2.se.custom.smartandfinal.integration;

import java.io.*;
import java.util.*;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.app.integrations.imports.TranslatorException;
import com.i2.se.app.integrations.imports.data.*;
import com.i2.se.bd.catalog.THierarchy;
import com.i2.se.bd.util.bo.session.TSession;

/**
 * Handle pipe delimited Store file containing following fields:
 *
 * StoreNumber,StoreName,Country,RegionNumber,RegionName,DistrictNumber,
 * DistrictName,PricingZone,BusinessUnit,ClosedFlag
 *
 * @author j1013162
 *
 */
public class StoreFileTranslator extends SAFConverterBase
{
  private Set<String> locationHierarchyIDs = new HashSet<String>( );
  // set of zone IDs we have created price adjustment nodes to
  
  protected String getAdapterName()
  {
    return "Stores";
  }
  
  private void addHierarchyIfMissing( String hierarchyID, String hierarchyName, String externalID,
      String parentHierarchyID, BackBusContainer target, ProductDataCache productDataCache )
  {
    if( !locationHierarchyIDs.contains( hierarchyID ) )
    {
      locationHierarchyIDs.add( hierarchyID );

      Hierarchy h = new Hierarchy( );
      h.setHierarchyID( hierarchyID );
      h.setHierarchyName( hierarchyName );
      h.setHierarchyType( THierarchy.LOCATION );
      h.setParentHierarchyID( parentHierarchyID );
      h.setExternalID( externalID );
      target.addHierarchy( h );
      productDataCache.addExternalIDToLocation( externalID, hierarchyID );
      // hierarchy path?
    }
  }

  @Override
  public int doConvert(TSession session, File importFile, BackBusContainer target, SAFSettings settings,
      ProductDataCache productDataCache) throws TranslatorException, IOException
  {
    BufferedReader bf = null;
    int trueLineNo = 0;
    int commentLineCount = 0;
    
    try
    {
      bf = new BufferedReader(
          new InputStreamReader( new FileInputStream( importFile ) ) );
      while( true )
      {
        String s = bf.readLine( );
        if( s == null )
        {
          break;
        }
        ++trueLineNo;
        if (StringUtils.isEmpty(s) || StringUtils.startsWith(s, "#"))
        {
          logInfo("Comment line " + trueLineNo + " : " + s);
          commentLineCount++;
          continue;
        }
        String[] elements = s.split( "\\|", -1 );
        int i = 0;
        String storeNumber = elements[ i++ ];
        String storeName = elements[ i++ ];
        String storeFormat = elements[ i++ ];
        String districtCode = elements[ i++ ];
        String district = elements[ i++ ];
        String regionCode = elements[ i++ ];
        String region = elements[ i++ ];
        if(region != null)
        {
          region= region.trim( );
         }
          
        String companyNumber = elements[ i++ ];
        String company = elements[ i++ ];
        String address = elements[ i++ ];
        String city = elements[ i++ ];
        String state = elements[ i++ ];
        String country = elements[ i++ ];
        String pricingZone = elements[ i++ ];

        String expCountry = getCountryName( country );

        if( expCountry != null )
        {
          country = expCountry;
        }

        String countryCode = getCountryISOCode( country );

        String hierarchyID = "L" + countryCode;

        
        boolean creatingSpecialZone = false;
        if ("SPECIAL_ZONE".equals(storeNumber))
        {
          creatingSpecialZone = true;
          logWarn("Creating special zone, "+ trueLineNo);
        }
        // validate field

        if( pricingZone == null || pricingZone.isEmpty( ) )
        {
          logWarn("Error reading store  " + storeNumber + ": pricing zone not found!" );
          continue;
        }

        if( !creatingSpecialZone && ( company == null || company.isEmpty( ) ) )
        {
          logWarn("Error reading store  " + storeNumber + ": company not found!" );
          continue;
        }

        // Country level
        addHierarchyIfMissing( hierarchyID, country, countryCode,
            Hierarchy.LOCATION_HIERARCHY_ROOT, target, productDataCache );

        // company level
        String parentHierarchyID = hierarchyID;
        hierarchyID = hierarchyID + "-" + companyNumber;
        addHierarchyIfMissing( hierarchyID, company, companyNumber, parentHierarchyID, target,
            productDataCache );

        // Pricing Zone
        parentHierarchyID = hierarchyID;
        hierarchyID = hierarchyID + "-" + pricingZone;
        addHierarchyIfMissing( hierarchyID, pricingZone, pricingZone, parentHierarchyID, target,
            productDataCache );

        if(creatingSpecialZone) 
        {
          continue;
        }
        
        // Store Level
        parentHierarchyID = hierarchyID;
        hierarchyID = hierarchyID + "-" + storeNumber;
        addHierarchyIfMissing( hierarchyID, storeNumber+"_"+storeName, storeNumber, parentHierarchyID, target,
            productDataCache );

        String partyOID = createBusiness(target, settings, storeNumber, companyNumber, storeName,
            region, regionCode, district, districtCode, storeFormat, address, city, countryCode, state);

        PartyPrice pap = new PartyPrice( );
        pap.setPartyID( partyOID );
        pap.setPartyType( THierarchy.LOCATION );
        pap.setHierarchyID( hierarchyID );
        target.addPartyPrice( pap );

        // create dummy business
        Set<String> specialZones = settings.getSpecialZones();
        String postfix = settings.getSpecialZoneBusinessPostFix();
        for( String zone : specialZones )
        {
            createBusiness(target, settings, storeNumber+postfix+zone, companyNumber, storeName,
              region, regionCode, district, districtCode, storeFormat, address, city, countryCode, state);
        }
      }

      logInfo("Read " + trueLineNo + " lines" );
      if (commentLineCount > 0)
      {
        logWarn("Comment line count : " + commentLineCount);
      }
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
      throw new TranslatorException(
          "Error reading file, line " + trueLineNo + ": " + e.getMessage( ), e );
    }
    catch( IOException e )
    {
      throw new TranslatorException(
          "IOException reading file, line " + trueLineNo + ": " + e.getMessage( ), e );
    }
    finally
    {
      if( bf != null )
      {
        bf.close( );
      }
    }
    return trueLineNo;
  }

  private String createBusiness(BackBusContainer target, SAFSettings settings, String storeNumber, String companyNumber, 
      String storeName, String region, String regionCode, String district, String districtCode, 
      String storeFormat, String address, String city, String countryCode, String state)
  {
    BusinessParty business = new BusinessParty( );
    String buPrefix = settings.getBusinessIdsPerCompany( ).get( companyNumber );
    String partyOID = buPrefix + storeNumber;
    business.setBusinessOID( partyOID );
    business.setExternalPartyID( storeNumber );
    business.setBusinessName( storeName + " " + storeNumber );
    business.setSubTypeCode( "BUSINESS" );
    business.setDefaultFlag( "false" );
    business.setDisplayCategoryVName( "Customer" );
    business.setCurrencyID( "USD" );
    business.setProductLanguageID( settings.getProductLanguageID( ) );
    String parentOid = settings.getParentBusinessOidForCompany(companyNumber);
    business.setParentBusinessOID( parentOid );
    target.addBusinessParty( business );

    PartyCharacteristic pc = new PartyCharacteristic( );
    pc.setPartyOID( partyOID );
    pc.setCharacteristicName( "Region" );
    pc.setCharacteristicValue( region );
    target.addPartyCharacteristic( pc );

    pc = new PartyCharacteristic( );
    pc.setPartyOID( partyOID );
    pc.setCharacteristicName( "Region Code" );
    pc.setCharacteristicValue( regionCode );
    target.addPartyCharacteristic( pc );

    pc = new PartyCharacteristic( );
    pc.setPartyOID( partyOID );
    pc.setCharacteristicName( "District" );
    pc.setCharacteristicValue( district );
    target.addPartyCharacteristic( pc );

    pc = new PartyCharacteristic( );
    pc.setPartyOID( partyOID );
    pc.setCharacteristicName( "District Code" );
    pc.setCharacteristicValue( districtCode );
    target.addPartyCharacteristic( pc );

    pc = new PartyCharacteristic( );
    pc.setPartyOID( partyOID );
    pc.setCharacteristicName( "StoreFormat" );
    pc.setCharacteristicValue( storeFormat );
    target.addPartyCharacteristic( pc );

    pc = new PartyCharacteristic( );
    pc.setPartyOID( partyOID );
    pc.setCharacteristicName( "Currency" );
    pc.setCharacteristicValue( "USD" );
    target.addPartyCharacteristic( pc );

    CommunicateMean cm = new CommunicateMean( );
    cm.setPartyOID( partyOID );
    cm.setPurposeDefaulfFlag( "true" );
    cm.setSubTypeCode( "ADDRESS" );
    cm.setUserLabel( "Default" );
    cm.setActDeactCode( "A" );
    cm.setDataTypeID( "1" );
    target.addCommunicateMean( cm );

    CommunicateMeanItem cmi = new CommunicateMeanItem( );

    cmi.setPartyOID( partyOID );
    cmi.setPartyName( storeName + " " + storeNumber );
    // cm.setExternalPartyID( storeNumber );
    cmi.setSubTypeCode( "ADDRESS" );
    cmi.setAddressLine1( address );
    cmi.setCityName( city );
    cmi.setIsoCountryID( countryCode );

    if( countryCode != "MX" )
    {
      cmi.setStateProvCode( state );
    }

    // No postal code is sent. Adding default now.
    cmi.setPostalCode( "00000" );

    target.addCommunicateMeanItem( cmi );

    return partyOID;
  }

  private String getCountryName( String country )
  {
    return ( "U.S.A.".equals( country ) ) ? "USA"
        : ( "MEXICO".equals( country ) ) ? "MEXICO" : null;
  }

  private String getCountryISOCode( String country )
  {
    return ( "USA".equals( country ) ) ? "US"
        : ( "MEXICO".equals( country ) ) ? "MX" : "US";
  }

}
