package com.i2.se.custom.smartandfinal.integration;

import java.io.*;
import java.nio.file.Files;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import com.i2.se.app.integrations.imports.TranslatorException;
import com.i2.se.app.integrations.imports.data.BackBusContainer;
import com.i2.se.bd.catalog.*;
import com.i2.se.bd.pricer.*;
import com.i2.se.bd.util.bo.session.*;

public class AdGroupTranslator extends SAFConverterBase
{

  protected String getAdapterName()
  {
    return "AdGroups";
  }

  public int doConvert(TSession session, File importFile, BackBusContainer target, SAFSettings settings,
      ProductDataCache productDataCache) throws TranslatorException, IOException
  {
    int trueLineNo = 0;

    String filePath = importFile.getAbsolutePath();
    logInfo("ADgroups file: " + filePath);
    String storeFileName = getStoreFile(importFile);
    logInfo("ADgroupStores file: " + storeFileName);

    File storeFile = new File(storeFileName);
    if (!storeFile.exists())
    {
      logError("AdGropup stores file not found: " + storeFileName);
      return 0;
    }

    AdGroupComparison comparison = null;
  
    // initial 
    String currentAdGroupsFile = settings.getCurrentAdGroupsFileName();
    String currentAdGroupStoresFile = settings.getCurrentAdGroupStoresFileName();

    AdGroupsSet newGroups = new AdGroupsSet(filePath, 0, 1, 2, storeFileName, this);
    AdGroupsSet currentGroups = new AdGroupsSet(currentAdGroupsFile, 0, 1, 2, currentAdGroupStoresFile, this );

    logDebug("--new:" + newGroups);
    logDebug("--cur:" + currentGroups);

    if (currentGroups.equals(newGroups))
    {
      // all are same just consume the file
      logWarn("Groups are identical to current ones");
    } else
    {
      comparison = currentGroups.getComparison(newGroups);
      // find new groups
      // find modified groups
      // find deleted groups
      createNewGroups(settings, session, comparison);

      logDebug("Comparison:" + comparison);

      // update list of current files:
      // move old as backup
      String timeStmp = ZonedDateTime.now().format(DateTimeFormatter.ofPattern("uuuuMMdd_HHmmss_"));
      
      moveFile(currentAdGroupsFile, timeStmp);
      moveFile(currentAdGroupStoresFile, timeStmp);
      
      // replace current, maybe this should only do copy
      copyFile(filePath, currentAdGroupsFile);
      copyFile(storeFileName, currentAdGroupStoresFile);
    }
    
    return trueLineNo;
  }

  private void copyFile(String filePath, String currentAdGroupsFile) throws IOException
  {
    File f = new File(filePath);
    File nf = new File(currentAdGroupsFile);
    Files.copy(f.toPath(), nf.toPath());
  }

  private void moveFile(String currentAdGroupsFile, String timeStmp)
  {
    File f = new File(currentAdGroupsFile);
    String newFile = f.getParent() + File.separator + timeStmp +f.getName();
    logWarn("Renaming: " + currentAdGroupsFile + " to " + newFile);
    File nf = new File(newFile);
    f.renameTo(nf);
  }

  public static String getStoreFile(File importFile)
  {
    String filePath = importFile.getAbsolutePath();
    if( filePath.indexOf(GroceryStoreAdapter.ADGROUPS_FILENAME) < 0 ) 
    {
      return null;
    }
    String storeFileName = filePath.replace(GroceryStoreAdapter.ADGROUPS_FILENAME,
        GroceryStoreAdapter.ADGROUPSTORES_FILENAME);
    return storeFileName;
  }

  private void createNewGroups(SAFSettings settings, TSession session, AdGroupComparison comparison)
  {
    TProductData pdata = session.getProductData();
    TPricer pricer = session.getPricer();

    try
    {
      String[] existingGroupNames = readExistingGroups(pricer);
     // Set<String> existingGroupNamesSet = new HashSet<String>();
     // existingGroupNamesSet.addAll(existingGroupNamesSet);

      logDebug("Creating and modifying groups:");

      for (AdGroup g : comparison.getNewGroups())
      {
        logDebug("Adding new groups:");
        createGroup(settings, pdata, pricer, existingGroupNames, g);
      }

      for (AdGroup g : comparison.getModifiedGroups())
      {
        logDebug("Modifying groups:");
        createGroup(settings, pdata, pricer, existingGroupNames, g);
      }

    } catch (TSDKInternalException e)
    {
      logError("Error creating dynamic groups: "+e);
    }
  }

  private void createGroup(SAFSettings settings, TProductData pdata, TPricer pricer, String[] existingGroupNames, AdGroup g)
      throws TSDKInternalException
  {
    logDebug("Ad Group:" + g);
    TDynamicGroup dg;
    dg = pricer.createDynamicGroup(TDynamicGroup.TYPE_LOCATION);
    String name = getName(settings, existingGroupNames, g.getId());
    dg.setName(name);
    if (g.getStartDate() != null)
    {
      dg.setStartDate(g.getStartDate());
    }
    if (g.getEndDate() != null)
    {
      dg.setEndDate(g.getEndDate());
    }
    
    for (String store : g.getStores())
    {
      THierarchy root = pdata.getHierarchy(TDynamicGroup.TYPE_LOCATION);
      THierarchyNode[] n = root.findNodesByExternalID(store, false); // TODO: why false?
      if (n.length != 1)
      {
        if (n.length < 1)
        {
          logWarn("Hierarchy node for store not found! " + store);
        } else
        {
          logWarn("Multiple hierarchy nodes for store! " + store + ", count=" + n);
        }
        continue;
      }

      THierarchyNode node = n[0];
      dg.addHierarchyNode(node.getOID());
      logDebug("Added store:" + store);
    }
    dg.commit();
  }

  /**
   * 
   * @param settings
   * @param existingGroupNames, version names sorted
   * @param id
   * @return
   */
  private String getName(SAFSettings settings, String[] existingGroupNames, String id)
  {
    String newName = settings.getDynamicGroupPrefix() + id;

    String lastMatching = getCurrentAdGroupNameByBaseName(existingGroupNames, newName);

    logDebug("Getting name for group: "+id+", last="+lastMatching);
    if (lastMatching != null)
    {
      int lastIdx = 0;
      if (lastMatching.length() > newName.length())
      {
        String lastIdxStr = lastMatching.substring(newName.length() + 1);
        lastIdx = Integer.parseInt(lastIdxStr);
      }
      newName += "_" + (lastIdx + 1);
    }
    logDebug("Next name: "+newName);

    return newName;
  }

  public static String getBooleanValue(String attValue)
  {
    return ("Y".equals(attValue)) ? "1" : "0";
  }

  public static String getValidValue(String attValue)
  {
    return ("?".equals(attValue)) ? "" : attValue;
  }
}
