package com.i2.se.custom.smartandfinal.integration;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.regex.*;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;

public class AdGroupsSet
{
  private int IDX_GROUP_ID = 0;
  private int IDX_GROUP_NAME = 1;
  private int IDX_GROUP_PRIORITY = 2;
  private int IDX_GROUP_STARTDATE = 3;
  private int IDX_GROUP_ENDDATE = 4;

  private static int counter = 0;

  private Map<String, AdGroup> groupMap = null;

  private SimpleDateFormat df;
  private SAFConverterBase converter;
  
  /**
   * Initialize group map
   * 
   * @param storeFileName
   * @param adGroupTranslator 
   * 
   * @param settings
   */
  public AdGroupsSet(String fileName, int idxGroupId, int idxGroupName, int idxGroupPriority, String storeFileName, SAFConverterBase conv)
  {
    this.converter = conv;
    IDX_GROUP_ID = idxGroupId;
    IDX_GROUP_NAME = idxGroupName;
    IDX_GROUP_PRIORITY = idxGroupPriority;
    
    df = new SimpleDateFormat("yyyy-MM-dd");
    
    List<AdGroup> groups = processInputFile(fileName);
    groupMap = new HashMap<String, AdGroup>();
    for (AdGroup g : groups)
    {
      if (!g.isTestGroup())
      {
        groupMap.put(g.getId(), g);
      }
    }
    processStoreFile(storeFileName);
  }

  private void processStoreFile(String storeFileName)
  {
    int trueLineNo = 0;

    BufferedReader bf = null;
    try
    {
      bf = new BufferedReader(new InputStreamReader(new FileInputStream(storeFileName)));
      while (true)
      {
        String s;
        s = bf.readLine();
        if (s == null)
        {
          break;
        }
        ++trueLineNo;
        if (StringUtils.isEmpty(s) || StringUtils.startsWith(s, "#"))
        {
          logInfo("Comment line " + trueLineNo + " : " + s);
          continue;
        }

        String regEx = ",";
        String[] elements = s.split(regEx, -1);

        int i = 0;
        String group = elements[i++];
        String store = elements[i++];
        
        if( StringUtils.isNotEmpty(group) )
        {
          group = group.trim();
        }
        if( StringUtils.isNotEmpty(store) )
        {
          store = store.trim();
        }

        AdGroup g = groupMap.get(group);
        if (g == null)
        {
          logError("Stores file contains group which is not groups file. Group: " + group+ ", at line="+trueLineNo);
          System.exit(0);
        }
        g.addStore(store);
        logWarn("Adding group to store:"+g.getId()+" - "+store);
      }

    } catch (IOException e)
    {
      logError("Error reading stores file:" + e);
    } finally
    {
      if (bf != null)
      {
        try
        {
          bf.close();
        } catch (IOException e)
        {
          logError("Error closing stores file:" + e);
        }
      }
    }
  }

  /**
   * Returns the priority for AdGroup from the AdGroups file
   * 
   * @param id
   * @return groups priority, calculated 10000-group id. If not found then returns
   *         -1
   */
  public int getAdGroupPriority(String id)
  {
    AdGroup group = groupMap.get(id);
    logDebug("Prioroty for group: " + id + " = " + group);
    if (group == null)
    {
      return -1;
    }
    return 10000 - group.getPriority();
  }

  private Function<String, AdGroup> mapToAdGroup = (line) ->
  {
    counter++;
    if (line.startsWith("#") || StringUtils.isEmpty(line) )
    {
      return AdGroup.getTestGroup();
    }

    try
    {
      String regEx = "\\s*(\\d*)\\s*,\\s*((?:\\\".*\\\")|[^,]*)\\s*,\\s*(\\d*)\\s*(?:,\\s*([^,]+)\\s*)?(?:,\\s*([^,]+)\\s*)?";
      Pattern p = Pattern.compile(regEx);
      Matcher matcher = p.matcher(line);
      int groupCount = matcher.groupCount();
      if( !matcher.matches() || groupCount < 3 || groupCount > 5 ) {
        logWarn("Not accepted line: " + counter + ", line: " + line);
        return AdGroup.getTestGroup();
      }
      
      String id = matcher.group(IDX_GROUP_ID+1);
      String name = matcher.group(IDX_GROUP_NAME+1);
      int priority = Integer.parseInt(matcher.group(IDX_GROUP_PRIORITY+1));
      Date start = null;
      Date end = null;
      
      if ( groupCount > IDX_GROUP_STARTDATE)
      {
        try
        {
          String startDate = matcher.group(IDX_GROUP_STARTDATE+1);
          if( startDate != null  )
          {
            start = df.parse(startDate);
          }
        } catch (java.text.ParseException e)
        {
          logWarn("Invalid start date for group: " + matcher.group(IDX_GROUP_STARTDATE) + ", line: " + line);
        }
      }
      if (groupCount > IDX_GROUP_ENDDATE)
      {
        try
        {
          String endDate = matcher.group(IDX_GROUP_ENDDATE+1);
          if( endDate != null  )
          {
            end = df.parse(matcher.group(IDX_GROUP_ENDDATE+1));
          }
        } catch (java.text.ParseException e)
        {
          logWarn("Invalid end date for group: " + matcher.group(IDX_GROUP_STARTDATE) + ", line: " + line);
        }
      }

      AdGroup item = new AdGroup(id, name, priority, start, end);
      logWarn("Group:" + item);
      return item;
    } catch (Exception e)
    {
      logWarn("Exception at LINE:" + line);
      logWarn("Exception at " + counter + ", " + e);
     
        throw e;
    }
  };

  private List<AdGroup> processInputFile(String inputFilePath)
  {
    File f = new File(inputFilePath);
    if (!f.exists())
    {
      logError("AdGroups file not found: " + inputFilePath);
      System.exit(0);
    }

    List<AdGroup> inputList = new ArrayList<AdGroup>();
    try
    {
      InputStream is = new FileInputStream(f);
      if (is != null)
      {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        // expects one header row.

        inputList = br.lines().map(mapToAdGroup).collect(Collectors.toList());
        br.close();
      }

    } catch (IOException e)
    {
      logError("Error reading AdGroups file: " + e);
      System.exit(0);
    }
    return inputList;
  }

  private void logError(String message)
  {
    converter.logError(message);
  }

  private void logWarn(String message)
  {
    converter.logWarn(message);
  }

  private void logInfo(String message)
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(), StaticLogger.INFO, 0, message);
  }

  private void logDebug(String message)
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(), StaticLogger.DEBUG, 0, message);
  }

  public AdGroupComparison getComparison(AdGroupsSet newGroups)
  {
    AdGroupComparison comparison = new AdGroupComparison();
    Set<String> newGroupIds = new HashSet<String>();
    newGroupIds.addAll(newGroups.groupMap.keySet());
    
    for( String groupId : groupMap.keySet() )
    {
      AdGroup currentGroup = getGroup( groupId );
      AdGroup newGroup = newGroups.getGroup( groupId );
      
      if( newGroup == null ) {
        comparison.deletedGroups.add(currentGroup);
      }
      else
      {
        newGroupIds.remove(groupId);
        if( !currentGroup.equals(newGroup) ) {
          comparison.modifiedGroups.add(newGroup);
        }
      }
    }
    for( String groupId : newGroupIds ) {
      comparison.newGroups.add(newGroups.getGroup(groupId));
    }
    
    return comparison;
  }

  private AdGroup getGroup(String groupId)
  {
    return groupMap.get(groupId);
  }
  
  

  @Override
  public int hashCode()
  {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((groupMap == null) ? 0 : groupMap.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj)
  {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    AdGroupsSet other = (AdGroupsSet) obj;
    if (groupMap == null)
    {
      if (other.groupMap != null)
        return false;
    } else if (!groupMap.equals(other.groupMap))
      return false;
    return true;
  }

  @Override
  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("AdGroupSet...\n");
    for( String group : groupMap.keySet() ) {
      sb.append( groupMap.get(group) +"\n");
    }
    return sb.toString();
  }
}
