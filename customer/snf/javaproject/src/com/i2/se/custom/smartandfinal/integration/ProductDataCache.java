package com.i2.se.custom.smartandfinal.integration;

import java.util.*;

import com.i2.se.bd.catalog.*;
import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.pricer.*;
import com.i2.se.bd.util.bo.security.OperationEnum;
import com.i2.se.bd.util.bo.session.TSDKInternalException;
import com.i2.se.bd.util.bo.session.TSession;
import com.i2.se.bd.util.searches.SearchQuery;
import com.i2.se.bd.util.searches.SearchQueryBuilder;

/**
 * Product data cache for item
 * 
 * @author J1013162
 *
 */
public class ProductDataCache
{
  private Map<String, String> skuToItem;
  
  private Map<String, String> skuGroupToItem;
  
  private Map<String, String> skuToPG;
  private Map<String, String> oidToPg;

  private Map<String, String> eventToAdjId;

  private Map<String, List<PricingRuleParameter>> priceRuleToParams;

  private Map<String, TAdjustmentNode> dynamicGroupAdjNodes;

  private Map<String, String> externalIDToLocation;
  
  private Map<String, String> externalIDToLocationPath;
  private Map <String,String> externalIDToZone;
  
  private Map<String, String> pgByPathMap;

  private Map<String, String[]> skuToItems;
  private Map<String, TBoxItem> oidToItem;

  public ProductDataCache( )
  {
    skuToItem = new HashMap<String, String>( );
    skuToItems = new HashMap<String, String[]>();
    
    skuToPG = new HashMap<String, String>( );
    oidToPg = new HashMap<String, String> ();
    oidToItem = new HashMap<String, TBoxItem> (); 
    
    skuGroupToItem = new HashMap<String, String>();
    
    eventToAdjId = new HashMap<String, String>( );
    dynamicGroupAdjNodes = new HashMap<String, TAdjustmentNode>( );

    externalIDToLocation = new HashMap<String, String>( );
    externalIDToLocationPath = new HashMap<String, String>();
    externalIDToZone = new HashMap<String, String>();
    
    priceRuleToParams = new HashMap<String, List<PricingRuleParameter>>( );
  }

  /**
   * @deprecated , returns random item
   * @param itemCode
   * @return
   */
  public String getSkuToItem( String itemCode )
  {
    return skuToItem.get( itemCode );
  }
  
  public String[] getCodeToOids(String itemCode) 
  {
    return skuToItems.get(itemCode);
  }
  
  private String getBusinessPrefix(String path)
  {
    int pos = path.indexOf("/");
    pos = path.indexOf("/", pos+1);
    pos = path.indexOf("/", pos+1);
    String businessPath = path.substring(0, pos);
    return businessPath;
  }
  
  public String getSkuToItem( String itemCode, String store ) 
  {
    String hierarchyPath = externalIDToLocationPath.get(store);
    logWarn("hier:"+hierarchyPath+", "+store);
    
    if( hierarchyPath == null) 
    {
      logWarn("Hierarchy path not found:"+hierarchyPath+", "+store);
      return null;
    }
    
    String businessPath = getBusinessPrefix(hierarchyPath);
   
    String productGroup = pgByPathMap.get( businessPath );
    logWarn("Hierarchy path :"+businessPath+", "+productGroup);
    
    return skuGroupToItem.get(itemCode+"-"+productGroup);  
  }

  public String getEventToAdjId( String eventId )
  {
    return eventToAdjId.get( eventId );
  }

  /**
   * @deprecated , returns random item
   * @param skuCode
   * @param styleCode
   */
  public void addSkuToItem( String skuCode, String styleCode )
  {
    skuToItem.put( skuCode, styleCode );
  }

  /** 
   * @deprecated
   * @param skuCode
   * @return
   */
  public String getSkuToPG( String skuCode )
  {
    return skuToPG.get( skuCode);
  }
  
  public void addSkuToPG( String skuCode, String pg )
  {
    skuToPG.put( skuCode, pg );
  }
  
  public String getOidToPG( String oid )
  {
    return oidToPg.get( oid );
  }

  public void addOidToPG( String oid, String pg )
  {
    oidToPg.put( oid, pg );
  }

  public void addEventToAdjId( String eventId, String adjId )
  {
    eventToAdjId.put( eventId, adjId );
  }

  public TAdjustmentNode getDynamicGroupAdjNode( String name )
  {
    return dynamicGroupAdjNodes.get( name );
  }

  public void addDynamicGroupAdjNode( String name, TAdjustmentNode adjNode )
  {
    dynamicGroupAdjNodes.put( name, adjNode );
  }

  public String getExternalIDToZoneLocation( String externalID )
  {
    String path = externalIDToLocationPath.get( externalID );
    if( path != null )
    {
      int pos = path.lastIndexOf("/");
      path = path.substring(0, pos);
      pos = path.lastIndexOf("/");
      path = path.substring(pos+1);
    }
    
    return path;
  }

  public String getExternalIDToLocation( String externalID )
  {
    return externalIDToLocation.get( externalID );
  }

  public void addExternalIDToLocation( String externalID, String hierarchyOID )
  {
    externalIDToLocation.put( externalID, hierarchyOID );
  }

  public String getExternalIDToLocationPath(String storeNumber)
  {
    return externalIDToLocationPath.get(storeNumber);
  }

  public String getExternalIDToZone(String storeNumber)
  {
    return externalIDToZone.get(storeNumber);
  }

  public Map<String, List<PricingRuleParameter>> getPriceRuleToParams( )
  {
    return priceRuleToParams;
  }

  public void loadProductData( TSession session, ImportSettings settings )
      throws TSDKInternalException
  {
    logInfo( "Load product data" );
    TProductData pd = session.getProductData( );
    TPricer pricer = session.getPricer( );
    List<String> pgIds = settings.getProductGroupIds( );
    pgByPathMap = settings.getProductGroupByPathMap();

    
    for( String pGroup : pgIds )
    {
      int pgGroupId = Integer.parseInt( pGroup );
      TGroup pg = pd.getGroup( pgGroupId );

      if( pg == null )
      {
        logInfo( "- product group not yet created, assuming full import" );
        return;
      }

      SearchQueryBuilder builder = new SearchQueryBuilder( "Code = *" );
      SearchQuery searchQuery = builder.build( SearchQuery.SearchType.SEARCH, new Object[]{} );
      TGroupSearch search = null;
      try
      {
        search = pd.startGroupSearch( searchQuery, pgGroupId,
            settings.getProductLanguageID( ) );
        while( search.isMoreInLastDirection( ) )
        {
          for( TBoxItem boxItem : search.nextBoxItems( 1024, true ) )
          {
            skuToItem.put( boxItem.getCode( ), boxItem.getObjectID( ) );
            
            skuToPG.put( boxItem.getCode( ), pGroup );
            oidToPg.put( boxItem.getObjectID(), pGroup);
            oidToItem.put( boxItem.getObjectID(), boxItem );
            skuGroupToItem.put(boxItem.getCode( ) +"-"+ boxItem.getGroupID(), boxItem.getObjectID( ));
            
            String [] oids = skuToItems.get( boxItem.getCode( ) );
            if( oids == null ) {
              oids = new String[] {boxItem.getObjectID()};
              skuToItems.put(boxItem.getCode(), oids);
            } else {
              String [] newOids = new String[oids.length+1];
              System.arraycopy(oids, 0, newOids, 0, oids.length);
              newOids[oids.length] = boxItem.getObjectID();
              skuToItems.put(boxItem.getCode(), newOids);
            }
          }
        }
      }
      finally
      {
        if( search != null )
        {
          search.endSearch( );
        }
      }

      SearchQueryBuilder builder1 = new SearchQueryBuilder( "Description = *" );
      SearchQuery searchQuery1 = builder1.build( SearchQuery.SearchType.SEARCH, new Object[]{} );
      TPricerSearch search1 = null;
      try
      {
        search1 = pricer.startPricerSearch( TPricerSearch.ADJUSTMENT, searchQuery1,
            settings.getProductLanguageID( ), new TAdjustmentSearchData( ),
            new OperationEnum[]{ OperationEnum.READ } );
        while( search1.isMoreInLastDirection( ) )
        {
          TAdjustment[] next = (TAdjustment[])search1.nextItems( 1024, true );
          for( TAdjustment c : next )
          {
            eventToAdjId.put( c.getOID( ), c.getOID( ) );
          }

        }
      }
      finally
      {
        if( search1 != null )
        {
          search1.endSearch( );
        }
      }

      StringBuffer searchString = new StringBuffer( );

      for( Iterator<String> it = settings.getPriceRules( ).iterator( ); it.hasNext( ); )
      {
        searchString.append( "Name = \"" );
        searchString.append( it.next( ) );
        searchString.append( "\" " );
        if( it.hasNext( ) )
        {
          searchString.append( " OR " );
        }
      }
      // Append DMA pricing rules
      String[] dmaPricingRules = settings.getDmaPricingRules();
      for( int i=0; dmaPricingRules != null && i<dmaPricingRules.length; i++  )
      {
        searchString.append( " OR " );
        
        searchString.append( "Name = \"" );
        searchString.append( dmaPricingRules[i] );
        searchString.append( "\" " );
      }
      logInfo("Search:"+searchString);
      
      SearchQueryBuilder builderRules = new SearchQueryBuilder( searchString.toString( ) );
      SearchQuery searchQueryPRules = builderRules.build( SearchQuery.SearchType.SEARCH,
          new Object[]{} );

      TPricerSearch searchPriceRules = null;
      try
      {
        searchPriceRules = pricer.startPricerSearch( TPricerSearch.PRICING_RULE, searchQueryPRules,
            settings.getProductLanguageID( ), null,
            new OperationEnum[]{ OperationEnum.READ } );
        while( searchPriceRules.isMoreInLastDirection( ) )
        {
          TPricingRule[] nextRules = (TPricingRule[])searchPriceRules.nextItems( 1024, true );
          for( TPricingRule c : nextRules )
          {
            TPricingRuleParameter[] params = c.getParameters( );
            
            List<PricingRuleParameter> priceParamList = new ArrayList<PricingRuleParameter>( );
            
            for(TPricingRuleParameter param :params)
            {
              priceParamList.add( new PricingRuleParameter( param.getName( ),
                  param.getDefaultValue( ), param.getDefaultValueCurrency( ) ) );
            }

            priceRuleToParams.put( c.getName( ), priceParamList );
          }

        }
      }
      finally
      {
        if( searchPriceRules != null )
        {
          searchPriceRules.endSearch( );
        }
      }

      // DynamicGroups
      TDynamicGroup[] dynamicGroups = pricer
          .getDynamicGroupsByType( TDynamicGroup.TYPE_LOCATION );

      for( TDynamicGroup dg : dynamicGroups )
      {
        TAdjustmentNode dgAdjustmentNode = TAdjustmentNode.createFromDynamicGroup2(
            TDynamicGroup.TYPE_LOCATION, dg.getOID( ), dg.getNodes( ) );

        dynamicGroupAdjNodes.put( dg.getName( ), dgAdjustmentNode );
      }

      // externalIDToLocation
      THierarchy location = pd.getHierarchy( THierarchy.LOCATION );
      for( THierarchyNode node : location.getNodes( ) )
      {
        if( !node.getExternalID( ).isEmpty( ) )
        {
          externalIDToLocation.put( node.getExternalID( ), node.getOID( ) );
          externalIDToLocationPath.put(node.getExternalID(), node.getHierarchyPath() );
          THierarchyNode zone = node.getParent();
          externalIDToZone.put(node.getExternalID( ), zone.getOID());
//        logDebug("Store -->"+node.getHierarchyPath() +", zone= "+zone.getExternalID()+", "+zone.getName());
//        logDebug("Added hierarchy path -->"+node.getHierarchyPath() +"<--, "+node.getExternalID()+", "+node.getName());
        }
      }
      logInfo( "Read " + externalIDToLocation.size( ) + " existing zone/store locations" );

    }
  }

  private void logInfo( String infoMessage )
  {
    ObjectFactory.getLogManager( ).getStaticLogger( ).addLogEntry(
        this.getClass( ), StaticLogger.INFO, 0, infoMessage );
  }
  private void logWarn( String infoMessage )
  {
    ObjectFactory.getLogManager( ).getStaticLogger( ).addLogEntry(
        this.getClass( ), StaticLogger.WARN, 0, infoMessage );
  } 
  private void logError( String infoMessage )
  {
    ObjectFactory.getLogManager( ).getStaticLogger( ).addLogEntry(
        this.getClass( ), StaticLogger.ERROR, 0, infoMessage );
  }

  public String getItemCode(String itemOID)
  {
    String code = null;
    TBoxItem item = oidToItem.get(itemOID);
    if (item == null)
    {
      logWarn("Item not found by OID=" + itemOID);
    } else
    {
      try
      {
        code = item.getCode();
      } catch (TSDKInternalException e)
      {
        logError("Item Not found for oid=" + itemOID + ". error:" + e);
      }
    }
    return code;
  }

}
