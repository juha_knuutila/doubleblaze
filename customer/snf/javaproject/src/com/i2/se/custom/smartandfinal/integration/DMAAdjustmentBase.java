package com.i2.se.custom.smartandfinal.integration;

import java.io.*;
import java.text.*;
import java.time.temporal.ChronoUnit;
import java.util.*;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.app.integrations.imports.TranslatorException;
import com.i2.se.app.integrations.imports.data.*;
import com.i2.se.bd.catalog.THierarchy;
import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.pricer.TAdjustment;

public abstract class DMAAdjustmentBase
{
  private static final String ADJUSTMENT_SCHEDULE_ID_POSTFIX = "-SCHEDULE";
  
  BackBusContainer target;
  ProductDataCache productDataCache;
  SAFSettings settings;
  File importFile;

  SAFConverterBase converter;
  DateFormat df;
  DateFormat dfForName = new SimpleDateFormat("YYYY-MM-dd");
  Set<String> adjustmentNodeIds;
  Set<String> allPostFixes;
  Set<String> adjustmentIds;
  String pricingRuleName;
  String adjustmentNamePattern;

  String adjustmentIdPattern;
  Date pricingStartDate = null;
  
  static Date adjustmentIDStartDate;
  
  static {
    try
    {
      adjustmentIDStartDate = (new SimpleDateFormat("MM/dd/yyyy")).parse("01/01/2000");
    } catch (ParseException e)
    {
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(DMAAdjustmentBase.class, StaticLogger.ERROR, 0, "Could not parse adjustment ID startdate,"+e);
      System.exit(0);
    } 
  }
  
  protected DMAAdjustmentBase(DMAFileTranslator dmaFileTranslator, BackBusContainer target,
      ProductDataCache productDataCache, SAFSettings settings, File importFile, DateFormat df,
      Set<String> adjustmentNodeIds, Set<String> allPostFixes, Set<String> adjustmentIds, Date pricingStartDate)
  {
    this.converter = dmaFileTranslator;
    this.target = target;
    this.productDataCache = productDataCache;
    this.settings = settings;
    this.importFile = importFile;
    this.df = df;
    this.allPostFixes = allPostFixes;
    this.adjustmentNodeIds = adjustmentNodeIds;
    this.adjustmentIds = adjustmentIds;
    this.pricingStartDate = pricingStartDate;
  }
  
  public static long betweenDates(Date firstDate, Date secondDate) throws IOException
  {
      return ChronoUnit.DAYS.between(firstDate.toInstant(), secondDate.toInstant());
  }

  boolean createAdjustment(BackBusContainer target, ProductDataCache productDataCache, SAFSettings settings,
      String[] elements, int trueLineNo, String itemCode, DateFormat df2, File importFile, String itemOID)
      throws TranslatorException
  {
    converter.logInfo("--Creating adjustment with rule: "+pricingRuleName+"---, line=" + trueLineNo);
    if( StringUtils.isEmpty(pricingRuleName)) {
      throw new TranslatorException("Empty pricing rule name row "+trueLineNo+ " price type = "+elements[DMAFileFields.PRC_EFF_PRICE_TYPE]);
    }

    String startDate = elements[DMAFileFields.PRC_EFF_SDATE];
    String endDate = elements[DMAFileFields.PRC_EFF_EDATE];
    String store = elements[DMAFileFields.STORE_ID];

    Date effectiveStartDate = parseDate(df, trueLineNo, importFile, startDate);
    Date effectiveEndDate = parseDate(df, trueLineNo, importFile, endDate);

    String messageInCaseOfError = "Error reading price rule parameters";
    List<PricingRuleParameter> priceRuleParams = getRuleParameters(productDataCache, pricingRuleName, trueLineNo,
        messageInCaseOfError);
    if (priceRuleParams == null)
    {
      return false;
    }

    long startOffset = 0, endOffset=0;
    try {
      startOffset = betweenDates(adjustmentIDStartDate, effectiveStartDate);
      endOffset = betweenDates(adjustmentIDStartDate, effectiveEndDate);
    } catch( IOException e) {
      throw new TranslatorException(importFile.getName() + ": Could not calculate start/enddate offset \"" + effectiveStartDate +", "+effectiveEndDate+ "\", line"
          + trueLineNo + ": " + e.getMessage(), e);
    }
    
    String formattedStartDate = effectiveStartDate == null ? "N/A" : dfForName.format(effectiveStartDate);
    String formattedEndDate = effectiveEndDate == null ? "N/A" : dfForName.format(effectiveEndDate);
    String adjustmentName = String.format(adjustmentNamePattern, elements[DMAFileFields.STORE_ID], formattedStartDate, formattedEndDate);

    // Use start date from filename?
    if( settings.getDMAUseFileDateAsPricingDateForAdjustments() ) 
    {
      effectiveStartDate = pricingStartDate;
      if(effectiveStartDate.after(effectiveEndDate)) {
        converter.logWarn("Adjustment will not be created as end date will be before the start date: "+dfForName.format(effectiveStartDate) + ", end:"+formattedEndDate+
            ", line=" + trueLineNo);
        return false;
      }
    }
    // Convert start and end dates to fixed start
    Date fixedStartDate = settings.getDmaFixedStartDate();
    if( effectiveStartDate.before(fixedStartDate)) {
      effectiveStartDate = fixedStartDate;
    }
    if( effectiveEndDate.before(fixedStartDate)) {
      effectiveEndDate = fixedStartDate;
    }
    String adjId = String.format(adjustmentIdPattern, store,  ""+startOffset, ""+endOffset);

    converter.logInfo("AdjName: " + adjustmentName+ ", id: "+adjId);
    converter.logDebug("Store/StartDate:" + elements[DMAFileFields.STORE_ID] + "/" + elements[DMAFileFields.PRC_EFF_SDATE]);
    converter.logDebug("Validity " + effectiveStartDate + " - " + effectiveEndDate+ ", original: "+formattedStartDate);
    
//    String defaultLHierarchyID = settings.getDefaultLocationId(); // todo: what is this
    String postFixID = adjId; // TODO or id?
    String valueLocation = itemCode + "_" + postFixID;
    String valueGroupId = "VG_" + valueLocation;
    String valueId = "V_" + valueLocation;
    String xRefId = "VX_" + valueLocation;
    String locationNodeId = "L_" + adjId; // + adGroup;
    String locationExtNodeId = locationNodeId + "_XREF";
//    String defaultLocationNodeId = "L-DEF_" + adjId;
    String defaultLocationExtNodeId = "L_" + adjId + "_XREF";
    String customerId = "C-ALL_" + adjId;
    String userNodeId = "U-ALL_" + adjId;
    String userExtNodeId = userNodeId + "_XREF";
    String customerExtNodeId = customerId + "_XREF";
    String productNodeId = itemCode + "_" + adjId;
    String pExtNodeId = productNodeId + "_XREF";

    if (allPostFixes.contains(postFixID))
    {
      converter.logInfo("Appending to existing adjustment:" + postFixID);
    }
    allPostFixes.add(postFixID);

    createBaseAdjustment(target, productDataCache, settings, elements, trueLineNo, effectiveStartDate, effectiveEndDate,
        pricingRuleName, priceRuleParams, adjustmentName, adjId, postFixID);

    createAdjustmentValueGroup(target, adjId, valueGroupId);
    createAdjustmentValue(target, adjId, valueGroupId, valueId);
    addRuleParameters(settings, target, priceRuleParams, itemCode, adjId, valueGroupId, valueId, elements, trueLineNo);
    
    String lExtNodeId = defaultLocationExtNodeId;
    String locationHierarchyID = productDataCache.getExternalIDToLocation(store);
    converter.logDebug("Hierarchy ID:" + locationHierarchyID);
    addHierarchyNode(target, adjustmentNodeIds, adjId, locationNodeId, locationExtNodeId, THierarchy.LOCATION,
        locationHierarchyID);

    createAdjustmentValueXref(target, adjId, valueGroupId, xRefId, userExtNodeId, customerExtNodeId, pExtNodeId,
        lExtNodeId);

    addHierarchyNode(target, adjustmentNodeIds, adjId, customerId, customerExtNodeId, THierarchy.CUSTOMER,
        Hierarchy.CUSTOMER_HIERARCHY_ROOT);
    addHierarchyNode(target, adjustmentNodeIds, adjId, userNodeId, userExtNodeId, THierarchy.USER,
        Hierarchy.USER_HIERARCHY_ROOT);

    addProductNode(target, itemCode, itemOID, adjId, pExtNodeId);

    return true;
  }

  
  void addRuleParameters(SAFSettings settings, BackBusContainer target, List<PricingRuleParameter> priceRuleParams,
      String itemCode, String adjId, String valueGroupId, String valueId, String[] elements, int trueLineNo)
  {
    int count = 0;
    for (PricingRuleParameter param : priceRuleParams)
    {
      String paramName = param.getName();
      String paramId = "P" + count + "-" + itemCode + "-" + adjId;
      PriceAdjustmentParameter priceParam = new PriceAdjustmentParameter();
      priceParam.setPriceAdjustmentID(adjId);
      priceParam.setPriceAdjustmentValueGroupID(valueGroupId);
      priceParam.setPriceAdjustmentValueID(valueId);
      priceParam.setPriceAdjustmentParameterID(paramId);
      priceParam.setPriceRuleParameterName(paramName);
      priceParam.setIndex(count);

      if(!setCustomRuleParams(paramName, priceParam, elements, trueLineNo))
      {
        priceParam.setValue(param.getDefaultValue());
        priceParam.setValueCurrencyID(param.getDefaultCurrency());
      }

      target.addPriceAdjustmentParameter(priceParam);

      count++;
    }
  }

  abstract boolean setCustomRuleParams(String paramName, PriceAdjustmentParameter priceParam, String[] elements, int trueLineNo);

  protected void addHierarchyNode(BackBusContainer target, Set<String> nodeIds, String adjId, String nodeId,
      String nodeExtId, int hierarchyType, String rootId)
  {
    if (!nodeIds.contains(nodeId))
    {
      PriceAdjustmentNode node = new PriceAdjustmentNode();
      node.setPriceAdjustmentID(adjId);
      node.setHierarchyType(hierarchyType);
      node.setHierarchyID(rootId);
      node.setPriceAdjustmentNodeID(nodeId);
      node.setPriceAdjustmentNodeXReferenceNodeID(nodeExtId);
      target.addPriceAdjustmentNode(node);

      nodeIds.add(nodeId);
    }
  }

  private void addProductNode(BackBusContainer target, String itemCode, String itemOID, String adjId, String pExtNodeId)
  {
    PriceAdjustmentNode product = new PriceAdjustmentNode();
    product.setPriceAdjustmentID(adjId);
    product.setHierarchyType(THierarchy.PRODUCT);
    product.setItemID(itemOID);

    product.setPriceAdjustmentNodeXReferenceNodeID(pExtNodeId);
    product.setPriceAdjustmentNodeID(itemCode + "_" + adjId);
    target.addPriceAdjustmentNode(product);
  }

  private void createAdjustmentValueXref(BackBusContainer target, String adjId, String valueGroupId, String xRefId,
      String userExtNodeId, String customerExtNodeId, String pExtNodeId, String lExtNodeId)
  {
    PriceAdjustmentValueXReference xref = new PriceAdjustmentValueXReference();
    xref.setPriceAdjustmentValueXReferenceID(xRefId);
    xref.setPriceAdjustmentID(adjId);
    xref.setPriceAdjustmentValueGroupID(valueGroupId);
    xref.setUserNodeID(userExtNodeId);
    xref.setCustomerNodeID(customerExtNodeId);
    xref.setProductNodeID(pExtNodeId);
    xref.setLocationNodeID(lExtNodeId);
    target.addPriceAdjustmentValueXReference(xref);
  }

  private int getPricetype(String[] elements, int trueLineNo, String adjustmentName)
  {
    int priceTypeInt = 0;
    String priceType = elements[DMAFileFields.PRC_EFF_PRICE_TYPE];
    if (StringUtils.isNumeric(priceType))
    {
      priceTypeInt = Integer.parseInt(priceType);
    } else
    {
      converter.logInfo("Using default priceType=0 as value is not valid numeric = '" + priceType + "', line: "
          + trueLineNo + ", adj=" + adjustmentName);
    }
    return priceTypeInt;
  }

  private void createBaseAdjustment(BackBusContainer target, ProductDataCache productDataCache, SAFSettings settings,
      String[] elements, int trueLineNo, Date effectiveStartDate, Date effectiveEndDate, String pricingRuleName,
      List<PricingRuleParameter> priceRuleParams, String adjustmentName, String adjId, String postFixID)
  {
    String adjustmentId = productDataCache.getEventToAdjId(adjId);
    if (adjustmentIds.contains(adjId))
    {
      converter.logDebug("Adjustment exists: " + adjId);
      adjustmentId = adjId;
    }
    if (adjustmentId == null)
    {
      int priceTypeInt = getPricetype(elements, trueLineNo, adjustmentName);

      PriceAdjustment adjustment = new PriceAdjustment();
      adjustment.setPriceAdjustmentID(adjId);
      adjustment.setEventID(adjId);
      adjustment.setPriceAdjustmentName(adjustmentName);
      // adjustment.setAdjustmentGroupPath( "/List Price" );
      adjustment.setPriceAdjustmentType(TAdjustment.ADJUST_TYPE);

      adjustment.setPriceRuleName(pricingRuleName);
      adjustment.setPriceType(priceTypeInt);

      target.addPriceAdjustment(adjustment);

      // Adjustment Description
      PriceAdjustmentName adjustmentNameObj = new PriceAdjustmentName();
      adjustmentNameObj.setPriceAdjustmentID(adjId);
      adjustmentNameObj.setProductLanguageID(settings.getProductLanguageID());
      adjustmentNameObj.setPriceAdjustmentName(adjustmentName);
      target.addPriceAdjustmentName(adjustmentNameObj);

      // Adjustment Schedule
      String scheduleID = ADJUSTMENT_SCHEDULE_ID_POSTFIX + "_" + postFixID;

      // Schedule
      PriceAdjustmentSchedule schedule = new PriceAdjustmentSchedule();
      schedule.setAdjustmentScheduleID(scheduleID);
      schedule.setPriceAdjustmentID(adjId);
      schedule.setIndex(0);
      schedule.setStartDate(effectiveStartDate);
      schedule.setEndDate(effectiveEndDate);
      target.addPriceAdjustmentSchedule(schedule);
      
      addDefaultParameters(priceRuleParams, pricingRuleName, adjId, postFixID, target, trueLineNo);
      adjustmentIds.add(adjId);
    }
  }

  protected Date parseDate(DateFormat df, int trueLineNo, File importFile, String startDate) throws TranslatorException
  {
    Date parsedDate = null;
    if (!startDate.isEmpty())
    {
      try
      {
        parsedDate = df.parse(startDate);
      } catch (ParseException e)
      {
        throw new TranslatorException(importFile.getName() + ": Invalid date format \"" + startDate + "\", line"
            + trueLineNo + ": " + e.getMessage(), e);
      }
    }
    return parsedDate;
  }

  private void createAdjustmentValue(BackBusContainer target, String adjId, String valueGroupId, String valueId)
  {
    PriceAdjustmentValue defaultValue = new PriceAdjustmentValue();
    defaultValue.setPriceAdjustmentID(adjId);
    defaultValue.setPriceAdjustmentValueGroupID(valueGroupId);
    defaultValue.setPriceAdjustmentValueID(valueId);
    target.addPriceAdjustmentValue(defaultValue);
  }

  private void createAdjustmentValueGroup(BackBusContainer target, String adjId, String valueGroupId)
  {
    PriceAdjustmentValueGroup defaultValueGroup = new PriceAdjustmentValueGroup();
    defaultValueGroup.setPriceAdjustmentID(adjId);
    defaultValueGroup.setDefaultFlag(false);
    defaultValueGroup.setPriceAdjustmentValueGroupID(valueGroupId);
    target.addPriceAdjustmentValueGroup(defaultValueGroup);
  }

  private void addDefaultParameters(List<PricingRuleParameter> priceRuleParams, String priceRuleName,
      String adjustmentId, String event_no, BackBusContainer target, int trueLineNo)
  {
    String defaultVg = "DefVG_" + event_no;
    String defaultValueId = "DefV_" + event_no;

    PriceAdjustmentValueGroup defaultValueGroup = new PriceAdjustmentValueGroup();
    defaultValueGroup.setPriceAdjustmentID(adjustmentId);
    defaultValueGroup.setDefaultFlag(true);
    defaultValueGroup.setEmptyFlag(false);
    defaultValueGroup.setPriceAdjustmentValueGroupID(defaultVg);
    target.addPriceAdjustmentValueGroup(defaultValueGroup);

    createAdjustmentValue(target, adjustmentId, defaultVg, defaultValueId);
    int count = 0;

    for (PricingRuleParameter param : priceRuleParams)
    {
      String paramName = param.getName();
      int paramCount = count + 1;
      String paramId = "P" + paramCount + "-" + event_no;
      PriceAdjustmentParameter defaultPromoPriceParam = new PriceAdjustmentParameter();
      defaultPromoPriceParam.setPriceAdjustmentID(adjustmentId);
      defaultPromoPriceParam.setPriceAdjustmentValueGroupID(defaultVg);
      defaultPromoPriceParam.setPriceAdjustmentValueID(defaultValueId);
      defaultPromoPriceParam.setPriceAdjustmentParameterID(paramId);
      defaultPromoPriceParam.setPriceRuleParameterName(paramName);
      defaultPromoPriceParam.setValue(param.getDefaultValue());
      defaultPromoPriceParam.setValueCurrencyID(param.getDefaultCurrency());
      target.addPriceAdjustmentParameter(defaultPromoPriceParam);
      count++;
    }
  }

  private List<PricingRuleParameter> getRuleParameters(ProductDataCache productDataCache, String pricingRuleName,
      int trueLineNo, String messageInCaseOfError)
  {
    List<PricingRuleParameter> priceRuleParams = productDataCache.getPriceRuleToParams().get(pricingRuleName);

    if (priceRuleParams == null || priceRuleParams.isEmpty())
    {
      converter.logWarn(messageInCaseOfError + " Line: " + trueLineNo + ", ruleName=" + pricingRuleName);
      priceRuleParams = null;
    }

    return priceRuleParams;
  }

}
