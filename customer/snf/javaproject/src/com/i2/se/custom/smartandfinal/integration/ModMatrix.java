package com.i2.se.custom.smartandfinal.integration;

import java.io.*;
import java.util.*;

import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.pricer.*;
import com.i2.se.bd.util.bo.session.*;

public class ModMatrix
{
  private static Map<String,String> matrixNames = new HashMap<String, String>();
  private static String MOD_MATRIX_MOD_NAME;
  
  private Map<String, String> matrixRowToMod = new HashMap<String, String>();
  
  static {
    Properties p = new Properties();
    try {
      InputStream is = Thread.currentThread().getContextClassLoader()
          .getResourceAsStream("SmartAndFinal.properties");
      p.load(is);
      String MATRIX_NAMES = p.getProperty("BusinessConstraints.PromoCalendar.MatrixName", "");
      String[] matrixes = MATRIX_NAMES.split(",");
      for( String m : matrixes )
      {
        String yearName[] = m.split("\\|");
        matrixNames.put(yearName[0], yearName[1]);
      }
      
      MOD_MATRIX_MOD_NAME = p.getProperty("BusinessConstraints.PromoCalendar.Col.ModName", "1.name");

    } catch (IOException e) {
      logError("Error reading constraint properties: " + e);
    }
  }
  
  public ModMatrix(TSession session)
  {
    try
    {
      Set<String> yearkeys = matrixNames.keySet();
      for (String yearkey : yearkeys)
      {
        TMatrix matrix = session.getPricer().getMatrixByName(matrixNames.get(yearkey));
        TMatrixVersion mver = matrix.getVersionByDate(Calendar.getInstance().getTime());

        TMatrixDimensionKeys[] keys = mver.getDimensionKeys();
        String[] keyval = keys[1].getKeys();
        for (String k : keyval)
        {
          String name = getMatrixValue(mver, MOD_MATRIX_MOD_NAME, k);
          matrixRowToMod.put(yearkey + "_"+ name, k);
        }
      }
    } catch (TSDKInternalException e)
    {
      e.printStackTrace();
    }
  }
  
  public String getDimensionKey( String year, String modValue ) {
    String modMatrixValue = matrixRowToMod.get( year+"_"+modValue);
    logInfo("ASK:"+modValue+" = "+modMatrixValue);
    return modMatrixValue;
  }
  
  private String getMatrixValue(TMatrixVersion mver, String xkey, String ykey) {
    String val = null;
    String[] skeys = new String[] { xkey, ykey };
    try {
      val = mver.getValue(skeys);
      logInfo("getting: [" +xkey+","+ykey+"] "+ val);
       
    } catch (Exception e) {
      logError("err: [" +xkey+","+ykey+"] "+ e.getMessage());
    }
    return val;
  }
  
  private static void logError(String message)
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(ModMatrix.class, StaticLogger.ERROR, 0, message);
  }
  private static void logWarn(String message)
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(ModMatrix.class, StaticLogger.WARN, 0, message);
  }
  private static void logInfo(String message)
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(ModMatrix.class, StaticLogger.INFO, 0, message);
  }
}
