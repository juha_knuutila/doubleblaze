package com.i2.se.custom.smartandfinal.integration;

import java.io.*;
import java.math.BigDecimal;
import java.text.*;
import java.util.*;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.app.integrations.imports.TranslatorException;
import com.i2.se.app.integrations.imports.data.*;
import com.i2.se.bd.catalog.THierarchy;
import com.i2.se.bd.pricer.*;
import com.i2.se.bd.util.bo.session.*;

public class PromoFileTranslator extends SAFConverterBase
{
  private static final String ADJUSTMENT_SCHEDULE_ID_POSTFIX = "-SCHEDULE";
  private static final Integer PRMO_MULTIPLE_MIN_QTY = 1;
  private static final Integer DEFAULT_MIN_QTY = 0;

  private Set<String> adjustmentIds = new HashSet<String>( );
  private Set<String> adjustmentNodeIds = new HashSet<String>( );
  private AdGroups adGroups = null;
  
  protected String getAdapterName()
  {
    return "PromoCop";
  }
  
  /**
   * removes quotes 
   * @param text
   * @return
   */
  private String getTextUnquoted(String text)
  {
    if( text != null ) {
      text = text.replaceAll("^\"|\"$", "");
    }
    return text;
  }
  
  // Columns
  // event_no,event_desc,mod,startDate,endDate,smartaDv,priceType,adGroup,storeOverride,itemId,upc,dealAmount,
  // promoCost,pmId,promoMultiple,promoPrice,limitQty,pkgMultiple,pkgPrice,discAmount,sfiFamilyCode,subPromo,
  // dispLoc,merchLoc,print,media,sourceId,priceLevel,signPrint,signDesc,updatedOn,fgDesc,isFamilyPrice
  //
  
  public int doConvert(TSession session, File importFile, BackBusContainer target, SAFSettings settings,
      ProductDataCache productDataCache) throws TranslatorException, IOException
  {
    // initialize Ad Group priority file
    adGroups = new AdGroups(settings);
    ModMatrix modMatrix = new ModMatrix(session);
    
    BufferedReader bf = null;
    int trueLineNo = 0;
    int ignoredLinesCount = 0;
    int commentLineCount = 0;
    
    try
    {
      bf = new BufferedReader(
          new InputStreamReader( new FileInputStream( importFile ) ) );
      
      SimpleDateFormat inputFileDateFormatter = new SimpleDateFormat( settings.getDateFieldFormat() );
      
      List<String> productGroupIds = settings.getProductGroupIds();
      String[] existingDnamicGroupNames = readExistingGroups(session.getPricer());
      
      while( true )
      {
        String s = bf.readLine( );
        if( s == null )
        {
          break;
        }
        ++trueLineNo;
        if (StringUtils.isEmpty(s) || StringUtils.startsWith(s, "#"))
        {
          logInfo("Comment line " + trueLineNo + " : " + s);
          commentLineCount++;
          continue;
        }
        String regEx = "\\|(?=(?:[^\"]*\"[^\"]*)*[^\"]*$)";
        String[] elements = s.split( regEx, -1 );
        

        int i = 0;
        String event_no = elements[ i++ ];
        String event_desc = getTextUnquoted(elements[ i++ ]);
        String mod = getTextUnquoted(elements[ i++ ]);
        String startDate = getTextUnquoted(elements[ i++ ]);
        String endDate = getTextUnquoted(elements[ i++ ]);
        String smartaDv = getTextUnquoted(elements[ i++ ]);
        String priceType = getTextUnquoted(elements[ i++ ]);
        String adGroup = getTextUnquoted(elements[ i++ ]);
        String storeOverride = getTextUnquoted(elements[ i++ ]);
        String itemId = getTextUnquoted(elements[ i++ ]);
        String upc = getTextUnquoted(elements[ i++ ]);
        String dealAmount = getTextUnquoted(elements[ i++ ]);
        String promoCost = getTextUnquoted(elements[ i++ ]);
        String pmId = getTextUnquoted(elements[ i++ ]);
        String promoMultiple = getTextUnquoted(elements[ i++ ]);
        String promoPrice = parseNumber( elements[ i++ ], trueLineNo, "PromoPrice" );
        String limitQty = getTextUnquoted(elements[ i++ ]);
        String pkgMultiple = getTextUnquoted(elements[ i++ ]);
        String pkgPrice = getTextUnquoted(elements[ i++ ]);
        String discAmount = getTextUnquoted(elements[ i++ ]);
        String sfiFamilyCode = getTextUnquoted(elements[ i++ ]);
        String subPromo = getTextUnquoted(elements[ i++ ]);
        String dispLoc = getTextUnquoted( elements[ i++ ]);
        String merchLoc = getTextUnquoted(elements[ i++ ]);
        String print = getTextUnquoted(elements[ i++ ]);
        String media = getTextUnquoted(elements[ i++ ]);
        String sourceId = getTextUnquoted(elements[ i++ ]);
        String priceLevel = getTextUnquoted(elements[ i++ ]);
        String signPrint = getTextUnquoted(elements[ i++ ]);
        String signDesc = getTextUnquoted(elements[ i++ ]);
        String updatedOn = getTextUnquoted(elements[ i++ ]);
        String fgDesc = getTextUnquoted(elements[ i++ ]);
        String promoQtyPrefix = "0";
        
        Date effectiveStartDate = parseDate(inputFileDateFormatter, trueLineNo, importFile, startDate);
        Date effectiveEndDate = parseDate(inputFileDateFormatter, trueLineNo, importFile, endDate);
        Calendar c = Calendar.getInstance();
        c.setTime(effectiveStartDate);
        String startYear = ""+c.get(Calendar.YEAR);
        logDebug( "Start year  " + startYear );
        
        String pricerMod = modMatrix.getDimensionKey(startYear, mod);

        if( "".equals( event_no ) )
        {
          logWarn( "Error reading Event no for  " + itemId
                  + ": Event number is empty, line: " + trueLineNo );
          continue;
        }
        
        if (settings.promoSpecialEvents())
        {
          if (event_no.charAt(4) != '1')
          {
            logWarn("Special event " + event_no + ", line: " + trueLineNo);
            mod = "";
            pricerMod = "";
          }
        }
        
        String familyPrice = "0";
        if ( i < elements.length ) 
        {
          familyPrice = getTextUnquoted(elements[ i++ ]);

          if( "1".equals(familyPrice) || "Y".equalsIgnoreCase(familyPrice) ) 
          {
            if( StringUtils.isEmpty(sfiFamilyCode) || "0".equals(sfiFamilyCode) )
            {
              logDebug("0-family code, will use item itself. "+ itemId+". At line: "+trueLineNo);
            }
            else
            {
              // find family item
              String familyItem = sfiFamilyCode + "_family";
              String[] itemOIDs = productDataCache.getCodeToOids(familyItem);
              if (itemOIDs != null)
              {
                // doing the magic,
                logDebug("USING family item: " + familyItem + ", instead of: " + itemId + ". At line: " + trueLineNo);
                itemId = familyItem;
              } else
              {
                logWarn("Row is flagged as family row, but family item not found: " + familyItem + ", item: " + itemId
                    + ". At line: " + trueLineNo);
              }
            }
          }

        } else {
          logWarn("Family item column missing ");
        }
        
        String adjId = event_no + "_" + subPromo + "_" + adGroup + "_" + DEFAULT_MIN_QTY;
        
        if( StringUtils.isEmpty( pricerMod ) ) {
          logWarn("Pricer Mod value not found for MOD = "+mod+", line: " + trueLineNo + ", adj="+adjId );
        }
        
        if( !"1".equals( priceLevel ) && "2".equals( priceLevel ) ) {
          logWarn("Not valid price level = "+priceLevel+", line: " + trueLineNo + ", adj="+adjId );
        }
        
        String priceRuleName = "";

        int promoQty;

        if( StringUtils.isNumeric( promoMultiple ) )
        {
          promoQty = Integer.parseInt( promoMultiple );
          if( promoQty == 0 ) {
            logWarn( "Ignoring line, due PromoMultiple=0, line: " + trueLineNo + ", adj="+adjId);
            ignoredLinesCount++;
            continue;
          }
        }
        else
        {
          logWarn( "Error reading PromoMultiple value for  " + itemId
                  + ": value =\""+promoMultiple+"\" not a number, line: " + trueLineNo + ", adj="+adjId);
          ignoredLinesCount++;
          continue;
        }

        int priceTypeInt = 0;

        if( StringUtils.isNumeric( priceType ) )
        {
          priceTypeInt = Integer.parseInt( priceType );
          priceTypeInt = settings.getConvertedPriceType(priceTypeInt);
        } else {
          logInfo( "Using default priceType=0 as value is not valid numeric = '"+priceType+"', line: " + trueLineNo + ", adj="+adjId);
        }

        priceRuleName = getPricingRuleName(settings, priceRuleName, promoQty);
        
        if( promoQty > 1 )
        {
          adjId = event_no + "_" + subPromo + "_" + adGroup + "_" + PRMO_MULTIPLE_MIN_QTY;
          promoQtyPrefix = "1";
        }

        String dynamicGroupName = getDynamicGroupName(settings, existingDnamicGroupNames, adGroup);
        logInfo("Effective dynamic group name: "+dynamicGroupName);
       
        String itemOID = null;
        String[] itemOIDs = productDataCache.getCodeToOids(itemId);
        if (itemOIDs != null)
        {
          for (int idx = 0; idx < itemOIDs.length; idx++)
          {
            String group = productDataCache.getOidToPG(itemOIDs[idx]);
            for (String groupId : productGroupIds)
            {
              if (group.equals(groupId))
              {
                itemOID = itemOIDs[idx];
                break;
              }
            }
            if (itemOID != null)
            {
              break;
            }
          }
        }
        
        if (itemOID == null)
        {
          logWarn("Error reading item  " + itemId + ": item not found! Line: " + trueLineNo + ", adj=" + adjId);
          ignoredLinesCount++;
          continue;
        }
       
        List<PricingRuleParameter> priceRuleParams = productDataCache.getPriceRuleToParams( )
            .get( priceRuleName );

        if( priceRuleParams == null )
        {
          logWarn( "Error reading price rule  for promo quantity  " + promoQty
              + ": Price rule not found! Line: " + trueLineNo + ", adj="+adjId );
          ignoredLinesCount++;
          continue;
        }

        if( priceRuleParams.isEmpty( ) )
        {
          logWarn( "Error reading price rule parameters for item " + itemId
                  + ": Price rule parameters not found! Line: "
                  + trueLineNo + ", adj="+adjId );
          ignoredLinesCount++;
          continue;
        }

        String itemId_forOID = itemId;
        
        int pos = itemId.indexOf("_family" );
        
        if( pos > 0)
        {
          itemId_forOID = itemId.substring(0, pos+2);
        }
        
        String defaultLHierarchyID = settings.getDefaultLocationId( );
        String postFixID = event_no + "_" + promoQtyPrefix + "_" + subPromo + "_" + adGroup;
        String valueLocationId = itemId_forOID + "_" + postFixID; // + "_" + adGroup;
        String valueGroupId = "VG_" + valueLocationId;
        String valueId = "V_" + valueLocationId;
        String xRefId = "VX_" + valueLocationId;
        String locationNodeId = "L_" + postFixID; // + adGroup;
        String locationExtNodeId = locationNodeId + "_XREF";
        String defaultLocationNodeId = "L-DEF_" + postFixID;
        String defaultLocationExtNodeId = "L_" + postFixID + "_XREF";
        String customerId = "C-ALL_" + postFixID;
        String userNodeId = "U-ALL_" + postFixID;
        String userExtNodeId = userNodeId + "_XREF";
        String customerExtNodeId = customerId + "_XREF";
        String productNodeId = itemId_forOID + "_" + postFixID;
        String pExtNodeId = productNodeId + "_XREF";

        String adjustmentId = productDataCache.getEventToAdjId( adjId );

        if( adjustmentIds.contains( adjId ) )
        {
          adjustmentId = adjId;
        }

        if( adjustmentId == null )
        {
          // Adjustment
          PriceAdjustment adjustment = new PriceAdjustment( );
          adjustment.setPriceAdjustmentID( adjId );
          adjustment.setEventID( event_no );
          adjustment.setPriceAdjustmentName( adjId );
          // adjustment.setAdjustmentGroupPath( "/List Price" );
          adjustment.setPriceAdjustmentType( TAdjustment.ADJUST_TYPE );
          
          adjustment.setPriceRuleName( priceRuleName );
          adjustment.setPriceType( priceTypeInt );
          int priority = adGroups.getAdGroupPriority( adGroup );
          if( priority == -1 ) {
            logWarn( "Ad group not found from priority file: " + adGroup + ": Line: "+ trueLineNo + ", adj="+adjId );
            continue;
          }
          adjustment.setPriority( ""+priority );
          target.addPriceAdjustment( adjustment );
         
          // Adjustment Description
          PriceAdjustmentName adjustmentName = new PriceAdjustmentName( );
          adjustmentName.setPriceAdjustmentID( adjId );
          adjustmentName.setProductLanguageID( settings.getProductLanguageID( ) );
          adjustmentName.setPriceAdjustmentName(event_no + "__" + event_desc);
          target.addPriceAdjustmentName( adjustmentName );

          // Adjustment Schedule
          String scheduleID = ADJUSTMENT_SCHEDULE_ID_POSTFIX + "_" + postFixID;

       
          // Schedule
          PriceAdjustmentSchedule schedule = new PriceAdjustmentSchedule( );
          schedule.setAdjustmentScheduleID( scheduleID );
          schedule.setPriceAdjustmentID( adjId );
          schedule.setIndex( 0 );
          schedule.setStartDate( effectiveStartDate );
          schedule.setEndDate( effectiveEndDate );
          target.addPriceAdjustmentSchedule( schedule );

          addDefaultParameters( priceRuleParams, priceRuleName, adjId, postFixID, promoMultiple, pricerMod,
              target, trueLineNo );

          adjustmentIds.add( adjId );

        }

        PriceAdjustmentValueGroup defaultValueGroup = new PriceAdjustmentValueGroup( );
        defaultValueGroup.setPriceAdjustmentID( adjId );
        defaultValueGroup.setDefaultFlag( false );
        defaultValueGroup.setPriceAdjustmentValueGroupID( valueGroupId );
        target.addPriceAdjustmentValueGroup( defaultValueGroup );

        PriceAdjustmentValue defaultValue = new PriceAdjustmentValue( );
        defaultValue.setPriceAdjustmentID( adjId );
        defaultValue.setPriceAdjustmentValueGroupID( valueGroupId );
        defaultValue.setPriceAdjustmentValueID( valueId );
        target.addPriceAdjustmentValue( defaultValue );

        addPriceRuleParameters( settings, priceRuleParams, priceRuleName, adjId, postFixID, itemId,
            priceLevel, promoMultiple, promoPrice, discAmount, mod, valueGroupId, valueId,
            pricerMod, adGroup, dispLoc,
            target, trueLineNo, itemId_forOID );
      
        TAdjustmentNode dgNode = productDataCache.getDynamicGroupAdjNode( dynamicGroupName );

        // decide location XREF id based on dynamic group existence
        String lExtNodeId;
        if( dgNode != null )
        {
          int count = 0;
          lExtNodeId = locationExtNodeId;

          TDynamicGroupNode[] dgNodes = dgNode.getDynamicGroupNodes( );

          if( dgNodes == null || dgNodes.length == 0 )
          {
            logWarn( "Dynamic group :" + dynamicGroupName + "  found but not valid. Line:   "
                + trueLineNo + ", adj="+adjId );
            ignoredLinesCount++;
            continue;
          }
          for( TDynamicGroupNode dynamicGroupNode : dgNode.getDynamicGroupNodes( ) )
          {
            count++;
            String LNodeId = locationNodeId + "_" + count;
            if( !adjustmentNodeIds.contains( LNodeId ) )
            {
              PriceAdjustmentNode location = new PriceAdjustmentNode( );
              location.setPriceAdjustmentID( adjId );
              location.setHierarchyType(
                  THierarchy.LOCATION );
              location.setDynamicGroupName( dynamicGroupName );
              if( dynamicGroupNode.isHierarchy( ) )
              {
                location.setHierarchyID( dynamicGroupNode.getHierarchy( ) );
              }
              else
              {
                location.setItemID( dynamicGroupNode.getItemID( ) );
              }
              location
                  .setPriceAdjustmentNodeID( LNodeId );
              location
                  .setPriceAdjustmentNodeXReferenceNodeID( lExtNodeId );
              target.addPriceAdjustmentNode( location );

              adjustmentNodeIds.add( LNodeId );
            }
          }

        }
        else
        {
          logWarn( "Dynamic Group nodes not found for adgroup: " + adGroup + ", line " + trueLineNo + ", adj="+adjId );

          lExtNodeId = defaultLocationExtNodeId;
          addHierarchyNode(target, adjustmentNodeIds, adjId, defaultLocationNodeId, defaultLocationExtNodeId, THierarchy.LOCATION, defaultLHierarchyID);
        }

        PriceAdjustmentValueXReference xref = new PriceAdjustmentValueXReference( );
        xref.setPriceAdjustmentValueXReferenceID( xRefId );
        xref.setPriceAdjustmentID( adjId );
        xref.setPriceAdjustmentValueGroupID( valueGroupId );
        xref.setUserNodeID( userExtNodeId );
        xref.setCustomerNodeID(
            customerExtNodeId );
        xref.setProductNodeID( pExtNodeId );
        xref.setLocationNodeID( lExtNodeId );
        target.addPriceAdjustmentValueXReference( xref );

        addHierarchyNode(target, adjustmentNodeIds, adjId, customerId, customerExtNodeId, THierarchy.CUSTOMER, Hierarchy.CUSTOMER_HIERARCHY_ROOT);
        addHierarchyNode(target, adjustmentNodeIds, adjId, userNodeId, userExtNodeId, THierarchy.USER, Hierarchy.USER_HIERARCHY_ROOT);
       
        PriceAdjustmentNode product = new PriceAdjustmentNode( );
        product.setPriceAdjustmentID( adjId );
        product.setHierarchyType( THierarchy.PRODUCT );
        product.setItemID( itemOID );
        if( promoQty > PRMO_MULTIPLE_MIN_QTY )
        {
          product.setBuyGroup( true );
          product.setGetGroup( true );
        }
        product.setPriceAdjustmentNodeXReferenceNodeID( pExtNodeId );
        product.setPriceAdjustmentNodeID( itemId_forOID + "_" + postFixID );
        target.addPriceAdjustmentNode( product );
      }

      logInfo( "Read " + trueLineNo + " lines" );
      if( ignoredLinesCount>0 ) {
        logWarn( "Skipped/ignored lines count : "+ ignoredLinesCount );
      }
      if (commentLineCount > 0)
      {
        logWarn("Comment line count : " + commentLineCount);
      }
      
      // TODO: purge old
      
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
      throw new TranslatorException(
          "Error reading file, line " + trueLineNo + ": " + e.getMessage( ), e );
    }
    catch( IOException e )
    {
      throw new TranslatorException(
          "IOException reading file, line " + trueLineNo + ": " + e.getMessage( ), e );
    } catch (TSDKInternalException e)
    {
      throw new TranslatorException(
          "Error reading dynamic groups, line " + trueLineNo + ": " + e.getMessage( ), e );
    }
    finally
    {
      if( bf != null )
      {
        bf.close( );
      }
    }
    return trueLineNo;
  }
  
  private String getDynamicGroupName(SAFSettings settings, String[] dynamicGroupNames, String adGroup)
  {
    String name = settings.getDynamicGroupPrefix() + adGroup;
    name = getCurrentAdGroupNameByBaseName(dynamicGroupNames, name);
    
    return name;
  }

  private String getPricingRuleName(SAFSettings settings, String priceRuleName, int promoQty)
  {
    Set<Integer> keys = settings.getPriceRulesPerQuantity( ).keySet( );

    for( int key : keys )
    {
      if( key <= promoQty )
      {
        priceRuleName = settings.getPriceRulesPerQuantity( ).get( key );
        logDebug("PROMORULE: "+key+"/"+ promoQty+ " - "+priceRuleName );
      }
    }
    return priceRuleName;
  }

  private void addPriceRuleParameters( ImportSettings settings,
      List<PricingRuleParameter> priceRuleParams,
      String priceRuleName,
      String adjId,
      String event_no, String itemId, String priceLevel, String promoMultiple,
      String promoPrice, String discountAmt, String modId, String valueGroupId, String valueId,
      String pricerMod,
      String adGroup, String dispLoc, BackBusContainer target, int trueLineNo, String itemId_forOID )
  {
   // logWarn("-mod:"+modId+", discountAmt:"+discountAmt);
    boolean isFixedPromoPriceEach = false;
    boolean isCentsOffPriceEach = false;
    boolean isFixedPromoPriceCase = false;
    boolean isCentsOffPriceCase = false;
    boolean isPromoMultiple = false;
    int promoQty = 0;

    if( priceLevel != null && promoMultiple != null )
    {
      if( "0".equals( priceLevel ) && "1".equals( promoMultiple ) )
      {
        isFixedPromoPriceEach = true;
        isCentsOffPriceEach = true;
      }

      if( "1".equals( priceLevel ) && "1".equals( promoMultiple ) )
      {
        isFixedPromoPriceCase = true;
        isCentsOffPriceCase = true;
      }

      promoQty = Integer.parseInt( promoMultiple );

      if( promoQty > 1 )
      {
        isPromoMultiple = true;
      }
    }

    String promoMutliPrice = settings.getPromoMultiplePrice( );
    String prmoQtyPrice = settings.getPromoQty( );
    String fixedPromoEach = settings.getFixedPromoPriceEach( );
    String fixedPromoCase = settings.getFixedPromoPriceCase( );
    String centsEach = settings.getCentsOffPriceEach( );
    String cetsCase = settings.getCentsOffPriceCase( );
    String mod = settings.getMod( );
    String pricerModName = settings.getPricerMod();
    String AdGroupParam = settings.getAdGroupParam();
    String dispLocParam = settings.getDispLocParam();

    
    int count = 0;
    for( PricingRuleParameter param : priceRuleParams )
    {
      String paramName = param.getName( );
      String paramId = "P" + count + "-" + itemId_forOID + "-" + event_no;
      PriceAdjustmentParameter priceParam = new PriceAdjustmentParameter( );
      priceParam.setPriceAdjustmentID( adjId );
      priceParam.setPriceAdjustmentValueGroupID( valueGroupId );
      priceParam.setPriceAdjustmentValueID( valueId );
      priceParam.setPriceAdjustmentParameterID( paramId );
      priceParam.setPriceRuleParameterName( paramName );
      priceParam.setIndex( count );

      if( isPromoMultiple && promoMutliPrice.equals( paramName ) )
      {
        priceParam.setValue( promoPrice );
      }
      else if( isPromoMultiple && prmoQtyPrice.equals( paramName ) )
      {
        priceParam.setValue( promoMultiple );
      }
      else if( isFixedPromoPriceEach && fixedPromoEach.equals( paramName ) )
      {
        if( "0".equals(promoPrice) && ( StringUtils.isEmpty(discountAmt) || "0".equals(discountAmt) ))  {
          logDebug("PROMO DISCAMOUNT "+itemId+" - "+ adjId);
        } else { 
          priceParam.setValue( promoPrice );
        }
      }
      else if( isFixedPromoPriceCase && fixedPromoCase.equals( paramName ) )
      {
        priceParam.setValue( promoPrice );
      }
      else if( isCentsOffPriceEach && centsEach.equals( paramName ) )
      {
        if( "0".equals(promoPrice) && ( StringUtils.isEmpty(discountAmt) || "0".equals(discountAmt) ))  {
          priceParam.setValue( "0" );
        } else {
          priceParam.setValue( discountAmt );
        }
      }
      else if( isCentsOffPriceCase && cetsCase.equals( paramName ) )
      {
        if( "0".equals(promoPrice) && ( StringUtils.isEmpty(discountAmt) || "0".equals(discountAmt) ))  {
          priceParam.setValue( "0" );
        } else {
          priceParam.setValue( discountAmt );
        }
      }
      else if( mod.equalsIgnoreCase( paramName ) )
      {
        priceParam.setValue( modId );
      }
      else if (pricerModName.equalsIgnoreCase( paramName )) 
      {
        priceParam.setValue( pricerMod );
      }
      else if( AdGroupParam.equalsIgnoreCase(paramName))
      {
        priceParam.setValue(adGroup);
      }
      else if( dispLocParam.equalsIgnoreCase(paramName))
      {
        String value = "Y".equalsIgnoreCase(dispLoc) ? "1" : "0";
        priceParam.setValue(value);
      }
      else
      {
        priceParam.setValue( param.getDefaultValue( ) );
        priceParam.setValueCurrencyID( param.getDefaultCurrency( ) );
      }

      target.addPriceAdjustmentParameter( priceParam );

      count++;
    }

  }

  private void addDefaultParameters( List<PricingRuleParameter> priceRuleParams,
      String priceRuleName,
      String adjustmentId,
      String event_no, String promoMultiple, String pricerMod, BackBusContainer target, int trueLineNo )
  {
    String defaultVg = "DefaultVG_" + event_no + "_" + promoMultiple;
    String defaultValueId = "DefaultV_" + event_no + "_" + promoMultiple;

    PriceAdjustmentValueGroup defaultValueGroup = new PriceAdjustmentValueGroup( );
    defaultValueGroup.setPriceAdjustmentID( adjustmentId );
    defaultValueGroup.setDefaultFlag( true );
    defaultValueGroup.setEmptyFlag( false );
    defaultValueGroup.setPriceAdjustmentValueGroupID( defaultVg );
    target.addPriceAdjustmentValueGroup( defaultValueGroup );

    PriceAdjustmentValue defaultValue = new PriceAdjustmentValue( );
    defaultValue.setPriceAdjustmentID( adjustmentId );
    defaultValue.setPriceAdjustmentValueGroupID( defaultVg );
    defaultValue.setPriceAdjustmentValueID( defaultValueId );
    target.addPriceAdjustmentValue( defaultValue );
    int count = 0;

//    logWarn("default vaiues: ");

    for( PricingRuleParameter param : priceRuleParams )
    {
      String paramName = param.getName( );
      int paramCount = count + 1;
      String paramId = "P" + paramCount + "-" + event_no + "_" + promoMultiple;
      PriceAdjustmentParameter defaultPromoPriceParam = new PriceAdjustmentParameter( );
      defaultPromoPriceParam.setPriceAdjustmentID( adjustmentId );
      defaultPromoPriceParam.setPriceAdjustmentValueGroupID( defaultVg );
      defaultPromoPriceParam.setPriceAdjustmentValueID( defaultValueId );
      defaultPromoPriceParam.setPriceAdjustmentParameterID( paramId );
      defaultPromoPriceParam.setPriceRuleParameterName( paramName );
      
//      logWarn("default: "+paramName+", "+param.getDefaultValue());
      defaultPromoPriceParam.setValue(param.getDefaultValue());
      defaultPromoPriceParam.setValueCurrencyID(param.getDefaultCurrency());
      target.addPriceAdjustmentParameter(
          defaultPromoPriceParam );
      count++;
    }
  }

  private String parseNumber( String rawValue, int trueLineNo, String column )
  {
    if( StringUtils.isEmpty(rawValue) ) {
      logWarn( "Empty value in column "+column+". Expecting number value, line: "+trueLineNo);
    } else {
    try
    {
      new BigDecimal( rawValue );
    }
    catch( NumberFormatException e )
    {
      String converted = rawValue.replace( ',', '.' );
      logWarn( "Invalid number format " + rawValue + " at column "+column+", line " + trueLineNo + " - converting to "
          + converted );
      return converted;
    }
    }
    return rawValue;
  }


}
