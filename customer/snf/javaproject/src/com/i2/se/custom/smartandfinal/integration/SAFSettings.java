package com.i2.se.custom.smartandfinal.integration;

import java.io.*;
import java.text.*;
import java.util.*;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;

public class SAFSettings extends ImportSettings {

  public static final String PUBLISH_LEVEL_APPROVED = "Approved";
  public static final String PUBLISH_LEVEL_PROPOSED = "Proposed";
  
	private String smartBuyTypeAttribute = "SmartBuyType";
	private String scaleIndicatorAttribute = "ScaleIndicator";
	private String styleItemAttribute = "EachCaseLink";
	private String itemStatusAttribute = "ItemStatus";
	private String dateFieldFormat = "d/M/yyyy";
	private String scpoDateFieldFormat = "";
  private String importLogFile = "";
	private String parentBusinessOids = "";
	private String priceGroupNameFormat = "";
	private String priceFileNamePattern = "";
	private String recommendedPriceFileNamePattern = "";
	private String dMAFileNamePattern = "";
	private String mdmItemCodeAttribute = "";
	private String generateCaseItemFile = "";
	private String priceApprovalState = "";
	private int priceApprovalProposedPriceLevel=1;
	private String DMADateFieldFormat = "";
	private String pricingRuleRegularRetailMB;
	private boolean enabledRegularRetailBM;
	private boolean enabledWeeklySpecial;
  
	Map<String,String> companyParentMap = new HashMap<String,String>();
  private boolean enabledRegularPrice;
  private boolean enabledBMSM; 
  private boolean enabledHardLimitDiscount;
  private boolean enabledClearance;
  private boolean enabledMixAndMatch;
  
  private Map<String, String> ruleParamNames = new HashMap<String,String>();
  
  private String adjustmentNamePatternRegularRetailMB;
  private String adjustmentNamePatternBMSM;
  private String adjustmentNamePatternWeeklySpecial;
  private String adjustmentNamePatternHardLimitDiscount;
  private String adjustmentNamePatternClearance;
  
  private String pricingRuleBMSM;
  private String pricingRuleWeeklySpecial;
  private String pricingRuleHardLimitDiscount;
  private String pricingRuleClearance;
  private Map<String, String> promoSymbolMap = new HashMap<String,String>();
  private int familyItemClassId;
  
  private Date dmaFixedStartDate = null;
  private boolean zonePricesEnabled;
  private String currentAdGroupsFileName;
  private String currentAdGroupStoresFileName;
  private boolean dMAUseFileDateAsPricingDate;
  private boolean dMAUseFileDateAsPricingDateForAdjustments;
  private String mixAndMatchAttributeName;
  private String dmaRegularRetailPriceGroup;
  private Set<String> specialZones;
  private String specialZonePriceGroup;
  private String specialZoneBusinessPostFix;
  private int pricingDateOffset;
  private boolean promoSpecialEvents;
  private boolean initialLoad;
  private String assortmentPriceBook;
  private String regularRetailMBPriceBook;
  
	public SAFSettings(Map<String, String> properties) {
		super(properties);
		this.smartBuyTypeAttribute = getProperty(properties, "SmartBuyTypeAttribute", "SmartBuyType");
		this.scaleIndicatorAttribute = getProperty(properties, "ScaleIndicatorAttribute", "ScaleIndicator");
		this.styleItemAttribute = getProperty(properties, "StyleItemAttribute", "EachCaseLink");
		this.itemStatusAttribute = getProperty(properties, "ItemStatusAttribute", "ItemStatus");
		this.dateFieldFormat = getProperty(properties, "DateFieldFormat", "d/M/yyyy");
    this.scpoDateFieldFormat = getProperty(properties, "scpo.DateFieldFormat", "d/M/yyyy");
		this.parentBusinessOids = getProperty(properties, "CompanyParentBusinessOID","");
		this.importLogFile = getProperty(properties, "ImportLogFile", "");
		this.priceFileNamePattern = getProperty(properties, "File.PriceFileNamePattern", "");
		this.recommendedPriceFileNamePattern = getProperty(properties, "File.RecommendedPriceFileNamePattern", "");
		this.dMAFileNamePattern = getProperty(properties, "File.DMAFileNamePattern", "");
		this.mdmItemCodeAttribute = getProperty(properties, "MDMItemCodeAttribute", "");
		this.priceGroupNameFormat = getProperty(properties, "PriceGroupNameFormat", ""); 
		this.generateCaseItemFile = getProperty(properties, "GenerateCaseItemFile", "");
		this.priceApprovalState = getProperty(properties, "PriceApprovalState", "");
		this.priceApprovalProposedPriceLevel = getProperty(properties, "PriceApprovalProposedPriceLevel", 1);
		this.DMADateFieldFormat  = getProperty(properties, "DMADateFieldFormat", "");
		this.pricingRuleRegularRetailMB = getProperty(properties, "dma.PricingRuleRegularRetailMB", "");
		this.pricingRuleBMSM = getProperty(properties, "dma.PricingRuleBMSM", "");
		this.pricingRuleWeeklySpecial = getProperty(properties, "dma.PricingRuleWeeklySpecial", "");
		this.pricingRuleHardLimitDiscount = getProperty(properties, "dma.PricingRuleHardLimitDiscount", "");
		this.pricingRuleClearance = getProperty(properties, "dma.PricingRuleClearance", "");
		this.mixAndMatchAttributeName = getProperty(properties, "MixAndMatchAttributeName", "MixAndMatch");
		this.dmaRegularRetailPriceGroup = getProperty(properties, "DmaRegularRetailPriceGroup", "");
		this.specialZonePriceGroup = getProperty(properties, "SpecialZonePriceGroup", "");
		this.specialZoneBusinessPostFix = getProperty(properties, "SpecialZoneBusinessPostFix", "");
		
		this.enabledRegularRetailBM = getProperty(properties, "dma.enabled.RegularRetailMB", false);
		this.enabledBMSM = getProperty(properties, "dma.enabled.BMSM", false);
		this.enabledRegularPrice = getProperty(properties, "dma.enabled.RegularPrice", false);
		this.enabledWeeklySpecial = getProperty(properties, "dma.enabled.WeeklySpecial", false);
		this.enabledHardLimitDiscount = getProperty(properties, "dma.enabled.HardLimitDiscount", false);
		this.enabledClearance = getProperty(properties, "dma.enabled.Clearance", false);
		this.enabledMixAndMatch = getProperty(properties, "dma.enabled.MixAndMatch", false);
		
		this.currentAdGroupsFileName = getProperty(properties, "adgroups.CurrentAdGroupsFileName", "");
		this.currentAdGroupStoresFileName = getProperty(properties, "adgroups.CurrentAdGroupStoresFileName", "");
		
		this.adjustmentNamePatternRegularRetailMB = getProperty( properties, "dma.adjustmentNamePatternRegularRetailMB", "");
		this.adjustmentNamePatternBMSM = getProperty(properties, "dma.adjustmentNamePatternBMSM", "");
		this.adjustmentNamePatternWeeklySpecial = getProperty(properties, "dma.adjustmentNamePatternWeeklySpecial", "");
		this.adjustmentNamePatternHardLimitDiscount = getProperty(properties, "dma.adjustmentNamePatternHardLimitDiscount", "");
		this.adjustmentNamePatternClearance = getProperty(properties, "dma.adjustmentNamePatternClearance", "");
		String zonePrices = getProperty(properties, "ZonePricesEnabled", "");
		this.zonePricesEnabled = "true".equalsIgnoreCase(zonePrices) || "1".equals(zonePrices);
		
		String promoSpecialEventsStr = getProperty(properties, "PromoSpecialEvents", "");
		this.promoSpecialEvents = "true".equalsIgnoreCase(promoSpecialEventsStr) || "1".equals(promoSpecialEventsStr);
		
		String initialLoadStr = getProperty(properties, "InitialLoad.Enabled", "");
		this.initialLoad = "true".equalsIgnoreCase(initialLoadStr) || "1".equals(initialLoadStr);
		    
		this.assortmentPriceBook = getProperty(properties, "InitialLoad.AssortmentPriceBook", "");
		this.regularRetailMBPriceBook = getProperty( properties, "dma.RegulaRetailMB.PriceBook", "");
		
		String familyItemClassIdStr = getProperty(properties, "FamilyItemClassId", "");
		familyItemClassId = Integer.parseInt(familyItemClassIdStr); // throws parse exception if value is invalid number
		String pricingDateOffsetStr = getProperty(properties, "PricingDateOffset", "0");
		pricingDateOffset = Integer.parseInt(pricingDateOffsetStr);
		
		this.dMAUseFileDateAsPricingDate = getProperty(properties, "dma.DMAUseFileDateAsPricingDate", false);
		this.dMAUseFileDateAsPricingDateForAdjustments = getProperty(properties, "dma.DMAUseFileDateAsPricingDateForAdjustments", false);
		
		String dmaFixedStartDateStr = getProperty(properties, "dma.FixedStartDate", "2000-01-01");
		DateFormat df = new SimpleDateFormat("yyyy-M-d");
		try
    {
      dmaFixedStartDate = df.parse(dmaFixedStartDateStr);
    } catch (ParseException e)
    {
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(), StaticLogger.ERROR, 0, "Invalid Property: dma.FixedStartDate ="+dmaFixedStartDateStr+","+e);
      System.exit(0);
    }
		
		if( StringUtils.isEmpty( this.parentBusinessOids ) ) {
		  ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(), StaticLogger.ERROR, 0, "Property not found: CompanyParentBusinessOID");
		  System.exit(0);
		}
		String []companyParent = this.parentBusinessOids.split("\\,");
    for( String cp: companyParent) {
      String [] busOid = cp.split("\\|");
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(), StaticLogger.WARN, 0, "CP "+busOid[0]+","+ busOid[1]);
      companyParentMap.put(busOid[0], busOid[1]);
    }
    String []ruleParamNamesProperty = getProperty(properties, "dma.RuleParamNames", "").split("\\,");
    for( String rp: ruleParamNamesProperty ) {
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(), StaticLogger.WARN, 0, "RuleParam "+rp);
      String [] ruleParam = rp.split("\\|");
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(), StaticLogger.WARN, 0, "RuleParam "+ruleParam[0]+","+ ruleParam[1]);
      ruleParamNames.put(ruleParam[0], ruleParam[1]);
    }
    String []promoSymbolMapProperty = getProperty(properties, "dma.PromoSymbolMap", "").split("\\,");
    for( String rp: promoSymbolMapProperty ) {
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(), StaticLogger.WARN, 0, "PromoSymbolMapProperty "+rp);
      String [] ruleParam = rp.split("\\|");
      promoSymbolMap.put(ruleParam[0], ruleParam[1]);
    }
    
    String []szones = getProperty(properties, "SpecialZones", "").split("\\,");
    specialZones = new HashSet<String>();
    for( String zone : szones )
    {
      specialZones.add(zone);
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(), StaticLogger.WARN, 0, "Special ZOne: "+zone);
      
    }
	}

	public String getSmartBuyTypeAttribute() {
		return smartBuyTypeAttribute;
	}

	public String getScaleIndicatorAttribute() {
		return scaleIndicatorAttribute;
	}

	public String getStyleItemAttribute() {
		return styleItemAttribute;
	}

	public String getItemStatusAttribute() {
		return itemStatusAttribute;
	}

	public String getDateFieldFormat()
  {
    return dateFieldFormat;
  }
	
  public String getScpoDateFieldFormat()
  {
    return scpoDateFieldFormat;
  }

  public String getParentBusinessOidForCompany(String companyNumber)
  {
    String parentOid = companyParentMap.get(companyNumber);
    if( StringUtils.isEmpty(parentOid)) {
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(), StaticLogger.WARN, 0, "Empty parent OID for company:"+companyNumber);
    }
    return parentOid;
  }
  
  public FileOutputStream getImportLogFile(String adapterName)
  {
    FileOutputStream logFile=null;
    
    if( StringUtils.isNotEmpty(importLogFile) ) {
      String logFileName = MessageFormat.format(importLogFile, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()), adapterName);
      try
      {
        logFile = new FileOutputStream(logFileName);
      } catch (FileNotFoundException e)
      {
        ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(), StaticLogger.WARN, 0, 
            "Could not open import log file:"+logFileName+", "+e);
      }
    }
    
    return logFile;
  }
  
  public String getPriceGroupNameFormat()
  {
    return priceGroupNameFormat;
  }
  
  public String getPriceFileNamePattern()
  {
    return priceFileNamePattern;
  }
  
  public String getRecommendedPriceFileNamePattern()
  {
    return recommendedPriceFileNamePattern;
  }
  
  public String getDMAFileNamePattern()
  {
    return dMAFileNamePattern;
  }

  @Override
  public String toString()
  {
    return "SAFSettings [smartBuyTypeAttribute=" + smartBuyTypeAttribute + ", scaleIndicatorAttribute="
        + scaleIndicatorAttribute + ", styleItemAttribute=" + styleItemAttribute + ", itemStatusAttribute="
        + itemStatusAttribute + ", dateFieldFormat=" + dateFieldFormat
        + ", parentBusinessOids=" + parentBusinessOids + "]";
  }

  public String getMDMItemCodeAttribute()
  {
     return mdmItemCodeAttribute;
  }

  public String getGenerateCaseItemFile()
  {
    return generateCaseItemFile;
  }
  
  public String getPriceApprovalState()
  {
    return priceApprovalState;
  }
  
  public int getPriceApprovalProposedPriceLevel()
  {
    return priceApprovalProposedPriceLevel;
  }

  public String getDMADateFieldFormat()
  {
    return DMADateFieldFormat;
  }

  public String getPricingRuleRegularRetailMB()
  {
    return pricingRuleRegularRetailMB;
  }

  public boolean isEnabledRegularRetailBM()
  {
    return enabledRegularRetailBM;
  }

  public boolean isEnabledWeeklySpecial()
  {
    return enabledWeeklySpecial;
  }

  public boolean isEnabledRegularPrice()
  {
    return enabledRegularPrice;
  }
  
  public boolean isEnabledBMSM()
  {
    return enabledBMSM;
  }
 
  public boolean isEnabledHardLimitDiscount()
  {
    return enabledHardLimitDiscount;
  }

  public boolean isEnabledClearance()
  {
    return enabledClearance;
  }
 
  public String getRuleParamName(String name)
  {
    return getRuleParamName(name, name);
  }
  
  public String getRuleParamName(String name, String defValue)
  {
    String value = ruleParamNames.get(name);
    if( StringUtils.isEmpty(value) ) {
      value = defValue;
    }
    return value;
  }

  public String getAdjustmentNamePatternRegularRetailMB()
  {
    return adjustmentNamePatternRegularRetailMB;
  }

  public String getAdjustmentNamePatternBMSM()
  {
    return adjustmentNamePatternBMSM;
  }

  public String getAdjustmentNamePatternWeeklySpecial()
  {
   return adjustmentNamePatternWeeklySpecial;
  }

  public String getAdjustmentNamePatternHardLimitDiscount()
  {
    return adjustmentNamePatternHardLimitDiscount;
  }

  public String getAdjustmentNamePatternClearance()
  {
     return adjustmentNamePatternClearance;
  }

  public String getPricingRuleBMSM()
  {
    return pricingRuleBMSM;
  }

  public String getPricingRuleWeeklySpecial()
  {
    return pricingRuleWeeklySpecial;
  }

  public String getPromoSymbolValue( String symbol ) 
  {
    return promoSymbolMap.get(symbol);
  }

  public String getPricingRuleClearance()
  {
    return pricingRuleClearance;
  }

  public String getPricingRuleHardLimitDiscount()
  {
     return pricingRuleHardLimitDiscount;
  }

  public int getFamilyItemClassId()
  {
    return familyItemClassId;
  }

  public Date getDmaFixedStartDate()
  {
    return dmaFixedStartDate;
  }

  public boolean isZonePricesEnabled()
  {
     return zonePricesEnabled;
  }
  
  public boolean isEnabledMixAndMatch()
  {
    return enabledMixAndMatch;
  }
  
  public String getCurrentAdGroupsFileName()
  {
    return currentAdGroupsFileName;
  }

  public String getCurrentAdGroupStoresFileName()
  {
    return currentAdGroupStoresFileName;
  }

  public boolean getDMAUseFileDateAsPricingDate()
  {
    return dMAUseFileDateAsPricingDate;
  }

  public boolean getDMAUseFileDateAsPricingDateForAdjustments()
  {
    return dMAUseFileDateAsPricingDateForAdjustments;
  }

  public String getMixAndMatchAttributeName()
  {
    return mixAndMatchAttributeName;
  }

  public String getDMARegularRetailPriceGroup()
  {
    return dmaRegularRetailPriceGroup;
  }

  public Set<String> getSpecialZones()
  {
    return specialZones;
  }

  public String getSpecialZonePriceGroup()
  {
    return specialZonePriceGroup;
  }

  public String getSpecialZoneBusinessPostFix()
  {
    return specialZoneBusinessPostFix;
  }

  public int getPricingDateOffset()
  {
    return pricingDateOffset;
  }

  public boolean promoSpecialEvents()
  {
    return promoSpecialEvents;
  }

  public boolean isInitialLoad()
  {
    return initialLoad;
  }

  public String getAssortmentPriceBook()
  {
    return assortmentPriceBook;
  }
  
  public String getRegularRetailMBPriceBook()
  {
    return regularRetailMBPriceBook;
  }
  
}
