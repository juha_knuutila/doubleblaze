package com.i2.se.custom.smartandfinal.integration;

import java.io.*;
import java.util.*;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.app.integrations.imports.*;
import com.i2.se.app.integrations.imports.data.BackBusContainer;
import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;

/**
 * Adapter for Grocery Industry.
 * 
 * @author J1013162
 *
 */
public class GroceryStoreAdapter implements ImportProvider
{
  private SAFSettings settings;

  // MDM
  private static final String STORE_FILENAME = "Store";
  private static final String COMPONENTHIERARCHY_FILENAME = "ProductHierarchy";
  private static final String ITEM_FILENAME = "Item";
  private static final String PRICE_FILENAME = "Price";
  public static final String ADGROUPS_FILENAME = "AdGroups";
  public static final String ADGROUPSTORES_FILENAME = "AdGroupStores";

  // SP
  private static final String RECOMMENDED_PRICE_FILENAME = "RecommendedPrice";
  // Promocops
  private static final String PROMO_FILENAME = "Promo";
  // DMA
  private static final String DMA_FILENAME = "DMA";

  private FileOutputStream logFile = null;

  private static String FEED_VARIABLE = "IMPORT_FEED";

  private static final String[] adapterFiles = { STORE_FILENAME, COMPONENTHIERARCHY_FILENAME, ITEM_FILENAME,
      PRICE_FILENAME, RECOMMENDED_PRICE_FILENAME, PROMO_FILENAME, DMA_FILENAME, ADGROUPS_FILENAME,
      ADGROUPSTORES_FILENAME };

  private void logInfo(String infoMessage)
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(), StaticLogger.INFO, 0, infoMessage);
  }

  private void logError(String infoMessage)
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(), StaticLogger.ERROR, 0, infoMessage);
  }

  public boolean parseProperties(Map<String, String> properties)
  {
    settings = new SAFSettings(properties);
    return true;
  }

  public List<File> acceptFiles(Map<String, List<File>> importFiles)
  {
    String feedName = System.getenv(FEED_VARIABLE);
    if (feedName != null)
    {
      logError("FeedName from env variable, importing:" + feedName);
    }
    List<File> rejectedFiles = new ArrayList<File>();
    for (String functionalName : adapterFiles)
    {
      List<File> matchingFiles = importFiles.get(functionalName);
      if (matchingFiles != null && !matchingFiles.isEmpty())
      {
        if (feedName != null && !functionalName.equals(feedName))
        {
          rejectedFiles.addAll(matchingFiles);
          importFiles.remove(functionalName);
          logInfo("Skipping: " + functionalName);
          continue;
        }
        logInfo("Accept: " + functionalName);
        if( ADGROUPS_FILENAME.equals(functionalName) ) {
          ArrayList<File> helper = new ArrayList<File>();
          helper.addAll(matchingFiles);
          for (File importFile : helper)
          {
            String storesFileName = AdGroupTranslator.getStoreFile(importFile);
            File additionalFile = new File(storesFileName);
            matchingFiles.add(additionalFile);
          }
        }
      }
    }
    // if( acceptedFiles.isEmpty() ) { acceptedFiles.add(new File("dummyapadter.txt")); }
    return rejectedFiles;
  }

  private void writeToLogFile(String message)
  {
    if (logFile != null)
    {
      try
      {
        logFile.write(message.getBytes());
        logFile.write('\r');
        logFile.write('\n');
      } catch (IOException e)
      {
        e.printStackTrace();
      }
    }
  }

  public void consumeFiles(PricerImport parent, Map<String, List<File>> importFiles) throws Exception
  {
    boolean filesImported = false;
    String names = "";
    String feedName = System.getenv(FEED_VARIABLE);
    if (feedName != null)
    {
      logError("FeedName from env variable, importing:" + feedName);
    }
    if (StringUtils.isEmpty(feedName))
    {
      for (String k : importFiles.keySet())
      {
        names += "_" + k;
      }
    } else
    {
      names = feedName;
    }
    logFile = settings.getImportLogFile(names);

    try
    {
      if (settings.isCleanWorkDirBeforeStart())
      {
        logInfo("Clean work directory before start.");
        parent.cleanWorkDir();
      }

      long start = Calendar.getInstance().getTimeInMillis();
      writeToLogFile("Import started " + Calendar.getInstance().getTime());

      BackBusContainer transfer = new BackBusContainer();
      transfer.setDefaultObjectLimit(settings.getMaxNoOfRecords());

      ProductDataCache productDataCache = new ProductDataCache();
      productDataCache.loadProductData(parent.getSession(), settings);

      if (settings.isCreateBaseData())
      {
        BaseDataCreator baseData = new BaseDataCreator();
        baseData.createBaseData(transfer, settings);
      }

      List<File> processedFiles = new ArrayList<File>();
      boolean doNotProcess = false;
      for (String functionalName : adapterFiles)
      {
        List<File> matchingFiles = importFiles.get(functionalName);
        if (matchingFiles != null && !matchingFiles.isEmpty())
        {
          for (File importFile : matchingFiles)
          {
            if (importFile.exists() && !importFile.isDirectory())
            {
              if (feedName != null && !functionalName.equals(feedName))
              {
                logInfo("Skipping: " + functionalName);
                continue;
              }
              filesImported = true;

              File additionalFile = null;
              writeToLogFile("Processing file " + importFile.getName());
              writeToLogFile("Convert " + functionalName + " started " + Calendar.getInstance().getTime());
              GroceryStoreFileTranslator translator = null;

              switch (functionalName)
              {
                case STORE_FILENAME:
                  translator = new StoreFileTranslator();
                  break;
                case COMPONENTHIERARCHY_FILENAME:
                  translator = new ComponentHierarchyFileTranslator();
                  break;
                // case CATEGORIESHIERARCHY_FILENAME:
                // translator = new CategoriesHierarchyFileTranslator( );
                // break;
                // case SUBCATEGORIESHIERARCHY_FILENAME:
                // translator = new SubCategoriesHierarchyFileTranslator( );
                // break;
                case ITEM_FILENAME:
                  translator = new ItemFileTranslator();
                  break;
                case PRICE_FILENAME:
                  translator = new PriceFileTranslator();
                  break;
                case RECOMMENDED_PRICE_FILENAME:
                  translator = new RecommendedPriceFileTranslator();
                  break;
                case PROMO_FILENAME:
                  translator = new PromoFileTranslator();
                  break;
                case DMA_FILENAME:
                  translator = new DMAFileTranslator();
                  break;
                case ADGROUPS_FILENAME:
                  translator = new AdGroupTranslator();
                  String storesFileName = AdGroupTranslator.getStoreFile(importFile);
                  if( storesFileName == null ) 
                  {
                    logInfo("Skipping invalid groups file: "+importFile.getAbsolutePath());
                    doNotProcess = true;
                    break;
                  }
                  additionalFile = new File(storesFileName);
                  if (!additionalFile.exists())
                  {
                    writeToLogFile("AdGroups Stores file not found: " + storesFileName);
                    throw new TranslatorException("AdGroups Stores file not found: " + storesFileName);
                  }
                  break;

                default:
                  throw new TranslatorException("Translator missing for " + functionalName);
              }
              if (!doNotProcess)
              {
                logInfo("- process " + functionalName + " file " + importFile.getName());
                if (translator instanceof SAFTranslator)
                {
                  ((SAFTranslator) translator).convert(parent.getSession(), importFile, transfer, settings,
                      productDataCache, logFile, parent);
                } else
                {
                  translator.convert(importFile, transfer, settings, productDataCache);
                }
              }
              processedFiles.add(importFile);
              if (additionalFile != null)
              {
                processedFiles.add(additionalFile);
              }
            }
          }
        }
      }
      if (filesImported)
      {
        logInfo("persist data");
        parent.persistData(transfer);

        if (settings.isArchiveFiles())
        {
          logInfo("Archive imported files");
          for (File importFile : processedFiles)
          {
            if (importFile.exists() && !importFile.isDirectory())
            {
              parent.archiveFile(importFile);
            }
          }
        }
      } else
      {
        logInfo("No files processed.");
      }

      long end = Calendar.getInstance().getTimeInMillis();
      Calendar.getInstance().getTime();
      writeToLogFile("Import ended " + Calendar.getInstance().getTime());
      writeToLogFile("Import time in seconds: " + (end - start) / 1000);

    } catch (Throwable e)
    {
      writeToLogFile("Convert aborted at : " + Calendar.getInstance().getTime());
      e.printStackTrace();
      throw e;
    } finally
    {
      if (logFile != null)
      {
        logFile.close();
      }
    }
  }
}
