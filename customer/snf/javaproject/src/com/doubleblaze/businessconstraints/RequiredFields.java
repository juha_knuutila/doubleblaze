package com.doubleblaze.businessconstraints;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.pricer.TVariableValue;
import com.i2.se.bd.util.bo.session.TSDKInternalException;
import com.i2.se.bd.util.constraints.v_1_0.ConstraintContext;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemConstraintBase;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemParameters;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemResponse;

/**
 * 
 * @author juhak
 * 
 * Checks that all required fields are populated for viev type.
 * 
 * View type is first read from pricing variable: ListPriceView
 * if not found there then from environment variable: Constraints_ListPriceView
 * 
 */

public class RequiredFields extends PricerItemConstraintBase {

	static String[] views = null;
	static Map<String, String[]> requiredFieldsForView = new HashMap<String, String[]>();

	static {
		Properties p = new Properties();
		String propTypeValue = "BusinessConstraints.requiredFields.Views";
		String propTypeViewPrefix = "BusinessConstraints.requiredFields.view.";

		try {
			InputStream is = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("SmartAndFinal.properties");
			p.load(is);
			String str = p.getProperty(propTypeValue, "");
			if (str != null && str.length() > 0) {
				views = str.split("\\|");
			}

			ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PriceTypeRegular.class, StaticLogger.DEBUG, 0,
					"BusinessConstraints.requiredFields.Views" + str);

			String[] fields = null;
			for (String view : views) {
				str = p.getProperty(propTypeViewPrefix + view, "");
				if (str != null && str.length() > 0) {
					fields = str.split("\\|");
					requiredFieldsForView.put(view, fields);
				}
				ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PriceTypeRegular.class, StaticLogger.DEBUG,
						0, "Required fields for view " + view + " are " + str);
			}

		} catch (IOException e) {
			ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PricerItemConstraintBase.class,
					StaticLogger.ERROR, 0, "Error reading required fields properties: " + e);
		}
	}

	@Override
	public String getID() {
		return "Check Required Fields";
	}

	@Override
	public String getDescription() {
		return "Checks that required fields for the viewq are populated";
	}

	@Override
	public void initialize(ConstraintContext context) {
		super.initialize(context);
	}

	@Override
	public PricerItemResponse run(ConstraintContext context, PricerItemParameters params) {

		ArrayList<String> messages = new ArrayList<String>();
		int warningLevel = WARNING_LEVEL_OK;

		String currentView = null;
		try {
			TVariableValue tv = params.getPricingParameters().getVariable("ListPriceView");
			if( tv != null ) {
				currentView = tv.stringValue();
			}
		} catch (TSDKInternalException e) {
			ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PricerItemConstraintBase.class,
					StaticLogger.ERROR, 0, "Error finding pricing variable ListPriceView. " + e);
		}
		if( StringUtils.isEmpty(currentView))
		{
		  currentView = System.getenv("Constraints_ListPriceView");
		  ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PriceTypeRegular.class, StaticLogger.DEBUG,
          0, "--Read current view from env variable: " + currentView);
		}
		
		if (currentView != null && currentView.length() > 0) {
			String[] requiredFields = requiredFieldsForView.get(currentView);
			if (requiredFields != null) {
				for (String field : requiredFields) {
					int warn = WARNING_LEVEL_OK;
					BigDecimal price = params.getItem().getPrice(field);

					if (price == null) {
						warn = WARNING_LEVEL_HARD;
						messages.add(field + " is required.");

					} else {
						double profit = price.doubleValue();
						if (profit == 0.0) {
							warn = WARNING_LEVEL_SOFT;
							messages.add(field + " is zero.");
						} else {
							messages.add(field + " is ok.");
						}
					}
					if (warn > warningLevel) {
						warningLevel = warn;
					}
				}
			} else {
				messages.add("Required fields for view were not found. View=" + currentView);
				ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PricerItemConstraintBase.class,
						StaticLogger.ERROR, 0, "Required fields for view were not defined: " + currentView);
			}
		} else {
			messages.add("Required fields are not checked");
		}
		return context.getConstraintFactory().createPricerItemResponse(warningLevel, messages.toString(),
				messages.toString());
	}
}