package com.doubleblaze.businessconstraints;

import java.io.*;
import java.math.BigDecimal;
import java.text.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.pricer.*;
import com.i2.se.bd.util.bo.session.TSDKInternalException;
import com.i2.se.bd.util.constraints.v_1_0.*;

/**
 * 
 * @author juhak
 * 
 *         Checks adjustment MOD value matches adjustment start and end dates.
 * 
 *         1. If mod's end and start dates match adjustment's start and end
 *         dates then this constraint passes 2. otherwise hard constraint is
 *         raised.
 *
 */

public class ModCheck extends PricerItemConstraintBase
{
  private static String PRICER_MOD_PARAM_NAME;
  
  private static String MOD_MATRIX_MOD_NAME;
  private static String MOD_MATRIX_START_DATE;
  private static String MOD_MATRIX_END_DATE;
  private static String MOD_MATRIX_WEEKS;
  private static String MOD_MATRIX_SYMBOL;
  private static String PROMO_START_YEAR_VARIABLE;
  
  private DateFormat dateFormatter = null;
  private Map<String, ModCheckResult> checkedAdjustments = null;
  
  private static Map<String,String> matrixNames = new ConcurrentHashMap<String, String>();
  private static String RULES_WITH_MOD_PARAM;

  enum MOD_STATUS
  {
    OK, START_NOT_MATCH, END_NOT_MATCH, TOO_MANY_SCHEDULES, INVALID_MOD_VALUES
  };

  static
  {
    Properties p = new Properties();
    try
    {
      InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("SmartAndFinal.properties");
      p.load(is);
      PRICER_MOD_PARAM_NAME = p.getProperty("BusinessConstraints.variable.PRICER_MOD", "PRICER_MOD");

      String MATRIX_NAMES = p.getProperty("BusinessConstraints.PromoCalendar.MatrixName", "");
      String[] matrixes = MATRIX_NAMES.split(",");
      for( String m : matrixes )
      {
        String yearName[] = m.split("\\|");
        matrixNames.put(yearName[0], yearName[1]);
      }
      
      MOD_MATRIX_MOD_NAME = p.getProperty("BusinessConstraints.PromoCalendar.Col.ModName", "1.name");
      MOD_MATRIX_START_DATE = p.getProperty("BusinessConstraints.PromoCalendar.Col.StartDate", "2.start");
      MOD_MATRIX_END_DATE = p.getProperty("BusinessConstraints.PromoCalendar.Col.EndDate", "3.end");
      MOD_MATRIX_WEEKS = p.getProperty("BusinessConstraints.PromoCalendar.Col.Weeks", "4.weeks");
      MOD_MATRIX_SYMBOL = p.getProperty("BusinessConstraints.PromoCalendar.Col.Symbol", "5.symbol");
      RULES_WITH_MOD_PARAM = p.getProperty("BusinessConstraints.ModCheck.RulesWithModParam", "");
      PROMO_START_YEAR_VARIABLE = p.getProperty("BusinessConstraints.variable.PromoStartYear","");

      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(ModCheck.class, StaticLogger.DEBUG, 0,
          "Pricer MOD Variable: PRICER_MOD_PARAM_NAME=" + PRICER_MOD_PARAM_NAME);

    } catch (IOException e)
    {
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PricerItemConstraintBase.class, StaticLogger.ERROR, 0,
          "Error reading constraint properties: " + e);
    }
  }

  @Override
  public String getID()
  {
    return "MOD Check";
  }

  @Override
  public String getDescription()
  {
    return "Checks that adjustment's MOD mathces start and end dates";
  }

  @Override
  public void initialize(ConstraintContext context)
  {
    super.initialize(context);

    synchronized (this)
    {
      dateFormatter = new SimpleDateFormat("yyyyMMdd");
      checkedAdjustments = new ConcurrentHashMap<String, ModCheckResult>();
    }

    debug("----MOD CHECK INIT-----");
  }

  private void debug(String msg)
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PricerItemConstraintBase.class, StaticLogger.DEBUG, 0,
        msg);
  }

  private void error(String msg)
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PricerItemConstraintBase.class, StaticLogger.DEBUG, 0,
        msg);
  }

  private ModInfo getModInfo(TPricer pricer, String mod, String year) throws TSDKInternalException
  {
    String matrixNameForYear = matrixNames.get(year);
    TMatrix matrix = pricer.getMatrixByName(matrixNameForYear);
    debug("Reading Matrix: " + matrix.getName() + "/ for mod " + mod);
    TMatrixVersion mver = matrix.getVersionByDate(Calendar.getInstance().getTime());

    // printMatrixInfo(mver);
    try { 
      TMatrixDimensionKeys[] dkeys = mver.getDimensionKeys();
      if( !dkeys[1].containsKey(mod) ) {
        ModInfo mi = new ModInfo(mod, null, null, null, null, 0);
        
        StringBuffer sb = new StringBuffer();
        for( String k : dkeys[1].getKeys() ) {
          sb.append(k+ ", ");
        }
        mi.setModValueNotFound(true);
        debug("Mod not found:" + mod + " in keys: " + sb.toString() + ", mi="+mi);
        return mi;
      }
    } catch (Exception e)
    {
      error("Error checking dimension keys: "+e);
      ModInfo mi = new ModInfo(mod, null, null, null, null, 0);
      return mi;
    }
    
    String start = getMatrixValue(mver, MOD_MATRIX_START_DATE, mod);
    String end = getMatrixValue(mver, MOD_MATRIX_END_DATE, mod);
    String symbol = getMatrixValue(mver, MOD_MATRIX_SYMBOL, mod);
    String weeks = getMatrixValue(mver, MOD_MATRIX_WEEKS, mod);
    String name = getMatrixValue(mver, MOD_MATRIX_MOD_NAME, mod);

    Date startDate = getDate(start);
    Date endDate = getDate(end);

    int iWeeks = 0;
    if (weeks != null)
    {
      try
      {
        iWeeks = Integer.parseInt(weeks);

      } catch (NumberFormatException e)
      {
        error("Not a number:" + e.getMessage());
      }
    }

    ModInfo mi = new ModInfo(mod, name, startDate, endDate, symbol, iWeeks);
    debug("Found MOD:" + mod + ", " + mi);
    return mi;
  }

  private void printMatrixInfo(TMatrixVersion mver) throws TSDKInternalException
  {
    TMatrixDimensionKeys[] dkeys = mver.getDimensionKeys();
    for (int i = 0; i < dkeys.length; i++)
    {
      String skeys[] = dkeys[i].getKeys();
      debug("Matrix keys:" + dkeys[i] + "/" + dkeys[i].getKeys());
      for (int ii = 0; ii < skeys.length; ii++)
      {
        debug("  keys ii:" + skeys[ii]);
      }
    }
  }

  private Date getDate(String start)
  {
    Date date = null;
    try
    {
      if (start == null)
      {
        debug("-----------NULL DATE VALUE IN MATRIX------");
        return null;
      }

      synchronized (dateFormatter)
      {
        date = dateFormatter.parse(start);
      }

    } catch (ParseException e)
    {
      error("Cannot find matrix value:" + e.getMessage());
    }
    return date;
  }

  private String getMatrixValue(TMatrixVersion mver, String xkey, String ykey)
  {
    String val = null;
    String[] skeys = new String[] { xkey, ykey };
    try
    {
      val = mver.getValue(skeys);
    } catch (Exception e)
    {
      error("Error, trying to lookup values: [" + xkey + "," + ykey + "], error=" + e);
    }
    return val;
  }

  private String getFormattedDate(Date d)
  {
    if (d == null)
    {
      return "";
    }

    String dateStr = null;
    synchronized (dateFormatter)
    {
      dateStr = dateFormatter.format(d);
    }

    return dateStr;
  }

  private static class ModCheckResult
  {
    private MOD_STATUS modStatus = MOD_STATUS.OK;
    private String reason = "";

    public MOD_STATUS getModStatus()
    {
      return modStatus;
    }

    public String getReason()
    {
      return reason;
    }

    public void appendReason(String newReason)
    {
      reason += ", " + newReason;
    }

    public ModCheckResult(MOD_STATUS modStatus, String reason)
    {
      super();
      this.modStatus = modStatus;
      this.reason = reason;
    }

    @Override
    public String toString()
    {
      return "ModCheckResult [modStatus=" + modStatus + ", reason=" + reason + "]";
    }
  }

  @Override
  public PricerItemResponse run(ConstraintContext context, PricerItemParameters params)
  {

    ModCheckResult checkResult = null;
    try
    {
      debug("Running MOD Check constraint ---------------");

      String startYear = null;
      try
      {
        startYear = "" + params.getItem().getPrice(PROMO_START_YEAR_VARIABLE).intValue();
      } catch (Exception e)
      {
        error("Error reading start year: " + e);
      }
      if( startYear == null ) {
        startYear = "2018";
      }
      
      TAdjustmentRef[] c = null;
      TPricerItem item = params.getItem();
      if( item != null ) 
      {
        TPriceVariableContainer prices = item.getPrices();
        if( prices != null )
        {
          TPricerTraceItem titem = prices.getTraceItem();
          if( titem != null )
          {
            c = titem.getFiredAdjustmentRefs();
          } else
          {
            debug("trace item is null"); 
          }
        } else
        {
          debug("price is null"); 
        }
      } else
      {
        debug("Item is null"); 
      }
      
      if (c != null)
      {
        if (StringUtils.isEmpty(RULES_WITH_MOD_PARAM))
        {
          debug("RulesWithModParam is empty. check propert file.");
        } else
        {
          Set<String> rulesWithModParam = new HashSet<String>();
          rulesWithModParam.addAll(Arrays.asList(RULES_WITH_MOD_PARAM.split("\\|")));

          for (String aRule : rulesWithModParam)
          {
            debug("Accept ModParamRule: \"" + aRule + "\"");
          }

          if (c.length < 1)
          {
            debug("No Adjustments fired");
          }

          for (TAdjustmentRef a : c)
          {
            String adjId = a.getAdjustmentID();

            ModCheckResult cachedResult = checkedAdjustments.get(adjId);
            if (cachedResult != null)
            {
              checkResult = cachedResult;
              continue;
            }

            TAdjustment adj = context.getSession().getPricer().getAdjustment(a.getAdjustmentID());
            debug("Fired adjustment: " + adjId + ", " + adj.getDescription());

            TPricingRule rule = adj.getPricingRule();
            String ruleName = rule.getName();
            debug("Adjustment Rule: " + ruleName);

            if (rulesWithModParam.contains(ruleName))
            {
              TSchedule sched[] = adj.getSchedules();
              int numOfSchedules = 0;
              if (sched != null)
              {
                numOfSchedules = sched.length;

                debug("Checking schedules: " + sched.length);
                if (sched.length > 1)
                {
                  checkResult = new ModCheckResult(MOD_STATUS.TOO_MANY_SCHEDULES,
                      String.format("Adjustment %s has %d schedules.", adjId, numOfSchedules));
                  checkedAdjustments.put(adjId, checkResult);
                  break;
                }
              }
              checkResult = checkMod(context, adj, adjId, rule, startYear);
              if( checkResult == null ) {
                checkResult = new ModCheckResult(MOD_STATUS.OK, "No value.");
              }
              checkedAdjustments.put(adjId, checkResult);
            }
          }
        }
        debug("MOD constraint end ---------------, result = " + checkResult);

      }
    } catch (TSDKInternalException e)
    {
      error("Error checking mod. "+e);
    }

    String warningCode = "";

    if (checkResult != null)
    {
      if (checkResult.getModStatus() == MOD_STATUS.OK)
      {
        return context.getConstraintFactory().createPricerItemResponse(WARNING_LEVEL_OK, warningCode, "MOD value ok. ");
      }
      if (checkResult.getModStatus() == MOD_STATUS.START_NOT_MATCH)
      {
        return context.getConstraintFactory().createPricerItemResponse(WARNING_LEVEL_HARD, warningCode,
            checkResult.getReason());
      }

      if (checkResult.getModStatus() == MOD_STATUS.END_NOT_MATCH)
      {
        return context.getConstraintFactory().createPricerItemResponse(WARNING_LEVEL_HARD, warningCode,
            checkResult.getReason());
      }

      if (checkResult.getModStatus() == MOD_STATUS.TOO_MANY_SCHEDULES)
      {
        return context.getConstraintFactory().createPricerItemResponse(WARNING_LEVEL_HARD, warningCode,
            checkResult.getReason());
      }
      if (checkResult.getModStatus() == MOD_STATUS.INVALID_MOD_VALUES)
      {
        return context.getConstraintFactory().createPricerItemResponse(WARNING_LEVEL_SOFT, warningCode,
            checkResult.getReason());
      }

    }

    return context.getConstraintFactory().createPricerItemResponse(WARNING_LEVEL_OK, warningCode, " not calculated");
  }

  private ModCheckResult checkMod(ConstraintContext context, TAdjustment adj, String adjId, TPricingRule rule, String year)
      throws TSDKInternalException
  {
    Map<String, ModInfo> modMap = new HashMap<String, ModInfo>();

    TPricer pricer = context.getSession().getPricer();
    ModCheckResult checkResult = null;

    Date adjStartDate = adj.getScheduleMinStartDate();
    Date adjEndDate = adj.getScheduleMaxEndDate();

    debug("Adjustment validity dates: " + getFormattedDate(adjStartDate) + " - " + getFormattedDate(adjEndDate));

    TPricingRuleParameter[] ruleparams = rule.getParameters();

    int modIdx = findModIndex(ruleparams);

    if (modIdx > 0)
    {
      boolean bListValues = adj.useListValues();

      TAdjustmentParameter pricerModParameter = null;
      try
      {
        pricerModParameter = adj.getDefaultValue().getParameter(modIdx);
      } catch (TSDKInternalException e)
      {
        error("Error: " + e.getMessage());
        TAdjustmentParameter[] aaa = adj.getDefaultValue().getParameters();
        int i = 0;
        for (TAdjustmentParameter p : aaa)
        {
          error("param " + i + "= " + p.getValue());
        }
      }

      String parentValue = pricerModParameter.getValue();
      debug("parentValue=" + parentValue);

      if (bListValues)
      {
        for (TAdjustmentListNodes node : adj.getListValueNodes())
        {
          for (TAdjustmentListValue listValue : node.getListValues())
          {
            TAdjustmentListParam param = listValue.getParameter(modIdx);
            String modStr = param.getValue();

            debug("Adjustment value=" + modStr);

            if (modStr != null && modStr.length() > 0)
            {

              ModInfo mi = modMap.get(modStr);
              if (mi == null)
              {
                mi = getModInfo(pricer, modStr, year);
                modMap.put(modStr, mi);
              }
              if( mi.isModValueNotFound()) {
                checkResult = new ModCheckResult(MOD_STATUS.INVALID_MOD_VALUES, "Mod value not found in matrix. ");
              } 
              else if (mi.isInvalid())
              {
                checkResult = new ModCheckResult(MOD_STATUS.INVALID_MOD_VALUES, "Mod data invalid in mod matrix. ");
              } else
              {
                Date startDate = listValue.getStartDate();
                checkResult = checkDateMatch(adjId, checkResult, mi.getStartDate(), startDate, adjStartDate,
                    mi.getName(), "start");

                Date endDate = listValue.getEndDate();
                checkResult = checkDateMatch(adjId, checkResult, mi.getEndDate(), endDate, adjEndDate, mi.getName(),
                    "end");
              }
            }
          }
        }

      } else
      {
        TAdjustmentValue[] v = adj.getValues();
        if (v.length < 2)
        {
          debug("No specialized values");
        }

        for (int i = 1; i < v.length; i++)
        {
          TAdjustmentParameter modPar = v[i].getParameter(modIdx);
          String modStr = modPar.getValue();

          if ((!modStr.equals(parentValue)))
          {
            debug(" SPECIALIZED VALUE : " + modStr + " / " + parentValue);
          }

          if (modStr != null && modStr.length() > 0)
          {
            debug("MOD:" + modStr);

            ModInfo mi = modMap.get(modStr);
            if (mi == null)
            {
              mi = getModInfo(pricer, modStr, year);
              modMap.put(modStr, mi);
            }
            if( mi.isModValueNotFound()) {
              checkResult = new ModCheckResult(MOD_STATUS.INVALID_MOD_VALUES, "Mod value not found in matrix. ");
            } 
            else if (mi.isInvalid())
            {
              checkResult = new ModCheckResult(MOD_STATUS.INVALID_MOD_VALUES, "Mod data invalid in mod matrix. ");
            } else
            {
              Date startDate = v[i].getStartDate();
              checkResult = checkDateMatch(adjId, checkResult, mi.getStartDate(), startDate, adjStartDate, mi.getName(),
                  "start");

              Date endDate = v[i].getEndDate();
              checkResult = checkDateMatch(adjId, checkResult, mi.getEndDate(), endDate, adjEndDate, mi.getName(),
                  "end");
            }
          } else
          {
            debug("MOD empty");
          }
        }
      }
    }
    return checkResult;
  }

  private ModCheckResult checkDateMatch(String adjId, ModCheckResult checkResult, Date modDate, Date dateInMatrix,
      Date adjustmentDate, String modName, String reasonStr)
  {

    if (checkResult != null && checkResult.getModStatus() != MOD_STATUS.OK)
    {
      debug("Skipping test " + reasonStr);
      return checkResult;
    }

    Date dateToCompare = dateInMatrix;
    if (dateToCompare == null)
    {
      dateToCompare = adjustmentDate;
      reasonStr += " in adjustment";
    }

    if (dateToCompare != null && !dateToCompare.equals(modDate))
    {
      debug(reasonStr + " not matching adj=" + dateToCompare + "  mod=" + modDate);
      checkResult = new ModCheckResult(MOD_STATUS.START_NOT_MATCH,
          String.format("Adjustment %s %s %s does not match %s in mod %s.", adjId, reasonStr,
              getFormattedDate(dateToCompare), getFormattedDate(modDate), modName));
    } else
    {
      debug(reasonStr + " matches adj=" + dateToCompare + "  mod=" + modDate);
      if (checkResult == null)
      {
        checkResult = new ModCheckResult(MOD_STATUS.OK, reasonStr + " matches");
      } else
      {
        checkResult.appendReason(reasonStr + " matches");
      }
    }

    return checkResult;
  }

  private int findModIndex(TPricingRuleParameter[] ruleparams) throws TSDKInternalException
  {
    int modIdx = -1;
    String[] ruleAttrNames = new String[ruleparams.length];
    int[] ruleAttrTypes = new int[ruleparams.length];

    for (int i = 0; i < ruleparams.length; i++)
    {
      ruleAttrNames[i] = ruleparams[i].getName();
      ruleAttrTypes[i] = ruleparams[i].getType();

      if (PRICER_MOD_PARAM_NAME.equals(ruleAttrNames[i]))
      {
        modIdx = i;
        debug("Found pricer mod index:" + modIdx);
        break;
      }
    }
    if (modIdx < 0)
    {
      debug("Mod idx not found. Check that adjustment rule has attribute parameter : " + PRICER_MOD_PARAM_NAME);
    }
    return modIdx;
  }

}