package com.doubleblaze.businessconstraints;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.pricer.TVariableValue;
import com.i2.se.bd.util.bo.session.TSDKInternalException;
import com.i2.se.bd.util.constraints.v_1_0.ConstraintContext;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemConstraintBase;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemParameters;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemResponse;

/**
 * 
 * @author juhak
 * 
 * Checks item's price type is one of given. Uses variables.
 *
 * Reads valuetypes parameter first from Pricing variable: MultiValueTypes
 * Then fall back is enviroment variable: Constraints_MultiValueTypes
 * 
 * property: BusinessConstraints.priceType.variable, default PRC_EFF_PRICE_TYPE
 * property: BusinessConstraints.priceType.clearance, default 150,151
 * 
 * 1. returns ok if type is what is expected
 * 2. otherwise returns soft constraint
 *
 *
 *
 */

public class PriceTypeMultiValues extends PriceTypeBase {
	private static String propTypeValue = "BusinessConstraints.multiValueTypes";
	private static String propTypeViewPrefix = "BusinessConstraints.multiValueTypes.";
	static Map<String, double[]> requiredFieldsForView = new HashMap<String, double[]>();
	
	private static String TYPE_NAME = "MultiValues";
	private static String TYPE_VARIABLE = "PRC_EFF_PRICE_TYPE";
	
	private static double[] typeValuesDouble = null;
	
	static String[] views = null;
	
	static {
		
		try {
			Properties p = new Properties();
			InputStream is = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("SmartAndFinal.properties");
			p.load(is);
			
			String str = p.getProperty(propTypeValue, "");
			if (str != null && str.length() > 0) {
				views = str.split("\\|");
			}
			
			for (String view : views) {
				str = p.getProperty(propTypeViewPrefix + view, "");
				if (str != null && str.length() > 0) {
					String typeValues[] = str.split(",");
					typeValuesDouble = new double[typeValues.length];
					int i=0;
					for( String val : typeValues ) {
						typeValuesDouble[i++] = Double.parseDouble(val);			
					}
					requiredFieldsForView.put(view, typeValuesDouble);
				}
				ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PriceTypeRegular.class, StaticLogger.DEBUG,
						0, "Required fields for view " + view + " are " + str);
			}
			
						
		} catch (IOException e) {
			ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
					PricerItemConstraintBase.class, StaticLogger.ERROR, 0, "Error reading constraint properties: "+ e );
		}
		
	}

	@Override
	public String getID() {
		return "PriceType_"+TYPE_NAME;
	}

	@Override
	public String getDescription() {
		return "Returns ok if price type is "+TYPE_NAME;
	}

	@Override
	public PricerItemResponse run(ConstraintContext context, PricerItemParameters params) {
		String multivalueTypes = null;
		try {
			TVariableValue tv = params.getPricingParameters().getVariable("MultiValueTypes");
			if( tv != null ) {
				multivalueTypes = tv.stringValue();
			}
		} catch (TSDKInternalException e) {
			ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PricerItemConstraintBase.class,
					StaticLogger.ERROR, 0, "Error finding pricing variable ListPriceView. " + e);
		}
		if( StringUtils.isEmpty(multivalueTypes))
    {
      multivalueTypes = System.getenv("Constraints_MultiValueTypes");
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PriceTypeRegular.class, StaticLogger.DEBUG,
          0, "--Read MultiValueTypes from env variable: " + multivalueTypes);
    }
		double[] typeValuesDouble = null;
		if (multivalueTypes != null && multivalueTypes.length() > 0) {
			 typeValuesDouble = requiredFieldsForView.get(multivalueTypes);
		} else {
			 typeValuesDouble = new double[0];
		}
		
		return checkConstraints(context, params, typeValuesDouble, TYPE_NAME);
	}

	@Override
	protected String getTypeVariable() {
		return TYPE_VARIABLE;
	}

}