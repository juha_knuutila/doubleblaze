package com.doubleblaze.businessconstraints;

import java.util.Date;

class ModInfo 
{
	private String mod;
	private String name;
	private Date startDate;
	private Date endDate;
	private String symbol;
	private int weeks;
	private boolean invalid=false;
	private boolean modValueNotFound = false;
	
	public ModInfo(String mod, String name, Date startDate, Date endDate, String symbol, int weeks) {
		super();
		this.mod = mod;
		this.name = name;
		this.startDate = startDate;
		this.endDate = endDate;
		this.symbol = symbol;
		this.weeks = weeks;
		invalid = name == null || startDate == null || endDate == null;
	}
	
	public String getName() {
		return name;
	}
	public Date getStartDate() {
		return startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public String getSymbol() {
		return symbol;
	}
	public int getWeeks() {
		return weeks;
	}
	public String getMod() {
		return mod;
	}
	
	public boolean isInvalid() {
		return invalid;
	}
	
	
	public boolean isModValueNotFound()
  {
    return modValueNotFound;
  }

  public void setModValueNotFound(boolean modValueNotFound)
  {
    this.modValueNotFound = modValueNotFound;
  }

  @Override
  public String toString()
  {
    return "ModInfo [mod=" + mod + ", name=" + name + ", startDate=" + startDate + ", endDate=" + endDate + ", symbol="
        + symbol + ", weeks=" + weeks + ", invalid=" + invalid + ", modValueNotFound=" + modValueNotFound + "]";
  }
  
}