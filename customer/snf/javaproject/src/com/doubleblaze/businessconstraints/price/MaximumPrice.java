package com.doubleblaze.businessconstraints.price;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Properties;

import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.util.constraints.v_1_0.ConstraintContext;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemConstraintBase;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemParameters;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemResponse;

/**
 * 
 * @author juhak
 * 
 * Checks that item's price at or less than maximum price.
 * 
 * property: BusinessConstraints.maximumPrice.variable
 * property: BusinessConstraints.maximumPrice.limit
 * 
 * 1. if price is over or equal to limit, soft constraint is raised
 * 2. otherwise margin is accepted. 
 *
 */

public class MaximumPrice extends PricerItemConstraintBase {
	private static String TOTAL_PRICE_VARIABLE;
	private static double LIMIT_PRICE;

	static {
		Properties p = new Properties();
		try {
			InputStream is = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("SmartAndFinal.properties");
			p.load(is);
			TOTAL_PRICE_VARIABLE = p.getProperty("BusinessConstraints.maximumPrice.variable", "Price");
			String limitstr = p.getProperty("BusinessConstraints.maximumPrice.limit", "1000.0");
			LIMIT_PRICE = Double.parseDouble(limitstr);
			
			ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
					MaximumPrice.class, StaticLogger.DEBUG, 0, 
						"MaximumPrice variables: "+ TOTAL_PRICE_VARIABLE + ", "+ LIMIT_PRICE);
		} catch (IOException e) {
			ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
					MaximumPrice.class, StaticLogger.ERROR, 0, "Error reading constraint properties: "+ e );
		}
	}

	public String getID() {
		return "Maxnimum Price";
	}

	public String getDescription() {
		return "Checks that price is less or equal to maximum price";
	}

	public void initialize(ConstraintContext context) {
		super.initialize(context);
	
	}

	public PricerItemResponse run(ConstraintContext context, PricerItemParameters params) {

		BigDecimal price = params.getItem().getPrice(TOTAL_PRICE_VARIABLE);
		
		if (price != null) {
			double priced = price.doubleValue();
		
			if (priced > LIMIT_PRICE) {
				int warningLevel = 2;
				String warningCode = "Price above maximum limit "+LIMIT_PRICE;
				String message = warningCode;
				return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode,
						null, message);
			}

			int warningLevel = 0;
			String warningCode = "ValidPrice";
			String message = "Price below limit :" + LIMIT_PRICE;
			return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode, null,
					message);
		}
		int warningLevel = 0;
		String warningCode = "MissingPrice";
		String message = "Price not calculated";
		return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode, message);
	}
}
