package com.doubleblaze.businessconstraints.price;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Properties;

import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.util.constraints.v_1_0.ConstraintContext;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemConstraintBase;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemParameters;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemResponse;

/**
 * 
 * @author juhak
 * 
 * Checks that item's price at or over than minimum price.
 * 
 * property: BusinessConstraints.minimumPrice.variable
 * property: BusinessConstraints.minimumPrice.limit
 * 
 * 1. if price is below or equal to limit, soft constraint is raised
 * 2. otherwise margin is accepted. 
 *
 */

public class MinimumPrice extends PricerItemConstraintBase {
	private static String TOTAL_PRICE_VARIABLE;
	private static double LIMIT_PRICE;

	static {
		Properties p = new Properties();
		try {
			InputStream is = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("SmartAndFinal.properties");
			p.load(is);
			TOTAL_PRICE_VARIABLE = p.getProperty("BusinessConstraints.minimumPrice.variable", "Price");
			String limitstr = p.getProperty("BusinessConstraints.minimumPrice.limit", "0.02");
			LIMIT_PRICE = Double.parseDouble(limitstr);
			
			ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
					MinimumPrice.class, StaticLogger.DEBUG, 0, 
						"MinimumPrice variables: "+ TOTAL_PRICE_VARIABLE + ", "+ LIMIT_PRICE);
		} catch (IOException e) {
			ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
					MinimumPrice.class, StaticLogger.ERROR, 0, "Error reading constraint properties: "+ e );
		}
	}

	public String getID() {
		return "Minimum Price";
	}

	public String getDescription() {
		return "Checks that price is above or equal to minimum price";
	}

	public void initialize(ConstraintContext context) {
		super.initialize(context);
	
	}

	public PricerItemResponse run(ConstraintContext context, PricerItemParameters params) {

		BigDecimal price = params.getItem().getPrice(TOTAL_PRICE_VARIABLE);
		
		if (price != null) {
			double priced = price.doubleValue();
		
			if (priced < LIMIT_PRICE) {
				int warningLevel = 2;
				String warningCode = "Price below limit "+LIMIT_PRICE;
				String message = warningCode;
				return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode,
						null, message);
			}

			int warningLevel = 0;
			String warningCode = "ValidPrice";
			String message = "Price above limit :" + LIMIT_PRICE;
			return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode, null,
					message);
		}
		int warningLevel = 0;
		String warningCode = "MissingPrice";
		String message = "Price not calculated";
		return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode, message);
	}
}
