package com.doubleblaze.businessconstraints;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Properties;

import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.util.constraints.v_1_0.ConstraintContext;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemConstraintBase;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemParameters;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemResponse;

public abstract class PriceTypeBase extends PricerItemConstraintBase {
	
	protected static String[] readProperties(String typeName, String defaultTypeValue) {
		String typeValue = "-1";
		String typeVariable = "";
		
		Properties p = new Properties();
		
		String propTypeValue = "BusinessConstraints.priceType."+typeName;
		String propPriceTypeVariable = "BusinessConstraints.priceType.variable."+typeName; 
		
		
		try {
			InputStream is = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("SmartAndFinal.properties");
			p.load(is);
			typeVariable = p.getProperty(propPriceTypeVariable, "PRC_EFF_PRICE_TYPE");
			typeValue = p.getProperty(propTypeValue, defaultTypeValue);
			
			ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
					PriceTypeRegular.class, StaticLogger.DEBUG, 0, 
						"priceType.variable="+ typeVariable + 
						", value="+ typeValue );
			
		} catch (IOException e) {
			ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
					PricerItemConstraintBase.class, StaticLogger.ERROR, 0, "Error reading constraint properties: "+ e );
		}
		return new String[] {typeVariable, typeValue};
	}
	

	@Override
	public void initialize(ConstraintContext context) {
		super.initialize(context);
	}
	
	protected abstract String getTypeVariable();
	
	protected PricerItemResponse checkConstraints(ConstraintContext context, PricerItemParameters params,
			double typeValueDouble, String typeName) {
		return checkConstraints(context, params, new double[] {typeValueDouble}, typeName);
	}
	
	protected PricerItemResponse checkConstraints(ConstraintContext context, PricerItemParameters params,
			double[] typeValuesDouble, String typeName) {
		
		String var = getTypeVariable();
		BigDecimal priceType = params.getItem().getPrice(var);
		
		if (priceType == null) {
			int warningLevel = WARNING_LEVEL_SOFT;
			String warningCode = "No Price Type";
			return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode,
					getTypeVariable() + " not available to check "+typeName);
		} else {
			double profit = priceType.doubleValue();
			boolean valueIsAccepted = false;
			for (double checkValue : typeValuesDouble) {
				if (profit == checkValue) {
					valueIsAccepted = true;
				}
			}
			if (valueIsAccepted) {
				int warningLevel = WARNING_LEVEL_OK;
				String warningCode = "Price Type Matching";
				return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode,
						getTypeVariable() + " matching " + typeName);

			} else {
				int warningLevel = WARNING_LEVEL_SOFT;
				String warningCode = "Price Type Not Matching";
				return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode,
						getTypeVariable() + " not matching " + typeName);

			}
		}
	}
	
}
