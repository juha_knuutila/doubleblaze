package com.doubleblaze.businessconstraints;

import com.i2.se.bd.util.constraints.v_1_0.ConstraintContext;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemParameters;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemResponse;

/**
 * 
 * @author juhak
 * 
 * Checks item's price type is buymoresavemore. Uses variables
 * 
 * property: BusinessConstraints.priceType.variable, default PRC_EFF_PRICE_TYPE
 * property: BusinessConstraints.priceType.buymoresavemore, default 10
 * 
 * 1. returns ok if type is what is expected
 * 2. otherwise returns soft constraint
 *
 */

public class PriceTypeBuyMoreSaveMore extends PriceTypeBase {
	private static String TYPE_NAME = "BuyMoreSaveMore";
	private static String TYPE_VARIABLE = "PRC_EFF_PRICE_TYPE";
	private static String TYPE_VALUE = "10";
	private static double typeValueDouble = 10.0;
	
	static {
		String ret[] = readProperties(TYPE_NAME, TYPE_VALUE ); 
		TYPE_VARIABLE = ret[0];
		TYPE_VALUE = ret[1];
		typeValueDouble = Double.parseDouble(TYPE_VALUE);
	}

	@Override
	public String getID() {
		return "PriceType_"+TYPE_NAME;
	}

	@Override
	public String getDescription() {
		return "Returns ok if price type is "+TYPE_NAME;
	}

	@Override
	public PricerItemResponse run(ConstraintContext context, PricerItemParameters params) {
		return checkConstraints(context, params, typeValueDouble, TYPE_NAME);
	}

	@Override
	protected String getTypeVariable() {
		return TYPE_VARIABLE;
	}
}