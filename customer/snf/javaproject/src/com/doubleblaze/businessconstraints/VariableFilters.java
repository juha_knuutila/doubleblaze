package com.doubleblaze.businessconstraints;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.pricer.TVariableValue;
import com.i2.se.bd.util.bo.session.TSDKInternalException;
import com.i2.se.bd.util.constraints.v_1_0.ConstraintContext;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemConstraintBase;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemParameters;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemResponse;

/**
 * 
 * @author juhak
 * 
 * Checks item's price type is one of given. Uses variables
 * 
 * property: BusinessConstraints.variableFilters default BMSM|Multibuy
 * 
 * 1. returns ok if variable value is what is expected
 * 2. otherwise returns soft constraint
 *
 */

public class VariableFilters extends PricerItemConstraintBase {
	private static String propTypeValue = "BusinessConstraints.variableFilters";
	private static String propTypeViewPrefix = "BusinessConstraints.variableFilters.";
	
	private static String propVariablePostfix = ".variable";
	private static String propValuePostfix = ".value";
	
	static Map<String, String[]> variablesForView = new HashMap<String, String[]>();
	private static int VARIABLE_POS = 0;
  private static int VALUE_POS = 1;
  
  static String[] views = null;
	
  private static String TYPE_NAME = "variableFilters";
  
	static {
		
		try {
			Properties p = new Properties();
			InputStream is = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("SmartAndFinal.properties");
			p.load(is);
			
			String str = p.getProperty(propTypeValue, "");
			if (str != null && str.length() > 0) {
				views = str.split("\\|");
			}
			
			for (String view : views) {
			  String variable = "";
			  String value = "";
			  
			  str = p.getProperty(propTypeViewPrefix + view + propVariablePostfix , "");
        variable = str;
        str = p.getProperty(propTypeViewPrefix + view + propValuePostfix , "");
        value = str;
        
				if ( StringUtils.isAnyEmpty( variable, value ) ) {
				  ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PriceTypeRegular.class, StaticLogger.ERROR,
              0, "Variable or value not defined or is empty for "+propTypeValue+" " + view + ". Variable="+variable+", Value="+value);
				} else {
				  variablesForView.put(view, new String[] {variable, value});
				}

				ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PriceTypeRegular.class, StaticLogger.DEBUG,
						0, "Required fields for view " + view + " are " + str);
			}
			
						
		} catch (IOException e) {
			ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
					PricerItemConstraintBase.class, StaticLogger.ERROR, 0, "Error reading constraint properties: "+ e );
		}
		
	}

	@Override
	public String getID() {
		return "VariableValueFilter";
	}

	@Override
	public String getDescription() {
		return "Returns ok if price variable value matches filter";
	}

	@Override
	public PricerItemResponse run(ConstraintContext context, PricerItemParameters params) {
		String currentView = null;
		try {
			TVariableValue tv = params.getPricingParameters().getVariable("variableFilters");
			if( tv != null ) {
				currentView = tv.stringValue();
			}
		} catch (TSDKInternalException e) {
			ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PricerItemConstraintBase.class,
					StaticLogger.ERROR, 0, "Error finding pricing variable ListPriceView. " + e);
		}
		String[] variableAndValue = null;
		if (currentView != null && currentView.length() > 0) {
			 variableAndValue = variablesForView.get(currentView);
		} else {
			 variableAndValue = new String[0];
		}
		
		return checkConstraints(context, params, variableAndValue, TYPE_NAME, currentView);
	}


	protected PricerItemResponse checkConstraints(ConstraintContext context, PricerItemParameters params,
      String[] variableAndValue, String typeName, String currentView) {
    
	  if( variableAndValue == null || variableAndValue.length < 1 ) {
	    int warningLevel = WARNING_LEVEL_SOFT;
      String warningCode = "View not defined";
      return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode,
          warningCode + ": "+ currentView);
	  }
	  
	  String var = variableAndValue[VARIABLE_POS];
    BigDecimal priceType = params.getItem().getPrice(var);
    
    if (priceType == null) {
      int warningLevel = WARNING_LEVEL_SOFT;
      String warningCode = "Variable not found";
      return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode,
          var + " not available to check "+typeName);
    } else {
      long profit = priceType.longValueExact();
      boolean valueIsAccepted = false;
      int expectedValue = Integer.parseInt( variableAndValue[VALUE_POS] );
      if (profit == expectedValue) {
          valueIsAccepted = true;
      }
      if (valueIsAccepted) {
        int warningLevel = WARNING_LEVEL_OK;
        String warningCode = "Variable value ok";
        return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode,
            var + " matching " + typeName);

      } else {
        int warningLevel = WARNING_LEVEL_SOFT;
        String warningCode = "Variable value not matching";
        return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode,
            var + " not matching " + typeName);

      }
    }
	}
}