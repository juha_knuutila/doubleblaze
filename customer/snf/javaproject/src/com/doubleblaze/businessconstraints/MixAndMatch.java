package com.doubleblaze.businessconstraints;

import java.io.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.pricer.TVariableValue;
import com.i2.se.bd.util.bo.session.TSDKInternalException;
import com.i2.se.bd.util.constraints.v_1_0.*;

/**
 * 
 * @author juhak
 * 
 * Returns hard constraint if MixAndMatch code is more than three digits.
 * 
 * property: BusinessConstraints.mixAndMatch.variable
 *
 *
 */

public class MixAndMatch extends PricerItemConstraintBase {
	private static BigDecimal minValue = new BigDecimal(0);
  private static BigDecimal maxValue = new BigDecimal(1000);
  private static String variableForExport;
  private static Map<String, String> viewToMamVariableMap = new ConcurrentHashMap<String, String>();
  
	static {
    Properties p = new Properties();
    
    String mixAndMatchVariableProp = "BusinessConstraints.mixAndMatch.variableMapping";
   
    try {
      InputStream is = Thread.currentThread().getContextClassLoader()
          .getResourceAsStream("SmartAndFinal.properties");
      p.load(is);
      
      String variableMapping = p.getProperty(mixAndMatchVariableProp, "");
      
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
          MixAndMatch.class, StaticLogger.DEBUG, 0, 
          "Pricer mix and match static init. "+ mixAndMatchVariableProp+ "="+ variableMapping );
      
      String [] types = null;
      
      if (variableMapping != null && variableMapping.length() > 0) {
        types = variableMapping.split(",");
      }
      for( String type: types ) 
      {
        String keys[] = type.split("\\|");
        if( StringUtils.isEmpty(keys[0] )) {
          variableForExport = keys[1];
        } else 
        {
          viewToMamVariableMap.put(keys[0], keys[1]);
        }
        ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PriceTypeRegular.class, StaticLogger.DEBUG,
            0, "Type to view mapping: " + keys[0] + " are " + keys[1]);
      }
      
    } catch (IOException | NumberFormatException e) {
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
          PricerItemConstraintBase.class, StaticLogger.ERROR, 0, "Error reading constraint properties: "+ e );
    }
  }
	
	@Override
	public String getID() {
		return "MixAndMatch";
	}

	@Override
	public String getDescription() {
		return "Returns ok if Mix And Match code is from one to three digits";
	}

	@Override
  public void initialize(ConstraintContext context)
  {
	  ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
        MixAndMatch.class, StaticLogger.DEBUG, 0, 
        "PricerMod init." );
    super.initialize(context);
  }
	
	@Override
	public PricerItemResponse run(ConstraintContext context, PricerItemParameters params) {
    try
    {
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(MixAndMatch.class, StaticLogger.DEBUG, 0,
          "Mix And Match --------------" );
      
      String currentView = null;
      try {
        TVariableValue tv = params.getPricingParameters().getVariable("ListPriceView");
        if( tv != null ) {
          currentView = tv.stringValue();
        }
      } catch (TSDKInternalException e) {
        ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PricerItemConstraintBase.class,
            StaticLogger.ERROR, 0, "Error finding pricing variable ListPriceView. " + e);
      }
      
      String mamVariable = null;
      if( currentView == null ) {
        mamVariable =  variableForExport;
      } else
      {
        mamVariable = viewToMamVariableMap.get(currentView);
      }
      
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(MixAndMatch.class, StaticLogger.DEBUG, 0,
          "Mix And Match: view, variable = "+ currentView+","+mamVariable );
      
      
      BigDecimal mam = params.getItem().getPrice(mamVariable);
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(MixAndMatch.class, StaticLogger.DEBUG, 0,
          "Mix And Match value=" + mam);

      if (mam != null)
      {
        if( mam.compareTo(minValue) < 0 || mam.compareTo(maxValue) >= 0 ) {
          int warningLevel = WARNING_LEVEL_HARD;
          String warningCode = "Not valid Mix And Match value : "+mam;
          return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode, warningCode);
        }
        BigDecimal mam2 = BigDecimal.valueOf(mam.abs().intValue());
        if(mam.compareTo(mam2) != 0) {
          int warningLevel = WARNING_LEVEL_HARD;
          String warningCode = "Not valid Mix And Match decimal value : "+mam;
          return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode, warningCode);
        }
      }
      
      int warningLevel = WARNING_LEVEL_OK;
      String warningCode = "Mix And Match value ok";
      return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode, warningCode);

    } catch (Exception e)
    {
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(MixAndMatch.class, StaticLogger.DEBUG, 0,
          "Error checking empty mod. " + e);
    }
    return context.getConstraintFactory().createPricerItemResponse(WARNING_LEVEL_OK, "Not checked due error",
        "Not checked due error");
  }
	  
}