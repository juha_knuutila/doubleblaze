package com.doubleblaze.businessconstraints;

import java.io.*;
import java.math.BigDecimal;
import java.util.*;

import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.util.constraints.v_1_0.*;

/**
 * 
 * @author juhak
 * 
 * Checks that all required fields are populated for line type.
 * 
 * Line type is read from pricing variable: PRC_EFF_PRICE_TYPE
 * 
 */

public class RequiredFieldsByLineType extends PricerItemConstraintBase {

  static String typeVariable = null;
  static Map<String, String> vievTypeValueToName = new HashMap<String, String>();
	static Map<String, String[]> requiredFieldsForView = new HashMap<String, String[]>();
	

	static {
		Properties p = new Properties();
		String propTypeVariable = "BusinessConstraints.requiredFieldsByLineType.variable";
		String propValueToViewMapping = "BusinessConstraints.requiredFieldsByLineType.valueToViewMapping";
		String propTypeViewPrefix = "BusinessConstraints.requiredFields.view.";
		
		try {
			InputStream is = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("SmartAndFinal.properties");
			p.load(is);

			typeVariable = p.getProperty(propTypeVariable, ""); 

			ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PriceTypeRegular.class, StaticLogger.DEBUG, 0,
			    "BusinessConstraints.requiredFieldsByLineType typeVariable" + typeVariable);

			String types[] = null;
			String str = p.getProperty(propValueToViewMapping, "");
			
	     ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PriceTypeRegular.class, StaticLogger.DEBUG, 0,
	          "BusinessConstraints.requiredFieldsByLineType mapping" + str);

	     
			if (str != null && str.length() > 0) {
			  types = str.split(",");
			}
			for( String type: types ) 
			{
			  String keys[] = type.split("\\|");
			  vievTypeValueToName.put(keys[0], keys[1]);
			  ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PriceTypeRegular.class, StaticLogger.DEBUG,
            0, "Type to view mapping: " + keys[0] + " are " + keys[1]);
			}
			
			ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PriceTypeRegular.class, StaticLogger.DEBUG, 0,
					"BusinessConstraints.requiredFieldsByLineType typeVariable" + typeVariable);

      Collection<String> values = vievTypeValueToName.values();
      
      if (values != null)
      {
        String[] views = values.toArray(new String[0]);

        String[] fields = null;
        for (String view : views)
        {
          str = p.getProperty(propTypeViewPrefix + view, "");
          if (str != null && str.length() > 0)
          {
            fields = str.split("\\|");
            requiredFieldsForView.put(view, fields);
          }
          ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PriceTypeRegular.class, StaticLogger.DEBUG, 0,
              "Required fields for view " + view + " are " + str);
        }
      }
    } catch (IOException e) {
			ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PricerItemConstraintBase.class,
					StaticLogger.ERROR, 0, "Error reading required fields properties: " + e);
		}
		
	}

	@Override
	public String getID() {
		return "Required Fields By Line Type";
	}

	@Override
	public String getDescription() {
		return "Checks that required fields for the line type are populated";
	}

	@Override
	public void initialize(ConstraintContext context) {
		super.initialize(context);
	}

	@Override
	public PricerItemResponse run(ConstraintContext context, PricerItemParameters params) {

		ArrayList<String> messages = new ArrayList<String>();
		int warningLevel = WARNING_LEVEL_OK;

    String currentView = null;

    String itemName = "NA";
    if( params.getItem() != null ) {
      itemName = params.getItem().getName();
    }

    BigDecimal typeValue = params.getItem().getPrice(typeVariable);
    if (typeValue != null)
    {
      String currentViewType = Integer.toString(typeValue.intValue());
      currentView = vievTypeValueToName.get(currentViewType);
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PricerItemConstraintBase.class,
          StaticLogger.DEBUG, 0, "Line type and view: " + currentViewType + ", "+ currentView);
      
    }
    else
    {
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PricerItemConstraintBase.class,
          StaticLogger.DEBUG, 0, "Line filtered out as type variable "+typeVariable+" is not populated item= "+itemName);
      return context.getConstraintFactory().createPricerItemResponse(WARNING_LEVEL_HARD, "type variable "+typeVariable+" is not populated",
          "type variable "+typeVariable+" is not populated");
      
    }
	
		if (currentView != null && currentView.length() > 0) {
			String[] requiredFields = requiredFieldsForView.get(currentView);
			if (requiredFields != null) {
				for (String field : requiredFields) {
					int warn = WARNING_LEVEL_OK;
					BigDecimal price = params.getItem().getPrice(field);

          ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PricerItemConstraintBase.class,
              StaticLogger.DEBUG, 0, "Checking field: " + field + ", value is = "+ price+", item= "+itemName);

          if (price == null) {
						warn = WARNING_LEVEL_HARD;
						messages.add(field + " is required.");
						
	          ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PricerItemConstraintBase.class,
	              StaticLogger.WARN, 0, "Filtering out by required field: " + field + ", value is = "+ price+", item= "+itemName);

					} else {
						double profit = price.doubleValue();
						if (profit == 0.0) {
							warn = WARNING_LEVEL_SOFT;
							messages.add(field + " is zero.");
						} else {
							messages.add(field + " is ok.");
						}
					}
					if (warn > warningLevel) {
						warningLevel = warn;
					}
				}
			} else {
				messages.add("Required fields for view were not found. View=" + currentView);
				ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PricerItemConstraintBase.class,
						StaticLogger.ERROR, 0, "Required fields for view were not defined: " + currentView);
			}
		} else {
			messages.add("Required fields are not checked");
		}
		
		return context.getConstraintFactory().createPricerItemResponse(warningLevel, messages.toString(),
				messages.toString());
	}
	
}