package com.doubleblaze.businessconstraints;

import java.io.*;
import java.math.BigDecimal;
import java.util.Properties;

import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.util.bo.session.TSDKInternalException;
import com.i2.se.bd.util.constraints.v_1_0.*;

/**
 * 
 * @author juhak
 * 
 * Filters in items which MOD is empty
 * 
 * property: BusinessConstraints.emergencyFilter.flagVariable
 
 * 
 * 1. returns ok if type is what is expected
 * 2. otherwise returns soft constraint
 *
 */

public class EmptyMod extends PricerItemConstraintBase {
	private static String modVariable = "";
  
	static {
    Properties p = new Properties();
    
    String modVariableProp = "BusinessConstraints.variable.PRICER_MOD";
   
    try {
      InputStream is = Thread.currentThread().getContextClassLoader()
          .getResourceAsStream("SmartAndFinal.properties");
      p.load(is);
      
      modVariable = p.getProperty(modVariableProp, "");
      
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
          EmptyMod.class, StaticLogger.DEBUG, 0, 
          "PricerMod static init. "+ modVariableProp+ "="+ modVariable );
      
    } catch (IOException | NumberFormatException e) {
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
          PricerItemConstraintBase.class, StaticLogger.ERROR, 0, "Error reading constraint properties: "+ e );
    }
  }
	
	@Override
	public String getID() {
		return "Empty Mod";
	}

	@Override
	public String getDescription() {
		return "Returns ok if item has mod populated and should be filtered in";
	}

	@Override
  public void initialize(ConstraintContext context)
  {
	  ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
        EmptyMod.class, StaticLogger.DEBUG, 0, 
        "PricerMod init." );
    super.initialize(context);
  }
	
	@Override
	public PricerItemResponse run(ConstraintContext context, PricerItemParameters params) {
    try
    {
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(EmptyMod.class, StaticLogger.DEBUG, 0,
          "PricerMod --------------" + modVariable);

      BigDecimal mod = params.getItem().getPrice(modVariable);
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(EmptyMod.class, StaticLogger.DEBUG, 0,
          "PricerMod value=" + mod);

      if (mod == null)
      {
        int warningLevel = WARNING_LEVEL_SOFT;
        String warningCode = "Empty MOD value";
        return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode, warningCode);
      } else
      {
        int warningLevel = WARNING_LEVEL_OK;
        String warningCode = "Mod has value";
        return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode, warningCode);
      }

    } catch (Exception e)
    {
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(EmptyMod.class, StaticLogger.DEBUG, 0,
          "Error checking empty mod. " + e);
    }
    return context.getConstraintFactory().createPricerItemResponse(WARNING_LEVEL_OK, "Not checked due error",
        "Not checked due error");
  }
	  
}