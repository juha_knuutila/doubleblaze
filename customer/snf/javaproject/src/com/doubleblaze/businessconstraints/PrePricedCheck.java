package com.doubleblaze.businessconstraints;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Properties;

import com.i2.se.bd.catalog.*;
import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.util.bo.session.TSDKInternalException;
import com.i2.se.bd.util.constraints.v_1_0.ConstraintContext;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemConstraintBase;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemParameters;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemResponse;

/**
 * 
 * @author juhak
 * 
 *         Checks that item price is below PrePrice. If current price is higher then raises hard constraint.
 *         Uses properties:
 *         BusinessConstraints.PrePiced.variable.PrePricedAttribute, default PrePricedItem
 *         BusinessConstraints.PrePiced.variable.PrePrice, default PRC_REG_MDM_PRICE
 *         BusinessConstraints.PrePiced.variable.RetailPrice, default PRC_REG_PRICE
 * 
 *         1. if price is above PrePrice, then hard constraint is raised
 *         2. otherwise price is accepted.
 *
 */

public class PrePricedCheck extends PricerItemConstraintBase
{
  private NumberFormat percentFormatter;

  private static String PRE_PRICED_ITEM_ATTR;
  private static String PRE_PRICE_VARIABLE;
  private static String RETAIL_PRICE_VARIABLE;

  static
  {
    Properties p = new Properties();
    try
    {
      InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("SmartAndFinal.properties");
      p.load(is);
      PRE_PRICED_ITEM_ATTR = p.getProperty("BusinessConstraints.PrePiced.variable.PrePricedAttribute", "PrePricedItem");
      PRE_PRICE_VARIABLE = p.getProperty("BusinessConstraints.PrePiced.variable.PrePrice", "PRC_REG_MDM_PRICE");
      RETAIL_PRICE_VARIABLE = p.getProperty("BusinessConstraints.PrePiced.variable.RetailPrice", "PRC_REG_PRICE");

      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PrePricedCheck.class, StaticLogger.DEBUG, 0,
          "PrePricedCheck variables: PrePricedAttribute=" + PRE_PRICED_ITEM_ATTR + ", MdmPrice" + PRE_PRICE_VARIABLE
              + ", RetailPrice=" + RETAIL_PRICE_VARIABLE);

    } catch (IOException e)
    {
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PricerItemConstraintBase.class, StaticLogger.ERROR, 0,
          "Error reading constraint properties: " + e);
    }
  }

  @Override
  public String getID()
  {
    return "PrePricedCheck";
  }

  @Override
  public String getDescription()
  {
    return "Checks that if item is PrePriced item then retail price must be below PrePrice";
  }

  @Override
  public void initialize(ConstraintContext context)
  {
    super.initialize(context);
    percentFormatter = getPercentFormatter(false);
  }

  @Override
  public PricerItemResponse run(ConstraintContext context, PricerItemParameters params)
  {

    boolean isPrePriced = false;
    try
    {
      TBoxItem bi = context.getSession().getProductData().getBoxItem(params.getItem().getOriginItemID());
      if( bi.hasAttribute(PRE_PRICED_ITEM_ATTR) )
      {
        TAttributeValue prePricedAttr = bi.getAttribute(PRE_PRICED_ITEM_ATTR);
        if (prePricedAttr != null)
        {
          String prePricedStr = prePricedAttr.getFirstValue();
          isPrePriced = "1".equals(prePricedStr);
        }
      }
    } catch (TSDKInternalException e)
    {
      e.printStackTrace();
    }

    if (isPrePriced)
    {
      BigDecimal prePrice = params.getItem().getPrice(PRE_PRICE_VARIABLE);
      BigDecimal retailPrice = params.getItem().getPrice(RETAIL_PRICE_VARIABLE);

      if (retailPrice == null || prePrice == null)
      {
        int warningLevel = WARNING_LEVEL_OK;
        String warningCode = "No Price";
        String reason = RETAIL_PRICE_VARIABLE + " " + (retailPrice == null ? "not calculated" : retailPrice.toPlainString());
        reason += ", " + PRE_PRICE_VARIABLE + " " + (prePrice == null ? "not calculated" : prePrice.toPlainString());
        return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode, reason);
      } else
      {
        String formattedRetailPrice, formattedPrePrice;

        synchronized (percentFormatter)
        {
          formattedRetailPrice = percentFormatter.format(retailPrice.doubleValue());
          formattedPrePrice = percentFormatter.format(prePrice.doubleValue());
        }

        if (retailPrice.compareTo(prePrice) > 0 )
        {
          int warningLevel = WARNING_LEVEL_HARD;
          String warningCode = "Retail price over PrePrice";
          String[] warningParameters = { formattedRetailPrice };
          String message = RETAIL_PRICE_VARIABLE + " is above Pre Price: " + formattedPrePrice;
          return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode, warningParameters,
              message);
        }

        int warningLevel = WARNING_LEVEL_OK;
        String warningCode = "PrePriced Check OK";
        String[] warningParameters = { formattedRetailPrice };
        String message = RETAIL_PRICE_VARIABLE + " is below PrePrice: " + formattedRetailPrice + " < "
            + formattedPrePrice;
        return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode, warningParameters,
            message);
      }
    }

    int warningLevel = WARNING_LEVEL_OK;
    String warningCode = "PrePriced Check OK";
    String[] warningParameters = { warningCode };
    String message = "Not PrePriced item.";
    return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode, warningParameters,
        message);
  }
}