package com.doubleblaze.businessconstraints;

import java.io.*;
import java.math.BigDecimal;
import java.time.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.pricer.*;
import com.i2.se.bd.util.bo.session.TSDKInternalException;
import com.i2.se.bd.util.constraints.v_1_0.*;

/**
 * 
 * @author juhak
 * 
 *         Filters in items which have Emergency = 1 & StartDate is beyond given
 *         date
 * 
 *         property: BusinessConstraints.emergencyFilter.flagVariable property:
 *         BusinessConstraints.emergencyFilter.startDateVariable
 * 
 *         1. returns ok if type is what is expected 2. otherwise returns soft
 *         constraint
 *
 */

public class EmergencyFilter extends PricerItemConstraintBase
{
  //private static String EMERGENCY_FLAG = "Emergency";
  private static double typeValueDouble = 1.0;

  private static long startDateOffset = 0;
  private static String emergencyFlagVariable = "";
  private static String startDateVariable = "";
  private static boolean turnOffEmergencyFlag = true;

  private static ConcurrentHashMap<String, Integer> ruleParameterMap = new ConcurrentHashMap<String, Integer>();
  Set<String> firedAdjustments = ConcurrentHashMap.newKeySet();
  
  private Date compareDate = null;

  static
  {
    Properties p = new Properties();
    startDateVariable = "BusinessConstraints.emergencyFilter.startDateVariable";

    try
    {
      InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("SmartAndFinal.properties");
      p.load(is);
      String startDateOffsetStr = p.getProperty("BusinessConstraints.emergencyFilter.startDateOffset", "0");
      startDateOffset = Long.parseLong(startDateOffsetStr);

      emergencyFlagVariable = p.getProperty("BusinessConstraints.emergencyFilter.flagVariable", "");

      logDebug("BusinessConstraints.emergencyFilter.startDateOffset=" + startDateOffset + ", variable="
          + emergencyFlagVariable);
      
      String turnOff = p.getProperty("BusinessConstraints.emergencyFilter.turnOffEmergencyFlag", "true");
      if(turnOff != null && (turnOff.equalsIgnoreCase("false") ||turnOff.equalsIgnoreCase("0")))
      {
        turnOffEmergencyFlag = false;
      }

    } catch (IOException | NumberFormatException e)
    {
      logError("Error reading constraint properties: " + e);
    }
  }

  @Override
  public String getID()
  {
    return "EmergencyFilter";
  }

  @Override
  public String getDescription()
  {
    return "Returns ok if item should be filtered in";
  }

  @Override
  public void initialize(ConstraintContext context)
  {
    super.initialize(context);

    synchronized (this)
    {
      LocalDate locDate = LocalDate.now().plusDays(startDateOffset);
      compareDate = Date.from(locDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
      logInfoMsg("dates: " + locDate + ", " + compareDate);
    }
  }

  @Override
  public PricerItemResponse run(ConstraintContext context, PricerItemParameters params)
  {
    return checkConstraints(context, params, typeValueDouble);
  }

  protected PricerItemResponse checkConstraints(ConstraintContext context, PricerItemParameters params,
      double typeValueDouble)
  {
    return checkConstraints(context, params, new double[] { typeValueDouble });
  }

  static int counter = 0;

  protected PricerItemResponse checkConstraints(ConstraintContext context, PricerItemParameters params,
      double[] typeValuesDouble)
  {
    BigDecimal emergencyFlag = params.getItem().getPrice(emergencyFlagVariable);
    BigDecimal startDate = params.getItem().getPrice(startDateVariable);

    try
    {
      logDebug("emergencyFlag = " + emergencyFlag + ", startDate = " + startDate + ", date="
          + params.getPricingParameters().getPricingDate() + ", compare date=" + compareDate);

      if (compareDate.compareTo(params.getPricingParameters().getPricingDate()) > 0)
      {
        logInfoMsg("Filtered out by date condition = " + params.getPricingParameters().getPricingDate()
            + ", compare date=" + compareDate);

        int warningLevel = WARNING_LEVEL_HARD;
        String warningCode = "Emergency Date later";
        return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode,
            emergencyFlagVariable + " date too early ");
      }
      
    } catch (TSDKInternalException e)
    {
      logError("Error checking constraint: " + e);
    }
    if (emergencyFlag == null)
    {
      int warningLevel = WARNING_LEVEL_HARD;
      String warningCode = "No Price Type";
      return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode,
          emergencyFlagVariable + " not available to check ");
    } else
    {
      double profit = emergencyFlag.doubleValue();

      logInfoMsg("Emergency flag:" + profit);
      boolean valueIsAccepted = false;
      for (double checkValue : typeValuesDouble)
      {
        if (profit == checkValue)
        {
          valueIsAccepted = true;
        }
      }
      if (valueIsAccepted)
      {
        resetEmergencyFlag(context, params);

        int warningLevel = WARNING_LEVEL_OK;
        String warningCode = "Price Type Matching";
        return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode,
            emergencyFlagVariable + " matching ");

      } else
      {
        int warningLevel = WARNING_LEVEL_HARD;
        String warningCode = "Price Type Not Matching";
        return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode,
            emergencyFlagVariable + " not matching ");
      }
    }
  }

  public void close(ConstraintContext context) {
    logInfo("Closing.... ");

    Iterator<String> it = firedAdjustments.iterator();
    while (it.hasNext())
    {
      String adjId = it.next();

      try
      {
        if( turnOffEmergencyFlag ) {
          resetEmergencyFlag(context, adjId);
        }

      } catch (Exception e)
      {
        logError("Error reseting emergency flag: " + e);
      }
    }
  }
  
  private void resetEmergencyFlag(ConstraintContext context, PricerItemParameters params)
  {
    TAdjustmentRef[] c;
    try
    {
      TPricePromotionEvent[] events = params.getItem().getPrices().getPromotionEvents();
      if (events != null)
      {
        if (events.length == 0)
        {
          logWarn("Promo Events 0 ");
        }
        for (TPricePromotionEvent event : events)
        {
          String adjId = event.getAdjustmentID();
          logDebug("Promo Event: " + adjId);
          //resetEmergencyFlag(context, adjId);
          if( !firedAdjustments.contains(adjId) ) 
          {
            firedAdjustments.add(adjId);
            logDebug("Added adjustment: " + adjId);
          }
        }
      } else
      {
        logWarn("events empty null");
      }

      /*
      c = params.getItem().getPrices().getTraceItem().getFiredAdjustmentRefs();
      if (c != null)
      {
        if (c.length < 1)
        {
          logDebug("No Adjustments fired: " + c.length);
        }
        
        for (TAdjustmentRef a : c)
        {
          String adjId = a.getAdjustmentID();
          //resetEmergencyFlag(context, adjId);
          if( !firedAdjustments.contains(adjId) ) 
          {
            firedAdjustments.add(adjId);
            logDebug("Added2 adjustment: " + adjId);
          }

        }
      }
      */
    } catch (Exception e)
    {
      logError("Error reading fired adjustments: " + e);
    }
  }

  
  
  private void resetEmergencyFlag(ConstraintContext context, String adjId)
      throws TSDKInternalException
  {
    //TProductData pd = context.getSession().getProductData();
    TAdjustment adj = context.getSession().getPricer().getAdjustment(adjId);

    // find rule parameter index
    logDebug("Fired adjustment: " + adjId + ", " + adj.getDescription());
    TPricingRule rule = adj.getPricingRule();
    if (rule != null)
    {
      String ruleName = rule.getName();
      logDebug(" Rule:" + ruleName);
      Integer idxs = ruleParameterMap.get(ruleName);

      if (idxs == null)
      {
        int index = -1;
        TPricingRuleParameter[] param = rule.getParameters();
        for (TPricingRuleParameter p : param)
        {
          if ("EMERGENCY_EXPORT".equals(p.getName()))
          {
            index = p.getIndex();
            break;
          }
        }
        if (index >= 0)
        {
          idxs = index;
          ruleParameterMap.put(ruleName, idxs);
        } else
        {
          logWarn("Emergency parameter not found from rule: " + ruleName);
          // return;
        }
      }
      logDebug(" Emergency param position:" + idxs);

      // adj.getAdjustmentValueGroup(userNode, customerNode, locationNode,
      // productNode);
      /*
      THierarchyIntersections inters = adj.getIntersections();
      TAdjustmentNode[] customers = inters.getCustomerNodes();
      TAdjustmentNode[] users = inters.getUserNodes();
      TAdjustmentNode[] locations = inters.getLocationNodes();
      TAdjustmentNode[] products = inters.getProductNodes();
      TAdjustmentValueRef[] valuerefs = inters.getValueReferences();

      
      for (TAdjustmentNode customer : customers)
      {
        hierarchyInfo(pd, customer, THierarchy.CUSTOMER, "Customer: ");
      }
      for (TAdjustmentNode user : users)
      {
        hierarchyInfo(pd, user, THierarchy.USER, "User: ");
      }
      for (TAdjustmentNode location : locations)
      {
        hierarchyInfo(pd, location, THierarchy.LOCATION, "Location: ");
      }
      for (TAdjustmentNode product : products)
      {
        hierarchyInfo(pd, product, THierarchy.PRODUCT, "Product: ");
      }
      for (TAdjustmentValueRef value : valuerefs)
      {
        logWarn("Value ref = " + value.toString());
        // hierarchyInfo(pd, customer, THierarchy.CUSTOMER, "Customer: ");
      }
       */
      TAdjustmentValueGroup[] vgroups = adj.getValueGroups();
      if (vgroups != null)
      {
        for (TAdjustmentValueGroup vgroup : vgroups)
        {
          if (vgroup != null)
          {
            TAdjustmentValue[] avs = vgroup.getValues();
            if (avs != null)
            {
              for (TAdjustmentValue av : avs)
              {
                String pv = av.getParameter(idxs).getValue();
                logDebug("AdjValue: " + pv);
                if (StringUtils.isNotEmpty(pv))
                {
                  logDebug("Setting value: " + pv);
                  TAdjustmentParameter rparam = av.getParameter(idxs);
                  rparam.setValue("");
                  // rparam.commit();
                  pv = av.getParameter(idxs).getValue();
                  logDebug("done: " + pv);
                }
              }
            }
          }
        }
        adj.commit();
      }
    }
  }
/*
  private void hierarchyInfo(TProductData pd, TAdjustmentNode adjNode, int hierType, String typetxt)
      throws TSDKInternalException
  {
    logWarn(typetxt + adjNode.getOID());
    if (adjNode.isDirectItem())
    {
      TBoxItem item = pd.findBoxItemByObjectID(adjNode.getOID());
      logDebug("Item: " + item.getCode());

    } else
    {
      THierarchy hier = pd.getHierarchy(hierType);
      THierarchyNode node = hier.findNode(adjNode.getOID());
      if (node != null)
      {
        logDebug("Name: " + node.getName() + ", " + node.getHierarchyPath());
      }
    }
  }
*/
  private void logWarn(String msg)
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PriceTypeRegular.class, StaticLogger.WARN, 0, msg);
  }

  private static void logDebug(String msg)
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PriceTypeRegular.class, StaticLogger.DEBUG, 0, msg);
  }

  private static void logInfoMsg(String msg)
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PriceTypeRegular.class, StaticLogger.INFO, 0, msg);
  }

  private static void logError(String msg)
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PriceTypeRegular.class, StaticLogger.ERROR, 0, msg);
  }
}