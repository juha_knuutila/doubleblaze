package com.doubleblaze.businessconstraints;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Properties;

import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.util.constraints.v_1_0.ConstraintContext;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemConstraintBase;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemParameters;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemResponse;

/**
 * 
 * @author juhak
 * 
 * Checks item margin. Uses variables
 * property: BusinessConstraints.variable.StoreMargin, default StoreMargin
 * property: BusinessConstraints.variable.StoreMarginLimit, default StoreMarginLimit
 * 
 * 1. if StoreMargin is below 0, hard constraint is raised
 * 2. if StoreMargin is below StoreMarginLimit, soft constraint is raised.
 * 3. otherwise margin is accepted. 
 *
 */

public class StoreItemMargin extends PricerItemConstraintBase {
	private NumberFormat percentFormatter;
	private static String STORE_MARGIN_VARIABLE;
	private static String STORE_MARGIN_LIMIT;

	static {
		Properties p = new Properties();
		try {
			InputStream is = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("SmartAndFinal.properties");
			p.load(is);
			STORE_MARGIN_VARIABLE = p.getProperty("BusinessConstraints.variable.StoreMargin", "StoreMargin");
			STORE_MARGIN_LIMIT = p.getProperty("BusinessConstraints.variable.StoreMarginLimit", "StoreMarginLimit");

			ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
					StoreItemMargin.class, StaticLogger.DEBUG, 0, 
						"StoreItemConstraint variables: "+ STORE_MARGIN_VARIABLE + ", "+ STORE_MARGIN_LIMIT);
		} catch (IOException e) {
			ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
					StoreItemMargin.class, StaticLogger.ERROR, 0, "Error reading constraint properties: "+ e );
		}
	}

	public String getID() {
		return "Store Item Margin Check";
	}

	public String getDescription() {
		return "Checks margin at store level";
	}

	public void initialize(ConstraintContext context) {
		super.initialize(context);
		this.percentFormatter = getPercentFormatter(false);
	}

	public PricerItemResponse run(ConstraintContext context, PricerItemParameters params) {

		BigDecimal storeMargin = params.getItem().getPrice(STORE_MARGIN_VARIABLE);
		BigDecimal storeMarginLimit = params.getItem().getPrice(STORE_MARGIN_LIMIT);

		if (storeMargin != null) {
			double profit = storeMargin.doubleValue();
			String formattedProfit;
			synchronized (this.percentFormatter) {
				formattedProfit = this.percentFormatter.format(profit);
			}

			if (profit < 0.0D) {
				int warningLevel = 2;
				String warningCode = "NegativeProfit";
				String[] warningParameters = { formattedProfit };
				String message = "Negative profit % :" + formattedProfit;
				return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode,
						warningParameters, message);
			}
			if (storeMarginLimit != null) {
				double marginLimit = storeMarginLimit.doubleValue();
				if (profit < marginLimit) {
					int warningLevel = 1;
					String warningCode = "LowProfit";
					String[] warningParameters = { formattedProfit };
					String message = "Low profit % :" + formattedProfit;
					return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode,
							warningParameters, message);
				}
			}
			int warningLevel = 0;
			String warningCode = "ValidProfit";
			String[] warningParameters = { formattedProfit };
			String message = "Valid profit % :" + formattedProfit;
			return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode, warningParameters,
					message);
		}
		int warningLevel = 0;
		String warningCode = "MissingProfit";
		String message = "Profit not calculated";
		return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode, message);
	}
}
