package com.doubleblaze.pricer.export;

import java.sql.*;
import java.util.*;

import com.i2.se.app.integrations.exports.priceexport.*;
import com.i2.se.bd.catalog.*;
import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.externalinterfaces.sql.*;
import com.i2.se.bd.pricer.*;
import com.i2.se.bd.util.bo.session.*;

/**
 * This class queries possible Emergency Export items from database.
 * First looks up for adjustments then items in them.
 *  
 * @author juhak
 *
 */
public class EmergencyItemFilter implements PriceExportItemFilter
{
  public List<TBoxItem> getItems(TSession session, PriceExportParams params)
  {
    TPricer pricer = session.getPricer();
    TProductData pd = session.getProductData();
    
    List<String> adjustments = getAdjustments();
    List<TBoxItem> itemList = getItems(pd, pricer, adjustments);
    // new PriceExportItemListFilter().removeVariantDuplicates(session, params, itemList);
   
    return itemList;
  }

  private List<TBoxItem> getItems(TProductData pd, TPricer pricer, List<String> adjustments)
  {
    List<TBoxItem> itemList = new ArrayList<TBoxItem>();
    Set<String> uniqueItems = new HashSet<String>();
    
    for (String oid : adjustments)
    {
      TAdjustment adjustment = pricer.getAdjustment(oid);
      THierarchyIntersections hi;
      try
      {
        hi = adjustment.getIntersections();

        for (TAdjustmentNode productNode : hi.getProductNodes())
        {
          if (!productNode.isDirectItem())
          {
            // ignoring other than direct items
            continue;
          }

          if (productNode.isExcluded())
          {
            continue;
          }

          String itemOID = productNode.getOID();
          TBoxItem item = pd.getBoxItem(itemOID);
        
          List<TBoxItem> linkedItems = getLinkedItems(item);
          if( linkedItems != null)
          {
            for( TBoxItem li : linkedItems )
            {
              if(!uniqueItems.contains(li.getObjectID()))
              {
                logDebug("Adding linked item candidate: "+li.getCode());
                uniqueItems.add(li.getObjectID());
                itemList.add(li);
              }
            }
          }
          
          if(!uniqueItems.contains(item.getObjectID()))
          {
            logDebug("Adding item candidate: "+item);
            uniqueItems.add(item.getObjectID());
            itemList.add(item);
          }
        }
      } catch (TSDKInternalException e)
      {
        e.printStackTrace();
      }
    }
    logDebug("Item candidate count: "+itemList.size());
    return itemList;
  }
  
  private List<TBoxItem> getLinkedItems( TBoxItem item ) throws TSDKInternalException
  {
    TPriceLink link = item.getPriceLink( );
    if( link != null )
    {
      return link.getLinkedBoxItems( );
    }
    return null;
  }
  
  
  /**
   * Returns all adjustments with EMERGENCY_FLAG = 1
   * 
   * @return
   */
  private List<String> getAdjustments()
  {
    List<String> adjustmentOids = new ArrayList<String>();

    TSqlManager manager = ObjectFactory.getSqlManager();
    TSql sql = manager.getTSqlInstance();

    String[] ruleNames = new String[] { "WEEKLY SPECIAL", "AMOUNT OFF", "BMSM", "BOGO", "CLEARANCE", "FREE_ITEM",
        "HARD LIMIT DISCOUNT", "REGULAR RETAIL", "SELL AT", "SMS TYPE5", "SMS TYPE6", "SZ REGULAR RETAIL" };

    for (String ruleName : ruleNames)
    {

      String query = "select pap_value, pap_adjustment_oid , pap_value_oid, pap_oid from padjustment_param where pap_index = ("
          + " select prp_index from ppri_rule_param p, ppricing_rule r where r.ppr_name = '" + ruleName + "' and "
          + " prp_name = 'EMERGENCY_EXPORT' and p.prp_parent_oid = r.ppr_oid ) " + " and pap_adjustment_oid in ( "
          + " select pad_oid from padjustment, ppricing_rule where pad_rule_oid = ppr_oid and ppr_name = '" + ruleName
          + "' " + " ) and pap_value != '0' and pap_deleted != 1";

      ResultSet rs = sql.executeQuery(query, null);

      try
      {
        while (rs != null && rs.next())
        {
          adjustmentOids.add(rs.getString(2));
          logDebug("Adding adjustment with OID: "+adjustmentOids);
        }
      }
      catch (SQLException ex)
      {
        logError( "Emergency export query failed: " + ex, ex );
      }

      sql.release();
    }

    return adjustmentOids;
  }
  
  private void logDebug(String msg)
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(), StaticLogger.DEBUG, 1005,
        msg);
  }

  private void logError(String msg, SQLException ex)
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(this.getClass(), StaticLogger.ERROR, 1005,
        msg, ex);
  }

}