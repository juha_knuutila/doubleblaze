package com.doubleblaze;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.externalinterfaces.security.TSecurityManager;
import com.i2.se.bd.internalinterfaces.InternalObjectFactory;
import com.i2.se.bd.internalinterfaces.tapi.TApiUtils;
import com.i2.se.bd.internalinterfaces.tapi.TBOUtils;
import com.i2.se.bd.pricer.TPriceVariableInfo;
import com.i2.se.bd.util.bo.debug.Debug;
import com.i2.se.bd.util.bo.session.TSDKInternalException;
import com.i2.se.bd.util.bo.session.TServerContext;
import com.i2.se.bd.util.bo.session.TSession;

public class RegisterAssets {
	private static TBOUtils tBoUtils = null;
	protected static TApiUtils tApiUtils = null;

	static {
		tBoUtils = InternalObjectFactory.getTBOUtils();
		tApiUtils = InternalObjectFactory.getTApiUtils();
	}

	public RegisterAssets(String propertyFile) throws TSDKInternalException, IOException {
		Debug.setDebugLevel(Debug.OFF);
		ObjectFactory.getLogManager().getStaticLogger().setLogLevel(StaticLogger.ERROR);

		TServerContext sc = tBoUtils.doSystemLogin();
		session = tBoUtils.getSession(sc);
		TSecurityManager securityManager = ObjectFactory.getSecurityManager();

		TPriceVariableInfo variables[] = session.getPricer().getVariableInfos();
		Map<String, TPriceVariableInfo> varmap = new HashMap<String, TPriceVariableInfo>();
		for (TPriceVariableInfo vinfo : variables) {
			// System.out.println("variable:"+vinfo.getName());
			varmap.put(vinfo.getOID(), vinfo);
		}
		Path path = FileSystems.getDefault().getPath(propertyFile);
		List<String> lines = Files.readAllLines(path);

		String assetsFile = "";
		for (String line : lines) {
			String[] values = line.split("=");
			if ("assetsFile".equals(values[0].trim())) {
				assetsFile = values[1].trim();
			}
		}
		
		if (assetsFile == null || assetsFile.length() < 1) {
			System.out.println("No assets file defined.");
			return;
		}

		// read assets file
		path = FileSystems.getDefault().getPath(assetsFile);
		lines = Files.readAllLines(path);

		int lineno = 0;
		for (String line : lines) {
			lineno++;
			String[] values = line.split("\\|");
			// System.out.println("Line:"+line+", 0="+values[0].trim());
			if ("Pricing Variable".equals(values[0].trim())) {
				// String vname = values[1].trim();
				// String oid = varmap.get(vname).getOID();
				String oid = values[1].trim();
				String vname = varmap.get(oid).getName();
				String assetKey = securityManager.convertPricingVariableOIDToAssetKey(oid);
				if (securityManager.isRegisteredAsset(sc, TSecurityManager.PRICING_VARIABLE, assetKey)) {
					System.out.println("Variable already registered: " + vname);
				} else {
					System.out.println("Registering variable: " + vname);
					securityManager.registerAsset(sc, TSecurityManager.PRICING_VARIABLE, assetKey, "", "");
				}
			} else if ("Pricer Hierarchy".equals(values[0].trim())) {

				String oid = values[1].trim();
				String assetKey = securityManager.convertPricerHierarchyOIDToAssetKey(oid);
				if (securityManager.isRegisteredAsset(sc, TSecurityManager.PRICER_HIERARCHY, assetKey)) {
					System.out.println("Hierarchy already registered: " + oid);
				} else {
					System.out.println("Registering hierarchy: " + oid);
					securityManager.registerAsset(sc, TSecurityManager.PRICER_HIERARCHY, assetKey, "", "");
				}

			} else if ("Price Group".equals(values[0].trim())) {

				String oid = values[1].trim();
				String assetKey = securityManager.convertPriceGroupIDToAssetKey(oid);
				if (securityManager.isRegisteredAsset(sc, TSecurityManager.PRICE_GROUP, assetKey)) {
					System.out.println("Price group already registered: " + oid);
				} else {
					System.out.println("Registering price group: " + oid);
					securityManager.registerAsset(sc, TSecurityManager.PRICE_GROUP, assetKey, "", "");
				}

			} else {
				System.out.println("Unknown asset type: " + values[0] + " in line: " + lineno);
			}
		}
	}

	private TSession session;

	public static void main(String[] args) {
		try {
			if (args.length < 1) {
				System.out.println("Please give property file");
			} else {
				String propertyFile = args[0];
				new RegisterAssets(propertyFile);
			}

		} catch (TSDKInternalException | IOException ex) {
			ex.printStackTrace();
		}
	}

	public TSession getSession() {
		return session;
	}

}
