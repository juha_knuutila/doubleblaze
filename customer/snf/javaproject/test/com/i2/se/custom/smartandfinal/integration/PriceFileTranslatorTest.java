package com.i2.se.custom.smartandfinal.integration;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import java.io.File;
import java.math.BigDecimal;
import java.util.*;

import org.junit.jupiter.api.Test;
import org.mockito.*;

import com.i2.se.app.integrations.imports.data.*;
import com.i2.se.bd.adapters.LogCapture;
import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.*;
import com.i2.se.custom.smartandfinal.integration.PriceFileTranslator.PriceElement;

public class PriceFileTranslatorTest
{
//  Properties props = mock(Properties.class, withSettings().useConstructor().defaultAnswer(CALLS_REAL_METHODS));
  Properties props = mock(Properties.class);
  @InjectMocks 
  ObjectFactory manager;
  
  
  public static class MyStaticLogger implements StaticLogger 
  {
    @Override
    public void addLogEntry(Class<?> arg0, int arg1, int arg2, String arg3)
    {
    }

    @Override
    public void addLogEntry(Class<?> arg0, int arg1, int arg2, String arg3, Throwable arg4)
    {
    }

    @Override
    public void clearThreadBuffer()
    {
    }

    @Override
    public LogCapture getLogCapture()
    {
      return null;
    }

    @Override
    public int getLogLevel()
    {
      return 0;
    }

    @Override
    public boolean getUseThreadBuffer()
    {
      return false;
    }

    @Override
    public void reloadConfiguration()
    {
    }

    @Override
    public void setLogLevel(int arg0)
    {
    }

    @Override
    public void setUseThreadBuffer(boolean arg0)
    {
    }

    @Override
    public void setUseThreadBuffer(Thread arg0, boolean arg1)
    {
    }
  }
  public static MyStaticLogger myLogger = new MyStaticLogger();
  
  public static class MyLogManager implements TLogManager {
    public StaticLogger getStaticLogger() {
      System.out.println("----static - logger");
      return myLogger;
    }

    @Override
    public TRFQLog getRFQLog()
    {
      return null;
    }
  }
  public static MyLogManager myLogManager = new MyLogManager();

  public static class MyObjectManager {
  }
  
  class FileMatcher implements ArgumentMatcher<File> {
    public boolean matches(File file) {
      System.out.println("arg:"+file);
      return file instanceof File;
    }
    public String toString() {
        //printed in verification errors
        return "[File matcher]";
    }
  }

//List mock = mock(List.class);
//when(mock.addAll(argThat(new ListOfTwoElements))).thenReturn(true);
//mock.addAll(Arrays.asList("one", "two"));
//verify(mock).addAll(argThat(new ListOfTwoElements()));

//To keep it readable you can extract method, e.g: 
//  verify(mock).addAll(argThat(new ListOfTwoElements()));
  //becomes
 // verify(mock).addAll(listOfTwoElements());

//In Java 8 you can treat ArgumentMatcher as a functional interfaceand use a lambda, e.g.: 
//  verify(mock).addAll(argThat(list -> list.size() == 2));


  
 
  public void addPriceLists() 
  {
    BackBusContainer target = new BackBusContainer( );
    SAFSettings settings = mock(SAFSettings.class);

    // setup test data
    final String fileName = "SFI_MDM_JDA-PRICER_ITEM_PRICE_20180905_TEST.dat";
    when(settings.getPriceGroupNameFormat()).thenReturn("{1}_{0}");
    when(settings.getListCostPriceGroup( )).thenReturn("ListCost");
    when(settings.isInitialLoad()).thenReturn(false);
    when(settings.getRegularEachRetailPriceGroup( )).thenReturn("ApprovedPrice");
    when(settings.getPriceFileNamePattern()).thenReturn("SFI_MDM_JDA-PRICER_ITEM_PRICE_(\\d{4})(\\d{2})(\\d{2})_.*.dat");
    
    String locationHierarchyID = "LOC-10-10";
    String storeName = "501";
    
    PriceFileTranslator priceTr = mock(PriceFileTranslator.class, Mockito.CALLS_REAL_METHODS);
    doNothing().when(priceTr).logInfo(anyString());
    doNothing().when(priceTr).logError(anyString());
    doNothing().when(priceTr).logDebug(anyString());  
    // test that price list date is resolved correctly
    Date pricingDate = priceTr.resolveFileDate(settings.getPriceFileNamePattern(), fileName);
    
    //System.out.println("-date-"+pricingDate);
    Calendar c = Calendar.getInstance();
    c.setTime(pricingDate);
    assertEquals(c.get(Calendar.DATE),5);
    assertEquals(c.get(Calendar.MONTH),8);
    assertEquals(c.get(Calendar.YEAR),2018);
    assertEquals("ItemPrices", priceTr.getAdapterName());
    
    // UNIT item, price to zone
    // 790|539672|1111154|1|1|.4444|0|.4444|WHS|1.49||0|0|16|539671
    // UNIT item, price only assortment
    // 790|539672|1111154|1|1|.4444|0|.4444|WHS|||0|0|16|539671
    // CASE item, price to special zone
    // 727|405591|1111154|1|1|2.34|0|2.34|WHS|22||0|0|62|405593
    // CASE item, price only assortment
    // 922|405591|1111154|1|1|2.34|0|2.34|WHS|||0|0|62|405593
    
    // assortment only for normal zone
    // 522|997171|1111154|1|1|10.49|0|10.49|WHS|||0|0|10|997172
    // assortment only for special zone
    // 522|897171|1111154|1|1|10.49|0|10.49|WHS|||0|0|31|997172
    
    
    // test that correct price groups and versions are created
    priceTr.addPriceLists( target, settings, locationHierarchyID, storeName, pricingDate, false, false );
    List<BackBusFile> files = target.getExportFiles();
    assertEquals(1,files.size());

    StringBuilder sb = new StringBuilder();
    for( BackBusFile bf : files ) {
      sb.append("FileName:"+bf.getFileName()+"\n");
      List<PriceList> pls = bf.getBackbus().getPriceList();
      for( PriceList pl : pls ) {
        sb.append("PriceListId:"+pl.getPriceListID()+"\n");
        sb.append("CurrencyId:"+pl.getCurrencyID()+"\n");
        sb.append("HierarchyId:"+pl.getLocationHierarchyID()+"\n");
        sb.append("PriceListName:"+pl.getPriceListName()+"\n");
      }
    }
    //System.out.println("-->"+sb.toString());
    
    String expectedResult = "FileName:ProductGroupDataExport\n" + 
        "PriceListId:ListCost\n" + 
        "CurrencyId:USD\n" + 
        "HierarchyId:LOC-10-10\n" + 
        "PriceListName:501_ListCost\n" + 
        "PriceListId:ApprovedPrice\n" + 
        "CurrencyId:USD\n" + 
        "HierarchyId:LOC-10-10\n" + 
        "PriceListName:501_ApprovedPrice\n";
    
    assertEquals(expectedResult, sb.toString());
    
    /*
    BufferedReader mockedReader = mock(BufferedReader.class);
    TSession session = mock(TSession.class);
    File importFile = mock(File.class);
    when(importFile.getName()).thenReturn(fileName);
    try
    {
      when(priceTr.getReader(argThat(new FileMatcher()))).thenReturn(mockedReader);
    } catch (FileNotFoundException e1)
    {
      e1.printStackTrace();
    }
    ProductDataCache productDataCache = mock(ProductDataCache.class);
    
    try
    {
      priceTr.doConvert(session, importFile, target, settings, productDataCache);
    } catch (TranslatorException | IOException e)
    {
      e.printStackTrace();
    }
    */
  }
  
  @Test
  public void addPrice()
  {
    BackBusContainer target = new BackBusContainer( );
    SAFSettings settings = mock(SAFSettings.class);
    ProductDataCache productDataCache = mock(ProductDataCache.class);
    when( productDataCache.getSkuToItem(anyString(), anyString())).thenReturn("aaa");
    
    // setup test data
    final String fileName = "SFI_MDM_JDA-PRICER_ITEM_PRICE_20180905_TEST.dat";
    when(settings.getPriceGroupNameFormat()).thenReturn("{1}_{0}");
    when(settings.getListCostPriceGroup( )).thenReturn("ListCost");
    when(settings.getRegularEachRetailPriceGroup( )).thenReturn("ApprovedPrice");
    when(settings.getPriceFileNamePattern()).thenReturn("SFI_MDM_JDA-PRICER_ITEM_PRICE_(\\d{4})(\\d{2})(\\d{2})_.*.dat");
    
    String locationHierarchyID = "LOC-10-10";
    String storeName = "501";
    
    PriceFileTranslator priceTr = mock(PriceFileTranslator.class, Mockito.CALLS_REAL_METHODS);
    doNothing().when(priceTr).logInfo(anyString());
    doNothing().when(priceTr).logError(anyString());
    doNothing().when(priceTr).logWarn(anyString());
    doNothing().when(priceTr).logDebug(anyString());
    
    final String ITEM_CODE = "000322";
    final String STORE_NUMBER = "598";
    final String ITEM_CODE_2 = "000332";
    
     priceTr.addItemPrice( productDataCache, settings, "7", STORE_NUMBER,ITEM_CODE, new BigDecimal(7.4), new BigDecimal(8.5), new BigDecimal(9.6),
        new BigDecimal(7.5), 1 , false);
     priceTr.addItemPrice( productDataCache, settings, "7", STORE_NUMBER,ITEM_CODE_2, null, null, null, null, 1, false );
     
     Map<String, Map<String, PriceElement>> prices = priceTr.getStoreItemPrices().get(STORE_NUMBER);
     Map<String, PriceElement> storePrices = prices.get(STORE_NUMBER);
     PriceElement itemPrices = storePrices.get(ITEM_CODE);
     
     // System.out.println("Price: "+itemPrices.getCurrentPriceType());
  // System.out.println("Price: "+itemPrices);
  // System.out.println("Price: "+itemPrices.getListCost());
  // System.out.println("Price: "+itemPrices.getRegEachRetailCost());
     PriceElement skuPrices = itemPrices.getSkuPrices().get(ITEM_CODE);
  // System.out.println("Price: "+skuPrices.getListCost());
  // System.out.println("Price: "+skuPrices.getRegEachRetailCost());
  // System.out.println("Price: "+skuPrices.getCurrentPriceType());
     
     assertEquals(skuPrices.getCurrentPriceType(), 1);
     assertEquals(skuPrices.getListCost(), new BigDecimal(7.4));
     assertEquals(skuPrices.getRegEachRetail(), new BigDecimal(7.5));
     
 
     itemPrices = storePrices.get(ITEM_CODE_2);
     skuPrices = itemPrices.getSkuPrices().get(ITEM_CODE_2);
     assertEquals(skuPrices.getCurrentPriceType(), 1);
     assertNull(skuPrices.getListCost());
     assertNull(skuPrices.getRegEachRetail());
     
  }


}
