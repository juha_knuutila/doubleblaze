package com.i2.se.custom.smartandfinal.integration;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import java.io.*;
import java.text.*;
import java.util.*;

import org.junit.jupiter.api.Test;
import org.mockito.*;

import com.i2.se.app.integrations.imports.TranslatorException;
import com.i2.se.app.integrations.imports.data.*;
import com.i2.se.bd.externalinterfaces.ObjectFactory;

public class DMAFileTranslatorTest
{
  @InjectMocks 
  ObjectFactory manager;
  
  @Test
  public void createItemCode() 
  {
    DMAFileTranslator dmaTranslator = mock(DMAFileTranslator.class, Mockito.CALLS_REAL_METHODS);
    
    //mock(ImportSettings.class);
    doNothing().when(dmaTranslator).logWarn(anyString());
    
    // not regular price price level
    String whseItem = "2";
    String priceLevel = "0";
    String itemCode = dmaTranslator.createItemCode(whseItem, priceLevel, 1);
    assertEquals("000022", itemCode);
    
    whseItem = "2";
    priceLevel = "5";
    itemCode = dmaTranslator.createItemCode(whseItem, priceLevel, 1);
    assertEquals("000022", itemCode);

    whseItem = "2";
    priceLevel = "1";
    itemCode = dmaTranslator.createItemCode(whseItem, priceLevel, 1);
    assertEquals("000021", itemCode);

    whseItem = "2";
    priceLevel = "6";
    itemCode = dmaTranslator.createItemCode(whseItem, priceLevel, 1);
    assertEquals("000021", itemCode);

    whseItem = "2";
    priceLevel = "2";
    itemCode = dmaTranslator.createItemCode(whseItem, priceLevel, 1);
    assertEquals("000002", itemCode);

    // too long
    whseItem = "3322223";
    priceLevel = "0";
    itemCode = dmaTranslator.createItemCode(whseItem, priceLevel, 1);
    assertEquals("3322223", itemCode);
    
    // null
    whseItem = null;
    priceLevel = "0";
    itemCode = dmaTranslator.createItemCode(whseItem, priceLevel, 1);
    assertNull(itemCode);
    
  }
  
  Map<String, List<PricingRuleParameter>> getPriceRuleToParams( )
  {
    Map<String, List<PricingRuleParameter>> ruleMap = new HashMap<String, List<PricingRuleParameter>>();
    List<PricingRuleParameter> paramList = new ArrayList<PricingRuleParameter>();
    PricingRuleParameter p = new PricingRuleParameter("Mod", "1", "USD");
    paramList.add(p);
    
    ruleMap.put("BMSM",paramList );
    return ruleMap;
  }
  
  @Test
  public void bmsm()
  {
    DMAFileTranslator dmaTranslator = mock(DMAFileTranslator.class, Mockito.CALLS_REAL_METHODS);
    BackBusContainer target = new BackBusContainer();
    ProductDataCache productDataCache = mock(ProductDataCache.class);
    File f = new File("./properties/PricerImport.properties");
    boolean found = f.exists();
    if( !f.exists() ) {
      f = new File("./pricer/properties/PricerImport.properties");
      found = f.exists();
    }
    Properties props = new Properties();
    try
    {
      props.load(new FileInputStream(f));
    } catch (IOException e2)
    {
      // TODO Auto-generated catch block
      e2.printStackTrace();
    }
    
    SAFSettings settings = mock(SAFSettings.class);
    File importFile = mock(File.class);
    
    doNothing().when(dmaTranslator).logInfo(anyString());
    doNothing().when(dmaTranslator).logWarn(anyString());
    doNothing().when(dmaTranslator).logDebug(anyString());
    doNothing().when(dmaTranslator).logError(anyString());
    
    Set<String> adjustmentIds = new HashSet<String>( );
    Set<String> allPostFixes = new HashSet<String>();
    Set<String> adjustmentNodeIds = new HashSet<String>( );
    SimpleDateFormat df = new SimpleDateFormat("M/d/yyyy");
    //"settings.getDMADateFieldFormat()
    
    
    when(settings.getAdjustmentNamePatternBMSM()).thenReturn(props.getProperty("sppromo.dma.adjustmentNamePatternBMSM"));
    when(settings.getListCostPriceGroup()).thenReturn(props.getProperty("sppromo.ListCostPriceGroup"));
    when(settings.getPricingRuleBMSM()).thenReturn(props.getProperty("sppromo.dma.PricingRuleBMSM"));
    when(settings.getPricingRuleBMSM()).thenReturn(props.getProperty("sppromo.dma.PricingRuleBMSM"));

    when(settings.getProductLanguageID()).thenReturn(props.getProperty("sppromo.dma.PricingRuleBMSM"));
    when(settings.getRuleParamName("SFS_CASE_LABEL")).thenReturn("SFS_CASE_LABEL");
    when(settings.getRuleParamName("PROMO_SYMBOL")).thenReturn("PROMO_SYMBOL");
    when(settings.getRuleParamName("MIX_N_MATCH")).thenReturn("MIX_N_MATCH");
    when(settings.getRuleParamName("PromoMultiQtyPrice")).thenReturn("PromoMultiQtyPrice");
    when(settings.getRuleParamName("PromoQty")).thenReturn("PromoQty");

    when(productDataCache.getPriceRuleToParams()).thenReturn ( getPriceRuleToParams() );
   
    DateFormat dfFD = new SimpleDateFormat("yyyy-M-d");
    Date dmaFixedStartDate=null;
    Date pricingStartDate = null; 
    try
    {
      dmaFixedStartDate = dfFD.parse("2018-03-01");
      dmaFixedStartDate = dfFD.parse("2018-10-10");
    } catch (ParseException e1)
    {
    
      e1.printStackTrace();
    }
    
    when(settings.getDmaFixedStartDate()).thenReturn(dmaFixedStartDate);
    
    DMAAdjustmentBMSM adjCreator = new DMAAdjustmentBMSM(dmaTranslator, target, productDataCache, settings, importFile, df,adjustmentNodeIds,allPostFixes, adjustmentIds, pricingStartDate);
    
    String s = "0|734471|522|10/18/2018 1:27:11 PM|06137|0004900017114|Hansens Natural Diet Variety Pack Soda|4/6/12z|EA|per OZ|40613700969|0|0|1111101|COMG|1.00000|17114|7|4|10|SODA - SIX PACKS|02|BEVERAGE|19.6900000|29.6900000|1|0.034|11/19/2009|12/31/9999|11/19/09|12/31/99|1119|1231|1|0|9.69|9.69|1|0.034|11/19/2009|12/31/9999|11/19/09|12/31/99|1119|1231|1|0|0|0|0.0000000|1|0|-1|0.00|0.00|0.00|1|0.000|0.00|+CRV|0|0|MKT 1 MESSAGE|MKT 2 MESSAGE|||Cool Data|e|2|0|0|0.00|0.000|-1|0|0|0.00|0|0|0|3|0|0|00C00000000000704010|0|1|4|1";
        
    String[] elements = s.split( "\\|", -1 );
    int trueLineNo = 1;
    String itemCode = "061372";
    String itemOID = "AAAAAA061372";
    
    try
    {
      boolean lineConsumed = adjCreator.createBMSM( elements, trueLineNo, itemCode, itemOID);
      assertTrue(lineConsumed);
      
      List<BackBusFile> files = target.getExportFiles();
      assertEquals(1,files.size());

      StringBuilder sb = new StringBuilder();
      for( BackBusFile bf : files ) {
        BackBus bb = bf.getBackbus();
        sb.append("FileName:"+bf.getFileName()+"\n");
        
        for( PriceAdjustment pa : bb.getPriceAdjustment() ) {
          sb.append("Adjustment:"+pa.getPriceAdjustmentName()+"\n");
          
          sb.append(pa.getEventID()+"\n");
          sb.append(pa.getPriceAdjustmentID()+"\n");
          sb.append(pa.getPriceAdjustmentType()+"\n");
          sb.append(pa.getPriority()+"\n");
          sb.append(pa.getState()+"\n");
        }
        assertNull(bb.getPriceList() );
        assertNull( bb.getPriceListVersion() );
        assertNull(bb.getItemProposedPrice());
        
        assertNull(bb.getAttribute());
        assertNull(bb.getItem());
        assertNull(bb.getItemPrice());
      
      }
      System.out.println("RAGE:"+sb.toString());
      String expected = "FileName:PriceDataExport\n" + 
      "Adjustment:BMSM_522_2009-11-19\n" + 
      "BMSM_522_3610\n" + 
      "BMSM_522_3610\n" + 
      "1\n" + 
      "null\n" + 
      "0\n"; 
      assertEquals( expected, sb.toString());
      
    } catch (TranslatorException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    
    
  }
}
