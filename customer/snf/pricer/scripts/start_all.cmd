@echo off

REM This script starts all ISS services
title Start All ISS Services (%~p0)
setlocal
cd /d %~dp0\..
call scripts\set_iss_env.cmd

REM Archive and clean existing logs first

call %SE_HOME%\scripts\archive_and_clean_logs.cmd nopause
REM call %SE_HOME%\scripts\clean_cached_files.cmd nopause

set LOCKFILE="c:\temp\saf_transfer.lock"
if exist %LOCKFILE% (
    del /F %LOCKFILE%
)

cd scripts

REM CM 516 platform services

if %ISS_PLATFORM% == Author (

  echo RmiRegistry
  sleep 500
  start /MIN start_rmiregistry.cmd

  echo Messaging Service
  sleep 500
  start /MIN start_messaging.cmd

)

REM ISS services

echo Sell Bridge
sleep 500
start /MIN start_sellbridge.cmd

for /L %%i in (1,1,%NUM_BO%) DO echo Engine Server %%i && sleep 500 && start /MIN start_engineserver.cmd

REM Application servers

if %APP_SERVER% == WEBLOGIC (

  echo WebLogic
  sleep 500
  start /MIN weblogic\start_server.cmd

) else if %APP_SERVER% == WEBSPHERE (

  echo WebSphere
  sleep 500
  start /MIN websphere\start_server.cmd

) else if %APP_SERVER% == SUNJES (

  echo SunJES
  sleep 500
  start /MIN sunjes\start_server.cmd

)

REM BPE runtime

if EXIST %SE_HOME%\applications\SCOS (

  echo BPE Locator
  start /MIN scos\start_bpe_locator.cmd

  echo BPE Server
  start /MIN scos\start_bpe_server.cmd

)

echo Done
sleep 500

endlocal
exit
