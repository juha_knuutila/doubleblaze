@echo off

REM This script processes pricer integration files and imports them to the server
title Check and import pricer import files (%~p0)
setlocal
cd /d %~dp0\..
call scripts\set_iss_env.cmd

set CLASSPATH=%SE_CLASSPATH%
set DATERAW1=%date: =%
set DATERAW2=%DATERAW1:.=%
set DATERAW3=%DATERAW2:/=%
set EXP_LOG_FILE=c:\temp\exportlog_%DATERAW3%.log
set SCRIPT_ID=Import_%IMPORT_FEED%_%time:.=%

echo %time:.=% Queue   %SCRIPT_ID% >> %EXP_LOG_FILE%
call scripts\saf_queue.cmd start %SCRIPT_ID% 
echo %time:.=% Started %SCRIPT_ID% >> %EXP_LOG_FILE%

"%JAVA_HOME%\bin\java" %ISS_JVM_ARGS% -Xms512m -Xmx4096m com.i2.se.app.integrations.imports.PricerImport -CheckImportDirectory
set IMPORT_ERROR=%ERRORLEVEL%

call scripts\saf_queue.cmd stop %SCRIPT_ID%
echo %time:.=% Ended   %SCRIPT_ID% >> %EXP_LOG_FILE%

IF %IMPORT_ERROR% NEQ 0 (
  exit /b %IMPORT_ERROR%
)

endlocal