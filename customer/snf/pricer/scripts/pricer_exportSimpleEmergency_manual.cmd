@echo off

REM This script finds all the price changes for the given duration and export them into file.
title Check and export pricer publish files (%~p0)
setlocal
cd /d %~dp0\..

set SCRIPT_ID=SimpleEmergency_manual_%time:.=%
set DATERAW1=%date: =%
set DATERAW2=%DATERAW1:.=%
set DATERAW3=%DATERAW2:/=%
set EXP_LOG_FILE=c:\temp\exportlog_%DATERAW3%.log

echo %time:.=% Queue   %SCRIPT_ID% >> %EXP_LOG_FILE%
call scripts\saf_queue.cmd start %SCRIPT_ID%
echo %time:.=% Started %SCRIPT_ID% >> %EXP_LOG_FILE%

call scripts\set_iss_env.cmd

python "%SE_HOME%\scripts\app\saf_find_emergencies.py" %SE_HOME%\properties\SmartAndFinal.properties %*% 
set IMPORT_ERROR=%ERRORLEVEL%
if %IMPORT_ERROR% EQU 0 (
    echo Nothing to export
    goto :release_lock
)

set CLASSPATH=%SE_CLASSPATH%
set Constraints_MultiValueTypes=Types2

FOR /F "tokens=*" %%g IN ('python -c "import time; print time.time()"') do (SET START_TIME=%%g)

"%JAVA_HOME%\bin\java" %ISS_JVM_ARGS% -Xms2048m -Xmx16384m com.i2.se.app.integrations.exports.priceexport.PriceExport PriceExportSimpleEmergency_manual.properties %*%
set IMPORT_ERROR=%ERRORLEVEL%

IF %IMPORT_ERROR% EQU 0 (
  python %SE_HOME%\scripts\app\saf_combine_approval.py %SE_HOME%\properties\SmartAndFinal.properties %START_TIME% %SE_HOME%\properties\PriceExportSimpleEmergency_manual.properties %*%
  IF %IMPORT_ERROR% EQU 0 (
    set IMPORT_ERROR=%ERRORLEVEL%
  )
)

:release_lock

call scripts\saf_queue.cmd stop %SCRIPT_ID%
echo %time:.=% Ended   %SCRIPT_ID% %IMPORT_ERROR% >> %EXP_LOG_FILE%

IF %IMPORT_ERROR% NEQ 0 (
  exit /b %IMPORT_ERROR%
)
endlocal

