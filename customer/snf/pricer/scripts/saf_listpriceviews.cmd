@echo off

REM This script exports / imports list price views
title Exporting/Importing shared searches (%~p0)
setlocal
cd /d %~dp0\..
call scripts\set_iss_env.cmd

set PYTHONPATH=%PYTHONPATH%;%SE_HOME%\scripts\app

python "%SE_HOME%\scripts\app\saf_listpriceviews.py" %SE_HOME%\properties\SmartAndFinal.properties %*% 

endlocal
if %1a EQU a pause
