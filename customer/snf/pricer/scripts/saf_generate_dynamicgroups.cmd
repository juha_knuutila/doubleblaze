@echo off

REM This script updates the integration database used by Pricer
title Updating Pricer integration database (%~p0)
setlocal
cd /d %~dp0\..
call scripts\set_iss_env.cmd

set PYTHONPATH=%PYTHONPATH%;%SE_HOME%\scripts\app

python "%SE_HOME%\scripts\app\saf_generate_dynamicgroups.py" %SE_HOME%\properties\SmartAndFinal.properties

endlocal
if %1a EQU a pause
