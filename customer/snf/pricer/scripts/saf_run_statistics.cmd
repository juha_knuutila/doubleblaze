@echo off

REM This script processes pricer integration files and imports them to the server
title Check and import pricer import files (%~p0)
setlocal
cd /d %~dp0\..
call scripts\set_iss_env.cmd

set PYTHONPATH=%PYTHONPATH%;%SE_HOME%\scripts\app

python "%SE_HOME%\scripts\app\saf_runstatistics.py" %SE_HOME%\properties\SmartAndFinal.properties %*% 

endlocal
