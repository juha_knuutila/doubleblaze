""" Finds latest export file and combines approval information from LP rows to I rows """

import glob
import os
import re
import ConfigParser
from StringIO import StringIO
import datetime
import shutil
from dbl_common import _setup_logging

class SFFixApproval(object):
    """Class to fix export file"""

    def __init__(self, property_file, start_date_value, export_properties):
        self.property_file = property_file

        cfg = ConfigParser.ConfigParser()
        cfg.read(self.property_file)

        self.logger = _setup_logging(self, cfg, "EXPORT_FILES", "log_file")
        self.export_file_pattern = cfg.get("EXPORT_FILES", "export_files")
        self.descriptions_file_current = cfg.get("EXPORT_FILES", "descriptions_file_current")
        self.descriptions_file_new = cfg.get("EXPORT_FILES", "descriptions_file_new")
        self.start_date = float(start_date_value)
        self.POS_LOCATION = 1
        self.POS_STORE = 3
        self.POS_START_DATE = 4
        self.POS_ITEM_NUMBER = 9
        self.POS_ADJ_NAME = 11
        self.POS_PRICE = 15
        self.KEY_CONSTRAINT_ALL = 'Constraint'
        self.KEY_CONSTRAINT_VALUE = 'ConstraintValue'
        self.KEY_NO_ASSORTMENT = 'No_Assortment'
        self.KEY_CONSTRAINT_REASON = 'ConstraintReason'
        self.price_type_priority = ['UserRegPrice', 'UserSzRegPrice', 'PricerPrice', 'ApprovedPrice', 'SpecialZonePrice', 'RegularEachRetail']
        self.store_location_re = re.compile("\w+\-\w+\-\w+\-\w+")
        self.filter_out_family_items = False
        self.filter_not_assorted_items = False
        self.filter_in_only_constraint_items = False
        self.assortmentRegex = re.compile("PRC_ASS_MDM_\d+ is (required|zero)")

        exp_cfg = ConfigParser.ConfigParser()
        with open(export_properties) as stream:
            stream = StringIO("[top]\n" + stream.read())  # This line does the trick.
            exp_cfg.readfp(stream)
        self.output_file_pattern = exp_cfg.get("top", "ExportFile")
        if exp_cfg.has_option('top', 'FilterOutFamilyItems'):
            self.filter_out_family_items = exp_cfg.get("top", "FilterOutFamilyItems")
        if exp_cfg.has_option('top', 'FilterOutNotAssortedItems'):
            self.filter_not_assorted_items = exp_cfg.get("top", "FilterOutNotAssortedItems")
        if exp_cfg.has_option('top', 'FilterInOnlyConstraintItems'):
            self.filter_in_only_constraint_items = exp_cfg.get("top", "FilterInOnlyConstraintItems")

    def get_latest_file(self, pattern, path, *paths):
        """Returns the name of the latest (most recent) file
        of the joined path(s)"""
        fullpath = os.path.join(path, *paths)
        list_of_files = glob.glob(fullpath)  # You may use iglob in Python3
        if not list_of_files:                # I prefer using the negation
            return None                      # because it behaves like a shortcut

        regex = re.compile(pattern)
        matching = filter(regex.search, list_of_files)
    #    print matching

        if len(matching) <= 0:
            self.logger.error("No matching files found!")
            return None

        latest_file = max(matching, key=os.path.getctime)

        # accept only if not older than 5 minutes
        mod_time = os.path.getmtime(latest_file)
        d = mod_time - self.start_date
        self.logger.info("DIFF:"+str(d))
        if(mod_time - self.start_date) < 0:
            self.logger.error("File older that 5 minutes")
            print "File older that 5 minutes"
            return None

        return latest_file

    def readDescriptions(self):
        """ Reads sign descriptions file """
        #is there new files..
        self.logger.info("File: "+ self.descriptions_file_new)
        fullpath = os.path.join(self.descriptions_file_new)
        list_of_files = glob.glob(fullpath)
        new_files = True
        if not list_of_files:
            new_files = False
        else:
            ## pick the first
            if len(list_of_files) > 1:
                self.logger.warn("More than one new description files: "+ list_of_files)
            new_file = list_of_files[0]
            self.logger.info("File: "+ str(list_of_files))

        target_dir = os.path.dirname(self.descriptions_file_current)
        if not os.path.exists(target_dir):
            os.makedirs(target_dir)

        if new_files:
            if os.path.exists(self.descriptions_file_current):
                #archive
                datestr = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
                arch_file = target_dir+"/arch_"+datestr+"_"+os.path.basename(self.descriptions_file_current)
                os.rename(self.descriptions_file_current, arch_file)

            shutil.copy2(new_file, self.descriptions_file_current)
            os.rename(new_file, target_dir+"/"+os.path.basename(new_file))

        desc_file = open(self.descriptions_file_current, "r")

        descriptions = {}
        for line in desc_file:
            linesp = line.rstrip('\n').split("|")
            descriptions[linesp[0]+"/"+linesp[1]] = linesp
        desc_file.close()

        return descriptions

    def get_zone_price_key(self, store, linesp):
        """ returns unique key for zone store """
        key = store+"_"+linesp[self.POS_ITEM_NUMBER]+"_"+linesp[self.POS_START_DATE]+"_"+linesp[self.POS_ADJ_NAME]
        return key

    def write_irow(self, ofile, working, approvals, start_dates, report_file):
        """ writes processed line to file, fixes start date if needed """

        workingsp = working.split('|')
        approval = ""

        # skip zone rows
        if(not self.store_location_re.match(workingsp[self.POS_LOCATION])):
            return
        
        if self.filter_in_only_constraint_items:
            if self.KEY_CONSTRAINT_VALUE in approvals:
                #self.logger.warn("VALUE:"+str(approvals[self.KEY_CONSTRAINT_VALUE] ))
                if approvals[self.KEY_CONSTRAINT_VALUE] < 1:
                    #self.logger.warn("Filter out..")
                    return
            if self.KEY_NO_ASSORTMENT and approvals[self.KEY_NO_ASSORTMENT]:
                #self.logger.warn("Filter out by assortment: "+str(approvals))
                return
            report = []
            report.extend(workingsp[1:4])
            report.extend(workingsp[9:11])
            report.append(approvals[self.KEY_CONSTRAINT_REASON])
            report_file.write("|".join(report)+"\n")
        
        if len(start_dates) > 0:
            start_date = workingsp[4]
            for pt in self.price_type_priority:
                if pt in start_dates:
                    start_date = start_dates[pt]
                    approval = approvals[pt]
                    break
            working = "|".join(workingsp[0:4]+[start_date]+workingsp[5:])

        if self.KEY_CONSTRAINT_ALL in approvals:
            approval = approvals[self.KEY_CONSTRAINT_ALL]
        
        if approval != "":
            working = working + "|"+ approval
        else:
            working = working + "||"
        ofile.write(working+"\n")

    def read_zone_prices(self, tmp_file):
        """ reads through the file and finds zone prices. Closes the file. """
        zone_prices = {}
        for line in tmp_file:
            if line.startswith("I"):
                linesp = line.split("|")
                store = linesp[3]
                pos = store.find("_SZ")
                
                if pos > 0:
                    zkey = self.get_zone_price_key(store[0:pos], linesp)
                    zone_prices[zkey] = linesp[self.POS_PRICE]
        tmp_file.close()
        return zone_prices

    def process_file(self):
        """ Finds latest export file and processes it """
        file_name = self.get_latest_file(".*\\.csv$", self.export_file_pattern)
        #print fileName
        if file_name is None:
            self.logger.warn("No export files found. Probably there was no prices exported.")
            return

        self.logger.info("FILENAME:"+file_name+"<-")

        tmp_file_name = file_name + ".bak"
        done_file_name = file_name + ".done"
        os.rename(file_name, tmp_file_name)

        orig_file = open(file_name, "w")
        tmp_file = open(tmp_file_name, "r")

        report_file = None
        if self.filter_in_only_constraint_items:
            report_file = open(file_name+"report.csv", "w")

        working = ""
        approvals = {}

        # handle descriptions
        #desc_map = self.readDescriptions()

        # read special zone prices
        #zone_prices = self.read_zone_prices(tmp_file)
        #print "ZONE PRICES:",zone_prices
        #tmp_file = open(tmp_file_name, "r")

        start_dates = {}
        lineno = 0
        for line in tmp_file:
            lineno += 1
            line = line.rstrip("\n")
            linesp = line.split("|")

            # handle special zone
            if line.startswith("I"):
                if self.filter_out_family_items:
                    if linesp[9].find("_family") > 0:
                        approvals = {}
                        start_dates = {}
                        continue
             
                store = linesp[3]
                pos = store.find("_SZ")
                if pos > 0:
                    line = "|".join(linesp[0:3]+[store[0:pos]]+linesp[4:])

            # handle descriptions
            #if line.startswith("I"):
            #    linesp = line.split("|")
            #    promodesc = linesp[10]
            #    if len(promodesc) > 1 and promodesc.find("_") > 0:
            #        pos = promodesc.find("_")
            #        eventid = promodesc[0:pos]

            #        promokey = eventid+"/"+linesp[9]
            #        if promokey in desc_map:
            #            new_desc = desc_map[promokey][3]
            #            self.logger.debug("Found key:"+new_desc)
            #            line = "|".join(linesp[0:38]+[new_desc]+linesp[39:])
            #            # disp loc to 1 ?

            # handle approval
            #    print line
            if line.startswith("I"):
                # check if there's zone price
                #zone_item_key = get_zone_price_key(linesp[self.POS_STORE],linesp)
                #if zone_item_key in zone_prices:
                #    line = "|".join(linesp[0:15]+[zone_prices[zone_item_key]]+linesp[16:])

                if working != "":
                    self.write_irow(orig_file, working, approvals, start_dates, report_file)
                
                working = line
                start_dates = {}
                approvals = {}

            if line.startswith("LP"):
                if "ListCost" not in line:
                    lp_type = linesp[2]
                    start_dates[lp_type] = linesp[8]
                    approvals[lp_type] = "|".join(linesp[6:8])

            if line.startswith("C"):
                cvalue = "|".join(linesp[0:4])
                if self.KEY_CONSTRAINT_ALL in approvals:
                    cvalue = approvals[self.KEY_CONSTRAINT_ALL] + "|" + cvalue
                approvals[self.KEY_CONSTRAINT_ALL] = cvalue

                cvalue = int(linesp[2])
                if "Required Fields By Line Type" == linesp[1]:
                    not_assorted = self.assortmentRegex.search(linesp[3]) <> None and cvalue == 2
                    approvals[self.KEY_NO_ASSORTMENT] = not_assorted
                    
                if cvalue > 0:
                    approvals[self.KEY_CONSTRAINT_REASON] = linesp[3]

                if cvalue < 2 and self.KEY_CONSTRAINT_VALUE in approvals:
                    if approvals[self.KEY_CONSTRAINT_VALUE] > cvalue:
                        cvalue = approvals[self.KEY_CONSTRAINT_VALUE]
                approvals[self.KEY_CONSTRAINT_VALUE] = cvalue
        #           print "APPROVAL:"+approval+":"
        #        else:
        #           print "SKIP : "+ line

        if working != "":
            self.write_irow(orig_file, working, approvals, start_dates, report_file)
        
        if self.filter_in_only_constraint_items:
            report_file.close()
        orig_file.close()
        open(done_file_name, 'a').close()

if __name__ == '__main__':
    import sys

    PROPERTY_FILE = sys.argv[1]
    start_date = sys.argv[2]
    EXPORT_FILE = sys.argv[3]

    HANDLER = SFFixApproval(PROPERTY_FILE, start_date, EXPORT_FILE)
    HANDLER.process_file()
   