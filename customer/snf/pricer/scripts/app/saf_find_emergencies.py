""" Utility to find EMERGENCY_EXPORT flags from database """

import collections
import json
import re
from copy import deepcopy
import ConfigParser
import cx_Oracle
#from securitymodel import SecurityModel, TYPE_LOCATION
#from openpyxl import load_workbook
from dbl_common import _setup_logging


class FindEmergencies(object):
    """ Utilitiy to find EMERGENCY_EXPORT flags from database """

    def __init__(self, property_file):
        """Setup logging and read properties"""
        self.property_file = property_file

        cfg = ConfigParser.ConfigParser()
        cfg.read(self.property_file)

        self.dbuser = cfg.get("DYNAMICGROUPS", "dbuser")
        self.dbpw = cfg.get("DYNAMICGROUPS", "dbpw")
        self.dbconn = cfg.get("DYNAMICGROUPS", "dbconnection")

    def read_emergencies_from_db(self):
        found = False
        for rule in ['WEEKLY SPECIAL', 'AMOUNT OFF', 'BMSM', 'BOGO', 'CLEARANCE', 'FREE_ITEM', 'HARD LIMIT DISCOUNT',
                     'REGULAR RETAIL', 'SELL AT', 'SMS TYPE5', 'SMS TYPE6', 'SZ REGULAR RETAIL']:
            found = self.find_emergencies_by_rule(rule)
            if found:
                break

        return found

    def find_emergencies_by_rule(self, rulename):
        """ Reads shared searches table from database """
        dbconn = cx_Oracle.connect(self.dbuser, self.dbpw, self.dbconn)
        sql_statement = ("select pap_value, pap_adjustment_oid , pap_value_oid, pap_oid from padjustment_param where pap_index = ("
                         " select prp_index from ppri_rule_param p, ppricing_rule r where r.ppr_name = '"+rulename+"' and "
                         " prp_name = 'EMERGENCY_EXPORT' and p.prp_parent_oid = r.ppr_oid ) "
                         " and pap_adjustment_oid in ( "
                         " select pad_oid from padjustment, ppricing_rule where pad_rule_oid = ppr_oid and ppr_name = '"+rulename+"' "
                         " ) and pap_value != '0' and pap_deleted != 1" )

        cursor = dbconn.cursor()
        cursor.execute(sql_statement)

        myfile = None
        records = {}
        
        foundandy = False
        print "Finding emergency flags on "+rulename+": \nValue | OID "
        for row in cursor:
            rowcombined = " "+ row[0]+"    | "+row[1]
            if(row[0] == '1'):
                foundandy = True
            print rowcombined
            # del row

        cursor.close()
        dbconn.close()
        return foundandy


if __name__ == '__main__':
    import sys

    # usage: this_file type parameters
    if len(sys.argv) != 2:
        print sys.argv
        print "Usage: \n", sys.argv[0], " propertyfile"
        exit()

    PROPERTY_FILE = sys.argv[1]

    HANDLER = FindEmergencies(PROPERTY_FILE)

    found = HANDLER.read_emergencies_from_db()
    
    if( found ):
      sys.exit(1)
    else:
      sys.exit(0)