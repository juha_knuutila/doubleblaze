"""Dynamic groups generator for Smart and Final"""
# -*- coding: utf-8 -*-
import ConfigParser
import cx_Oracle
import csv
import uuid
import collections

from dbl_common import _setup_logging

class SmartAndFinalDynamicGroups(object):
    """Class to generate backbus file to which generates dynamic gropus"""

    def __init__(self, property_file):
        """Setup logging and read properties"""
        self.property_file = property_file

        cfg = ConfigParser.ConfigParser()
        cfg.read(self.property_file)

        self.logger = _setup_logging(self, cfg, "PROMOCOP", "gen_dynamicgroups")

        self.dbuser = cfg.get("DYNAMICGROUPS", "dbuser")
        self.dbpw = cfg.get("DYNAMICGROUPS", "dbpw")
        self.dbconn = cfg.get("DYNAMICGROUPS", "dbconnection")

        self.party_file_enabled = cfg.get("DYNAMICGROUPS", "partyfile_enabled")
        self.party_file_path = cfg.get("DYNAMICGROUPS", "partyfile_path")

        self.backbus_file = cfg.get("DYNAMICGROUPS", "backbus_file")
        self.adgroups_file = cfg.get("DYNAMICGROUPS", "adgroups_file")
        self.adgroup_stores_file = cfg.get("DYNAMICGROUPS", "adgroup_stores_file")

        self.adgroup_stores_file_pos_group = 0
        self.adgroup_stores_file_pos_stores = 1

        self.adgroups_file_pos_group = 0
        self.adgroups_file_pos_name = 1

        print "Reading ad groups file: ", self.adgroups_file
        print "Reading ad groups stores file: ", self.adgroup_stores_file
        print "Writing to backbus file: ", self.backbus_file

    def readBusinesses(self):
        """Reads businesses and their hierarchy information from database"""

        db = cx_Oracle.connect(self.dbuser, self.dbpw, self.dbconn)
        print(db.version)

        SQL = "select external_party_id, hi_oid, party_id from hierarchy, party_price, party where hierarchy.hi_oid = pap_hierarchy_oid and party_price.pap_party_id = party_oid"

        FILE = None
        output = None
        if "1" == self.party_file_enabled:
            filename = self.party_file_path
            FILE = open(filename, "w")
            output = csv.writer(FILE, dialect='excel')

        partymapping = {}
        cursor = db.cursor()
        cursor.execute(SQL)
        for row in cursor:
            if "1" == self.party_file_enabled:
                output.writerow(row)
            partymapping[row[0]] = row[1]
        cursor.close()
        db.close()
        if "1" == self.party_file_enabled:
            FILE.close()
        return partymapping

    def writeBackbus(self, groupsandnames, groupsandstores, partymapping):
        """Generates the backbus file based on adgroups information"""
        filename = self.backbus_file
        file = open(filename, "w")
        file.write("<BackBus>\n  <Schema>\n    <schemaID>12.209;17.14;18.4;19.4;20.6;21.2;22.16;23.6;24.1;25.4;26.9;27.153;28.3;29.10;30.8</schemaID>\n  </Schema>\n")

        groupoids = {}

        #for key,name in groupsandnames.items():
        for key, name in groupsandnames.items():
            ui = uuid.uuid4()
            oid = str(ui).replace("-", "").upper()
            groupoids[key] = oid
            file.write("<DynamicGroup>\n")
            file.write("    <dynamicGroupID>")
            file.write(oid)
            file.write("</dynamicGroupID>\n")
            file.write("    <dynamicGroupName><![CDATA[")
            file.write("DG_"+key)
            #file.write( key )
            file.write("]]></dynamicGroupName>\n")
            file.write("    <dynamicGroupType>6</dynamicGroupType>\n")
            file.write("    <endDate></endDate>\n")
            file.write("    <externalFlag>0</externalFlag>\n")
            file.write("    <startDate></startDate>\n")
            file.write("</DynamicGroup>\n")

        for key, name in groupsandnames.items():
            if key == "ADGROUP":
                print ("invalid key:", key, name)
                continue

            if not key in groupsandstores:
                print ('No assignments for this Ad Group:', key)
                continue

            stores = groupsandstores[key]
            #print( "Stores for group ", key, " - ", stores )

            for storeid in stores:
                if storeid not in partymapping:
                    print("Store business not found:", storeid)
                    continue

                ui = uuid.uuid4()
                oid = str(ui).replace("-", "").upper()

                file.write("<DynamicGroupHierarchy>\n")
                file.write("    <dynamicGroupHierarchyID>")
                file.write(oid)
                file.write("</dynamicGroupHierarchyID>\n")
                file.write("    <dynamicGroupID>")
                file.write(groupoids[key])
                file.write("</dynamicGroupID>\n")
                file.write("    <dynamicGroupName><![CDATA[")
                file.write("DG_"+key)
                file.write("]]></dynamicGroupName>\n")

                file.write("    <hierarchyType>6</hierarchyType>\n")
                file.write("    <hierarchyID>")
                file.write(str(partymapping[storeid]))
                file.write("</hierarchyID>\n")
                file.write("</DynamicGroupHierarchy>\n")

        file.write("</BackBus>")

    def readAddgroups(self, partymapping):
        """Reads the available groups and then the stores in groups. Finally calls backbus fle generation"""
        groups = []
        groupsandnames = {}
        groupsandstores = {}

        with open(self.adgroups_file, "r") as csvfile:
            myreader = csv.reader(csvfile, delimiter=';')
            for row in myreader:
                if "BANNERID" == row[0]:
                    #print ("SKIP HEADER: ", row)
                    continue
                if row[0].startswith("#"):
                    print ("SKIP comment: ", row)
                    continue
                name = row[self.adgroups_file_pos_name].strip()
                groupid = row[self.adgroups_file_pos_group].strip()
                if name.startswith("*"):
                    name = name[1:]
                groups.append(groupid)
                groupsandnames[groupid] = name
        groupsandnames = collections.OrderedDict(sorted(groupsandnames.items()))

        with open(self.adgroup_stores_file, "r") as csvfile:
            myreader = csv.reader(csvfile, delimiter=';')
            notfoudgroups = set()
            for row in myreader:

                if len (row ) < 1:
                    print "Skip empty line"
                    continue

                if "BANNERID" == row[0]:
                    #print ("SKIP HEADER: ", row)
                    continue
                if row[0].startswith("#"):
                    print ("SKIP comment: ", row)
                    continue

                gid = row[self.adgroup_stores_file_pos_group].strip()
                store = row[self.adgroup_stores_file_pos_stores].strip()
                if gid not in groups:
                    print('group', gid, 'not found: ', row)
                    notfoudgroups.add(gid)
                    continue
                if gid not in groupsandstores:
                    groupsandstores[gid] = []
                if store in groupsandstores[gid]:
                    print ('Store alredy in this group: ', gid, ', ', store)
                else:
                    groupsandstores[gid].append(store)

            print ("not found groups", list(sorted(notfoudgroups)))

        #print( groupsandnames )
        #print( set(groups) )

        self.writeBackbus(groupsandnames, groupsandstores, partymapping)


if __name__ == '__main__':
    import sys

    PROPERTY_FILE = sys.argv[1]

    HANDLER = SmartAndFinalDynamicGroups(PROPERTY_FILE)
    partymapping = HANDLER.readBusinesses()
    HANDLER.readAddgroups(partymapping)
