""" Utility to find pricebook prices from database """

import collections
import json
import re
from copy import deepcopy
import ConfigParser
import cx_Oracle
from dbl_common import _setup_logging


class FindPBPrices(object):
    """ Utilitiy to find pricebook prices from database """

    def __init__(self, property_file):
        """Setup logging and read properties"""
        self.property_file = property_file

        cfg = ConfigParser.ConfigParser()
        cfg.read(self.property_file)

        self.dbuser = cfg.get("DYNAMICGROUPS", "dbuser")
        self.dbpw = cfg.get("DYNAMICGROUPS", "dbpw")
        self.dbconn = cfg.get("DYNAMICGROUPS", "dbconnection")

    def find_prices(self, pricebooks):
        """ Reads price book prices from database """

        pb_cond = ''
        for pb in pricebooks:
            if pb_cond != '':
                pb_cond = pb_cond + ", "
            pb_cond = pb_cond + "'"+pb+"' "
        
        dbconn = cx_Oracle.connect(self.dbuser, self.dbpw, self.dbconn)
        sql_statement = ("select mo.mo_code, ma_value, pg_id, mp.mp_price2, pv.pgv_date, pg.pg_location_hierarchy, "
                            "mp.mp_change_user, mp.mp_change_time from moduleprice mp, pricegroup pg, module mo, "
                            "pricegroup_version pv, module_attribute ma where pv.pgv_version = mp.mp_version and "
                            "pv.pgv_pricegroup = mp_pricegroup and mo.mo_module = mp.mp_module and "
                            "mo.mo_pmgroup = mp.mp_pmgroup and ma_attribute = 'Family' and ma_module = mo_module "
                            "and pg.pg_pricegroup = mp_pricegroup and pg_id in ("+pb_cond+") and mp_pmgroup = '20001' "
                            "and ((mp_price is not null and mp_price != 0 ) or ( mp_price2 is not null and mp_price2 != 0 )) "
                            "order by mo_code" )

        cursor = dbconn.cursor()
        cursor.execute(sql_statement)

        myfile = None
        records = {}

        foundandy = False
        for row in cursor:
            #rowcombined = " "+ row[0]+"    | "+row[1]+" | "+row[2]+" | "+row[3]+" | "+row[4]
            foundandy = True
            start_date = row[4]
            if start_date:
                start_date = start_date.strftime("%m/%d/%Y")
            change_date = row[7]
            if change_date:
                change_date = change_date.strftime("%m/%d/%Y")
            print "{},{},{},{},{},{},{},{}".format(row[0],row[1],row[2],row[3],start_date,row[5],row[6],change_date)

        cursor.close()
        dbconn.close()
        return foundandy

    def find_pb_prices(self):
        ret = self.find_prices(['UserSzRegPrice', 'UserRegPrice', 'PricerPrice'])
        return ret

if __name__ == '__main__':
    import sys

    # usage: this_file type parameters
    if len(sys.argv) != 2:
        print sys.argv
        print "Usage: \n", sys.argv[0], " propertyfile"
        exit()

    PROPERTY_FILE = sys.argv[1]

    HANDLER = FindPBPrices(PROPERTY_FILE)

    ret = HANDLER.find_pb_prices()
    sys.exit(0)