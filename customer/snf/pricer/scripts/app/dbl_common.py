"""Common utility methods"""

import os
import logging

def _ensure_dir_exists( targetfile ):
    if not os.path.exists( targetfile ):
            newdir = "\\".join( targetfile.split("\\")[:-1] )
            if not os.path.exists( newdir ):
                print ( "Creating paths for ", targetfile )
                os.makedirs( newdir)

def _setup_logging(cls, config, namespace, loggername):
    """Instantiates the logging, reads configuration from the given config file.
    Parameters:
    cls - class name
    config - configuration file
    namespace - config section in config file
    loggername - name that is used for logging"""
    logging.basicConfig(level=logging.getLevelName(config.get(namespace, "log_level")))
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    log_file = config.get(namespace, "log_file")
    _ensure_dir_exists( log_file )
    logfile_handle = logging.FileHandler(log_file)
    logfile_handle.setFormatter(formatter)
    logger = logging.getLogger(loggername)
    logger.addHandler(logfile_handle)
    return logger
