""" Utility to find item level overrides on family prices from database """

import collections
import json
import re
from copy import deepcopy
import ConfigParser
import cx_Oracle
from dbl_common import _setup_logging


class FindFamilyOverrides(object):
    """ Utilitiy to find item level overrides on family prices from database """

    def __init__(self, property_file):
        """Setup logging and read properties"""
        self.property_file = property_file

        cfg = ConfigParser.ConfigParser()
        cfg.read(self.property_file)

        self.dbuser = cfg.get("DYNAMICGROUPS", "dbuser")
        self.dbpw = cfg.get("DYNAMICGROUPS", "dbpw")
        self.dbconn = cfg.get("DYNAMICGROUPS", "dbconnection")

    def find_prices(self, pricebooks):
        """ Reads price book prices from database """

        pb_cond = ''
        for pb in pricebooks:
            if pb_cond != '':
                pb_cond = pb_cond + ", "
            pb_cond = pb_cond + "'"+pb+"' "
        
        dbconn = cx_Oracle.connect(self.dbuser, self.dbpw, self.dbconn)
        sql_statement = ("select mo.mo_code, ma_value, pg_id, mp.mp_price2, pv.pgv_date, pg.pg_location_hierarchy, mp.mp_change_user, "
                         "mp.mp_change_time from moduleprice mp, pricegroup pg, module mo, pricegroup_version pv, module_attribute ma "
                         " where pv.pgv_version = mp.mp_version and pv.pgv_pricegroup = mp_pricegroup and mo.mo_module = mp.mp_module "
                         " and mo.mo_pmgroup = mp.mp_pmgroup and ma_attribute = 'Family' and ma_module = mo_module "
                         " and pg.pg_pricegroup = mp_pricegroup and pg_id in ("+pb_cond+" ) and mp_pmgroup = '20001' "
                         " and ((mp_price is not null and mp_price != 0 ) or ( mp_price2 is not null and mp_price2 != 0 )) " )

        cursor = dbconn.cursor()
        cursor.execute(sql_statement)

        myfile = None
        records = {}
        family_items = {}
        normal_items = {}

        foundandy = False
        #print "Finding prices : \nValue | OID "
        for row in cursor:
            #rowcombined = " "+ row[0]+"    | "+row[1]+" | "+row[2]+" | "+row[3]+" | "+row[4]
            if(row[0] == '1'):
                foundandy = True
            if( row[0].endswith("_family")):
                #print row[0] , "|", row
                family_items[row[0][:-7]] = row
            else:
                normal_items[row[0]] = row
            # del row

        cursor.close()
        dbconn.close()
        return [ foundandy, family_items, normal_items ]

    def analyze_prices(self, family_items, normal_items):
        for itemnumber in normal_items.keys():
            item = normal_items[itemnumber]
            family = item[1]
            item_pb = item[2]
            item_price = item[3]
            item_date = item[4].strftime("%m/%d/%Y")
            item_loc = item[5]
            #print "family:"+str(family)

            if family in family_items:
                family_price = family_items[family][3]
                family_pb = family_items[family][2]
                family_date = family_items[family][4].strftime("%m/%d/%Y")
                family_loc = family_items[family][5]
                if family_price != item_price:
                    if item_loc.startswith(family_loc):
                        if itemnumber.endswith("1"):
                            #skipping cases
                            pass
                        else:
                            print 'Item Price diffent than family, item={}, {}, pb={}, loc={}, date={}, family={}, {}, pb={}, date={}'.format(
                                itemnumber,item_price,item_pb,item_loc,item_date,family,family_price,family_pb,family_date)
                    #else:
                        #print "Not same location ", item, family_items[family]

    def find_item_level_prices(self):
        ret = self.find_prices(['UserSzRegPrice', 'UserRegPrice'])
        self.analyze_prices(ret[1], ret[2])

        ret = self.find_prices(['PricerPrice'])
        self.analyze_prices(ret[1], ret[2])


if __name__ == '__main__':
    import sys

    # usage: this_file type parameters
    if len(sys.argv) != 2:
        print sys.argv
        print "Usage: \n", sys.argv[0], " propertyfile"
        exit()

    PROPERTY_FILE = sys.argv[1]

    HANDLER = FindFamilyOverrides(PROPERTY_FILE)

    ret = HANDLER.find_item_level_prices()

    sys.exit(0)