""" Utility to run statistics """

import ConfigParser
import cx_Oracle
from dbl_common import _setup_logging


class RunStatistics(object):
    """ DB utility for oracle db """

    def __init__(self, property_file):
        """Setup logging and read properties"""
        self.property_file = property_file

        cfg = ConfigParser.ConfigParser()
        cfg.read(self.property_file)

        self.dbuser = cfg.get("DYNAMICGROUPS", "dbuser")
        self.dbpw = cfg.get("DYNAMICGROUPS", "dbpw")
        self.dbconn = cfg.get("DYNAMICGROUPS", "dbconnection")

    def run_stats_db(self):
        """ runs statistics to oracle db """
        dbconn = cx_Oracle.connect(self.dbuser, self.dbpw, self.dbconn)
        statement = """
            BEGIN
                dbms_stats.gather_schema_stats('{owner}');
            END run_stat;
            """.format(owner=self.dbuser)
#        print "CMD:"+statement
        cursor = dbconn.cursor()
        cursor.execute(statement)
        cursor.close()
        dbconn.close()

if __name__ == '__main__':
    import sys

    PROPERTY_FILE = sys.argv[1]
    HANDLER = RunStatistics(PROPERTY_FILE)

    HANDLER.run_stats_db()
  