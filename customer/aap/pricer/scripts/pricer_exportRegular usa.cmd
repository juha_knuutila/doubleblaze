@echo off

REM This script finds all the price changes for the given duration and export them into file.
title Check and export pricer publish files (%~p0)
setlocal
cd /d %~dp0\..

timeout 4 > NUL
set SCRIPT_ID=SimpleDaily_%time:.=%
set DATERAW1=%date: =%
set DATERAW2=%DATERAW1:.=%
set DATERAW3=%DATERAW2:/=%
set EXP_LOG_FILE=c:\temp\exportlog_%DATERAW3%.log

echo %time:.=% Queue   %SCRIPT_ID% >> %EXP_LOG_FILE%
call scripts\dblz_queue.cmd start %SCRIPT_ID%
echo %time:.=% Started %SCRIPT_ID% >> %EXP_LOG_FILE%

call scripts\set_iss_env.cmd

set CLASSPATH=%SE_CLASSPATH%

FOR /F "tokens=*" %%g IN ('python -c "import time; print time.time()"') do (SET START_TIME=%%g)

"%JAVA_HOME%\bin\java" %ISS_JVM_ARGS% -Xms2048m -Xmx16384m com.i2.se.app.integrations.exports.priceexport.PriceExport PriceExportRegular_USA.properties %*%
set IMPORT_ERROR=%ERRORLEVEL%

call scripts\dblz_queue.cmd stop %SCRIPT_ID%
echo %time:.=% Ended   %SCRIPT_ID% %IMPORT_ERROR% >> %EXP_LOG_FILE%

IF %IMPORT_ERROR% NEQ 0 (
  exit /b %IMPORT_ERROR%
)
endlocal
