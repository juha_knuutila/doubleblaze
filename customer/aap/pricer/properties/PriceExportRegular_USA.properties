#
# Default Pricing template ID in case customer hierarchy businesses are not traversed.
# If omitted, system default template is requested and validated
#
DefaultTemplateID=2415AF0F90194AAA8EBC0ED04EB773CD

#
# Default currency used in case customer hierarchy businesses are not traversed
#
DefaultCurrency=USD

#
# Language ID
#
LanguageID=Pen

#
# Export output type, File or Custom
#
ExportOutputType=File

#
# Custom output properties. Must include CustomOutputWriterClass and CustomOutputIdentifier properties.
#
CustomOutputProperties=Kafka.properties

#
# Directory where export file is generated.
# 
ExportDirectory=C:/temp

#
# Filename to export prices to
#
# The value can be fixed string, that may contain variables inside {}'s.
# The following variables are supported, variable names are case insensitive:
#
# ExtractDate    - Extract date in YYYYMMDD format
# ExtractTime    - Extract time in HHMMSS format
#
# It is also possible to use ExportRow variables, for example to direct export to separate files by Business
#ExportFile=PriceExport_{ExtractDate}_{ExtractTime}_{CustomerID}.csv
#
ExportFile=PriceExport_{ExtractDate}_{ExtractTime}.csv

#
# Done file prefix
#
# If no value mentioned for DoneFileExtension and DoneFilePrefix then 
# done file will not be created.
# So the done will format will be :
# {DoneFilePrefix}{ExportFile}{DoneFileExtension}
# 
#PX_PriceExport_{ExtractDate}_{ExtractTime}.csv.done
DoneFilePrefix=PX_

#
# Done file extension
#
# If no value mentioned for DoneFileExtension and DoneFilePrefix then 
# done file will not be created.
# So the done will format will be :
# {DoneFilePrefix}{ExportFile}{DoneFileExtension}
# 
#PX_PriceExport_{ExtractDate}_{ExtractTime}.csv.done
DoneFileExtension=.done

#
# Field separator character, defaults to ";" if omitted.
#
FieldSeparator=;

#
# Comma separated list of export field names
#
ExportFields=Identifier,Name,LocationHierarchyOID,ChangeDate,RegularEndDate,PromoStartDate,PromoEndDate,Total,WinningPrice,Currency
#
# Definitions for specified fields. The field property is ExportField.<FieldName>. The value can be fixed string, that may contain variables inside {}'s.
#
# The following variables are supported, variable names are case insensitive:
#
# {CustomerID}                              Business external ID; empty string if TraverseCustomerChildren = 0
# {LocationID}                              Location business external ID; empty string if TraverseLocationChildren = 0
# {Date}                                    Price change date
# {Box.Code}
# {Box.Name}
# {Box.GroupID}
# {Box.ModuleID}
# {Box.ItemLevel}
# {Box.ItemType}
# {Box.ExternalID}
# {Box.ID}
# {Box.RefCode}
# {Box.ClassID}
# {Box.StyleModuleID}
# {Box.StyleCode} - returns style item code, or item code in case it is not variant
# {Box.VariantCode} - returns item code in case item is variant, otherwise an empty string
# {Box.Price.<Price variable name>}
# {Box.PriceF.<Functional price name>}
# {Box.Attribute.<Attribute name>}
# {Currency}
# {Promo.Description}
# {Promo.StartDate}
# {Promo.EndDate}
# {PossiblePromo.Description}
# {PossiblePromo.StartDate}
# {PossiblePromo.EndDate}
#

ExportField.Identifier=I
ExportField.LocationHierarchyOID={Location.OID}
ExportField.GroupID={Box.GroupID}
ExportField.Name={Box.Code}
ExportField.Description={Box.Name}
ExportField.Customer={CustomerID}
ExportField.Location={LocationID}
ExportField.ChangeDate={Date}
ExportField.RegularEndDate={Item.Attribute.RegEndDate}
ExportField.PromoStartDate={Promo.StartDate}
ExportField.PromoEndDate={Promo.EndDate}
ExportField.Total={Box.PriceF.Total}
ExportField.WinningPrice={Box.Price.WinningPriceBookPrice}
ExportField.Currency={Currency}
ExportField.Style={Box.StyleCode}
ExportField.Variant={Box.VariantCode}


#
# Comma separated list of additional row definitions.
# Each definition will have <name>Fields property to contain field names, similar to ExportFields property.
# In addition, optional <name>Loop property can be set to make the row loop through named entity
# The Loop can have below types:
# 1) Variant - All the mentioned variant fields of the item row will be exported to the file.
# 2) Location - All the mentioned location fields of the item row will be exported to the file.
# 3) Attribute - All the mentioned attribute fields of the item row will be exported to the file.
# 4) Promotion - All the mentioned promotion fields applied on item row will be exported to the file.
# 5) PossiblePromotion - All the mentioned possible promotion fields applied on item row will be exported to the file.
# 6) ListPrice - All the price groups that were used in price calculation.
# 7) Constraint - All constraint results (see ConstraintClasses key).
#
AdditionalRows=
#ItemAttributes,Promotions,PossiblePromotions,Locations,PriceLists

ItemAttributesFields=Identifier,AttributeName,Key1,Key2,Key3,Key4,AttributeValue
ItemAttributesLoop=Attribute
ItemAttributes.Identifier=-IA
ItemAttributes.AttributeName={Attribute.Name}
ItemAttributes.Key1={Attribute.Key1}
ItemAttributes.Key2={Attribute.Key2}
ItemAttributes.Key3={Attribute.Key3}
ItemAttributes.Key4={Attribute.Key4}
ItemAttributes.AttributeValue={Attribute.Value}

PromotionsFields=Identifier,Promo,PromoStartDate,PromoEndDate,ValueStartDate,ValueEndDate
PromotionsLoop=Promotion
Promotions.Identifier=P
Promotions.Promo={Promo.Description}
Promotions.PromoStartDate={Promo.StartDate}
Promotions.PromoEndDate={Promo.EndDate}
Promotions.ValueStartDate={Promo.Value.StartDate}
Promotions.ValueEndDate={Promo.Value.EndDate}

PossiblePromotionsFields=Identifier,Promo,PromoStartDate,PromoEndDate
PossiblePromotionsLoop=PossiblePromotion
PossiblePromotions.Identifier=PP
PossiblePromotions.Promo={PossiblePromo.Description}
PossiblePromotions.PromoStartDate={PossiblePromo.StartDate}
PossiblePromotions.PromoEndDate={PossiblePromo.EndDate}

LocationsFields=Identifier,LocationID,Name,StartDate,EndDate
LocationsLoop=Location
Locations.Identifier=L
Locations.LocationID={Location.ID}
Locations.Name={Location.Name}
Locations.StartDate={Location.StartDate}
Locations.EndDate={Location.EndDate}

PriceListsFields=Identifier,State,PriceGroupID,CurrentPrice,PriceType,StartDate,EndDate,AuditUser,AuditTime
PriceListsLoop=ListPrice
PriceLists.Identifier=LP
PriceLists.State={ListPrice.State}
PriceLists.PriceGroupID={ListPrice.PriceGroupID}
PriceLists.CurrentPrice={ListPrice.Price}
PriceLists.PriceType={ListPrice.PriceType}
PriceLists.StartDate={ListPrice.StartDate}
PriceLists.EndDate={ListPrice.EndDate}
PriceLists.AuditUser={ListPrice.Audit.UserName}
PriceLists.AuditTime={ListPrice.Audit.ChangeTime}

ConstraintsFields=Identifier,ID,WarningCode,WarningLevel,Message
ConstraintsLoop=Constraint
Constraints.Identifier=C
Constraints.ID={Constraint.ID}
Constraints.WarningCode={Constraint.WarningCode}
Constraints.WarningLevel={Constraint.WarningLevel}
Constraints.Message={Constraint.Message}

#
# Start date offset from current date. If omitted, defaults to current date.
#
StartDateOffset=-300

#
# End date offset from start date. If omitted, defaults to 1 calendar month after start date.
#
EndDateOffset=330

#
# Use start date - 1 as reference day to check for price changes (true/false).
# If omitted, the value is true.
#
UseReferenceDate=true

#
# User hierarchy OID. When omitted, hierarchy root is used.
#
#UserHierarchyOID=

#
# Customer hierarchy OID. When omitted, hierarchy root is used.
#
#CustomerHierarchyOID=

#
# Customer hierarchy traversal; 0=hierarchy node only, 1=assigned businesses at hierarchy node, 2=assigned businesses at or below hierarchy node, 3=hierarchy leaf nodes
#
TraverseCustomerChildren=0


#
# Location hierarchy OID. When omitted, hierarchy root is used.
#
LocationHierarchyOID=LUSA

#
# Location hierarchy traversal; 0=hierarchy node only, 1=assigned businesses at hierarchy node, 2=assigned businesses at or below hierarchy node, 3=hierarchy leaf nodes
#
TraverseLocationChildren=0

#
# Product group ID
#
ProductGroupID=6001

#
# Expand style item changes to all variants (true/false)
#
ExpandVariants=true

# Comma separated list of price type IDs. Only adjustments having defined price type are exported.
# Can be left empty.
#
PriceTypes=

# Comma separated list of pricing variable names that are used to check if item price has changed from previous row.
# Can be left empty, in which case all pricing variable values are compared.
#
ComparePriceVariables=
#Retail

#
# price approval level number (0-9). Considered in pricing.
#
PriceLevel=0

#
# Number of price change elements queried in a batch; change this number to smaller in case there are memory related problems querying price changes
#
PriceElementQuerySize=64

#
# Number of items queried in a batch; change this number to smaller in case there are memory related problems on price export
# If omitted, the whole item group is processed in a batch
#
ItemQuerySize=5000

#
# Max number of items in product group in case ItemQuerySize is used
#
MaxItemQueryItemCount=1000000


#
# Number of threads created per CPU core. This number should be kept less than or equal to 1.0.
# For example value 0.5 would use half of the cores for price export price calculation.
#
ThreadsPerCPUCore=1.0

#
# Comma separated list of the price group id's whose pending prices are to be published after price export.
# Regular expression supported * and ?.
#
PublishPriceLists=Dummy
#BECost,basepricelist,costpricelist

#
# Comma separated list of constraint classes used within AdditionalRows for Constraints loop.
#
ConstraintClasses=

#
# Comma separated list of constraint filters to limit exported rows.
#
# Each definition will have <name>Class property to contain full class name of the constraint.
#
# In addition, optional <name>WarningLevel property can be defined for the regular expression to limit exported row based on constraint WarningLevel.
# Default WarningLevels are WARNING_LEVEL_HARD=2, WARNING_LEVEL_SOFT=1, WARNING_LEVEL_OK=0.
#
# And, optional <name>WarningCode property can be defined for the regular expression to limit exported row based on constraint WarningCode.
#
#ConstraintFilters=ProfitCheckConstraint,StockStatusCheckConstraint
#ProfitCheckConstraintClass=com.i2.se.bd.util.constraints.v_1_0.examples.ProfitCheckConstraint
#StockStatusCheckConstraintClass=com.i2.se.bd.util.constraints.v_1_0.examples.StockStatusCheckConstraint
#ProfitCheckConstraintWarningLevel=2
#StockStatusCheckConstraintWarningLevel=2
#ProfitCheckConstraintWarningCode=NegativeProfit
#StockStatusCheckConstraintWarningCode=OutOfStock|StockNotAvailable

