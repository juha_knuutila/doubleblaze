package com.doubleblaze.businessconstraints.price;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Properties;

import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.util.bo.session.TSDKInternalException;
import com.i2.se.bd.util.constraints.v_1_0.ConstraintContext;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemConstraintBase;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemParameters;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemResponse;

/**
 * 
 * @author juhak
 * 
 * Checks that item's price at or over zero.
 * This is sample class only.
 * 
 * 1. if price is below or equal to zero, hard constraint is raised
 * 2. otherwise margin is accepted. 
 *
 */

public class ZeroPriceSample extends PricerItemConstraintBase {
	private static String TOTAL_PRICE_VARIABLE;
	private static double LIMIT_PRICE = 0.0;

	static {
		Properties p = new Properties();
		try {
			InputStream is = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("DoubleBlaze.properties");
			p.load(is);
			TOTAL_PRICE_VARIABLE = p.getProperty("BusinessConstraints.zeroPrice.variable", "Price");
			
			ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
					ZeroPriceSample.class, StaticLogger.DEBUG, 0, 
						"MinimumPrice variables: "+ TOTAL_PRICE_VARIABLE + ", "+ LIMIT_PRICE);
		} catch (IOException e) {
			ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
					ZeroPriceSample.class, StaticLogger.ERROR, 0, "Error reading constraint properties: "+ e );
		}
	}

	public String getID() {
		return "Zero Price Sample";
	}

	public String getDescription() {
		return "Checks that price is over zero";
	}

	public void initialize(ConstraintContext context) {
		super.initialize(context);
	
	}

	public PricerItemResponse run(ConstraintContext context, PricerItemParameters params) {

		BigDecimal price = params.getItem().getPrice(TOTAL_PRICE_VARIABLE);
		
		if (price != null) {
			double priced = price.doubleValue();
		
			if ( priced > LIMIT_PRICE) {
				int warningLevel = 0;
				String warningCode = "Price is above "+LIMIT_PRICE;
				String message = warningCode;
				return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode,
						null, message);
			}

			try
      {
        ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
              ZeroPriceSample.class, StaticLogger.WARN, 0, "-- Item filtered out by ZeroPrice constraint: "+ params.getItem().getCode() + "/"+ params.getItem().getGroupName() );
      } catch (TSDKInternalException e)
      {
        e.printStackTrace();
      }
			
			int warningLevel = 2;
			String warningCode = "ZeroPrice";
			String message = "Price not above zero";
			return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode, null,
					message);
		}
		int warningLevel = 0;
		String warningCode = "MissingPrice";
		String message = "Price not calculated";
		return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode, message);
	}
}
