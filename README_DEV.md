### How to compile DoubleBlazeCustom

There's gradle support. You need to install gradle. 
JDA libraries need to be copied to \lib folder. 

To compile the code you just run
```
    gradle build
```
and the new class files will be placed under pricer folder and installation package is availabe in distributions directory.

### Install/apply the code to your Pricer instance

Modify util/install.properties according to your enviroment. 
Run .\util\install.py in your dev env


### Eclipse

There's eclipse .project file under java/DoubleBlazeCustom folder. Update the paths in .classpath file and import the project.


## Code placement

### java
This contains utilities which are written in java. Including Asset Registration and business constraints.
Compiled classes are placed on pricer/lib/override directory.

### python
This contains common modules

### pricer
This contains the scripts and utilities which will be installed on top of pricer installation


## Setting up initial data

### Empty database
    - Ensure that there are business characteristics in place
    - ListPrices role


### Import MDM data
    - Use pricer import to import: Stores, Components, Items and Item prices

### Import Pricing model



### Create dynamic groups
	- run saf_generate_dynamicgroups.cmd
	- export file is created to C:\smart_and_final\to_pricer\promocop\archive (check from properties)
	- import it to pricer using backbus
	
### Import Promocop data
	- Notice that Ad Groups priority file must be found in order to import promocop data

### Create README.pdf
    - Install "Markdown PDF" plugin to VSC. Open README.md in VCS and export it as pdf.
    