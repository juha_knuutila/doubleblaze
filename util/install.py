import ConfigParser
import os
import re
import sys
import shutil
from shutil import copyfile

## This file copies scripts and properties to installation directory

def copyFile(sourcefile, targetfile, replaces):
    with open(sourcefile) as f:
        new_text = f.read()
        if replaces != None:
            for replace in replaces:
                #print ("Replacing: ", replace[0], "->", replace[1] )
                new_text=new_text.replace(replace[0], replace[1])

    if not os.path.exists(targetfile):
        newdir = "\\".join(targetfile.split("\\")[:-1])
        if not os.path.exists(newdir):
            print ("Creating paths for ", targetfile)
            os.makedirs(newdir)
    with open(targetfile, "w") as f:
        f.write(new_text)

def copyBinaryFile(sourcefile, targetfile):
    if not os.path.exists(targetfile):
        newdir = "\\".join(targetfile.split("\\")[:-1])
        if not os.path.exists(newdir):
            print ("Creating paths for ", targetfile)
            os.makedirs(newdir)
    shutil.copy2(sourcefile, targetfile)

def copyFiles(source_dir, target_dir, files):
    for file_name in files:
        try:
            with open( os.path.join( source_dir, file_name) ) as infile:
                newText = infile.read()
                if not os.path.exists( target_dir ):
                    print ( "Creating paths for ", target_dir )
                    os.makedirs( target_dir )
                with open( os.path.join( target_dir, file_name), "w" ) as outfile:
                    outfile.write( newText )
        except Exception as e:
            print( "Error copying file: %s, %s", file_name, e)


# read where the installation is located

propspath = "\\".join(os.path.abspath(sys.argv[0]).split("\\")[:-1]) +"\\install.properties"
print propspath
cfg = ConfigParser.ConfigParser()
cfg.read(propspath)
installdir = cfg.get("INSTALL", "directory")
safdir = cfg.get("INSTALL", "doubleblazedir")

dbuser = cfg.get("INSTALL", "dbuser")
dbpw = cfg.get("INSTALL", "dbpw")
dbconn = cfg.get("INSTALL", "dbconn")

#print installdir
sourcepath = "\\".join(os.path.abspath(sys.argv[0]).split("\\")[:-2])
pricerpath = sourcepath +"\\pricer"
pythonpath = sourcepath +"\\python"

print( "Copying files to ", installdir )
print( "Doubleblaze data dir ", safdir )

smProps = "\\properties\\DoubleBlaze_aap.properties"

# copy files to installation directory, and replace default values with user settings
copyFile(pricerpath + smProps, installdir + smProps,
         [["D:\\jda\\wec\\8.2", installdir], 
          ["D:\\doubleblaze", safdir],
          ["db_user_name", dbuser], 
          ["db_user_pw", dbpw], 
          ["db_conn_str", dbconn]])

scriptsfromdir = os.path.join(pricerpath, "properties")
scriptstodir = os.path.join(installdir, "properties")
copyFiles(scriptsfromdir, scriptstodir, ["PriceExportRegular.properties" ])

scriptsfromdir = os.path.join(pricerpath, "scripts")
scriptstodir = os.path.join(installdir, "scripts")

copyFiles(scriptsfromdir, scriptstodir, ["dblz_run_statistics.cmd",
                                         "dblz_register_assets.cmd",
                                         "dblz_listpriceviews.cmd",
                                         "dblz_queue.cmd"])

scriptsfromdir = os.path.join(scriptsfromdir, "app")
scriptstodir = os.path.join(scriptstodir, "app")

copyFiles(scriptsfromdir, scriptstodir, [
                                         "dblz_common.py", "dblz_securitymodel.py", "dblz_runstatistics.py",
                                         "dblz_listpriceviews.py"
                                         ] )

registerAssets = "\\lib\\override\\com\\doubleblaze\\RegisterAssets.class"
copyBinaryFile( pricerpath + registerAssets, installdir + registerAssets )

# copy class files
businessConstraints = ["MinMaxPrice.class",
    "\\price\\MinimumPrice.class", "\\price\\MaximumPrice.class", "\\price\\NegativeUSave.class",
    "\\price\\ZeroPrice.class",
    "\\price\\ZeroPriceSample.class" ]

for bf in businessConstraints:
    bfPath = "\\lib\\override\\com\\doubleblaze\\businessconstraints\\" + bf
    copyBinaryFile( pricerpath + bfPath, installdir + bfPath )

#integrationAdapters = ["CategoriesHierarchyFileTranslator.class", "ComponentHierarchyFileTranslator.class"  ]

#for bf in integrationAdapters:
#    bfPath = "\\lib\\override\\com\\i2\\se\\custom\\smartandfinal\\integration\\" + bf
#    copyBinaryFile( pricerpath + bfPath, installdir + bfPath )

# copy portal property files
propertyFiles = [ "pricer-domain.properties"]
for bf in propertyFiles:
    bfPath = "\\applications\\SEUILibrary\\context\\xml\\RetailAdminPortal\\" + bf
    copyBinaryFile( pricerpath + bfPath, installdir + bfPath )

# copy model files
propertyFiles = [ "AAP.cnd"]
for bf in propertyFiles:
    bfPath = "\\applications\\SEUILibrary\\models\\xml\\" + bf
    copyBinaryFile( pricerpath + bfPath, installdir + bfPath )

# copy resource files
resourceFiles = [ "PortalCommon_en_US.properties", "PricingEventUI_en_US.properties"]
for bf in resourceFiles:
    bfPath = "\\applications\\SEUILibrary\\Resources\\" + bf
    copyBinaryFile( pricerpath + bfPath, installdir + bfPath )
    
#copyFile( pythonpath + "\\__init__.py", installdir + "\\scripts\\python\\python\\__init__.py", None )

resourceFiles = ["doubleblaze_version.txt"]
for bf in resourceFiles:
    bfPath = "\\" + bf
    copyBinaryFile( sourcepath + bfPath, installdir + bfPath )

def doReplace(fname, replace):
    
    anythingReplaced = 0

    with open(fname) as f:
        print "Checking file: ",fname
        newText=f.read()
        for repl in replace:
            line = repl[0].strip('\n').strip('\t')
            print "Searching ",line
            src_str = re.compile(repl[0])

            if src_str.search(newText) == None:
                print "Not found"
            else:
                print "Replacing ",line," with ",repl[1].strip('\n').strip('\t')
                print "Doing replace in ", fname
                anythingReplaced = 1
                newText = src_str.sub(repl[1], newText)

        if anythingReplaced == 1:
            candidate = fname + ".bak"
            i = 0
            while os.path.isfile( candidate ):
                i += 1
                candidate = fname+"."+str(i)+".bak"
    
            print(candidate)
            copyfile(fname, candidate)

            print "BACKUP: ", candidate
            print "Writing "
            with open(fname, "w") as f:
                f.write(newText)

def getXmlPattern(repl):
    pattern = "<"+repl[0]+">.*</"+repl[0]+">"
    new_value = "<"+repl[0]+">"+repl[1]+"</"+repl[0]+">"
    return [pattern, new_value]
    
def getPropertyFilePattern(repl):
    pattern = repl[0]+"\s*=.*"
    new_value = repl[0]+" = "+repl[1]
    return [pattern, new_value]

def doSetValue(fname, replace, patternFunction):
    
    anythingReplaced = 0

    with open(fname) as f:
        print "Checking file: ",fname
        newText=f.read()
        for repl in replace:
            fixed_replaces = patternFunction(repl)
            pattern = fixed_replaces[0]
            new_value = fixed_replaces[1]
            print "Setting ",new_value
            src_str = re.compile(pattern)

            match = src_str.search(newText)

            if match == None:
                print "Not found"
            else:
                if match.group() == new_value:
                    print "Already ok"
                else:
                    print "Replacing ",match.group()," with ",new_value
                    print "Doing replace in ", fname
                    anythingReplaced = 1
                    newText = src_str.sub(new_value, newText)

        if anythingReplaced == 1:
            candidate = fname + ".bak"
            i = 0
            while os.path.isfile( candidate ):
                i += 1
                candidate = fname+"."+str(i)+".bak"
    
            print(candidate)
            copyfile(fname, candidate)

            print "BACKUP: ", candidate
            print "Writing "
            with open(fname, "w") as f:
                f.write(newText)

fname = os.path.join(installdir, "properties/Server.properties")
replace = [ ["SocketTimeOut", "-1" ] ]
doSetValue(fname, replace, getPropertyFilePattern)

fname = os.path.join(installdir, "properties/sellservers.ini")
replace = [ 
            ["ThreadMonitorSleepTime", "6000000" ],
            ["ThreadMonitorSlowLimit", "30000000" ],
            ["ThreadMonitorBreakLimit", "60000000" ],
            ["ThreadMonitorHangLimit", "180000000" ],
            ["ThreadMonitorCrashDumpCount", "0" ],
            ["SessionCloseTimeout", "7200" ]  ]
doSetValue(fname, replace, getPropertyFilePattern)

fname = os.path.join(installdir, "properties/DNA.properties")
replace = [ ["SESSION.LOCAL.EXPIRATION", "6500000" ] ]
doSetValue(fname, replace, getPropertyFilePattern)

#fname = os.path.join(installdir, "applications/SEUILibrary/portal-root/WEB-INF/web.xml")
#replace = [ ["session-timeout", "90" ] ]
#doSetValue(fname, replace, getXmlPattern)

#fname = os.path.join(installdir, "applications/SEUILibrary/RCI/WEB-INF/web.xml")
#replace = [ ["session-timeout", "90" ] ]
#doSetValue(fname, replace, getXmlPattern)

#fname = os.path.join(installdir, "applications/SCOS/BPE_runtime/WEB-INF/web.xml")
#replace = [ ["session-timeout", "90" ] ]
#doSetValue(fname, replace, getXmlPattern)


print('Installation done successfully.')
