package com.doubleblaze.businessconstraints.price;

import java.io.*;
import java.math.BigDecimal;
import java.util.Properties;

import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.util.constraints.v_1_0.*;

/**
 * 
 * @author juhak
 * 
 * Checks that item's USave variable value is not negative or zero.
 * 
 * property: BusinessConstraints.usave.variable
 * property: BusinessConstraints.minimumvalue
 * 
 * 1. if price is below or equal to the limit, hard constraint is raised
 *
 */

public class NegativeUSave extends PricerItemConstraintBase {
  private static String USAVE_VARIABLE;
  private static double LIMIT_PRICE;

  static {
    Properties p = new Properties();
    try {
      InputStream is = Thread.currentThread().getContextClassLoader()
          .getResourceAsStream("DoubleBlaze.properties");
      p.load(is);
      USAVE_VARIABLE = p.getProperty("BusinessConstraints.usave.variable", "U_SAVE");
      String limitstr = p.getProperty("BusinessConstraints.usave.limit", "0.0");
      LIMIT_PRICE = Double.parseDouble(limitstr);
      
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
          NegativeUSave.class, StaticLogger.DEBUG, 0, 
            "MaximumPrice variables: "+ USAVE_VARIABLE + ", "+ LIMIT_PRICE);
    } catch (IOException e) {
      ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
          NegativeUSave.class, StaticLogger.ERROR, 0, "Error reading constraint properties: "+ e );
    }
  }

  public String getID() {
    return "Negative USave";
  }

  public String getDescription() {
    return "Checks that USave is not below minimum level";
  }

  public void initialize(ConstraintContext context) {
    super.initialize(context);
  
  }

  public PricerItemResponse run(ConstraintContext context, PricerItemParameters params) {

    BigDecimal price = params.getItem().getPrice(USAVE_VARIABLE);
    
    if (price != null) {
      double priced = price.doubleValue();
    
      if (priced <= LIMIT_PRICE) {
        int warningLevel = 2;
        String warningCode = "USave "+priced+" is <= minimum limit "+LIMIT_PRICE;
        String message = warningCode;
        return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode,
            null, message);
      }

      int warningLevel = 0;
      String warningCode = "ValidPrice";
      String message = "USave  above limit :" + LIMIT_PRICE;
      return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode, null,
          message);
    }
    int warningLevel = 0;
    String warningCode = "MissingPrice";
    String message = "USave is not calculated";
    return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode, message);
  }
}
