package com.doubleblaze.businessconstraints;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Properties;

import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.util.constraints.v_1_0.ConstraintContext;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemConstraintBase;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemParameters;
import com.i2.se.bd.util.constraints.v_1_0.PricerItemResponse;

/**
 * 
 * @author juhak
 * 
 * Checks item price is between min and max prices. Uses variables
 * property: BusinessConstraints.variable.RetailPrice, default RetailPrice
 * property: BusinessConstraints.variable.MinPrice, default MinPrice
 * property: BusinessConstraints.variable.MaxPrice, default MaxPrice
 * 
 * 1. if price is below min or above max, then hard constraint is raised
 * 2. otherwise price is accepted. 
 *
 */

public class MinMaxPrice extends PricerItemConstraintBase {
	private NumberFormat percentFormatter;

	private static String RETAIL_PRICE;
	private static String MIN_PRICE_VARIABLE;
	private static String MAX_PRICE_VARIABLE;

	static {
		Properties p = new Properties();
		try {
			InputStream is = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("SmartAndFinal.properties");
			p.load(is);
			RETAIL_PRICE = p.getProperty("BusinessConstraints.variable.RetailPrice", "RetailPrice");
			MIN_PRICE_VARIABLE = p.getProperty("BusinessConstraints.variable.MinPrice", "MinPrice");
			MAX_PRICE_VARIABLE = p.getProperty("BusinessConstraints.variable.MaxPrice", "MaxPrice");
			
			ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
					MinMaxPrice.class, StaticLogger.DEBUG, 0, 
						"MinMaxPrice variables: RetailPrice="+ RETAIL_PRICE + 
						", MinPrice"+ MIN_PRICE_VARIABLE + 
						", MaxPrice="+MAX_PRICE_VARIABLE);
			
		} catch (IOException e) {
			ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
					PricerItemConstraintBase.class, StaticLogger.ERROR, 0, "Error reading constraint properties: "+ e );
		}
	}

	@Override
	public String getID() {
		return "Min/Max Price";
	}

	@Override
	public String getDescription() {
		return "Checks that item's price is between min man limits";
	}

	@Override
	public void initialize(ConstraintContext context) {
		super.initialize(context);
		percentFormatter = getPercentFormatter(false);
	}

	@Override
	public PricerItemResponse run(ConstraintContext context, PricerItemParameters params) {
		BigDecimal retailPrice = params.getItem().getPrice(RETAIL_PRICE);
		BigDecimal minPrice = params.getItem().getPrice(MIN_PRICE_VARIABLE);
		BigDecimal maxPrice = params.getItem().getPrice(MAX_PRICE_VARIABLE);

		if (retailPrice == null) {
			int warningLevel = WARNING_LEVEL_SOFT;
			String warningCode = "No Price";
			return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode,
					RETAIL_PRICE + " not calculated");
		} else {
			double profit = retailPrice.doubleValue();
			String formattedRetailPrice, formattedMinPrice, formattedMaxPrice;
			
			synchronized (percentFormatter) {
				formattedRetailPrice = percentFormatter.format(profit);
				formattedMinPrice = minPrice == null ? "N/A" : percentFormatter.format(minPrice.doubleValue());
				formattedMaxPrice = maxPrice == null ? "N/A" : percentFormatter.format(maxPrice.doubleValue());
			}
			if (minPrice == null) {
				ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
						MinMaxPrice.class, StaticLogger.DEBUG, 0, "MinPrice not defined. Variable="+MIN_PRICE_VARIABLE);
			}
			if (minPrice != null && retailPrice.compareTo(minPrice) < 0) {
				int warningLevel = WARNING_LEVEL_HARD;
				String warningCode = "Under Min Price";
				String[] warningParameters = { formattedRetailPrice };
				String message = RETAIL_PRICE + " is below Min Price: " + formattedMinPrice;
				return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode,
						warningParameters, message);
			}
			if (maxPrice == null) {
				ObjectFactory.getLogManager().getStaticLogger().addLogEntry(
						MinMaxPrice.class, StaticLogger.DEBUG, 0, "MaxPrice not defined. Variable="+MAX_PRICE_VARIABLE);
			}
			if (maxPrice != null && retailPrice.compareTo(maxPrice) > 0) {
				int warningLevel = WARNING_LEVEL_HARD;
				String warningCode = "Over Max Price";
				String[] warningParameters = { formattedRetailPrice };
				String message = RETAIL_PRICE + " is above Max Price: " + formattedMaxPrice;
				return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode,
						warningParameters, message);
			}
			int warningLevel = WARNING_LEVEL_OK;
			String warningCode = "ValidMinMax";
			String[] warningParameters = { formattedRetailPrice };
			String message = RETAIL_PRICE + " is between min and max prices: "+ 
					formattedMinPrice+ " < "+ formattedRetailPrice + " < " + formattedMaxPrice;
			return context.getConstraintFactory().createPricerItemResponse(warningLevel, warningCode, warningParameters,
					message);
		}
	}

}