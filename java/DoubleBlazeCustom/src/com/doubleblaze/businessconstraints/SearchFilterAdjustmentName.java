package com.doubleblaze.businessconstraints;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.*;

import org.apache.commons.lang3.StringUtils;

import com.i2.se.bd.externalinterfaces.ObjectFactory;
import com.i2.se.bd.externalinterfaces.log.StaticLogger;
import com.i2.se.bd.pricer.*;
import com.i2.se.bd.util.bo.session.TSDKInternalException;
import com.i2.se.bd.util.constraints.v_1_0.*;

/**
 * 
 * @author juhak
 * 
 * Returns OK if adjustment name and oid conditions match one of fired
 * adjustments.
 * Returns SOFT if at least one of conditions matches.
 * Returns HARD if either of conditions don't match.
 *         
 * 
 */

public class SearchFilterAdjustmentName extends PricerItemConstraintBase
{
  private Map<String, String> nameMap;
  TPricer pricer = null;

  @Override
  public String getID()
  {
    return "Adjustment Name Filter";
  }

  @Override
  public String getDescription()
  {
    return "Checks that adjustment name or oid matches the condition";
  }

  @Override
  public void initialize(ConstraintContext context)
  {
    super.initialize(context);
    synchronized (this)
    {
      nameMap = new ConcurrentHashMap<String, String>();
    }
    pricer = context.getSession().getPricer();
  }

  private String getNameByOID(String oid)
  {
    String name = nameMap.get(oid);
    if (name == null)
    {
      TAdjustment adj = pricer.getAdjustment(oid);
      try
      {
        name = adj.getDescription();
      } catch (TSDKInternalException e)
      {
        logError("Error getting adjustment name of : " + oid + ", error=" + e );
        name = "<NA>";
      }
      nameMap.put(oid, name);
    }

    return name;
  }

  @Override
  public PricerItemResponse run(ConstraintContext context, PricerItemParameters params)
  {
    ArrayList<String> messages = new ArrayList<String>();
    int warningLevel = WARNING_LEVEL_OK;
    String scAdjName = ".*";
    String scAdjOID = ".*";
    boolean anymatches = false;
    boolean orMatch = false;
    boolean nameCond = false;
    boolean oidCond = false;
    int condCount = 0;

    try
    {
      TVariableValue tv = params.getPricingParameters().getVariable("Filter.AdjustmentName");
      if (tv != null)
      {
        scAdjName = tv.stringValue();
        if (StringUtils.isNotEmpty(scAdjName))
        {
          nameCond = true;
          condCount++;
        }
      }

      tv = params.getPricingParameters().getVariable("Filter.AdjustmentOID");
      if (tv != null)
      {
        scAdjOID = tv.stringValue();
        if (StringUtils.isNotEmpty(scAdjOID))
        {
          oidCond = true;
          condCount++;
        }
      }

    } catch (TSDKInternalException e)
    {
      logError("Error finding pricing variable ListPriceView. " + e);
    }

    logDebugMsg("Cond count = " + condCount+", name="+nameCond+", oid="+oidCond);
    
    if (nameCond || oidCond)
    {
      TAdjustmentRef[] adjustments = null;
      try
      {
        adjustments = params.getItem().getPrices().getTraceItem().getFiredAdjustmentRefs();

      } catch (TSDKInternalException e)
      {
        logError("Error getting fired adjustments, error= " + e);
      }
      
      if (adjustments == null)
      {
        logDebugMsg( "No adjustments: ");
      } else
      {
        if (nameCond)
        {
          if (StringUtils.isEmpty(scAdjName))
          {
            scAdjName = ".*";
          } else
          {
            scAdjName = scAdjName.replaceAll("\\*", ".*");
          }
        }

        if (oidCond)
        {
          if (StringUtils.isEmpty(scAdjOID))
          {
            scAdjOID = ".*";
          } else
          {
            scAdjOID = scAdjOID.replaceAll("\\*", ".*");
          }
        }
        
        Pattern pattern = Pattern.compile(scAdjName);
        Pattern oidPattern = Pattern.compile(scAdjOID);

        logDebugMsg("Adj:"+adjustments.length+", "+ scAdjName);

        for (TAdjustmentRef adj : adjustments)
        {
          int condMatch = 0;
          
          String oid = adj.getAdjustmentID();
          String name = getNameByOID(oid);

          logDebugMsg("Fired adjustment: " + oid + ", name=" + name);
       
          Matcher matcher = pattern.matcher(name);
          Matcher oidMatcher = oidPattern.matcher(oid);

          if (nameCond)
          {
            if (matcher.matches())
            {
              condMatch++;
              messages.add("Matches " + name + "\n");
            }
          }

          if (oidCond)
          {
            if (oidMatcher.matches())
            {
              condMatch++;
              messages.add("Matches " + oid + "\n");
            }
          }

          logDebugMsg("Match count: " + condMatch+ "/"+ condCount);
          if (condMatch >= condCount)
          {
            anymatches = true;
          } else if ( condMatch > 0 )
          {
            orMatch = true;
          }
        }
      }

    } else
    {
      messages.add("Required fields are not checked");
      logWarn("Not checked");
    }

    logDebugMsg("Any matches : " + anymatches);

    if (anymatches)
    {
      return context.getConstraintFactory().createPricerItemResponse(warningLevel, messages.toString(),
          messages.toString());
    } else if ( orMatch )
    {
      return context.getConstraintFactory().createPricerItemResponse(WARNING_LEVEL_SOFT, messages.toString(),
          messages.toString());
    } else
    {
      logDebugMsg("no matching adjustments found\n");
      return context.getConstraintFactory().createPricerItemResponse(WARNING_LEVEL_HARD, messages.toString(),
          messages.toString());
    }

  }

  private void logDebugMsg(String msg)
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PricerItemConstraintBase.class, StaticLogger.DEBUG, 0,
        msg);
  }

  private void logError(String msg)
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PricerItemConstraintBase.class, StaticLogger.ERROR, 0,
        msg);
  }

  private void logWarn(String msg)
  {
    ObjectFactory.getLogManager().getStaticLogger().addLogEntry(PricerItemConstraintBase.class, StaticLogger.WARN, 0,
        msg);
  }
  
}