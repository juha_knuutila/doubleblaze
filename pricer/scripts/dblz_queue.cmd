@echo off

REM This scripts lets only one process to go through at a time.
setlocal
cd /d %~dp0\..

set LOCKFILE="c:\temp\dblz_transfer.lock"
set LOCKFILE_PASS="c:\temp\dblz_transfer.nolock"

if exist %LOCKFILE_PASS% (
    exit /b 0
)

if "%1" == "stop" (
    del %LOCKFILE%
    exit /b 0
)

REM Is there lock

:loop

echo In queue %2

if exist %LOCKFILE_PASS% (
    exit /b 0
)

if exist %LOCKFILE% (
  timeout 10 > NUL
  goto :loop

) else (
    REM Request lock
    fsutil file createnew  %LOCKFILE% 1
)

endlocal