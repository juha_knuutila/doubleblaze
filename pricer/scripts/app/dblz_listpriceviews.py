""" Utility to export and import list price views """

import collections
import json
import re
from copy import deepcopy
import ConfigParser
import cx_Oracle
from dblz_securitymodel import SecurityModel, TYPE_LOCATION
from openpyxl import load_workbook
from dblz_common import _setup_logging

KEY_NAME = 'Name'
KEY_OWNER = 'Owner'
KEY_PRIVATE = 'Private'
KEY_SEARCHCRITERIA = 'SearchCriteria'
KEY_SEARCHSETTINGS = 'SearchSettings'
KEY_PRICEBOOKLOCATION = 'PriceBookLocation'

PRICE_BOOK_PREFIX = "PriceBookLocation="

class LPView(object):
    """ utility object to model ListPriceView """

    properties = collections.OrderedDict()
    search_settings = collections.OrderedDict()

    def add_property(self, prop_name, value):
        """ View general properties like name an owner """
        self.properties[prop_name] = value

    def add_search_setting(self, prop_name, value):
        """ View Search Criteria etc """
        self.search_settings[prop_name] = value

    def get_copy(self):
        """ Retuns duplicate of this object """
        acopy = LPView()
        acopy.properties = deepcopy(self.properties)
        acopy.search_settings = deepcopy(self.search_settings)
        return acopy

    def set_field(self, field, name):
        """ inserts or updates a field value """
        self.properties[field] = name

    def set_settings(self, field, name):
        """ inserts or updates setting value """
        self.search_settings[field] = name

    def get_search_setting(self, field):
        """ returns a search criteria """
        return self.search_settings[field]

    def get_field(self, field):
        """ returns a filed value """
        return self.properties[field]

    def get_settings(self):
        """ returns all search criterias """
        return self.search_settings

    def is_expandable(self):
        """ returns true if there's a rule how this criteria gets expanded """
        return len(self.search_settings) > 0

    def get_location_rule(self):
        """ returns price book location search criteria """
        rules = []
        location_rules = self.properties[KEY_PRICEBOOKLOCATION]
        for location_rule in location_rules:
            location_rule = location_rule.replace("*", ".*")
            rules.append(location_rule)
        return rules

    def __repr__(self):
        return str(self)

    def __str__(self):
        return "View PROPS: "+str(self.properties)+"\n SETT: "+str(self.search_settings)


class ListPriceViews(object):
    """ Utilities to read security model file and produce needed export files """

    def __init__(self, property_file):
        """Setup logging and read properties"""
        self.property_file = property_file

        cfg = ConfigParser.ConfigParser()
        cfg.read(self.property_file)

        self.logger = _setup_logging(self, cfg, "SECURITYIMPORT", "integration_database")
        self.shared_searches_file = cfg.get("SHAREDSEARCHES", "sharedSearchesFile")
        self.listpriceviews_xls = cfg.get("SHAREDSEARCHES", "listPriceViewsFile")

        self.dbuser = cfg.get("DYNAMICGROUPS", "dbuser")
        self.dbpw = cfg.get("DYNAMICGROUPS", "dbpw")
        self.dbconn = cfg.get("DYNAMICGROUPS", "dbconnection")

    def read_searches_from_db(self, write_to_file):
        """ Reads shared searches table from database """
        dbconn = cx_Oracle.connect(self.dbuser, self.dbpw, self.dbconn)
        sql_statement = ("select ss_owner, ss_name, ss_value, ss_is_private from shared_searches where ss_is_deleted != '1' and  ss_page_name = 'ListPrice'")
        cursor = dbconn.cursor()
        cursor.execute(sql_statement)

        myfile = None
        if write_to_file is True:
            myfile = open(self.shared_searches_file, "w")
        records = {}

        for row in cursor:
            rowcombined = row[0]+"|"+row[1]+"|"+str(row[3])+"|"+str(row[2])+"|"+str(row[3])
            if write_to_file is True:
                myfile.write(rowcombined +"\n")
            key = row[0]+"/"+row[1]
            records[key] = rowcombined
            del row

        cursor.close()
        dbconn.close()
        if write_to_file is True:
            myfile.close()
        return records

    def write_searches_to_db(self):
        """ Writes shared searches from excel to database """

        # read existing searches
        old = self.read_searches_from_db(False)
        #newViews = []
        to_be_inserted = []
        to_be_updated = []

        over_lapping = False
        myfile = open(self.shared_searches_file, "r")

        pattern = re.compile( r"(\{.+\}|[^|]+)", re.M | re.DOTALL );
        for line in myfile:
            #fields = line.split("|")
            fields = pattern.findall( line )
            if fields:
                #print "GROUPS: ", match
                #for g in match:
                #    print "G:", g
            
                #newViews.append(fields)
                key = fields[0]+"/"+fields[1]
                if key in old:
                    print("This view already exists: "+key)
                    over_lapping = True
                    to_be_updated.append(fields)
                else:
                    to_be_inserted.append(fields)
            else:
                print "NO match:", line

        if over_lapping:
            answer = raw_input('Do you want to override (Y/N)?')
            if answer.upper() != "Y":
                print("No changes done.")
                return

        print("Writing to db...")
        dbconn = cx_Oracle.connect(self.dbuser, self.dbpw, self.dbconn)
        cur = dbconn.cursor()
        statement = 'insert into shared_searches(ss_page_name, ss_owner, ss_name, ss_value, ss_is_private, ss_is_deleted) values (:1, :2, :3, :4, :5, :6)'
        for fields in to_be_inserted:
            cur.execute(statement, ('ListPrice', fields[0], fields[1], fields[3], int(fields[2]), 0))
        dbconn.commit()
        cur.close()

        cur = dbconn.cursor()
        statement = 'update shared_searches set ss_value = :1, ss_is_private = :2 where ss_owner = :3 and ss_name = :4'
        for fields in to_be_updated:
            cur.execute(statement, (fields[3], int(fields[2]), fields[0], fields[1]))
        dbconn.commit()
        cur.close()

    def update_shared_searches(self):
        """ Reads shared searches from excel and database and updates them to db """
        self.read_searches_from_db(False)

    def find_anchor(self, name, column, sheet):
        """ finds row in the sheet where the given anchor is located """
        row = 1
        while(sheet.cell(row=row, column=column).value != name  and row < sheet.max_row):
            row += 1

        if row >= sheet.max_row:
            print "Anchor was not found! ", name, column, sheet
            exit()
        return row

    def write_to_transfer_file(self, all_views):
        """ Writes views to transfer file """

        myfile = open(self.shared_searches_file, "w")

        for view in all_views:
           # print view
           # print "KEYS:", view.keys
            owner = view.get_field(KEY_OWNER)
            name = view.get_field(KEY_NAME)
            settings = view.get_settings()
            private = view.get_field(KEY_PRIVATE)
            myfile.write(owner+"|"+name+"|"+json.dumps(settings)+"|"+str(private)+"\n")

        myfile.close()


    def read_model_file(self):
        """ Reads excel file and generates saved searches export file  """

        # Load in the workbook
        workbook = load_workbook(self.listpriceviews_xls)
        uisheet = workbook['ListPriceViews']

        orig_criteria_col = criteria_col = 2
        # titleRow where column titles are, row - current row
        row = title_row = self.find_anchor("Criteria", criteria_col, uisheet)+1

        all_views = []

        while row < uisheet.max_row:
            row += 1
            criteria_col = orig_criteria_col
            if uisheet.cell(row=row, column=criteria_col).value is None:
                continue
            else:
                found_criteria = True
                curr_view = LPView()
                if uisheet.cell(row=title_row, column=criteria_col).value == KEY_NAME:
                    name = uisheet.cell(row=row, column=criteria_col).value
                    criteria_col += 1
                    curr_view.add_property(KEY_NAME, name)
                else:
                    print("Error, Name column not found at ", row, criteria_col)

                if uisheet.cell(row=title_row, column=criteria_col).value == KEY_OWNER:
                    owner = uisheet.cell(row=row, column=criteria_col).value
                    criteria_col += 1
                    curr_view.add_property(KEY_OWNER, owner)
                else:
                    print("Error, Owner column not found at ", row, criteria_col)

                if uisheet.cell(row=title_row, column=criteria_col).value == KEY_PRIVATE:
                    private = uisheet.cell(row=row, column=criteria_col).value
                    curr_view.add_property(KEY_PRIVATE, private)
                else:
                    print("Error, Private column not found at ", row, criteria_col)

                criteria_col += 2

                print("Doing : ", curr_view)

                while found_criteria:
                    criteria = uisheet.cell(row=title_row, column=criteria_col).value
                    if criteria is None:
                        found_criteria = False
                    else:
                        condition_row = row
                        value_type = uisheet.cell(row=title_row, column=criteria_col+1).value
                        condition = uisheet.cell(row=condition_row, column=criteria_col).value

                        if value_type == 'm': # Metadata
                            value_list = []
                            found = True
                            while found:
                                condition = uisheet.cell(row=condition_row, column=criteria_col).value
                                if condition is None:
                                    found = False
                                else:
                                    value_list.append(condition)
                                condition_row = condition_row + 1

                            curr_view.add_property(criteria, value_list) # meta info is lost

                        elif value_type == 's': # Single value
                            curr_view.add_property(criteria, condition)
                            #found = False
                        else:
                            value_list = []
                            found = True
                            while found:
                                condition = uisheet.cell(row=condition_row, column=criteria_col).value
                                if condition is None:
                                    found = False
                                else:
                                    text = uisheet.cell(row=condition_row, column=(criteria_col+1)).value
                                    # maybe have data type next to condition description
                                    orig_text = uisheet.cell(row=condition_row, column=criteria_col+2).value

                                    if orig_text is None:
                                        value_list.append({'id':condition, 'text':text})
                                    else:
                                        value_list.append({'id':condition, 'text':text, 'originalText':orig_text})

                                condition_row = condition_row + 1
                            curr_view.add_search_setting(criteria, value_list)

                    criteria_col += 4
                #print ("CI:", curr_view)

                # are there expandable criterias?
                self.expand_conditions(curr_view, all_views)

        self.write_to_transfer_file(all_views)

    def expand_conditions(self, curr_view, all_views):
        """ expands the search views if there are such rules """

        # Load product hierarchies
        sec_model = SecurityModel(PROPERTY_FILE)
        prod_hier = sec_model.read_hierarchies(TYPE_LOCATION, sec_model.location_hierarchy_root, False)

        # are there expandable criterias?
        expand_done = False
        if curr_view.is_expandable():
            location_rules = curr_view.get_location_rule()
            print ("Expanding: ", location_rules)

            expanded_conditions = []
            for location in prod_hier:
                for location_rule in location_rules:
                    if re.match(location_rule, str(location[1])):
                        expanded_conditions.append(location)

            # is there pricebook condition
            contains_price_book_condition = False

            for search_criteria in curr_view.get_search_setting(KEY_SEARCHCRITERIA):
                print "C", search_criteria
                if search_criteria['id'].startswith(PRICE_BOOK_PREFIX):
                    contains_price_book_condition = True

            if contains_price_book_condition:
                #do expand
                for expanded_condition in expanded_conditions:
                    acopy2 = curr_view.get_copy()

                    new_name = acopy2.get_field(KEY_NAME) + "_" + expanded_condition[2].strip().replace(" ", "")
                    acopy2.set_field(KEY_NAME, new_name)

                    new_criteria = []
                    for search_criteria in curr_view.get_search_setting(KEY_SEARCHCRITERIA):
                        new_sc = search_criteria
                        if search_criteria['id'].startswith(PRICE_BOOK_PREFIX):
                            new_sc = {'id' : PRICE_BOOK_PREFIX+expanded_condition[1],
                                      'text' : PRICE_BOOK_PREFIX+expanded_condition[2].strip(),
                                      'originalText' : expanded_condition[0]
                                     }
                        new_criteria.append(new_sc)

                    acopy2.set_settings(KEY_SEARCHCRITERIA, new_criteria)
                    all_views.append(acopy2)
                    expand_done = True

        if not expand_done:
            all_views.append(curr_view)

if __name__ == '__main__':
    import sys

    # usage: this_file type parameters
    if len(sys.argv) != 3:
        print sys.argv
        print "Usage: \n", sys.argv[0], " propertyfile operation"
        print "Where operation = read_from_db|write_to_db|read_from_xls\n"
        exit()

    OPERATION = sys.argv[2]
    PROPERTY_FILE = sys.argv[1]

    HANDLER = ListPriceViews(PROPERTY_FILE)

    print "Doing operation: ", OPERATION

    if OPERATION == "read_from_db":
        HANDLER.read_searches_from_db(True)
    elif OPERATION == "write_to_db":
        HANDLER.write_searches_to_db()
    elif OPERATION == "read_from_xls":
        HANDLER.read_model_file()
    else:
        print "Unknown operation: ", OPERATION
