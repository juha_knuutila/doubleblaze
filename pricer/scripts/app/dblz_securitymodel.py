""" Utility to generate various security exports """

import csv
import collections
import ConfigParser
from openpyxl import load_workbook
import cx_Oracle
from dblz_common import _setup_logging

TYPE_LOCATION = 6
TYPE_PRODUCT = 2

ASSET_TYPE_PRICEGROUP = "Price Group"
ASSET_TYPE_PRICINGVARIABLE = "Pricing Variable"
ASSET_TYPE_PRICERHIERARCHY = "Pricer Hierarchy"

class SecurityModel(object):
    """ Utilities to read security model file and produce needed export files """

    def __init__(self, property_file):
        """Setup logging and read properties"""
        self.property_file = property_file

        cfg = ConfigParser.ConfigParser()
        cfg.read(self.property_file)

        self.logger = _setup_logging(self, cfg, "SECURITYIMPORT", "integration_database")

        self.security_model_xls = cfg.get("SECURITYIMPORT", "securityModelXLS")
        self.variable_file = cfg.get("SECURITYIMPORT", "variablefile")
        self.hierarchy_file = cfg.get("SECURITYIMPORT", "hierarchyfile")
        self.pricegroup_file = cfg.get("SECURITYIMPORT", "pricegroupfile")
        self.business_mapping_file = cfg.get("SECURITYIMPORT", "businessMappingFile")
        self.assets_file = cfg.get("SECURITYIMPORT", "assetsFile")
        self.ui_security_file = cfg.get("SECURITYIMPORT", "uiSecurityFile")

        self.product_hierarchy_root = cfg.get("SECURITYIMPORT", "productHierarchyRoot")
        self.location_hierarchy_root = cfg.get("SECURITYIMPORT", "locationHierarchyRoot")

        self.dbuser = cfg.get("DYNAMICGROUPS", "dbuser")
        self.dbpw = cfg.get("DYNAMICGROUPS", "dbpw")
        self.dbconn = cfg.get("DYNAMICGROUPS", "dbconnection")

        # mapping from business name to party_oid
        self.businessMapping = {}
        self.read_business_mapping()

    def print_assets(self):
        """ prints out assets in the uisecurit file """
        with open(self.ui_security_file, "r") as csvfile:
            myreader = csv.reader(csvfile, delimiter=';')
            for row in myreader:
                print row[2]

    def get_next_col(self, col):
        """ returns next column name in excel """
        if col == 'Z':
            print "last col"
        num = ord(col)+1
        return chr(num)

    def generate_ui_security_file(self):
        """ Read ui security model """
        # Load in the workbook
        wb = load_workbook(self.security_model_xls)
        uisheet = wb.get_sheet_by_name('UI security')

        # how many roles
        rolecount = 0
        found = True
        roles = []
        col = 'B'
        while found:
            cell = col+'2'
            role = uisheet[cell].value
            if role is None:
                found = False
            else:
                roles.append(role)
                rolecount += 1
                print("found role: ", role)
                col = self.get_next_col(col)

        # how many assets
        row = 3
        found = True
        assets = collections.OrderedDict()
        #print "Roles:", rolecount

        while found:
            cell = 'A'+str(row)
            asset = uisheet[cell].value
            if asset is None:
                found = False
            else:
                # read access
                access = []
                for rolecol in range(2, rolecount+2):
                    #print rolecol
                    access.append(uisheet.cell(row=row, column=rolecol).value)
                assets[asset] = access
                #print access
                row += 1

        # output model
        myfile = open(self.ui_security_file, "w")
        for asset in assets:
            row = 'UI_CONTROL;'+asset+';read;'
            col = 0
            for access in assets[asset]:
                if access == 'x' or access == 'X':
                    if col > 0:
                        row += ','
                    row += roles[col]
                col += 1

            print row
            myfile.write(row+"\n")

        myfile.close()

    def read_business_mapping(self):
        """ Reads business-party_oid mapping from database """
        db = cx_Oracle.connect(self.dbuser, self.dbpw, self.dbconn)
        SQL = ("select bus_name, party_oid from party, business where subtype_ccode = 'BUSINESS' and party_id = bus_sub_id")
        cursor = db.cursor()
        cursor.execute(SQL)
        myfile = open(self.business_mapping_file, "w")

        for row in cursor:
            self.businessMapping[row[0]] = row[1]
            rowcombined = str(row[0])+"|"+row[1]
            #print row
            myfile.write(rowcombined +"\n")

        cursor.close()
        db.close()
        myfile.close()

    def read_roles(self, sheet, col, row):
        """ reads roles from sheet at given position """
        rolecount = 0
        found = True
        roles = []
        roles_col = col
        while found:
            rolename = sheet.cell(row=row, column=roles_col).value

            if rolename is None:
                found = False
            else:
                zone = sheet.cell(row=row-3, column=roles_col).value
                storegroup = sheet.cell(row=row-2, column=roles_col).value
                store = sheet.cell(row=row-1, column=roles_col).value
                role = {'role':rolename, 'zone':zone, 'storegroup':storegroup, 'store':store}
                roles.append(role)
                rolecount += 1
                print("found role: ", role)
                roles_col += 1
        return roles

    def java_string_hashcode(self, s):
        """ Returns same hashcode than java code would """
        h = 0
        for c in s.upper():
            h = (31 * h + ord(c)) & 0xFFFFFFFF
        return ((h + 0x80000000) & 0xFFFFFFFF) - 0x80000000

    def write_assets(self, assets, asset_type):
        """ writes all assets to file, this file will be used to check if assets are registered """
        file = open(self.assets_file, "w")
        for asset in assets:
            file.write(asset_type + "|"+ asset +"\n")
        file.close()

    def find_anchor(self, name, column, sheet):
        """ finds row in the sheet where the given anchor is located """
        row = 1
        while(sheet.cell(row=row, column=column).value != name  and row < sheet.max_row):
            row += 1

        if row >= sheet.max_row:
            print "Anchor was not found! ", name, column, sheet
            exit()
        return row

    def write_privilege(self, file, party, identifier, ownership, operation, rolename, asset_type):
        """ outputs SecurityPrivilege entry with given parameters """

        file.write("  <SecurityPrivilege>\n"
                   "    <domainRef>BLM:CM.Security.Domain:["+party+"]</domainRef>\n"
                   "    <identifier>"+identifier+"</identifier>\n"
                   "    <ownershipFlag>"+ownership+"</ownershipFlag>\n"
                   "    <securityAssetTypeName>"+asset_type+"</securityAssetTypeName>\n"
                   "    <securityOperationName>"+operation+"</securityOperationName>\n"
                   "    <securityRoleName>"+rolename+"</securityRoleName>\n"
                   "  </SecurityPrivilege>\n")


    def generate_privileges_for_assets(self, filename, assets, roles, identifierPrefix, asset_type):
        """ Generates backbus securityPrivilegeExport.xml file with given parameters """
        
        file = open(filename, "w")

        file.write("<BackBus>\n"
                   "  <Schema>\n"
                   "    <schemaID>12.210;17.14;18.4;19.4;20.6;21.2;22.16;23.6;24.1;25.4;26.9;27.153;28.3;29.10;30.8</schemaID>\n"
                   "  </Schema>\n")

        for asset in assets:
            col = 0
            for access in assets[asset]:
                if access != None:
                    access = access.upper().split(',')
                    party = self.businessMapping[roles[col]['store']]
                    print party
                    if 'R' in access or 'RO' in access:
                        ownership = "false"
                        if 'RO' in access:
                            ownership = "true"
                        rolename = roles[col]['role']
                        self.write_privilege(file, party, identifierPrefix+asset+"]", ownership, "read", rolename, asset_type)
                    if 'W' in access or 'WO' in access:
                        ownership = "false"
                        if 'WO' in access:
                            ownership = "true"
                        rolename = roles[col]['role']
                        self.write_privilege(file, party, identifierPrefix+asset+"]", ownership, "write", rolename, asset_type)
                    if 'D' in access or 'DO' in access:
                        ownership = "false"
                        if 'DO' in access:
                            ownership = "true"
                        rolename = roles[col]['role']
                        self.write_privilege(file, party, identifierPrefix+asset+"]", ownership, "delete", rolename, asset_type)
                    if 'X' in access or 'XO' in access:
                        ownership = "false"
                        if 'XO' in access:
                            ownership = "true"
                        rolename = roles[col]['role']
                        self.write_privilege(file, party, identifierPrefix+asset+"]", ownership, "execute", rolename, asset_type)

                col += 1

        file.write("</BackBus>\n")
        file.close()

    def read_privileges(self, uisheet, rolecount, assetColumn, row, firstRoleColumn):
        """ Read which access has been given to which role in the sheet """
        found = True
        assets = collections.OrderedDict()
        while found:
            cell = uisheet.cell(row=row, column=assetColumn)
            asset = cell.value
            if asset == None:
                found = False
            else:
                # read access
                access = []
                wasvalues = False
                for rolecol in range(firstRoleColumn, rolecount+firstRoleColumn):
                    #print rolecol
                    val = uisheet.cell(row=row, column=rolecol).value
                    access.append(val)
                    if val != None:
                        wasvalues = True
                if wasvalues:
                    assets[asset] = access
                #print access
                row += 1

        return assets

    def generate_variable_file(self):
        """ Generates securityPrivilegeExport file for variables, reads model from excel sheet """

        # Load in the workbook
        wb = load_workbook(self.security_model_xls)
        uisheet = wb.get_sheet_by_name('Variable')
        roles = self.read_roles(uisheet, 3, 5)

        # how many assets
        # find anchor "OID" in first column
        row = self.find_anchor("OID", 1, uisheet) +1
        rolecount = len(roles)

        print "Found roles", roles

        assets = self.read_privileges(uisheet, rolecount, 1, row, 3)

        #write assets
        self.write_assets(assets, ASSET_TYPE_PRICINGVARIABLE)
        self.generate_privileges_for_assets(self.variable_file, assets, roles, "BLM:CM.Pricing.Variable:[", ASSET_TYPE_PRICINGVARIABLE)

    def generate_hierarchy_file(self):
        """ Generates securityPrivilegeExport file for location hierarchies, reads model from excel sheet """

        # Load in the workbook
        wb = load_workbook(self.security_model_xls)
        uisheet = wb.get_sheet_by_name('Location')
        roles = self.read_roles(uisheet, 4, 5)

        # how many assets
        # find anchor "OID" in first column
        row = self.find_anchor("OID", 2, uisheet) +1
        assets = collections.OrderedDict()
        rolecount = len(roles)

        print "Found roles", roles

        assets = self.read_privileges(uisheet, rolecount, 2, row, 4)

        #write assets
        self.write_assets(assets, ASSET_TYPE_PRICERHIERARCHY)
        self.generate_privileges_for_assets(self.hierarchy_file, assets, roles, "BLM:CM.Pricing.HierarchyNode:[", ASSET_TYPE_PRICERHIERARCHY)

    def generate_product_hierarchy_file(self):
        """ Generates securityPrivilegeExport file for location hierarchies, reads model from excel sheet """

        # Load in the workbook
        wb = load_workbook(self.security_model_xls)
        uisheet = wb.get_sheet_by_name('ProductHierarchy')
        roles = self.read_roles(uisheet, 4, 5)

        # how many assets
        # find anchor "OID" in first column
        row = self.find_anchor("OID", 2, uisheet) +1
        rolecount = len(roles)

        print "Found roles", roles
        assets = self.read_privileges(uisheet, rolecount, 2, row, 4)

        #write assets
        self.write_assets(assets, ASSET_TYPE_PRICERHIERARCHY)
        self.generate_privileges_for_assets(self.hierarchy_file, assets, roles, "BLM:CM.Pricing.HierarchyNode:[", ASSET_TYPE_PRICERHIERARCHY)

    def generate_price_group_file(self):
        """ Generates securityPrivilegeExport file for price groups, reads model from excel sheet """

        # Load in the workbook
        wb = load_workbook(self.security_model_xls)
        uisheet = wb.get_sheet_by_name('PriceGroup')
        roles = self.read_roles(uisheet, 3, 5)

        # how many assets
        # find anchor "OID" in first column
        row = self.find_anchor("OID", 1, uisheet) +1
        rolecount = len(roles)

        print "Found roles", roles
        assets = self.read_privileges(uisheet, rolecount, 1, row, 3)

        #write assets
        self.write_assets(assets, ASSET_TYPE_PRICEGROUP)
        self.generate_privileges_for_assets(self.pricegroup_file, assets, roles, "BLM:CM.Pricing.PriceGroup:[", ASSET_TYPE_PRICEGROUP)

    def read_hierarchies(self, hierarchy_type, rootnode, print_info ):
        """ Reads location hierarchies from db and prints them """

        hierarchies = []
        db = cx_Oracle.connect(self.dbuser, self.dbpw, self.dbconn)

        SQL = ("select level, hi_oid, LPAD (' ', 2 * (LEVEL - 1)) || hi_name from hierarchy "+
               "where hi_type = "+str(hierarchy_type)+
               " start with hi_oid = '"+rootnode+"' "+
               "connect by prior  hi_oid=hi_parent_oid ")

        cursor = db.cursor()
        cursor.execute(SQL)
        for row in cursor:
            if print_info:
                print str(row[0])+";"+row[1]+";"+row[2]
            #partymapping[row[0]] = row[1]
            hierarchies.append(row)

        cursor.close()
        db.close()

        return hierarchies

    def read_variables(self):
        """ reads pricing variables from db and prints them """
        db = cx_Oracle.connect(self.dbuser, self.dbpw, self.dbconn)
        #print(db.version)

        SQL = ("select prv_oid, prv_variable from pvariable where prv_deleted != '1' order by prv_order, prv_variable ")

        cursor = db.cursor()
        cursor.execute(SQL)
        for row in cursor:
            print str(row[0])+";"+row[1]
            #partymapping[row[0]] = row[1]

        cursor.close()
        db.close()

    def read_price_groups(self):
        """ reads price groups from db and prints them """
        db = cx_Oracle.connect(self.dbuser, self.dbpw, self.dbconn)
        #print(db.version)

        SQL = ("select pg_pricegroup from pricegroup ")

        cursor = db.cursor()
        cursor.execute(SQL)
        for row in cursor:
            print str(row[0])

        cursor.close()
        db.close()

if __name__ == '__main__':
    import sys

    # usage: this_file type parameters
    if len(sys.argv) != 3:
        print sys.argv
        print "Usage: \n", sys.argv[0], " propertyfile operation"
        print "Where operation = do_backbus_hierarchies_location|"
        print "                  do_backbus_hierarchies_product|"
        print "                  do_backbus_variables|"
        print "                  do_backbus_pricegroups|"
        print "                  do_uisecurity"
        print "                  print_pricegroups"
        print "                  print_hierarchies_product"
        print "                  print_hierarchies_location"
        print "                  print_variables"
        exit()

    operation = sys.argv[2]
    PROPERTY_FILE = sys.argv[1]

    HANDLER = SecurityModel(PROPERTY_FILE)

    print "Doing operation: ", operation

    if operation == "print_variables":
        HANDLER.read_variables()
    elif operation == "print_hierarchies_product":
        HANDLER.read_hierarchies(TYPE_PRODUCT, HANDLER.product_hierarchy_root, True)
    elif operation == "print_hierarchies_location":
        HANDLER.read_hierarchies(TYPE_LOCATION, HANDLER.location_hierarchy_root, True)
    elif operation == "print_pricegroups":
        HANDLER.read_price_groups()
    elif operation == "do_uisecurity":
        HANDLER.generate_ui_security_file()
    elif operation == "do_backbus_hierarchies_location":
        HANDLER.generate_hierarchy_file()
    elif operation == "do_backbus_hierarchies_product":
        HANDLER.generate_product_hierarchy_file()
    elif operation == "do_backbus_variables":
        HANDLER.generate_variable_file()
    elif operation == "do_backbus_pricegroups":
        HANDLER.generate_price_group_file()
    else:
        print "Unknown operation: ", operation
